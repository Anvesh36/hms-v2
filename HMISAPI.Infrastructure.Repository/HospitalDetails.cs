﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
{
    public partial class FloorRepository
    {
        public int FloorId { get; set; }
        public string FloorType { get; set; }
        public int TowerID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual HospitalTowerRepository HospitalTower { get; set; }
    }
    public class HospitalTowerRepository
    {
        public int TowerId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Hospitalid { get; set; }
        public bool isActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
    
        public virtual ICollection<FloorRepository> Floors { get; set; }
        public virtual HospitalRepository Hospital { get; set; }
        public virtual ICollection<RoomRepository> Rooms { get; set; }
    
    }
   
    public class BedRepository
    {  public int BedId { get; set; }
        public int BedNo { get; set; }
        public string BedType { get; set; }
        public string WardClass { get; set; }
        public string BedBooKCat { get; set; }
        public string WardType { get; set; }
        public Nullable<int> FloorId { get; set; }
        public string BedName { get; set; }
        public int RoomId { get; set; }
        public string BedStatus { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
    
       
        public virtual WardRepository Ward { get; set; }
        
    }
    public class WardRepository
    {
        public int RoomId { get; set; }
        public string RoomTypeId { get; set; }
        public string RoomName { get; set; }
        public decimal Cost { get; set; }
        public int BlockId { get; set; }
        public int HospitalBranchId { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
    
        public virtual ICollection<BedRepository> Beds { get; set; }
        public virtual HospitalRepository Hospital { get; set; }
       
    
    }
    public partial class RoomRepository
    {
        public int RoomId { get; set; }
        public string RoomTypeId { get; set; }
        public string RoomName { get; set; }
        public decimal Cost { get; set; }
        public int TowerId { get; set; }
        public int HospitalBranchId { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }

        public virtual HospitalRepository Hospital { get; set; }
        public virtual HospitalTowerRepository HospitalTower { get; set; }
    }
    public class HospitalRepository
    {
        public int HospitalId { get; set; }
        public string HospitalName { get; set; }
        public Nullable<int> ParentHospitalId { get; set; }
        public string HospitalCode { get; set; }
        public int AddressID { get; set; }
        public decimal PrimaryContactNo { get; set; }
        public Nullable<decimal> SecodaryContactNo { get; set; }
        public Nullable<decimal> OtherContactNo { get; set; }
        public string PriContactPersonName { get; set; }
        public decimal PriMobileNumber { get; set; }
        public string RegistrationNo { get; set; }
        public string TIN { get; set; }
        public string VAT { get; set; }
        public string PAN { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
        public AddressRepository Address { get; set; }
        public virtual ICollection<HospitalTowerRepository> HospitalTowers { get; set; }
        public virtual ICollection<RoomRepository> Rooms { get; set; }
        public virtual ICollection<WardRepository> Wards { get; set; }
    }

}
