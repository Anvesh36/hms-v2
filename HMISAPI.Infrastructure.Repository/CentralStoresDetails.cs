﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
  
{
    public class EquipmentRepository
    {
        public int EquipmentID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string ManufacturedBy { get; set; }
        public Nullable<System.DateTime> ManufacturedDate { get; set; }
        public Nullable<System.DateTime> ExpireDate { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Comments { get; set; }
    }
    public class ChemicalPurchaseRepository
    {
        public int ChemicalID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string ManufacturedBy { get; set; }
        public Nullable<System.DateTime> ManufacturedDate { get; set; }
        public System.DateTime Expirydate { get; set; }
        public Nullable<decimal> PurchaseCostPerQuantity { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> SalesCostPerQuantity { get; set; }
        public Nullable<decimal> TotalCost { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Comments { get; set; }
        public Nullable<int> BranchID { get; set; }

        public virtual ICollection<ChemicalUsedRepository> ChemicalUsed { get; set; }
    }
    public partial class ChemicalUsedRepository
    {
        public int TransactionID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string TransactionType { get; set; }
        public int ChemicalID { get; set; }
        public Nullable<decimal> PurchaseCostPerQuantity { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> SalesCostPerQuantity { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }

        public virtual ChemicalPurchaseRepository ChemicalPurchase { get; set; }
    }
    public partial class StoreRepository
    {
        public int ItemNumber { get; set; }
        public string Name { get; set; }
        public string ManufacturedBy { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Cost { get; set; }
        public string SupplierName { get; set; }
        public Nullable<int> InVoice { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public bool IsActive { get; set; }
        public string Comments { get; set; }
        public Nullable<int> BranchID { get; set; }

       // public virtual HospitalBranchDetail tblHospitalBranchDetail { get; set; }
    }
    }

