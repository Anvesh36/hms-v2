﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
{
    //public class PatientDetails
    //{
    //    public Patientinfo Patients { get; set; }
    //    public PatientContactInfo PatientContactInfo { get; set; }
    //    public PatientInsuranceDetails PatientInsurance { get; set; }
    //}
    public class Patientinfo
    {
        public int PatientID { get; set; }
        public int HospitalBranchId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CallName { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<decimal> Age { get; set; }
        public string GenderName { get; set; }
        public string ContactNumber { get; set; }
        public string GuardianName { get; set; }
        public string GuardianContactNumber { get; set; }
        public int? LkGuardianRelation { get; set; }
        public string PatientCode { get; set; }
        public string Email { get; set; }
        public string PatientType { get; set; }
        public string AdharNo { get; set; }
        public string BloodGroup { get; set; }
        public string MaritalStatus { get; set; }
        public string Occupation { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
        public string Syndrome { get; set; }
        public int AppointmentID { get; set; }
        public DateTime AppointmentTime { get; set; }
        public int OPAdmissionID { get; set; }
        public AppointmentRepository Appointment { get; set; }
        public PatientDiseaseRepository PatientDisease { get; set; }
        public string PaymentType { get; set; }

        public string DoctorName { get; set; }
        public AddressRepository Address { get; set; }
        public List<PatientInsuranceRepository> lstPatientInsurance { get; set; }
        public PatientInsuranceRepository PatientInsurance { get; set; }
        public List<TreatmentRepository> lsttreatmentrep { get; set; }

        public AppointmentRepository Apprep { get; set; }
        public TransactionRepository transaction { get; set; }
        public BedAllocationRepository bedallocation { get; set; }
    }

    public class PatientDetailsRepository
    {
        public int PatientID { get; set; }
        public string PatientName { get; set; }
        public Nullable<decimal> Age { get; set; }
        public string Gender { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public string DoctorName { get; set; }
        public string Department { get; set; }
        public DateTime? AdmintDate { get; set; }
        public string patientType { get; set; }
        public TimeSpan? AdmitTime { get; set; }
    }

    public class PatientInsuranceRepository
    {
        public int PatientInsuranceID { get; set; }
        public int PatientID { get; set; }
        public string InsuranceName { get; set; }
        public Nullable<int> RelationToInsurd { get; set; }
        public string CompanyName { get; set; }
        public System.DateTime EffictiveFrom { get; set; }
        public System.DateTime EffictiveTo { get; set; }
        public Nullable<decimal> Limit { get; set; }
        public Nullable<int> BankID { get; set; }
        public string IFSCCode { get; set; }
        public string AcctHolderName { get; set; }
        public string AcctNumber { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
    public partial class AppointmentRepository
    {
        public int AppointmentID { get; set; }
        public int PatientID { get; set; }
        public int DeptID { get; set; }
        public int DoctorID { get; set; }
        public string PatientName { get; set; }
        public string DoctorName { get; set; }
        public string Branch { get; set; }
        public string Department { get; set; }
        public int? BranchID { get; set; }
        public string TokenNo { get; set; }
        public string AppointmentMode { get; set; }
        public string AppointmentStatus { get; set; }
        public string AppointmentType { get; set; }
        public System.DateTime StartDateTime { get; set; }
        public System.DateTime EndDateTime { get; set; }
        public decimal ConsultFee { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public string PayMode { get; set; }
        public string PayStatus { get; set; }
        public Nullable<bool> IsWaiveOff { get; set; }
        public Nullable<int> ReviewID { get; set; }
        public string Comments { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
    public class PatientContactInfo
    {
        public int PatientContactInfoID { get; set; }
        public int PatientID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int City { get; set; }
        public string Landmark { get; set; }
        public string Pincode { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public int AddressType { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
    public partial class PatientDiseaseRepository
    {
        public int PatientDiseaseId { get; set; }
        public int PatientId { get; set; }
        public int DiseaseId { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
        public string Syndrome { get; set; }

        public virtual Patientinfo Patient { get; set; }
    }

    public class PatientInsuranceDetails
    {
        public int PatientInsuranceID { get; set; }
        public int PatientID { get; set; }
        public string InsuranceName { get; set; }
        public Nullable<int> RelationToInsurd { get; set; }
        public string CompanyName { get; set; }
        public System.DateTime EffictiveFrom { get; set; }
        public System.DateTime EffictiveTo { get; set; }
        public Nullable<decimal> Limit { get; set; }
        public Nullable<int> BankID { get; set; }
        public string IFSCCode { get; set; }
        public string AcctHolderName { get; set; }
        public string AcctNumber { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }

    
    public partial class DoctorScheduleRepository
    {
        public int DoctorSchID { get; set; }
        public int DoctorID { get; set; }
        public int BranchID { get; set; }
        public int SlotDuration { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string WorkingDays { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public EmployeesRepository Employee { get; set; }
    }
    public class EmployeesRepository
    {
        public int EID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public decimal Age { get; set; }
        public string Gender { get; set; }
        public System.DateTime DOJ { get; set; }
        public string EmploymentType { get; set; }
        public string EmployeeId { get; set; }
        public string Qualification { get; set; }
        public Nullable<decimal> Experience { get; set; }
        public Nullable<int> DesignationId { get; set; }
        public int DepartmentId { get; set; }
        public Nullable<int> WorkLocation { get; set; }
        public Nullable<int> HomeBranch { get; set; }
        public Nullable<int> ManagerId { get; set; }
        public Nullable<decimal> Salary { get; set; }
        public string Achievements { get; set; }
        public string MobileNo { get; set; }
        public string LandLine { get; set; }
        public string Email { get; set; }
        public string EmerContactName { get; set; }
        public string EmerContactNo { get; set; }
        public string DesignationName { get; set; }
        public string Location { get; set; }
        public string EmployeeType { get; set; }
        public string EmrRelation { get; set; }
        public Nullable<int> PersAddressID { get; set; }
        public int ProfAddressID { get; set; }
        public string AadhaarNo { get; set; }
        public string PanNo { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
        public int AppointmentID { get; set; }
        public string DepartmentName { get; set; }
        public DateTime StartAppDate { get; set; }
        public AddressRepository AddressRep { get; set; }
        public AppointmentRepository appointment { get; set; }
    } 
    public class BedAllocationRepository
    {
        public int BedAllocationId { get; set; }
        public int PatientId { get; set; }
        public string PTName { get; set; }
        public int BedId { get; set; }
        public string BedStatus { get; set; }
        public System.DateTime AllocatedDate { get; set; }
        public Nullable<System.DateTime> VacatedDate { get; set; }
        public System.DateTime TenDisDate { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
    }

    public class TreatmentRepository
    {
        public int TreatmentId { get; set; }
        public int PatientId { get; set; }
        public Nullable<int> ApntID { get; set; }
        public Nullable<int> AdmissionID { get; set; }
        public int DoctorId { get; set; }
        public DateTime? TreatmentDate { get; set; }
        public string DiscussionSummary { get; set; }
        public string Instruction { get; set; }
        public bool IsDiagnosisSuggested { get; set; }
        public bool IsMedicineSuggested { get; set; }
        public Nullable<System.DateTime> NextVisit { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
        public  AddressRepository AddressRep { get; set; }

    


        public List<TreatmentVitalSignRepository> lstVitalsignsRep { get; set; }
        public List<TreatmentDiagnosisRepository> lstDiagnosisRep { get; set; }
        public List<TreatmentMendicianRepository> lstMedicianRep { get; set; }

        public TreatmentVitalSignRepository VitalsignsRep { get; set; }
        public TreatmentDiagnosisRepository DiagnosisRep { get; set; }
        public TreatmentMendicianRepository MedicianRep { get; set; }
    }

    public class TreatmentVitalSignRepository
    {
        public int VitalsignsID { get; set; }
        public int TreatmentID { get; set; }
        public string VitalSignType { get; set; }
        public double Value1 { get; set; }
        public Nullable<double> Value2 { get; set; }
        public string Note { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        
    }
    public class TreatmentDiagnosisRepository
    {
        public int TrDiagnosisID { get; set; }
        public int TreatmentID { get; set; }
        public Nullable<int> DiagnosisID { get; set; }
        public string DiagnosisName { get; set; }
        public Nullable<bool> InMandatory { get; set; }
        public Nullable<int> Priority { get; set; }
        public string Comments { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public List<TreatmentVitalSignRepository> lstVitalsigns { get; set; }
    } 
    public class TreatmentMendicianRepository
    {
        public int TrMedicineID { get; set; }
        public Nullable<int> TreatmentID { get; set; }
        public Nullable<int> MedicineID { get; set; }
        public string MedicineName { get; set; }
        public string Dose { get; set; }
        public int Quantity { get; set; }
        public Nullable<int> NoofDays { get; set; }
        public string Directions { get; set; }
        public string Advice { get; set; }
        public string Comments { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
    


    public class IpAdmissionRepository
    {
        public int ipAdmissionID { get; set; }
        public Nullable<int> PatientID { get; set; }
        public Nullable<int> FacilityBranch { get; set; }
        public Nullable<System.DateTime> AdmitDate { get; set; }
        public Nullable<System.TimeSpan> AdmitTime { get; set; }
        public string AdmittingPhysician { get; set; }
        public string AttendingPhysician { get; set; }
        public Nullable<int> Department { get; set; }
        public string WardType { get; set; }
        public Nullable<int> BedID { get; set; }
        public string AdmitStatus { get; set; }
        public string AdmitSource { get; set; }
        public Nullable<int> Diagnosiscode { get; set; }
        public string DiagnosisDescription { get; set; }
        public Nullable<int> ProcedureCode { get; set; }
        public Patientinfo Patient { get; set; }
    }

    public class DrsettingsRepository
    {
        public int DrSettingsID { get; set; }
        public Nullable<int> DoctorID { get; set; }
        public Nullable<int> SpecialityID { get; set; }
        public string DoctorName { get; set; }
        public decimal Age { get; set; }
        public string SpecializationName { get; set; }
        public string Gender { get; set; }
        public Nullable<decimal> OPFee { get; set; }
        public Nullable<decimal> IPFee { get; set; }
        public string Qualification { get; set; }
        public string IsHead { get; set; }
        public string Membership { get; set; }
        public string Awards { get; set; }
        public Nullable<System.DateTime> CertificationDate { get; set; }
        public Nullable<System.DateTime> CertificationExpires { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<System.TimeSpan> OpSlotTime { get; set; }
        public Nullable<System.TimeSpan> AvgSlotTime { get; set; }
    }
}
