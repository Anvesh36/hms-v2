﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
{
    public partial class EmpFamilyRepository
    {

        public int FamilyID { get; set; }
        public int EID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Gender { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Relationship { get; set; }
        public Nullable<byte> Age { get; set; }
        public string BloodGroup { get; set; }
        public Nullable<bool> IsDependent { get; set; }
        public Nullable<bool> IsNominee { get; set; }
        public string Occupation { get; set; }
        public Nullable<bool> IsDeath { get; set; }
        public Nullable<bool> IsEmergencyContact { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }

        public virtual EmployeesRepository Employee { get; set; }
    }
    public partial class EmpProffesionalInfoRepository
    {
        public int ProffID { get; set; }
        public int EID { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string Qualification { get; set; }
        public string Specialization { get; set; }
        public string Section { get; set; }
        public Nullable<int> Experience { get; set; }
        public string EmpType { get; set; }
        public string EmployementType { get; set; }
        public string WorkLocation { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual EmployeesRepository Employee { get; set; }
    }
    public class EmployeePersonalInfoRepository
    {
        public int PersID { get; set; }
        public int EID { get; set; }
        public string Name { get; set; }
        public decimal Age { get; set; }
        public string Gender { get; set; }
        public string AddressLine { get; set; }
        public string AddreesLine2 { get; set; }
        public string MobileNo { get; set; }
        public string LandLine { get; set; }
        public string Email { get; set; }
        public string Nationality { get; set; }
        public string Religion { get; set; }
        public string MaritalStatus { get; set; }
        public Nullable<System.DateTime> DateOfMarriage { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Pincode { get; set; }
        public string AadharNumber { get; set; }
        public string PassPortNumber { get; set; }
        public string PanCardNumber { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public EmployeesRepository Emplyee { get; set; }
    }
    public class HrmsDashboardRepository
    {
        public int EID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public decimal Age { get; set; }
        public string Gender { get; set; }
        public System.DateTime DOJ { get; set; }
        public string EmploymentType { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeType { get; set; }
        public string Qualification { get; set; }
        public Nullable<decimal> Experience { get; set; }
        public string DesignationName { get; set; }
        public int DepartmentId { get; set; }
        public Nullable<int> WorkLocation { get; set; }
        public Nullable<int> HomeBranch { get; set; }
        public Nullable<int> ManagerId { get; set; }
        public Nullable<decimal> Salary { get; set; }
        public string Achievements { get; set; }
        public string MobileNo { get; set; }
        public string LandLine { get; set; }
        public string Email { get; set; }
        public string EmerContactName { get; set; }
        public string EmerContactNo { get; set; }
        public string EmrRelation { get; set; }
        public Nullable<int> PersAddressID { get; set; }
        public int ProfAddressID { get; set; }
        public string AadhaarNo { get; set; }
        public string PanNo { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
    }
    public class CtcRepository
    {
        public int SlipID { get; set; }
        public int EID { get; set; }
        public int PayID { get; set; }
        public System.DateTime Month { get; set; }
        public byte TotalDays { get; set; }
        public byte TotalAttendanceDays { get; set; }
        public byte TotalLeaveDays { get; set; }
        public decimal Basic { get; set; }
        public decimal HRA { get; set; }
        public decimal OtherAllowances { get; set; }
        public decimal MedicalAllowances { get; set; }
        public decimal Conveyance { get; set; }
        public decimal LTA { get; set; }
        public decimal ProfTax { get; set; }
        public decimal TDS { get; set; }
        public decimal PF { get; set; }
        public decimal Loan { get; set; }
        public decimal FoodCoupons { get; set; }
        public decimal TelephoneExpenses { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public EmpPaySettings emppaysetting { get; set; }

    }
    public class EmpPaySettings
    {
        public int PayID { get; set; }
        public int EID { get; set; }
        public string AccountNo { get; set; }
        public string BankName { get; set; }
        public string IFSCCode { get; set; }
        public string AccountType { get; set; }
        public string BranchName { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifidBy { get; set; }
        public string Purpose { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
