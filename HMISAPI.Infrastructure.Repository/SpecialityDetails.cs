﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
{
    public partial class SpecialityRepository
    {


        public int SpID { get; set; }
        public int DrSettingsID { get; set; }
        public string SpName { get; set; }
        public string Description { get; set; }
        public string Summary { get; set; }
        public Nullable<System.DateTime> EstFrom { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        // public virtual ICollection<tblDrSetting> tblDrSettings { get; set; }
        //public virtual tblDrSetting tblDrSetting { get; set; }
        //public virtual ICollection<tblSpecializationField> tblSpecializationFields { get; set; }
        //public virtual ICollection<tblSpecializationFieldLookUp> tblSpecializationFieldLookUps { get; set; }
    }

}
