﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
{
    class UserAccessDetails
    {
    }

    public class RolesRepository
    {
        public int RoleID { get; set; }
        public string RoleTitle { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
    public class UserInfoRepository
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public int RoleID { get; set; }
        public Nullable<int> EID { get; set; }
        public string SecurityQuestion1 { get; set; }
        public string SecurityAnswer1 { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public string Comments { get; set; }
    }

    public class MenuRepository
    {
        public int MenuId { get; set; }
        public int ParentID { get; set; }
        public string MenuName { get; set; }
        public string NavigateURL { get; set; }
        public string MenuLogo { get; set; }
        public Nullable<int> MenuOrder { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }

    }
    public class RoleMenuRepository
    {
        public int RoleMenuID { get; set; }
        public int RoleID { get; set; }
        public int MenuID { get; set; }
        public string LkAccessID { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
