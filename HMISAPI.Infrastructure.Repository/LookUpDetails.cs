﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
{
    public class PatientLookUps
    {
        public List<LookUp> Titles { get; set; }
        public List<LookUp> Genders { get; set; }
        public List<LookUp> BloodGroups { get; set; }
        public List<LookUp> MaritalStatuses { get; set; }
        public List<LookUp> RelationShips { get; set; }
    }

    public class LookUp
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    
    public class LookUpCatergory
    {
        public int LookupCategoryId { get; set; }
        public string LookupCategoryName { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string LookupCategoryCode { get; set; }
        public string Comments { get; set; }
    }
    public class LookUpRepository
    {
        public int LookupId { get; set; }
        public int LookupCategoryId { get; set; }
        public string LookupName { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
        public string LookupCode { get; set; }

    }
}
