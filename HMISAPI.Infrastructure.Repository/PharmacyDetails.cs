﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
{
    class PharmacyDetails
    {
    }
    public class SupplierAddressRepository
    {
        public SupplierRepository suppliers { get; set; }
        public AddressRepository address { get; set; }
    }
    public class AddressRepository
    {
        public int AddressID { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }

    public class SupplierRepository
    {
        public int SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string TinNo { get; set; }
        public string Comments { get; set; }
        public bool IsActive { get; set; }
        public AddressRepository Address { get; set; }
        public int ModifiedBy { get; set; }
    }

    public class ManufacturerRepository
    {
        public int MfgID { get; set; }
        public string MfgName { get; set; }
        public string AgentName { get; set; }
        public string AgentContactNumber { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public bool IsActive { get; set; }
        public string Location { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public int CityID { get; set; }
    }

    public class ProductsRepository
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int MfgID { get; set; }
        public string MfgName { get; set; }
        public string FormCategory { get; set; }
        public string GroupName { get; set; }
        public string FormName { get; set; }
        public int GroupID { get; set; }
        public string QuantityPerUnit { get; set; }
        public int MinQuantity { get; set; }
        public string LicenceNo { get; set; }
        public bool IsActive { get; set; }
        public string RackLocation { get; set; }
        public string Comments { get; set; }
        public string Schedule { get; set; }
        public string Composition { get; set; }
        public string AgentName { get; set; }
        public string AgentContactNumber { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public Nullable<int> AvailableQuantity { get; set; }
    }

    public class MedicineTypeRepository
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string FormName { get; set; }
        public string GroupName { get; set; }
        public string FormCategory { get; set; }
        public string ManufactureName { get; set; }
        public int MfgID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }

    }

    public class MedicineGroupRepository
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string FormName { get; set; }
        public string GroupName { get; set; }
        public int MfgID { get; set; }
        public string FormCategory { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public string ManufactureName { get; set; }
    }
    public class MedicineBatchRepository
    {
        public String BatchNo { get; set; }
        public Nullable<int> AvailableQuantity { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public DateTime ExpDate { get; set; }
        public int ProductId { get; set; }
    }

    public class MedicineInvoiceRepository
    {
        public int InvoiceID { get; set; }
        public System.DateTime InvoiceDate { get; set; }
        public string ReferredDoctor { get; set; }
        public Nullable<int> PharmacyID { get; set; }
        public decimal TotalAmount { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public List<MedicineInvoiceInfoRepository> lstMedicineInvoiceInfo { get; set; }
        public Patientinfo Patient { get; set; }
        public CustomerRepository Customer { get; set; }
        
    }

    public class MedicineInvoiceInfoRepository
    {
        public int InvoiceID { get; set; }
        public int ProductID { get; set; }
        public int PurchaseID { get; set; }
        public int Quantity { get; set; }
        public string BatchNo { get; set; }
        public string Usage { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public decimal SubTotal { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public ProductsRepository Product { get; set; }
        public MedicinePurchaseInfoRepository MedicinePurchaseInfo { get; set; }
    }

    public class CustomerRepository
    {
        public int CustomerID { get; set; }
        public Nullable<int> PatientID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNumber { get; set; }
        public Nullable<decimal> Age { get; set; }
        public string GenderName { get; set; }
        public  Nullable<int> LkGender { get; set; }
        public Nullable<bool> IsPatient { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
    }

    public class salesReturninfoRepository
    {
        public int SalesReturnID { get; set; }
        public int InvoiceID { get; set; }
        public int ProductID { get; set; }
        public string BatchNo { get; set; }
        public int PurchaseID { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }

    public class MedicinePurchaseRepository
    {
        public int PurchaseID { get; set; }
        public int SupplierID { get; set; }
        public string SupplierName { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string BillNo { get; set; }
        public List<MedicinePurchaseInfoRepository> lstMedicinePurchaseInfo { get; set; }
    }

    public class MedicinePurchaseInfoRepository
    {
        public int PurchaseID { get; set; }
        public ProductsRepository Product { get; set; }
        public string BatchNo { get; set; }
        public int Quantity { get; set; }
        public int FreeQuantity { get; set; }
        public int PackQuantity { get; set; }
        public decimal? PurchaseCostPerUnit { get; set; }
        public decimal SellingCostPerUnit { get; set; }
        public decimal PackPurchaseCost { get; set; }
        public decimal VatPercentage { get; set; }
        public decimal Discount { get; set; }
        public DateTime MfgDate { get; set; }
        public DateTime ExpDate { get; set; }
        public bool IsActive { get; set; }
    }
    public class salesReturnRepository
    {
        public int SalesReturnID { get; set; }
        public DateTime ReturnDate { get; set; }
        public Nullable<int> PaymentType { get; set; }
        public Nullable<int> PharmacyID { get; set; }
        public decimal TotalAmount { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public salesReturninfoRepository salesReturnInfo { get; set; }
        public List<salesReturninfoRepository> lstSalesReturnInfo { get; set; }
        
    }

    public class ExpiredRepository
    {
        public string ProductName { get; set; }
        public string MfgName { get; set; }
        public string AgentName { get; set; }
        public string AgentContactNumber { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int DaysToExpiry { get; set; }
        public string RackLocation { get; set; }

    }

    public class EndOfStockRepository
    {
        public string ProductName { get; set; }
        public string MfgName { get; set; }
        public string AgentName { get; set; }
        public string AgentContactNumber { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int DaysToExpiry { get; set; }
        public string RackLocation { get; set; }
    }

    public class MedicineStockReturnRepository
    {
        public int StockReturnId { get; set; }
        public int PharmacyId { get; set; }
        public DateTime ReturnDate { get; set; }
        public decimal TotalAmount { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int Modifiedby { get; set; }

    }
    public class MedicineStockReturnInfoRepository
    {
        public int StockReturninfoId { get; set; }
        public int ProductID { get; set; }
        public string BatchNo { get; set; }
        public int PurchaseID { get; set; }
        public int StockReturnId { get; set; }
        public decimal Amount { get; set; }
        public int Quantity { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int Modifiedby { get; set; }

    }
    public class StockReturnMedicineDetailsRepository
    {
        public string ProductName { get; set; }
        public string MfgName { get; set; }
        public string FormName { get; set; }
        public string SupplierName { get; set; }
        public int SupplierID { get; set; }
        public DateTime PurchaseDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public String BatchNo { get; set; }
        public int PurchaseID { get; set; }
        public int PharmacyID { get; set; }
        public int PurchasedQuantity { get; set; }
        public int? AvailableStock { get; set; }
        public int ProductID { get; set; }
        public decimal? PurchaseCostPerUnit { get; set; }
        public int? QuantityToBeReturned { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public decimal? Amount { get; set; }
        public decimal? TotalAmount { get; set; }
        public List<MedicineStockReturnInfoRepository> lstStockRetrunInfo { get; set; }
    }

    public class DailySalesReportRepository
    {
        public string ProductName { get; set; }
        public string MfgName { get; set; }
        public string Type { get; set; }
        public string BatchNo { get; set; }
        public int Quantity { get; set; }
        public decimal? PurchageAmount { get; set; }
        public decimal? SalesAmount { get; set; }
        public decimal? Profit { get; set; }
        public string User { get; set; }

    }

    public class SalesReportRepository
    {
        public string ProductName { get; set; }
        public string MfgName { get; set; }
        public string Type { get; set; }
        public string BatchNo { get; set; }
        public int Quantity { get; set; }
        public decimal? PurchaseCostPerUnit { get; set; }
        public decimal? SellingCostPerUnit { get; set; }
        public decimal? PurchageAmount { get; set; }
        public decimal? SalesAmount { get; set; }
        public decimal? Profit { get; set; }
        public string User { get; set; }
        public DateTime InvoiceDate { get; set; }
    }
    public class SalesReturnReportRepository
    {
        public string ProductName { get; set; }
        public string MfgName { get; set; }
        public string Type { get; set; }
        public string BatchNo { get; set; }
        public int OrderCode { get; set; }
        public int Quantity { get; set; }
        public DateTime ReturnDate { get; set; }
    }
    public class StockReturnReportRepository
    {
        public string ProductName { get; set; }
        public string MfgName { get; set; }
        public string AgentName { get; set; }
        public string AgentContactNumber { get; set; }
        public string Type { get; set; }
        public int OrderCode { get; set; }
        public DateTime PurchaseDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int PurchaseQuantity { get; set; }
        public int ReturnQuantity { get; set; }
        public int? AvailableQuantity { get; set; }
        public DateTime ReturnDate { get; set; }
        public string Amount { get; set; }
    }

    public class PatientSalesReportRepository
    {
        public int InvoiceId { get; set; }
        public string PatientName { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string ProductName { get; set; }
        public string MfgName { get; set; }
        public string Type { get; set; }
        public string BatchNo { get; set; }
        public int Quantity { get; set; }
        public decimal? PurchaseAmount { get; set; }
        public decimal? SalesAmount { get; set; }
        public decimal? Profit { get; set; }
    }

    public class CollectionReportRepository
    {
        public int InvoiceId { get; set; }
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public decimal Amount { get; set; }
        public string TransType { get; set; }
        public string PaymentType { get; set; }
        public string UserName { get; set; }
    }
    public class PatientInvoiceReportRepository
    {
        public int InvoiceId { get; set; }
        public string PatientName { get; set; }
        public decimal? Age { get; set; }
        public string MobileNumber { get; set; }
        public DateTime PurchaseDate { get; set; }
        public decimal TotalAmount { get; set; }
    }
    public class MedicineStockReportRepository
    {
        public string MedicineName { get; set; }
        public string CompanyName { get; set; }
        public string Type { get; set; }
        public int AvailableQuantity { get; set; }
        public decimal StockAmount { get; set; }
    }
    public class ConsultationReportRepository
    {
        public int InvoiceId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal? Age { get; set; }
        public string MobileNumber { get; set; }
        public string gender { get; set; }
        public DateTime ConsultationDate { get; set; }
        public decimal Consultation { get; set; }

    }
    public class MedicineSalesandStockReportRepository
    {
        public string MedicineName { get; set; }
        public string CompanyName { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public int stock { get; set; }
        public int Received_Quantity { get; set; }
        public int Selled_Quantity { get; set; }
        public int SalesReturn_Quantity { get; set; }
        public int StockReturn_Quantity { get; set; }
        public decimal? Remaining_Quantity { get; set; }
    }
    public class LabTestReportRepository
    {
        public int LabTestID { get; set; }
        public string Invs_Name { get; set; }
        public Nullable<int> Invs_Code { get; set; }
        public decimal Price { get; set; }
        public decimal ReferAmount { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
    }

    public class LabOpRequestReportRepository
    {
        public int OpLabRequestID { get; set; }
        public int AppointmentID { get; set; }
        public int LabTestID { get; set; }
        public string TestName { get; set; }
        public string Status { get; set; }
        public string DoctorName { get; set; }
        public DateTime StartDate { get; set; }
    }
    public class DrugPrescriptionRepository
    {
        public int TrMedicineID { get; set; }
        public Nullable<int> TreatmentID { get; set; }
        public Nullable<int> MedicineID { get; set; }
        public string MedicineName { get; set; }
        public string Dose { get; set; }
        public int Quantity { get; set; }
        public Nullable<int> NoofDays { get; set; }
        public string Directions { get; set; }
        public string Advice { get; set; }
        public string Comments { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string PatientName { get; set; }

        public string Gender { get; set; }

        public string ContactNumber { get; set; }

        public string DoctorName { get; set; }
        public TreatmentRepository TreatmentDetails { get; set; }

    }

    public class LabPrescriptionsRepository
    {
        public int TrDiagnosisID { get; set; }
        public int TreatmentID { get; set; }
        public Nullable<int> DiagnosisID { get; set; }
        public string DiagnosisName { get; set; }
        public Nullable<bool> InMandatory { get; set; }
        public Nullable<int> Priority { get; set; }
        public string Comments { get; set; }
        public int CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string PatientName { get; set; }

        public string Gender { get; set; }

        public string ContactNumber { get; set; }

        public string DoctorName { get; set; }
        public TreatmentRepository TreatmentDetails { get; set; }
    }
}
