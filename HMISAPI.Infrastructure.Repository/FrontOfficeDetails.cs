﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace HMISAPI.Infrastructure.Repository
{
    class FrontOfficeDetails
    {
    }

    public  class CallRegisterRepository
    {
        public int CallInteractionID { get; set; }
        public string CallerName { get; set; }
        public string ContactNo { get; set; }
        public string CallingFrom { get; set; }
        public string Area { get; set; }
        public string CallType { get; set; }
        public int DepartmentId { get; set; }
        public string CallDetails { get; set; }
        public Nullable<System.DateTime> CallStartDt { get; set; }
        public Nullable<System.DateTime> CallEndDt { get; set; }
        public int PatientId { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public DepartmentRepository DeptInfo { get; set; }
        public Patientinfo patient { get; set; }
    }

   

    public partial class DepartmentRepository
    {

        public int DeptID { get; set; }
        public string DeptName { get; set; }
        public Nullable<int> DeptHead { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
        public virtual ICollection<CallRegisterRepository> tblCallRegisters { get; set; }
    }
}
