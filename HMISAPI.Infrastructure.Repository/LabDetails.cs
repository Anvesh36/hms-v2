﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
{
    
    public class DiagnosisRepository
    {
        public int DiagnosisID { get; set; }
        public string DiagDepartmentName { get; set; }
        public string DiagName { get; set; }
        public string Comments { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public DiagnosisComponentsRepository diagcomp { get; set; }
        public ComponentStandardRangesRepository CompStrdRang { get; set; }
        public List<DiagnosisComponentsRepository> lstdiagcomp { get; set; }      


        
        

    }

    public class RequestSummaryRepository
    {
        public int DiagReqID { get; set; }
        public Nullable<int> PatientID { get; set; }
        public string PatientName { get; set; }
        public string ReferredDoctor { get; set; }
        public System.DateTime ReportDate { get; set; }
        public decimal Amount { get; set; }
        public string ReportStatus { get; set; }
        public string PaymentStatus { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal DueAmount { get; set; }
        public bool? IsDelivered { get; set; }
        public System.DateTime? DispatchedOn { get; set; }
        public bool? IsSampleCollected { get; set; }
        public System.DateTime? SampleCollectedOn { get; set; }
        public bool? IsReportReady { get; set; }
        public System.DateTime? ReportReadyOn { get; set; }

    }
    public class DiagnosisRequisitionRepository
    {
        public int DiagReqID { get; set; }
        public Nullable<int> PatientID { get; set; }
        public System.DateTime ReportDate { get; set; }
        public string ReferredDoctor { get; set; }
        public string Comments { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public string PaymentType { get; set; }
        public Nullable<int> LabID { get; set; }
        public Nullable<int> TreatmentID { get; set; }
        public List<DiagnosisRequisitioninfoRepository> lstdiagreqinfo { get; set; }
        public DiagnosisRequisitioninfoRepository diagreqinfo { get; set; }
        public  Patientinfo Patients { get; set; }


    }
    public class  DiagnosisRequisitioninfoRepository
    {
        public int DiagReqInfoID { get; set; }
        public int DiagReqID { get; set; }
        public int DiagnosisID { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public decimal Amount { get; set; }
        public Nullable<bool> IsDelivered { get; set; }
        public Nullable<System.DateTime> DispatchedOn { get; set; }
        public Nullable<bool> IsSampleCollected { get; set; }
        public Nullable<System.DateTime> SampleCollectedOn { get; set; }
        public Nullable<bool> IsReportReady { get; set; }
        public Nullable<System.DateTime> ReportReadyOn { get; set; }
        public string Comments { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }   
    }
    public class OpCollectionsReport
    {
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public decimal Amount { get; set; }
        public DateTime TransDate { get; set; }

    }
    public class DiagnosisComponentsRepository
    {
        public int ComponentID { get; set; }
        public int DiagnosisID { get; set; }
        public string CompName { get; set; }
        public string Units { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public ComponentStandardRangesRepository CompStrdRang { get; set; }
        public ComponentValueRepository CompValue { get; set; }
        public ICollection<ComponentStandardRangesRepository> LstCompStrdRang { get; set; }
        public ICollection<ComponentValueRepository> LstCompValue { get; set; }
        public ICollection<DiagnosisComponentsRepository> lstDiagcomprep { get; set; }
    }

    public class  ComponentStandardRangesRepository
    {
        public int CmpStdID { get; set; }
        public int ComponentID { get; set; }
        public string Gender { get; set; }
        public Nullable<int> MinAge { get; set; }
        public Nullable<int> MaxAge { get; set; }
        public string StandardRange { get; set; }
        public Nullable<decimal> MinRange { get; set; }
        public Nullable<decimal> MaxRange { get; set; }
        public string Comments { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
    public class ComponentValueRepository
    {
        public int CmpValID { get; set; }
        public int ComponentID { get; set; }
        public string ValName { get; set; }
        public string Comments { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
    }
    public class TransactionRepository
    {
        public int TransID { get; set; }
        public int HospitalBranchID { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string Purpose { get; set; }
        public System.DateTime TransDate { get; set; }
        public Nullable<int> PatientID { get; set; }
        public Nullable<int> EID { get; set; }
        public Nullable<double> CashAmount { get; set; }
        public Nullable<double> CardAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public string PaymentMode { get; set; }
        public string TransType { get; set; }
        public string PaidTo { get; set; }
        public Nullable<double> TotalPaid { get; set; }
        public Nullable<double> DueAmount { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
    }

  
    public class GetDiagnosisReportRepository
    {
        public int DiagnosisID { get; set; }
        public string DiagName { get; set; }
        public int ComponentID { get; set; }
        public string CompName { get; set; }
        public string ValName { get; set; }
        public string StandardRange { get; set; }
        public Nullable<decimal> MinRange { get; set; }
        public Nullable<decimal> MaxRange { get; set; }
        public string Gender { get; set; }
        public Nullable<int> MinAge { get; set; }
        public Nullable<int> MaxAge { get; set; }
        public string ComponentResValue { get; set; }
    }
    public class DiagRequisitionInfoResultRepository
    {
        public int DiagReqResultID { get; set; }
        public int DiagReqInfoID { get; set; }
        public int ComponentID { get; set; }
        public string ComponentResValue { get; set; }
        public string ReportResult { get; set; }
        public string Comments { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
    

}
