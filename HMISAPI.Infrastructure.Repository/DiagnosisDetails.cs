﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
{
    class DiagnosisDetails
    {
    }
    public class GetPatientDetailsRepository
    {
        public int PatientID { get; set; }
        public int HospitalBranchId { get; set; }
        public int Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CallName { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<decimal> Age { get; set; }
        public int Gender { get; set; }
        public string ContactNumber { get; set; }
        public string GuardianName { get; set; }
        public string GuardianContactNumber { get; set; }
        public string GuardianRelation { get; set; }
        public string PatientCode { get; set; }
        public string Email { get; set; }
        public int PatientTypeId { get; set; }
        public string AdharNo { get; set; }
        public int BloodGroup { get; set; }
        public int MStatus { get; set; }
        public string Occupation { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string Comments { get; set; }
    }
    public class GetAmountByDiagnosisRepository
    {
        public int DiagnosisID { get; set; }
        public int LkDiagDepartmentID { get; set; }
        public string DiagName { get; set; }
        public string Comments { get; set; }
        public decimal? Amount { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    }
}
