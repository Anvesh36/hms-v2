﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HMISAPI.Infrastructure.Repository.Hospital
{
    public class BedModel:BaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BStatus { get; set; }
        public int RoomId { get; set; }
        public int Number { get; set; }
        public string Type { get; set; }
        public string RType { get; set; }
        public string Class { get; set; }
    }
}