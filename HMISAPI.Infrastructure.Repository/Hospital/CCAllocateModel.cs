﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository.Hospital
{
    public class CCAllocateModel
    {
        public int CC_AllocatedId { get; set; }
        public int BedId { get; set; }
        public DateTime StDateTime { get; set; }
        public int PtId { get; set; }
        public string PtName { get; set; }
        public DateTime TenDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string TrStatus { get; set; }
        public string Status { get; set; }
    }
}
