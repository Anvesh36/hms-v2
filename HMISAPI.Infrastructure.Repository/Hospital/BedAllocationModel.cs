﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository.Hospital
{
    public class BedAllocationModel : BaseModel
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public int BedId { get; set; }
        public string BStatus { get; set; }
        public DateTime AllocatedDate { get; set; }
        public DateTime VacatedDate { get; set; }
        public DateTime TenDisDate { get; set; }
    }
}
