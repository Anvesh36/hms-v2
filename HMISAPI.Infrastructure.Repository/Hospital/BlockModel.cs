﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HMISAPI.Infrastructure.Repository.Hospital
{
    public class BlockModel:BaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int HospitalId { get; set; }
    }
}