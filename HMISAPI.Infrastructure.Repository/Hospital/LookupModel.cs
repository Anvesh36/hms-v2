﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HMISAPI.Infrastructure.Repository.Hospital
{
    public class LookupModel
    {
        public int LookupId { get; set; }
        public int LookupCategoryId { get; set; }
        public string LookupName { get; set; }
        public bool IsActive { get; set; }
        public string LookupCode { get; set; }
        public string Comments { get; set; }
    }
}