﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HMISAPI.Infrastructure.Repository.Hospital
{
    public class WardModel:BaseModel
    {
        public int Id { get; set; }
        public int HospitalId { get; set; }
        public int LookupId { get; set; }
        public string Name { get; set; }
        public decimal Cost { get; set; }
        public int BlockId { get; set; }
    }
}