﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HMISAPI.Infrastructure.Repository.Hospital
{
    public class LookupCategoryModel:BaseModel
    {
        public int LookupCategoryId { get; set; }
        public string LookupCategoryName { get; set; }
        public string LookupCategoryCode { get; set; }
    }
}