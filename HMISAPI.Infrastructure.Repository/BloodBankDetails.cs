﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Infrastructure.Repository
{
    public class BBDonorRepository
    {
        public int DonorId { get; set; }
        public string DonorName { get; set; }
        public string Gender { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public decimal Age { get; set; }
        public string Occupation { get; set; }
        public string MaritalStatus { get; set; }
        public string MobileNo { get; set; }
        public string EmergencyCtName { get; set; }
        public string EmergencyCtno { get; set; }
        public string Email { get; set; }
        public string ContactMode { get; set; }
        public string DonationType { get; set; }
        public string BloodGroup { get; set; }
        public string DonorSource { get; set; }
        public string OldRefNo { get; set; }
        public Nullable<int> AddressID { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual AddressRepository Address { get; set; }

    }

    public partial class BBPhysicalExaminationRepository
    {
        public int PhyExamID { get; set; }
        public int DonationId { get; set; }
        public Nullable<int> Heamoglobin { get; set; }
        public Nullable<int> Temperature { get; set; }
        public Nullable<int> Weight { get; set; }
        public Nullable<int> Pulse { get; set; }
        public Nullable<int> Systolic { get; set; }
        public Nullable<int> Diastolic { get; set; }
        public string RhType { get; set; }
        public string BGbyDonoar { get; set; }
        public string BGafterScreening { get; set; }
        public string BagType { get; set; }
        public int BagVolume { get; set; }
        public int Refrigeratorid { get; set; }
        public string Remarks { get; set; }
        public string DeferralType { get; set; }
        public string DeferralRemarks { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual BBDonorRepository tblBBDonation { get; set; }
        public virtual BBRefrigeratorRepository tblBBRefrigerator { get; set; }
    }
    public class BBRefrigeratorRepository
    {
        public int Refrigeratorid { get; set; }
        public string RefrigratorName { get; set; }
        public Nullable<int> ProductSize { get; set; }
        public Nullable<int> Capacity { get; set; }
        public Nullable<int> NoofRacks { get; set; }
        public Nullable<double> Totalpackets { get; set; }
        public string ElectricalReq { get; set; }
        public Nullable<double> Price { get; set; }
        public Nullable<System.DateTime> Purchasedon { get; set; }
        public Nullable<System.DateTime> WarrantyTilldate { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public virtual ICollection<BBPhysicalExaminationRepository> BBPhysicalExaminations { get; set; }
    }
    public partial class BBGroupingCellRepository
    {
        public int BBGroupingcellid { get; set; }
        public int Donationid { get; set; }
        public string AntiA { get; set; }
        public string AntiB { get; set; }
        public string AntiAB { get; set; }
        public string AntiD { get; set; }
        public string AntiH { get; set; }
        public string AntiA1 { get; set; }
        public string BloodGroup { get; set; }
        public string RHType { get; set; }
        public string IrregulatorAB { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime Createdon { get; set; }
        public int CreatedBy { get; set; }
        public string Assignedto { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

         public virtual BBDonationRepository BBDonation { get; set; }
    }
    public partial class BBGroupingSerumRepository
    {
        public int BBGroupingSerumid { get; set; }
        public Nullable<int> Donationid { get; set; }
        public string A { get; set; }
        public string B { get; set; }
        public string O { get; set; }
        public string SerumGroup { get; set; }
        public string IrregulatorAB { get; set; }
        public string Assignedto { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Remarks { get; set; }

        //  public virtual tblBBDonation tblBBDonation { get; set; }

    }
    public partial class BBDiscardRepository
    {
        public int Discardid { get; set; }
        public int Donationid { get; set; }
        public System.DateTime DisDate { get; set; }
        public string DisComponent { get; set; }
        public string DisReason { get; set; }
        public Nullable<int> DisBy { get; set; }
        public string DisSummary { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        // public virtual tblBBDonation tblBBDonation { get; set; }
    }
    public partial class BBQuestionnaireRepository
    {
        public int QuetID { get; set; }
        public int Donationid { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime Createdon { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> Modifiedon { get; set; }

        
    }

    public partial class BBTransfusionRequestRepository
    {
        public int RequestID { get; set; }
        public int Donationid { get; set; }
        public int Donarid { get; set; }
        public string DonorName { get; set; }
        public System.DateTime DateofExpiry { get; set; }
        public int Patientid { get; set; }
        public string PatientType { get; set; }
        public string BloodGroup { get; set; }
        public string BGafterScreening { get; set; }
        public string Component { get; set; }
        public string CompName { get; set; }
        public Nullable<System.DateTime> CompExpiry { get; set; }
        public string NoOfUnits { get; set; }
        public string BagType { get; set; }
        public string Attachments { get; set; }
        public Nullable<bool> isCrossMatch { get; set; }
        public string RequestedBy { get; set; }
        public string AssignTo { get; set; }
        public string ReqStatus { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Patientinfo patient { get; set; }
        public virtual BBDonationRepository Donation { get; set; }
        public virtual ICollection<BBIssueRepository> BBIssueDetails { get; set; }
    }
 

    public partial class BBSettingRepository
    {
        public int BBSettingID { get; set; }
        public int BloodbankId { get; set; }
        public string DisplayName { get; set; }
        public byte[] logo { get; set; }
        public int AddressID { get; set; }
        public string AuthDrName { get; set; }
        public string BagsCapacity { get; set; }
        public Nullable<int> NoOfRefrigrators { get; set; }
        public Nullable<System.DateTime> establishedOn { get; set; }
        public Nullable<int> LicenceNum { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public AddressRepository address { get; set; }
        public BloodBankRepository BloodBank { get; set; }

    }


    public class BBMandatoryTestrRepository
    {
        public int MandatoryTestID { get; set; }
        public int Donationid { get; set; }
        public string HIV1 { get; set; }
        public string HIV2 { get; set; }
        public string HBSAG { get; set; }
        public string HCV { get; set; }
        public string VDRL { get; set; }
        public string MP { get; set; }
        public string TestResult { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }

    public class BBHaemotologyTestRepository
    {
        public int HaemotologyID { get; set; }
        public int Donationid { get; set; }
        public int Heamoglobin { get; set; }
        public int PlateletCount { get; set; }
        public int WBCCount { get; set; }
        public Nullable<int> MP { get; set; }
        public Nullable<int> G6PD { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
    public class BBScreeningBiologyRepository
    {
        public int Screeningid { get; set; }
        public int Donationid { get; set; }
        public int Glucose { get; set; }
        public int Urea { get; set; }
        public int ALT { get; set; }
        public string TestResult { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }

    public class BBIssueRepository
    {
        public int IssueID { get; set; }
        public int RequestId { get; set; }
        public int Donationid { get; set; }
        public int Donorid { get; set; }
        public string NoofUnits { get; set; }
        public Nullable<bool> isCrossMatch { get; set; }
        public Nullable<System.DateTime> IssuedOn { get; set; }
        public string IssuedTo { get; set; }
        public string IssueStatus { get; set; }
        public string BarCode { get; set; }
        public Nullable<int> IssuedBy { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public BBDonationRepository Donation { get; set; }
        public BBTransfusionRequestRepository TransfusionRequest { get; set; }
        public BBDonorRepository Donor { get; set; }

    }

    public class BloodBankRepository
    {
        public int Bloodbankid { get; set; }
        public string Name { get; set; }
        public int Towerid { get; set; }
        public int Hospitalid { get; set; }
        public int Floorid { get; set; }
        public string InchargeName { get; set; }
        public string Inchargeno { get; set; }
        public string OperatingTimes { get; set; }
        public string BBShortCode { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public virtual FloorRepository Floor { get; set; }
        public virtual HospitalRepository Hospital { get; set; }
        public virtual HospitalTowerRepository HospitalTower { get; set; }
    }

    public class BBDonationRepository
    {
        public int Donationid { get; set; }
        public int Donarid { get; set; }
        public System.DateTime donationDate { get; set; }
        public System.DateTime DateofExpiry { get; set; }
        public int Bloodbankid { get; set; }
        public int Patientid { get; set; }
        public string DonorComments { get; set; }
        public string Status { get; set; }
        public string CompName { get; set; }
        public string BarCode { get; set; }
        public Nullable<System.DateTime> CompExpiry { get; set; }
        public Nullable<System.DateTime> compCreatedOn { get; set; }
        public Nullable<int> compCreatedBy { get; set; }
        public int CampId { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public BBDonorRepository BBDonor { get; set; }
    }
    public class BBCampRegistrationRepository
    {
        public int CampID { get; set; }
        public string BBCode { get; set; }
        public string District { get; set; }
        public string Mandal { get; set; }
        public string CityorVillage { get; set; }
        public string Location { get; set; }
        public string CampName { get; set; }
        public string DateOfCamp { get; set; }
        public Nullable<System.DateTime> StartDateTime { get; set; }
        public Nullable<System.DateTime> EndDateTime { get; set; }
        public string Incharge { get; set; }
        public string InchargeCtNo { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

    }

}
