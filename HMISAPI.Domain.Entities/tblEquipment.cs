//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMISAPI.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEquipment
    {
        public int EquipmentID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string ManufacturedBy { get; set; }
        public Nullable<System.DateTime> ManufacturedDate { get; set; }
        public Nullable<System.DateTime> ExpireDate { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Comments { get; set; }
    }
}
