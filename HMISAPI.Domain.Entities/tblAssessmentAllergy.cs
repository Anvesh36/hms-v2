//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMISAPI.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblAssessmentAllergy
    {
        public int AsseAllergyId { get; set; }
        public int AssessmentID { get; set; }
        public int Foodcode { get; set; }
        public string Reaction { get; set; }
        public string Comments { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModofoedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    
        public virtual tblAsseFood tblAsseFood { get; set; }
        public virtual tblAssessment tblAssessment { get; set; }
    }
}
