//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMISAPI.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblComponentStandardRanx
    {
        public int CmpStdID { get; set; }
        public int ComponentID { get; set; }
        public string Gender { get; set; }
        public Nullable<int> MinAge { get; set; }
        public Nullable<int> MaxAge { get; set; }
        public string StandardRange { get; set; }
        public Nullable<decimal> MinRange { get; set; }
        public Nullable<decimal> MaxRange { get; set; }
        public string Comments { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual tblDiagnosisComponent tblDiagnosisComponent { get; set; }
    }
}
