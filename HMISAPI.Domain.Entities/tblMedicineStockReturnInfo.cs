//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMISAPI.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblMedicineStockReturnInfo
    {
        public int StockReturnInfoID { get; set; }
        public int PurchaseID { get; set; }
        public int StockReturnID { get; set; }
        public int ProductID { get; set; }
        public string BatchNo { get; set; }
        public int Quantity { get; set; }
        public string Amount { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual tblMedicinePurchase tblMedicinePurchase { get; set; }
        public virtual tblMedicineStockReturn tblMedicineStockReturn { get; set; }
    }
}
