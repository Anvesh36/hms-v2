//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMISAPI.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPtOperation
    {
        public tblPtOperation()
        {
            this.tblOprAttndDoctors = new HashSet<tblOprAttndDoctor>();
            this.tblOprAttndNurses = new HashSet<tblOprAttndNurs>();
        }
    
        public int OperationID { get; set; }
        public Nullable<int> AdmissionID { get; set; }
        public Nullable<int> DoctorID { get; set; }
        public Nullable<int> BedID { get; set; }
        public string OperationName { get; set; }
        public string Procedure { get; set; }
        public string Risk { get; set; }
        public string ExistingDiseases { get; set; }
        public string DiagnosisSummery { get; set; }
        public string PostOperationNotes { get; set; }
        public string UsedDrugs { get; set; }
        public string TreatmentSummery { get; set; }
        public string PatientStatus { get; set; }
        public string OperationStatus { get; set; }
        public string Advices { get; set; }
        public System.DateTime StartDateTime { get; set; }
        public Nullable<System.DateTime> EndDateTime { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
    
        public virtual ICollection<tblOprAttndDoctor> tblOprAttndDoctors { get; set; }
        public virtual ICollection<tblOprAttndNurs> tblOprAttndNurses { get; set; }
    }
}
