//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMISAPI.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblDiagRequisition
    {
        public tblDiagRequisition()
        {
            this.tblDiagRequisitionInfoes = new HashSet<tblDiagRequisitionInfo>();
        }
    
        public int DiagReqID { get; set; }
        public Nullable<int> PatientID { get; set; }
        public System.DateTime ReportDate { get; set; }
        public Nullable<System.DateTime> DispatchedOn { get; set; }
        public string ReportStatus { get; set; }
        public string PaymentStatus { get; set; }
        public string ReferredDoctor { get; set; }
        public string Comments { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public string PaymentType { get; set; }
        public Nullable<int> LabID { get; set; }
        public Nullable<int> TreatmentID { get; set; }
    
        public virtual tblPatient tblPatient { get; set; }
        public virtual ICollection<tblDiagRequisitionInfo> tblDiagRequisitionInfoes { get; set; }
    }
}
