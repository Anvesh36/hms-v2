//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMISAPI.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblBloodBank
    {
        public tblBloodBank()
        {
            this.tblBBDonations = new HashSet<tblBBDonation>();
            this.tblBBSettings = new HashSet<tblBBSetting>();
        }
    
        public int Bloodbankid { get; set; }
        public string Name { get; set; }
        public int Towerid { get; set; }
        public int Hospitalid { get; set; }
        public int Floorid { get; set; }
        public string InchargeName { get; set; }
        public string Inchargeno { get; set; }
        public string OperatingTimes { get; set; }
        public string BBShortCode { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    
        public virtual ICollection<tblBBDonation> tblBBDonations { get; set; }
        public virtual ICollection<tblBBSetting> tblBBSettings { get; set; }
        public virtual tblFloor tblFloor { get; set; }
        public virtual tblHospital tblHospital { get; set; }
        public virtual tblHospitalTower tblHospitalTower { get; set; }
    }
}
