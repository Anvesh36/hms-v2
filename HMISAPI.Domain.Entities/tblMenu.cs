//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HMISAPI.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblMenu
    {
        public tblMenu()
        {
            this.tblRoleMenus = new HashSet<tblRoleMenu>();
        }
    
        public int MenuId { get; set; }
        public int ParentID { get; set; }
        public string MenuName { get; set; }
        public string NavigateURL { get; set; }
        public string MenuLogo { get; set; }
        public Nullable<int> MenuOrder { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Comments { get; set; }
    
        public virtual ICollection<tblRoleMenu> tblRoleMenus { get; set; }
    }
}
