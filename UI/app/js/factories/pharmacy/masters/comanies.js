'use strict';
app.factory('pharmacyCompaniesFactory', ["baseFactory", function (baseFactory) {
    angular.extend(this, baseFactory);
    this.url = this.api_url + "manufacturers/";
    return this;
}]);