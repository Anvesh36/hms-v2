'use strict';
app.factory('pharmacyMedicinesFactory', ["baseFactory", function (baseFactory) {
    angular.extend(this, baseFactory);
    this.url = this.api_url + "products/";
    return this;
}]);