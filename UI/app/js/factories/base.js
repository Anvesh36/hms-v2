'use strict';
app.factory('baseFactory', ["$http", "$q", "api_url", function ($http, $q, api_url) {
    $http.defaults.headers.common['Accept'] = 'application/json';
    $http.defaults.headers.common['Content-Type'] = 'application/json';
    this.api_url = api_url;
    this.all = function () {
        return $http.get(this.url + "all");
    };
    this.get = function () {
        return $http.get(this.url);
    };
    this.fetch = function (itemId) {
        return $http.get(this.url + itemId);
    };

    this.create = function (item) {
        return $http.post(this.url, item);
    };

    this.update = function (itemId, item) {
        return $http.put(this.url + itemId, item);
    };

    this.destroy = function (itemId) {
        return $http.delete(this.url + itemId);
    };
    this.form_list = function(path) {
        return $http.get(this.api_url + path);
    }
    return this;
}]);