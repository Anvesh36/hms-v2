'use strict';
app.factory('lookUpFactory', ["$http", "$q", "api_url", function ($http, $q, api_url) {
    $http.defaults.headers.common['Accept'] = 'application/json';
    $http.defaults.headers.common['Content-Type'] = 'application/json';
    this.url = api_url+ "lookups/";
    this.all = function (name) {
        return $http.get(this.url + name +"/all");
    };
    this.get = function(name) {
        return $http.get(this.url + name);
    }
    this.fetch = function (itemId) {
        return $http.get(this.url + itemId);
    };

    this.create = function (item) {
        return $http.post(this.url, item);
    };

    this.update = function (itemId, item) {
        return $http.put(this.url + itemId, item);
    };

    this.destroy = function (itemId) {
        return $http.delete(this.url + itemId);
    };
    return this;
}]);