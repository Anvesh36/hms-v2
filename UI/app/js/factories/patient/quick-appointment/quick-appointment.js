﻿'use strict';
app.factory('quickAppointmentFactory', ["$http", "$q", "api_url", function ($http, $q, api_url) {
    this.getPatientInfo = function () {
        var dfd = $q.defer();
        $http.get(api_url +"patient/get").success(function (response) {
            dfd.resolve(response);
        }).error(function (response) {
            dfd.reject(response);
        });
        return dfd.promise;
    }

    return this;
        
}]);
