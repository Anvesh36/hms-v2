﻿'use strict';
app.factory('ipRegistrationFactory', ["$http", "$q", "api_url", function ($http, $q, api_url) {
    this.getPatientInfo = function () {
        var dfd = $q.defer();
        $http.get(api_url +"patient/get").success(function (response) {
            dfd.resolve(response);
        }).error(function (response) {
            dfd.reject(response);
        });
        return dfd.promise;
    },
    this.savePatientInfo = function (input) {
        var deferred = $q.defer();
        var promise = $http({
            method: 'POST',
            url: api_url +"patient/post",
            data: input
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        });
        return promise;
    },
    this.GetLookups = function () {
        var deferred = $q.defer();
        $http.get(api_url + "patient/GetLookups"
        ).success(function (data) {
            deferred.resolve(data);
        }).error(function (data) {
            deferred.reject(data);
        });

        return deferred.promise;
    },
    this.getUsersList = function () {
        var dfd = $q.defer();
        $http.get(api_url + "GetUsers").success(function (response) {
            dfd.resolve(response);
        }).error(function (response) {
            dfd.reject(response);
        });
        return dfd.promise;
    },
    this.getUser = function (user) {
        return $http.get(api_url + "GetUser/" + user.UserId);
    },
    this.addUser = function (user) {
        return $http.post(api_url + "AddUser", user);
    },
    this.deleteUser = function (user) {
        return $http.delete(api_url + "DeleteUser/" + user.UserId);
    },
    this.updateUser = function (user) {
        return $http.put(api_url + "ModifyUser/" + user.UserId, user);
    }
    return this;
}]);
