'use strict';

angular.module('app').constant("lookup_constants",  {
		"medicine_types": ["pills","tablets","capsules","injection","liquids","syrups","lozenges","inhalants","creams","ointments","suppositories","aerosol"],
		"states": [{ "name": "Andhra Pradesh", "cities": ["Visakhapatnam","Vijayawada","Guntur","Nellore","Kurnool","Tirupati","Kadapa","Rajahmundry","Kakinada","Anantapur","Vizianagaram","Eluru","Ongole","Nandyal","Machilipatnam","Adoni","Tenali","Proddatur","Chittoor","Hindupur","Bhimavaram","Madanapalle","Guntakal","Srikakulam","Dharmavaram","Gudivada","Narasaraopet","Tadipatri","Tadepalligudem","Amaravati","Chilakaluripet"]},
					{ "name": "Telangana", "cities": ["Adilabad","Hyderabad","Karimnagar","Khammam","Mahbubnagar","Miryalaguda","Nalgonda","Nizamabad","Ramagundam","Siddipet","Suryapet","Warangal"]}]
});