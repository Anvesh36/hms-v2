var app =  angular.module('app')
  .config(
    [        '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
    function ($controllerProvider,   $compileProvider,   $filterProvider,   $provide) {
        
        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.filter     = $filterProvider.register;
        app.factory    = $provide.factory;
        app.service    = $provide.service;
        app.constant   = $provide.constant;
        app.value      = $provide.value;
    }
  ]);
  
  app.config(['growlProvider', function (growlProvider) {
	  growlProvider.globalTimeToLive(5000);
	  growlProvider.globalDisableIcons(true);
	  growlProvider.globalDisableCountDown(true);
	}]);
  
  app.directive('input', [function() {
    return {
        restrict: 'E',
        require: '?ngModel',
        link: function(scope, element, attrs, ngModel) {
            if (
                   'undefined' !== typeof attrs.type
                && 'number' === attrs.type
                && ngModel
            ) {
                ngModel.$formatters.push(function(modelValue) {
                    return parseFloat(modelValue);
                });

                ngModel.$parsers.push(function(viewValue) {
                    return parseFloat(viewValue);
                });
            }
        }
    }
}]);
  
