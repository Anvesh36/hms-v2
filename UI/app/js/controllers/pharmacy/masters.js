'use strict';

app.controller('pharmacyMastersCtrl', ["$scope", "$timeout", "$state", "$location", function ($scope, $timeout, $state, $location) {
    var path = $location.path().split('/')
    var tab_name = path[path.length-1];
    if(tab_name =="masters"){
        $state.go('app.pharmacy-masters.companies');
    } else if (tab_name != "companies") {
        $scope.tab_master_index = (tab_name == "medicine") ? 2 : 1;
    }
}]);