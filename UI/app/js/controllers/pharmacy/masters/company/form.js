'use strict';

app.controller('pharmacyCompanyFormCtrl', ['$scope','lookup_constants', '$filter', '$uibModalInstance','pharmacyCompaniesFactory', '$log', 'company', 'is_new', 'growl', function($scope, lookup_constants, $filter, $modalInstance, pharmacyCompaniesFactory, $log, company, is_new, growl) {
    $scope.cities = [];  
    $scope.company = angular.copy(company);
    angular.forEach(lookup_constants.states, function(state){
        $scope.cities.push.apply($scope.cities, state.cities);
    });
    $scope.is_new = is_new;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.submitForm = function(is_valid) {
        if (is_valid) {
            $scope.is_form_submit = true;
            if ($scope.is_new) {
                pharmacyCompaniesFactory.create($scope.company)
                    .then(function (response) {
                    	growl.success($scope.company.MfgName + " created successfully.");
                        $modalInstance.close(response.data);
                    },function(error) {
                    	growl.error("Unable to create drug company: "+ $scope.company.MfgName);
                    }).then(function() {
                        $scope.is_form_submit = false;
                    });
            } else {
                pharmacyCompaniesFactory.update($scope.company.MfgID, $scope.company)
                    .then(function (response) {
                    	growl.success($scope.company.MfgName + " updated successfully.");
                        $modalInstance.close($scope.company);
                    },function(error) {
                    	growl.error("Unable to update drug company: "+ $scope.company.MfgName);
                    }).then(function() {
                        $scope.is_form_submit = false;
                    });
            }
        }
    };
}]);