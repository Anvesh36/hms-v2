'use strict';

app.controller('pharmacyCompaniesCtrl', ["$scope", "pharmacyCompaniesFactory", '$timeout', '$uibModal', '$log', '$window', 'growl', function ($scope, pharmacyCompaniesFactory, $timeout, $modal, $log, $window, growl) {
    $scope.loading_message = "Drugs companies loading..." ;
    $scope.manufactures = [];
    $scope.manufactures_loading = false;
    pharmacyCompaniesFactory.all()
        .then(function (response) {
            $scope.manufactures = response.data;
            $timeout(function(){
                $('.table').trigger('footable_redraw');
            }, 100);
            $scope.manufactures_loading = true;
        },function() {
            $scope.loading_message = "Network Error: Unable to load drug companies."
        });
    $scope.open =  function (company) {
        var is_new = (company == undefined)
        var formInstance = $modal.open({
            templateUrl: 'partials/pharmacy/masters/company/form.html',
            controller: 'pharmacyCompanyFormCtrl',
            resolve: {
                company: is_new ? { IsActive: true } : company,
                is_new: is_new
            }
        });

        formInstance.result.then(function (response) {
            if (response){
                if (is_new) {
                    $timeout(function(){
                        $scope.manufactures.push(response);
                        $('.table').trigger('footable_redraw');
                    }, 100);
                }
                else {
                    angular.extend(company, response);
                }
            }
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.remove = function (company) {
        $scope.manufactures.splice($scope.manufactures.indexOf(company),1);
        var delete_company = $window.confirm('Are you absolutely sure you want to delete drug company?');
        if(delete_company) {
            pharmacyCompaniesFactory.destroy(company.MfgID)
                .success(function (response) {
                    if(response.data) {
                        $scope.manufactures.splice($scope.manufactures.indexOf(company),1);
                    }
                });
        }
    };
}]);