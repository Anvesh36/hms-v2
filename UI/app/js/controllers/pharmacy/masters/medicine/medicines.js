'use strict';

app.controller('pharmacyMedicinesCtrl', ["$scope", "pharmacyMedicinesFactory", '$timeout', '$uibModal', '$log', function ($scope, pharmacyMedicinesFactory, $timeout, $modal, $log) {
    $scope.medicines_loading = false;
    $scope.loading_message = "Medicines are loading...."
    pharmacyMedicinesFactory.get()
    .then(function (response) {
            $scope.medicines = response.data;
            $timeout(function(){
                $('.table').trigger('footable_redraw');
                $scope.medicines_loading = true;
            }, 100);
        },function() {
            $scope.loading_message = "Network Error: Unable to load medicines."
        });
    
     $scope.open =  function (medicine) {
        var is_new = (medicine == undefined)
        var formInstance = $modal.open({
            templateUrl: 'partials/pharmacy/masters/medicines/form.html',
            controller: 'pharmacyMedicineFormCtrl',
            resolve: {
                medicine: is_new ? { } : medicine,
                is_new: is_new
            }
        });

        formInstance.result.then(function (response) {
            if (is_new) {
                $scope.medicines.push(response);
            }
            else {
                angular.extend(medicine, response);
            }
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }
}]);