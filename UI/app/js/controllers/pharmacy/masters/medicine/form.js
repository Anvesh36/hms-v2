'use strict';

app.controller('pharmacyMedicineFormCtrl', ['$scope', '$filter', '$uibModalInstance','pharmacyMedicinesFactory', '$log', 'medicine', 'is_new', 'lookup_constants', 'growl', function($scope, $filter, $modalInstance, pharmacyMedicinesFactory, $log, medicine, is_new, lookup_constants, growl) {
    $scope.medicine = angular.copy(medicine);
    $scope.medicine.CreatedBy =1;
    $scope.medicine.MfgName = "marodinbo";
    pharmacyMedicinesFactory.form_list("manufacturers")
    .then(function (response) {
        $scope.companies = response.data;
    },function() {
    }).then(function() {
    });
    $scope.medicine_types = lookup_constants.medicine_types
    $scope.is_new = is_new;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.submitForm = function(is_valid) {
        if (is_valid) {
            if ($scope.is_new) {
                pharmacyMedicinesFactory.create($scope.medicine)
                    .then(function (response) {
                        var new_medicine = response.data;
                        var drug_company = $filter('filter')($scope.companies, {MfgID:  $scope.medicine.MfgID})[0];
                        new_medicine.MfgName = drug_company.MfgName;
                        new_medicine.AgentName = drug_company.AgentName;
                        new_medicine.AgentContactNumber = drug_company.AgentContactNumber;
                    	growl.success($scope.medicine.ProductName + " created successfully.");
                        $modalInstance.close(new_medicine);
                    },function(error) {
                    	growl.error("Unable to create medicine: "+ $scope.medicine.ProductName);
                    }).then(function() {
                    });
            } else {
                pharmacyMedicinesFactory.update($scope.medicine.ProductID, $scope.medicine)
                    .then(function (response) {
                        var drug_company = $filter('filter')($scope.companies, {MfgID:  $scope.medicine.MfgID})[0];
                        $scope.medicine.MfgName = drug_company.MfgName;
                        $scope.medicine.AgentName = drug_company.AgentName;
                        $scope.medicine.AgentContactNumber = drug_company.AgentContactNumber;
                    	growl.success($scope.medicine.ProductName + " updated successfully.");
                        $modalInstance.close($scope.medicine);
                    },function(error) {
                    	growl.error("Unable to update medicine: "+ $scope.medicine.ProductName);
                    }).then(function() {
                    });
            }
        }
    };
}]);