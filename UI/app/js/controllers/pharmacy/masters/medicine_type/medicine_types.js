'use strict';

app.controller('pharmacyMedicineTypesCtrl', ["$scope", "lookup_constants", '$timeout', '$uibModal', '$log', function ($scope, lookup_constants, $timeout, $modal, $log) {
    $scope.medicine_types = lookup_constants.medicine_types
}]);