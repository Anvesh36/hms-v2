'use strict';

app.controller('pharmacyTransactionReturnCtrl', ["$scope", "$state", function ($scope, $state) {
    $state.go('app.pharmacy-transaction-return.sales');
    $scope.tab_text = "Sales Return";
    $scope.redirect = function (url, tab_text) {
        $state.go(url);
        $scope.tab_text = tab_text;
    }
}]);