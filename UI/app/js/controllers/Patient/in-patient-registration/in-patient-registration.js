'use strict';

app.controller('ipRegistrationCtrl', ["$scope", "ipRegistrationFactory", function ($scope, ipRegistrationFactory) {
    GetLookups();
    $scope.patientInfo = {
        Patients: {
            AdharNo: "",
            Age: "",
            BloodGroup: "",
            FirstName: "",
            LastName: "",
            Title: "",
            DOB: "",
            Email: "",
            ContactNumber: "",
            MStatus: "",
            Occupation: "",
            GuardianContactNumber: "",
            GuardianName: "",
            GuardianRelation: "",
            Gender: "",
            HospitalBranchId: 1,
            PatientTypeId: 2
        },
        PatientContactInfo: {
            Address1: "",
            Address2: "",
            City: "",
            HomePhone: "",
            Landmark: "",
            Pincode: "",
            WorkPhone: ""
        },
        PatientInsurance: {
            InsuranceName: "",
            RelationToInsurd: 33,
            EffictiveFrom: "",
            EffictiveTo: "",
            CompanyName: "",
            Limit: 2.9,
            AcctHolderName: "",
            AcctNumber: "",
            IFSCCode: ""
        }
    };
    // $scope.patientInfo.Patients = {}
    // $scope.patientInfo.Patients.AdharNo = "";
    // $scope.patientInfo.Patients.Age = "";
    // $scope.patientInfo.Patients.BloodGroup = "";
    // $scope.patientInfo.Patients.FirstName = "";
    // $scope.patientInfo.Patients.LastName = "";
    // $scope.patientInfo.Patients.Title = "";
    // $scope.patientInfo.Patients.DOB = "";
    // $scope.patientInfo.Patients.Email = "";
    // $scope.patientInfo.Patients.ContactNumber = "";
    // $scope.patientInfo.Patients.MStatus = "";
    // $scope.patientInfo.Patients.Occupation = "";
    // $scope.patientInfo.Patients.GuardianContactNumber = "";
    // $scope.patientInfo.Patients.GuardianName = "";
    // $scope.patientInfo.Patients.GuardianRelation = "";
    // $scope.patientInfo.Patients.Gender = "";
    // $scope.patientInfo.Patients.HospitalBranchId = 1;
    // $scope.patientInfo.Patients.PatientTypeId = 2;
    //$scope.patientInfo.PatientInsurance.IsActive = 1;
    //$scope.patientInfo.PatientInsurance.CreatedBy = 1;
    //$scope.patientInfo.PatientInsurance.CreatedDate = "01-01-1989";


    // $scope.patientInfo.PatientContactInfo = new Object();
    // $scope.patientInfo.PatientContactInfo.Address1 = "";
    // $scope.patientInfo.PatientContactInfo.Address2 = "";
    // $scope.patientInfo.PatientContactInfo.City = "";
    // $scope.patientInfo.PatientContactInfo.HomePhone = "";
    // $scope.patientInfo.PatientContactInfo.Landmark = "";
    // $scope.patientInfo.PatientContactInfo.Pincode = "";
    // $scope.patientInfo.PatientContactInfo.WorkPhone = "";
    //$scope.patientInfo.PatientInsurance.IsActive = 1;
    //$scope.patientInfo.PatientInsurance.CreatedBy = 1;
    //$scope.patientInfo.PatientInsurance.CreatedDate = "01-01-1989";

    // $scope.patientInfo.PatientInsurance = new Object();
    // $scope.patientInfo.PatientInsurance.InsuranceName = "";
    // $scope.patientInfo.PatientInsurance.RelationToInsurd = 33;
    // $scope.patientInfo.PatientInsurance.EffictiveFrom = "";
    // $scope.patientInfo.PatientInsurance.EffictiveTo = "";
    // $scope.patientInfo.PatientInsurance.CompanyName = "";
    // $scope.patientInfo.PatientInsurance.Limit = 2.9;
    // $scope.patientInfo.PatientInsurance.AcctHolderName = "";
    // $scope.patientInfo.PatientInsurance.AcctNumber = "";
    // $scope.patientInfo.PatientInsurance.IFSCCode = "";
    //$scope.patientInfo.PatientInsurance.IsActive = 1;
    //$scope.patientInfo.PatientInsurance.CreatedBy = 1;
    //$scope.patientInfo.PatientInsurance.CreatedDate = "01-01-1989";


    //RelationToInsurd



    var init = function () {
        ipRegistrationFactory.getPatientInfo().then(function (res) {
            console.log(res);
            $scope.patientInfo = res[0];
        }, function (res) {
        });
    };
    // init();

    $scope.savePatientInfo = function () {
        ipRegistrationFactory.savePatientInfo($scope.patientInfo).then(function (res) {
            console.log(res);
            $scope.patientInfo = res[0];
        }, function (res) {
        });
    };
    function GetLookups() {
        ipRegistrationFactory.GetLookups()
            .then(function (look_ups) {
                $scope.blood_groups = look_ups.BloodGroup;
                $scope.genders = look_ups.Gender;
                $scope.marital_statuses = look_ups.MaritalStatus;
                $scope.relations = look_ups.RelationShip;
                $scope.titles = look_ups.Title;
            },
            function (errorPl) {
                console.log("Some Error in Getting Records." + errorPl);
            });
    }
    // $scope.GetLookups = function () {
    //
    //     var promiseGet = ipRegistrationFactory.GetLookups();
    //     //alert(promiseGet); return true;
    //     //  var removeTemplate = '<input type="button" value="Edit"  data-toggle="modal" data-target="#myModalEdit" ng-click="editlookupcategorys(row.entity)" /> &nbsp;&nbsp;<input type="button" value="Remove"  ng-click="deleteLookUpCategory(row.entity)"  ng-click="updateLookUpCategoryDetails(row.entity)" /> ';
    //     promiseGet.then(function (pl) {
    //         $scope.p2 = pl;
    //     },
    //           function (errorPl) {
    //               console.log("Some Error in Getting Records." + errorPl);
    //           });
    //
    //
    // }
}]);

