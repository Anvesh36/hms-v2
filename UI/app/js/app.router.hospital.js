/// <reference path="patient/IPRegistration/Controller/ipRegistrationCtrl.js" />
/// <reference path="patient/IPRegistration/Controller/ipRegistrationCtrl.js" />
'use strict';

/**
 * Config for the router
 */
angular.module('app')
    .config(
        ['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG',
            function($stateProvider, $urlRouterProvider, JQ_CONFIG) {

                $urlRouterProvider
                    .otherwise('/app/hospital/front-office-dashboard');
                $stateProvider

                    .state('app', {
                        abstract: true,
                        url: '/app',
                        templateUrl: 'partials/app.html'
                    })

                    .state('app.front-office-dashboard', {
                        url: '/hospital/front-office-dashboard',
                        templateUrl: 'partials/dashboard/front-office-dashboard.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load('chart.js').then(
                                        function() {
                                            return $ocLazyLoad.load('js/dashboard/front-office-dashboardCtrl.js');
                                        }
                                    ).then(
                                        function() {
                                            return $ocLazyLoad.load('../bower_components/font-awesome/css/font-awesome.css');
                                        }
                                    );
                                }
                            ]
                        }
                    })

                    .state('app.nurse-dashboard', {
                        url: '/hospital/nurse-dashboard',
                        templateUrl: 'partials/dashboard/nurse-dashboard.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css']);
                                }
                            ]
                        }
                    })

                    .state('app.in-patient-registration', {
                        url: '/hospital/inpatient-registration',
                        templateUrl: 'partials/patient/in-patient-registration.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/patient/in-patient-registration/in-patient-registration.js', 'js/factories/patient/in-patient-registration/in-patient-registration.js']);
                                }
                            ]
                        }
                    })

                    .state('app.quick-appointment', {
                        url: '/hospital/quick-appointment',
                        templateUrl: 'partials/patient/quick-appointment.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/patient/quick-appointment/quick-appointment.js', 'js/factories/patient/quick-appointment/quick-appointment.js']);
                                }
                            ]
                        }
                    })

                    .state('app.bed-occupancy', {
                        url: '/hospital/bed-occupancy',
                        templateUrl: 'partials/patient/bed-occupancy.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/patient/bed-occupancy/bed-occupancyCtrl.js']);
                                }
                            ]
                        }
                    })

                    .state('app.my-appointments', {
                        url: '/hospital/my-appointments',
                        templateUrl: 'partials/doctor/my-appointments.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css']);
                                }
                            ]
                        }
                    })

                    .state('app.my-revenue', {
                        url: '/hospital/my-revenue',
                        templateUrl: 'partials/doctor/my-revenue.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load('chart.js').then(
                                        function() {
                                            return $ocLazyLoad.load('js/controllers/chartjs.js');
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.appointment-summary', {
                        url: '/hospital/appointment-summary',
                        templateUrl: 'partials/appointment/appointment-summary.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css']);
                                }
                            ]
                        }
                    })
                    .state('app.op-review', {
                        url: '/hospital/op-review',
                        templateUrl: 'partials/doctor/op-review.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load('ngMorris').then(
                                        function() {
                                            return $ocLazyLoad.load(['js/controllers/morris.js', '../bower_components/font-awesome/css/font-awesome.css']);
                                        }
                                    );
                                }
                            ]
                        }
                    })

                    .state('app.add-operation-details', {
                        url: '/hospital/add-operation-details',
                        templateUrl: 'partials/patient/add-operation-details.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/patient/bed-occupancy/bed-occupancyCtrl.js']);
                                }
                            ]
                        }
                    })

                    .state('app.bill-summary', {
                        url: '/hospital/bill-summary',
                        templateUrl: 'partials/billing/bill-summary.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/patient/bed-occupancy/bed-occupancyCtrl.js']);
                                }
                            ]
                        }
                    })

                    .state('app.roaster', {
                        url: '/hospital/roaster',
                        templateUrl: 'partials/dashboard/roaster.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/patient/bed-occupancy/bed-occupancyCtrl.js']);
                                }
                            ]
                        }
                    })
                    .state('app.case-sheet', {
                        url: '/hospital/case-sheet',
                        templateUrl: 'partials/patient/case-sheet.html',
                        /*  resolve: {
                         deps: ['uiLoad',
                         function(uiLoad) {
                         return uiLoad.load(['../bower_components/font-awesome/css/font-awesome.css']);
                         }
                         ]
                         }*/

                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load('ngMorris').then(
                                        function() {
                                            return $ocLazyLoad.load(['js/controllers/morris.js', '../bower_components/font-awesome/css/font-awesome.css']);
                                        }
                                    );
                                }
                            ]
                        }




                    })

                    .state('app.invoice-new-request', {
                        url: '/hospital/invoice-new-request',
                        templateUrl: 'partials/lab/invoice-new-request.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load('ngMorris').then(
                                        function() {
                                            return $ocLazyLoad.load(['js/controllers/morris.js', '../bower_components/font-awesome/css/font-awesome.css']);
                                        }
                                    );
                                }
                            ]
                        }
                    })


                    .state('app.collections', {
                        url: '/hospital/collections',
                        templateUrl: 'partials/lab/collections.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load('ngMorris').then(
                                        function() {
                                            return $ocLazyLoad.load(['js/controllers/morris.js', '../bower_components/font-awesome/css/font-awesome.css']);
                                        }
                                    );
                                }
                            ]
                        }
                    })


                    .state('app.voucher-payment', {
                        url: '/hospital/voucher-payment',
                        templateUrl: 'partials/lab/voucher-payment.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load('ngMorris').then(
                                        function() {
                                            return $ocLazyLoad.load(['js/controllers/morris.js', '../bower_components/font-awesome/css/font-awesome.css']);
                                        }
                                    );
                                }
                            ]
                        }
                    })

                    .state('app.lab-op', {
                        url: '/hospital/lab-op',
                        templateUrl: 'partials/lab/lab-op.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load('ngMorris').then(
                                        function() {
                                            return $ocLazyLoad.load(['js/controllers/morris.js', '../bower_components/font-awesome/css/font-awesome.css']);
                                        }
                                    );
                                }
                            ]
                        }
                    })

                    .state('app.pharmacy-masters', {
                        url: '/hospital/pharmacy/masters',
                        templateUrl: 'partials/pharmacy/masters.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load('ngMorris').then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/masters.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-masters.companies', {
                        url: '/companies',
                        views: {
                        	'companies': {
                        		templateUrl: 'partials/pharmacy/masters/company/list.html'
                        	}
                        },
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/masters/company/companies.js', 'js/controllers/pharmacy/masters/company/form.js', 'js/factories/pharmacy/masters/comanies.js', 'js/factories/lookup.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-masters.medicine-type', {
                        url: '/medicine_type',
                        views: {
                        	'medicine-type': {
                        		templateUrl: 'partials/pharmacy/masters/medicine_types/list.html'
                        	}
                        },
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/masters/medicine_type/medicine_types.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-masters.medicine', {
                        url: '/medicine',
                        views: {
                        	'medicnes': {
                        		templateUrl: 'partials/pharmacy/masters/medicines/list.html'
                        	}
                        },
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/masters/medicine/medicines.js', 'js/controllers/pharmacy/masters/medicine/form.js', 'js/factories/pharmacy/masters/medicines.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-masters.order', {
                        url: '/order',
                        templateUrl: 'partials/pharmacy/masters/orders/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/masters/order/orders.js', 'js/factories/pharmacy/masters/orders.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-transaction-order', {
                        url: '/transaction/order',
                        templateUrl: 'partials/pharmacy/transaction/orders/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/transaction/order/orders.js', 'js/factories/pharmacy/transaction/orders.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-transaction-medicine-invoice', {
                        url: '/transaction/medicine-invoice',
                        templateUrl: 'partials/pharmacy/transaction/medicine_invoice/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/transaction/medicine_invoice/medicine_invoices.js', 'js/factories/pharmacy/transaction/medicine_invoice.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-transaction-return', {
                        url: '/pharmacy/transaction/return',
                        templateUrl: 'partials/pharmacy/transaction/transaction_return.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/transaction/transaction_return.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-transaction-return.sales', {
                        url: '/sales',
                        templateUrl: 'partials/pharmacy/transaction/sales_return/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/transaction/sales_return/sales_returns.js', 'js/factories/pharmacy/transaction/sales_return.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-transaction-return.stock', {
                        url: '/stock',
                        templateUrl: 'partials/pharmacy/transaction/stock_return/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/transaction/stock_return/stock_returns.js', 'js/factories/pharmacy/transaction/stock_return.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-transaction-refill', {
                        url: '/pharmacy/transaction/refill',
                        templateUrl: 'partials/pharmacy/transaction/refill/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/transaction/refill/refill.js', 'js/factories/pharmacy/transaction/refill.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-alert-expiry-medicine', {
                        url: '/pharmacy/alert/expiry_medicine',
                        templateUrl: 'partials/pharmacy/alert/expiry_medicine/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/alert/expiry_medicine/expiry_medicine.js', 'js/factories/pharmacy/alert/expiry_medicine.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-alert-stock-alerts', {
                        url: '/pharmacy/alert/stock_alerts',
                        templateUrl: 'partials/pharmacy/alert/stock_alerts/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/alert/stock_alerts/stock_alerts.js', 'js/factories/pharmacy/alert/stock_alerts.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-reports-collection', {
                        url: '/pharmacy/reports/collection',
                        templateUrl: 'partials/pharmacy/reports/collection/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/reports/collection/collection.js', 'js/factories/pharmacy/reports/collection.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-reports-sales', {
                        url: '/pharmacy/reports/sales',
                        templateUrl: 'partials/pharmacy/reports/sales/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/reports/sales/sales.js', 'js/factories/pharmacy/reports/sales.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-reports-stock', {
                        url: '/pharmacy/reports/stock',
                        templateUrl: 'partials/pharmacy/reports/stock/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/reports/stock/stock.js', 'js/factories/pharmacy/reports/stock.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-reports-patience-invoice', {
                        url: '/pharmacy/reports/patience_invoice',
                        templateUrl: 'partials/pharmacy/reports/patience_invoice/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/reports/patience_invoice/patience_invoice.js', 'js/factories/pharmacy/reports/patience_invoice.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-reports-sales-return', {
                        url: '/pharmacy/reports/sales_return',
                        templateUrl: 'partials/pharmacy/reports/sales_return/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/reports/sales_return/sales_return.js', 'js/factories/pharmacy/reports/sales_return.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-reports-medicine-stock', {
                        url: '/pharmacy/reports/medicine_stock',
                        templateUrl: 'partials/pharmacy/reports/medicine_stock/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/reports/medicine_stock/medicine_stock.js', 'js/factories/pharmacy/reports/medicine_stock.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-reports-medicine-sales-stock',{
                        url: '/pharmacy/reports/medicine_sales_stock',
                        templateUrl: 'partials/pharmacy/reports/medicine_sales_stock/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/reports/medicine_sales_stock/medicine_sales_stock.js', 'js/factories/pharmacy/reports/medicine_sales_stock.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-specialities-templates',{
                        url: '/pharmacy/specialities/templates',
                        templateUrl: 'partials/pharmacy/specialities/templates/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/specialities/templates/templates.js', 'js/factories/pharmacy/specialities/templates.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-specialities-vendor-contacts',{
                        url: '/pharmacy/specialities/vendor_contacts',
                        templateUrl: 'partials/pharmacy/specialities/vendor_contacts/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/specialities/vendor_contacts/vendor_contacts.js', 'js/factories/pharmacy/specialities/vendor_contacts.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-specialities-inventory-mgmt',{
                        url: '/pharmacy/specialities/inventory_mgmt',
                        templateUrl: 'partials/pharmacy/specialities/inventory_mgmt/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/specialities/inventory_mgmt/inventory_mgmt.js', 'js/factories/pharmacy/specialities/inventory_mgmt.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    }).state('app.pharmacy-specialities-auto-order',{
                        url: '/pharmacy/specialities/auto_order',
                        templateUrl: 'partials/pharmacy/specialities/auto_order/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/specialities/auto_order/auto_order.js', 'js/factories/pharmacy/specialities/auto_order.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-specialities-barcode-mgmt',{
                        url: '/pharmacy/specialities/barcode_mgmt',
                        templateUrl: 'partials/pharmacy/specialities/barcode_mgmt/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/specialities/barcode_mgmt/barcode_mgmt.js', 'js/factories/pharmacy/specialities/barcode_mgmt.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    }).state('app.pharmacy-specialities-store-transfer',{
                        url: '/pharmacy/specialities/store_transfer',
                        templateUrl: 'partials/pharmacy/specialities/store_transfer/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/specialities/store_transfer/store_transfer.js', 'js/factories/pharmacy/specialities/store_transfer.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-specialities-alternatives',{
                        url: '/pharmacy/specialities/alternatives',
                        templateUrl: 'partials/pharmacy/specialities/alternatives/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/specialities/alternatives/alternatives.js', 'js/factories/pharmacy/specialities/alternatives.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    }).state('app.pharmacy-specialities-accounting',{
                        url: '/pharmacy/specialities/accounting',
                        templateUrl: 'partials/pharmacy/specialities/accounting/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/specialities/accounting/accounting.js', 'js/factories/pharmacy/specialities/accounting.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-specialities-trend-report',{
                        url: '/pharmacy/specialities/trend_report',
                        templateUrl: 'partials/pharmacy/specialities/trend_report/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/specialities/trend_report/trend_report.js', 'js/factories/pharmacy/specialities/trend_report.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.pharmacy-specialities-plannogram',{
                        url: '/pharmacy/specialities/plannogram',
                        templateUrl: 'partials/pharmacy/specialities/plannogram/list.html',
                        resolve: {
                            deps: ['$ocLazyLoad',
                                function($ocLazyLoad) {
                                    return $ocLazyLoad.load(['ngMorris']).then(
                                        function() {
                                            return $ocLazyLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/pharmacy/specialities/plannogram/plannogram.js', 'js/factories/pharmacy/specialities/plannogram.js']);
                                        }
                                    );
                                }
                            ]
                        }
                    })
                    .state('app.blood-bank-camp-registration',{
                        url: '/blood_bank/camp_registration',
                        templateUrl: 'partials/blood_bank/camp_registration/list.html'
                    })
                    .state('app.blood-bank-donor-mgmt-donor-registration-workflows-components',{
                        url: '/blood_bank/donor_mgmt/donor_registration/workflows/components',
                        templateUrl: 'partials/blood_bank/donor_mgmt/donor_registration/workflows/components/list.html'
                    })
                    .state('app.blood-bank-donor-mgmt-donor-registration-workflows-grouping',{
                        url: '/blood-bank/donor_mgmt/donor_registration/workflows/grouping',
                        templateUrl: 'partials/blood_bank/donor_mgmt/donor_registration/workflows/grouping/list.html'
                    })
                    .state('app.blood-bank-donor-mgmt-donor-registration-workflows-questionaries',{
                        url: '/blood_bank/donor_mgmt/donor_registration/workflows/questionaries',
                        templateUrl: 'partials/blood_bank/donor_mgmt/donor_registration/workflows/questionaries/list.html'
                    })
                    .state('app.blood-bank-donor-mgmt-donor-registration-workflows-registration',{
                        url: '/blood_bank/donor_mgmt/donor_registration/workflows/registration',
                        templateUrl: 'partials/blood_bank/donor_mgmt/donor_registration/workflows/registration/list.html'
                    })
                    .state('app.blood-bank-donor-mgmt-my-request-status',{
                        url: '/blood_bank/donor_mgmt/my_request_status',
                        templateUrl: 'partials/blood_bank/donor_mgmt/my_request_status/list.html'
                    })
                    .state('app.blood-bank-donor-mgmt-registered-donor-view',{
                        url: '/blood_bank/donor_mgmt/registered_donor_view',
                        templateUrl: 'partials/blood_bank/donor_mgmt/registered_donor_view/list.html'
                    })
                    .state('app.blood-bank-donor-mgmt-stock-availability',{
                        url: '/blood_bank/donor_mgmt/stock_availability',
                        templateUrl: 'partials/blood_bank/donor_mgmt/stock_availability/list.html'
                    })
                    .state('app.blood-bank-donor-mgmt-stock-requisition',{
                        url: '/blood_bank/donor_mgmt/stock-requisition',
                        templateUrl: 'partials/blood_bank/donor_mgmt/stock_requisition/list.html'
                    })
                    .state('app.blood-bank-lab-mgmt-blood-grouping',{
                        url: '/blood_bank/lab_mgmt/blood_grouping',
                        templateurl: 'partials/blood_bank/lab_mgmt/blood_grouping/list.html'
                    })
                    .state('app.blood-bank-lab-mgmt-blood-screening',{
                        url: '/blood_bank/lab_mgmt/blood_screening',
                        templateurl: 'partials/blood_bank/lab_mgmt/blood_screening/list.html'
                    })
                    .state('app.blood-bank-lab-mgmt-component-preparation',{
                        url: '/blood_bank/lab_mgmt/component_preparation',
                        templateurl: 'partials/blood_bank/lab_mgmt/component_preparation/list.html'
                    })
                    .state('app.blood-bank-lab-mgmt-discarded-worklist',{
                        url: '/blood_bank/lab_mgmt/discarded_worklist',
                        templateurl: 'partials/blood_bank/lab_mgmt/discarded_worklist/list.html'
                    })
                    .state('app.blood-bank-reports',{
                        url: '/blood_bank/reports',
                        templateurl: 'partials/blood_bank/reports/list.html'
                    })
                    .state('app.blood-bank-transfusion-mgmt-blood-request',{
                        url: '/blood_bank/transfusion_mgmt/blood_request',
                        templateurl: 'partials/blood_bank/transfusion_mgmt/blood_request/list.html'
                    })
                    .state('app.blood-bank-transfusion-mgmt-blood-request-issue',{
                        url: '/blood_bank/transfusion_mgmt/blood_request_issue',
                        templateurl: 'partials/blood_bank/transfusion_mgmt/blood_request_issue/list.html'
                    })
                    .state('app.blood-bank-transfusion-mgmt-discard-worklist',{
                        url: '/blood_bank/transfusion_mgmt/discard_worklist',
                        templateurl: 'partials/blood_bank/transfusion_mgmt/discard_worklist/list.html'
                    })
                    .state('app.blood-bank-lab-mgmt-cross-matching-worklist',{
                        url: '/blood_bank/lab_mgmt/cross_matching_worklist',
                        templateurl: 'partials/blood_bank/lab_mgmt/cross_matching_worklist/list.html'
                    })




                /*.state('app.mail', {
                 abstract: true,
                 url: '/mail',
                 //template: '<div ui-view class=""></div>',
                 templateUrl: 'partials/mail.html',
                 // use resolve to load other dependences
                 resolve: {
                 deps: ['uiLoad',
                 function(uiLoad) {
                 return uiLoad.load(['../bower_components/font-awesome/css/font-awesome.css', 'js/controllers/mail.js',
                 'js/services/mail-service.js',
                 JQ_CONFIG.moment
                 ]);
                 }
                 ]
                 }
                 })
                 .state('app.mail.list', {
                 url: '/{fold}',
                 templateUrl: 'partials/mail-list.html'
                 })
                 .state('app.mail.compose', {
                 url: '/compose',
                 templateUrl: 'partials/mail-compose.html'
                 })
                 .state('app.mail.view', {
                 url: '/{mailId:[0-9]{1,4}}',
                 templateUrl: 'partials/mail-view.html'
                 })

                 .state('app.hos-doctors', {
                 url: '/hospital/doctors',
                 templateUrl: 'partials/hos-doctors.html',
                 resolve: {
                 deps: ['uiLoad',
                 function(uiLoad) {
                 return uiLoad.load(['js/controllers/doctors.js']);
                 }
                 ]
                 }
                 })
                 .state('app.hos-doctor-add', {
                 url: '/hospital/doctor-add',
                 templateUrl: 'partials/hos-doctor-add.html'
                 })
                 .state('app.hos-doctor-edit', {
                 url: '/hospital/doctor-edit',
                 templateUrl: 'partials/hos-doctor-edit.html'
                 })
                 .state('app.hos-doctor-profile', {
                 url: '/hospital/doctor-profile',
                 templateUrl: 'partials/hos-doctor-profile.html',
                 resolve: {
                 deps: ['uiLoad',
                 function(uiLoad) {
                 return uiLoad.load(['../bower_components/font-awesome/css/font-awesome.css']);
                 }
                 ]
                 }

                 })
                 .state('app.hos-patients', {
                 url: '/hospital/patients',
                 templateUrl: 'partials/hos-patients.html',
                 resolve: {
                 deps: ['uiLoad',
                 function(uiLoad) {
                 return uiLoad.load(['js/controllers/patients.js']);
                 }
                 ]
                 }
                 })
                 .state('app.hos-patient-add', {
                 url: '/hospital/patient-add',
                 templateUrl: 'partials/hos-patient-add.html'
                 })
                 .state('app.hos-patient-edit', {
                 url: '/hospital/patient-edit',
                 templateUrl: 'partials/hos-patient-edit.html'
                 })
                 .state('app.hos-patient-profile', {
                 url: '/hospital/patient-profile',
                 templateUrl: 'partials/hos-patient-profile.html',
                 resolve: {
                 deps: ['uiLoad',
                 function(uiLoad) {
                 return uiLoad.load(['../bower_components/font-awesome/css/font-awesome.css']);
                 }
                 ]
                 }

                 })

                 .state('app.hos-book-appointment', {
                 url: '/hospital/staff-add',
                 templateUrl: 'partials/hos-book-appointment.html'
                 })
                 .state('app.hos-doc-schedule', {
                 url: '/hospital/doc-schedule',
                 templateUrl: 'partials/hos-doc-schedule.html',
                 resolve: {
                 deps: ['$ocLazyLoad', 'uiLoad',
                 function($ocLazyLoad, uiLoad) {
                 return uiLoad.load(
                 JQ_CONFIG.fullcalendar.concat('js/controllers/doctor-schedule.js')
                 ).then(
                 function() {
                 return $ocLazyLoad.load('ui.calendar');
                 }
                 )
                 }
                 ]
                 }
                 })

                 .state('app.hos-payments', {
                 url: '/hospital/payments',
                 templateUrl: 'partials/hos-payments.html'
                 })
                 .state('app.hos-payment-add', {
                 url: '/hospital/payment-add',
                 templateUrl: 'partials/hos-payment-add.html',
                 controller: 'HospitalPaymentAddCtrl',
                 resolve: {
                 deps: ['$ocLazyLoad',
                 function($ocLazyLoad) {
                 return $ocLazyLoad.load('xeditable').then(
                 function() {
                 return $ocLazyLoad.load('js/controllers/hos-payment-add.js');
                 }
                 );
                 }
                 ]
                 }
                 })
                 .state('app.hos-patient-invoice', {
                 url: '/hospital/patient-invoice',
                 templateUrl: 'partials/hos-patient-invoice.html'
                 })
                 .state('app.hos-staffs', {
                 url: '/hospital/staffs',
                 templateUrl: 'partials/hos-staffs.html',
                 resolve: {
                 deps: ['uiLoad',
                 function(uiLoad) {
                 return uiLoad.load(['js/controllers/hos-staffs.js']);
                 }
                 ]
                 }
                 })
                 .state('app.hos-staff-add', {
                 url: '/hospital/staff-add',
                 templateUrl: 'partials/hos-staff-add.html'
                 })
                 .state('app.hos-staff-edit', {
                 url: '/hospital/staff-edit',
                 templateUrl: 'partials/hos-staff-edit.html'
                 })
                 .state('app.hos-staff-profile', {
                 url: '/hospital/staff-profile',
                 templateUrl: 'partials/hos-staff-profile.html',
                 resolve: {
                 deps: ['uiLoad',
                 function(uiLoad) {
                 return uiLoad.load(['../bower_components/font-awesome/css/font-awesome.css']);
                 }
                 ]
                 }

                 })
                 .state('app.hos-events', {
                 url: '/hospital/events',
                 templateUrl: 'partials/hos-events.html',
                 resolve: {
                 deps: ['$ocLazyLoad', 'uiLoad',
                 function($ocLazyLoad, uiLoad) {
                 return uiLoad.load(
                 JQ_CONFIG.fullcalendar.concat('js/controllers/hos-events.js')
                 ).then(
                 function() {
                 return $ocLazyLoad.load('ui.calendar');
                 }
                 )
                 }
                 ]
                 }
                 })*/



                /*.state('app.hos-centres', {
                 url: '/hospital/centres',
                 templateUrl: 'partials/hos-centres.html',
                 resolve: {
                 deps: ['$ocLazyLoad',
                 function($ocLazyLoad) {
                 return $ocLazyLoad.load('js/controllers/hos-centres.js');
                 }
                 ]
                 }
                 })

                 .state('app.hos-report-patient', {
                 url: '/hospital/report-patient',
                 templateUrl: 'partials/hos-report-patient.html',
                 resolve: {
                 deps: ['$ocLazyLoad',
                 function($ocLazyLoad) {
                 return $ocLazyLoad.load('chart.js').then(
                 function() {
                 return $ocLazyLoad.load('js/controllers/hos-report-patient.js');
                 }
                 );
                 }
                 ]
                 }
                 })
                 .state('app.hos-report-hospital', {
                 url: '/hospital/report-hospital',
                 templateUrl: 'partials/hos-report-hospital.html',
                 resolve: {
                 deps: ['$ocLazyLoad',
                 function($ocLazyLoad) {
                 return $ocLazyLoad.load('chart.js').then(
                 function() {
                 return $ocLazyLoad.load('js/controllers/hos-report-hospital.js');
                 }
                 );
                 }
                 ]
                 }
                 })
                 .state('app.hos-report-sales', {
                 url: '/hospital/report-sales',
                 templateUrl: 'partials/hos-report-sales.html',
                 resolve: {
                 deps: ['$ocLazyLoad',
                 function($ocLazyLoad) {
                 return $ocLazyLoad.load(['js/controllers/hos-report-sales.js']);
                 }
                 ]
                 }
                 })  */

            }
        ]
    );
