﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Net.Http;
using System.Web.Http;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Web.Http.Cors;
using HMISAPI.Domain.Interfaces;
using HMIS.Domain.Interfaces;


namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HrmsController : ApiController
    {
        IHrms objHrms = new Hrms();
        #region EmpFamily
        [Route("api/Hrms/PostEmpFamily")]
        [HttpPost]
        public EmpFamilyRepository PostEmpFamily(EmpFamilyRepository PobjEmpFamily)
        {
            return objHrms.PostEmpFamily(PobjEmpFamily);
        }
        [Route("api/Hrms/GetEmpFamily")]
        [HttpGet]
        public List<EmpFamilyRepository> GetEmpFamily()
        {
            return objHrms.GetEmpFamily();
        }

        [Route("api/Hrms/GetEmpFamily/{id}")]
        [HttpGet]
        public EmpFamilyRepository GetEmpFamily(int id)
        {
            return objHrms.GetEmpFamily(id);
        }
        [Route("api/Hrms/PutEmpFamily/{id}")]
        [HttpPost]
        public bool PutEmpFamily(int id, EmpFamilyRepository PobjEmpFamily)
        {
            return objHrms.PutEmpFamily(id, PobjEmpFamily);
        }
        [Route("api/Hrms/DeleteEmpFamily/{id}")]
        [HttpPost]
        public bool DeleteEmpFamily(int id)
        {
            return objHrms.DeleteEmpFamily(id);
        }
        #endregion
        #region Professional
        [Route("api/Hrms/PostEmpProfess")]
        [HttpPost]
        public EmpProffesionalInfoRepository PostEmpProfess(EmpProffesionalInfoRepository PobjEmpPro)
        {
            return objHrms.PostEmpProfess(PobjEmpPro);
        }
        [Route("api/Hrms/GetEmpProfess/{id}")]
        [HttpGet]
        public List<EmpProffesionalInfoRepository> GetEmpProfess(int? id)
        {
            return objHrms.GetEmpProfess(id);
        }
        [Route("api/Hrms/PutEmpProfess/{id}")]
        [HttpPost]
        public bool PutEmpProfess(int id, EmpProffesionalInfoRepository PobjEmpPro)
        {
            return objHrms.PutEmpProfess(id, PobjEmpPro);
        }
        [Route("api/Hrms/DeleteEmpProfess/{id}")]
        [HttpPost]
        public bool DeleteEmpProfess(int id)
        {
            return objHrms.DeleteEmpProfess(id);
        }
        #endregion

        #region EmpPersonal
        [Route("api/Hrms/PostEmpPersonalInfo")]
        [HttpPost]
        public EmployeePersonalInfoRepository PostEmpPersonalInfo(EmployeePersonalInfoRepository EmployeePersonalInfo)
        {
            return objHrms.PostEmpPersonalInfo(EmployeePersonalInfo);
        }

        [Route("api/Hrms/GetEmpPersonalInfo")]
        [HttpGet]
        public List<EmployeePersonalInfoRepository> GetEmpPersonalInfo()
        {
            return objHrms.GetEmpPersonalInfo();
        }


        [Route("api/Hrms/GetEmpPersonalInfo/{id}")]
        [HttpGet]
        public EmployeePersonalInfoRepository GetEmpPersonalInfo(int id)
        {
            return objHrms.GetEmpPersonalInfo(id);
        }
        [Route("api/Hrms/PutEmpPersonalInfo/{id}")]
        [HttpPost]
        public bool PutEmpPersonalInfo(int id, EmployeePersonalInfoRepository EmpPersonalInfo)
        {
            return objHrms.PutEmpPersonalInfo(id, EmpPersonalInfo);
        }

        [Route("api/Hrms/DeleteEmpPersonalInfo/{id}")]
        [HttpPost]
        public bool DeleteEmpPersonalInfo(int id)
        {
            return objHrms.DeleteEmpPersonalInfo(id);
        }

        #endregion

        [Route("api/Hrms/GetHrmsDashBoard")]
        [HttpGet]
        public List<HrmsDashboardRepository> GetHrmsDashBoard()
        {
            return objHrms.GetHrmsDashBoard();
        }
        [Route("api/Hrms/PostCTC")]
        [HttpGet]
        public CtcRepository PostCTC([FromBody]CtcRepository LobjCTC)
        {


            return objHrms.PostCTC(LobjCTC);
        }

        #region Employee

        [Route("api/Hrms/GetEmployees")]
        [HttpGet]
        public List<HrmsDashboardRepository> GetEmployees()
        {
            return objHrms.GetEmployees();
        }
        [Route("api/Hrms/GetEmployees/{id}")]
        [HttpGet]
        public HrmsDashboardRepository GetEmployees(int id)
        {
            return objHrms.GetEmployees(id);
        }

        //Post Employee
        [Route("api/Hrms/PostEmployee")]
        [HttpPost]
        public HrmsDashboardRepository PostEmployee([FromBody]HrmsDashboardRepository LobjPostEmployee)
        {
            return objHrms.PostEmployee(LobjPostEmployee);
        }
        //Put Employee
        [Route("api/Hrms/PutEmployee")]
        [HttpPost]
        public bool PutEmployee(int id, [FromBody]HrmsDashboardRepository LobjPutEmployee)
        {
            return objHrms.PutEmployee(id, LobjPutEmployee);
        }

        //Delete Employees
        [Route("api/Hrms/DeleteEmployee")]
        [HttpDelete]
        public bool DeleteEmployee(int id)
        {
            return objHrms.DeleteEmployee(id);
        }

        #endregion
    }
}