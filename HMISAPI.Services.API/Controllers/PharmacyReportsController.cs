﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Web.Http.Cors;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PharmacyReportsController : ApiController
    {
        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();

        #region Reports
        [Route("api/PharmacyReports/GetCollectionReportByMonth/{dtMonthDate}")]
        [HttpGet]
        public List<CollectionReportRepository> GetCollectionReportByMonth(DateTime dtMonthDate)
        {
            //DateTime dtMonthDate = new DateTime(2016, 3, 1);
            return objPharmacy.GetCollectionReportByMonth(dtMonthDate);
        }
        [Route("api/PharmacyReports/GetCollectionReportByDate/{dtFromdate},{dtTodate}")]
        [HttpGet]
        public List<CollectionReportRepository> GetCollectionReportByDate(DateTime dtFromdate, DateTime dtTodate)
        {
            //DateTime dtFromdate = new DateTime(2016, 3, 1);
            //DateTime dtTodate = new DateTime(2016, 4, 1);
            return objPharmacy.GetCollectionReportByDate(dtFromdate, dtTodate);
        }
        [Route("api/PharmacyReports/GetDailySalesReport")]
        [HttpGet]
        public List<DailySalesReportRepository> GetDailySalesReport()
        {
            return objPharmacy.GetDailySalesReport();
        }
        [Route("api/PharmacyReports/GetSalesReportByMonth/{dtMonthDate}")]
        [HttpGet]
        public List<SalesReportRepository> GetSalesReportByMonth(DateTime dtMonthDate)
        {
            //DateTime dtMonthDate = new DateTime(2016, 4, 1);
            return objPharmacy.GetSalesReportByMonth(dtMonthDate);
        }

        [Route("api/PharmacyReports/GetSalesReportByDate/{dtFromdate},{dtTodate}")]
        [HttpGet]
        public List<SalesReportRepository> GetSalesReportByDate(DateTime dtFromdate, DateTime dtTodate)
        {
            //DateTime dtFromdate = new DateTime(2015, 3, 1);
            //DateTime dtTodate = new DateTime(2016, 4, 6);
            return objPharmacy.GetSalesReportByDate(dtFromdate, dtTodate);
        }
        [Route("api/PharmacyReports/GetSalesReturnReportByMonth/{dtMonthDate}")]
        [HttpGet]
        public List<SalesReturnReportRepository> GetSalesReturnReportByMonth(DateTime dtMonthDate)
        {
            //DateTime dtMonthDate = new DateTime(2016, 4, 1);
            return objPharmacy.GetSalesReturnReportByMonth(dtMonthDate);
        }
        [Route("api/PharmacyReports/GetSalesReturnReportByDate/{dtFromdate},{dtTodate}")]
        [HttpGet]
        public List<SalesReturnReportRepository> GetSalesReturnReportByDate(DateTime dtFromdate, DateTime dtTodate)
        {
            //DateTime dtFromdate = new DateTime(2016, 4, 1);
            //DateTime dtTodate = new DateTime(2016, 11, 1);
            return objPharmacy.GetSalesReturnReportByDate(dtFromdate, dtTodate);
        }
        [Route("api/PharmacyReports/GetStockReturnReportByMonth/{dtMonthDate}")]
        [HttpGet]
        public List<StockReturnReportRepository> GetStockReturnReportByMonth(DateTime dtMonthDate)
        {
            //DateTime dtMonthDate = new DateTime(2016, 4, 1);
            return objPharmacy.GetStockReturnReportByMonth(dtMonthDate);
        }
        [Route("api/PharmacyReports/GetStockReturnReportByDate/{dtFromdate},{dtTodate}")]
        [HttpGet]
        public List<StockReturnReportRepository> GetStockReturnReportByDate(DateTime dtFromdate, DateTime dtTodate)
        {
            //DateTime dtFromdate = new DateTime(2016, 3, 1);
            //DateTime dtTodate = new DateTime(2016, 12, 2);
            return objPharmacy.GetStockReturnReportByDate(dtFromdate, dtTodate);
        }
        [Route("api/PharmacyReports/GetPatientInvoiceReportByDate/{dtFromdate},{dtTodate}")]
        [HttpGet]
        public List<PatientInvoiceReportRepository> GetPatientInvoiceReportByDate(DateTime dtFromdate, DateTime dtTodate)
        {
            //DateTime dtFromdate = new DateTime(2016, 3, 1);
            //DateTime dtTodate = new DateTime(2016, 5, 2);
            return objPharmacy.GetPatientInvoiceReportByDate(dtFromdate, dtTodate);
        }
        [Route("api/PharmacyReports/GetPatientInvoiceReportByPatientName/{PatientName}")]
        [HttpGet]
        public List<PatientInvoiceReportRepository> GetPatientInvoiceReportByPatientName(string PatientName)
        {
            return objPharmacy.GetPatientInvoiceReportByPatientName(PatientName);
        }
        [Route("api/PharmacyReports/GetConsultationReportByMonth/{dtMonthDate}")]
        [HttpGet]
        public List<ConsultationReportRepository> GetConsultationReportByMonth(DateTime dtMonthDate)
        {
            //DateTime dtMonthDate = new DateTime(2015, 3, 1);
            return objPharmacy.GetConsultationReportByMonth(dtMonthDate);
        }
        [Route("api/PharmacyReports/GetConsultationReportByDate/{dtFromdate},{dtTodate}")]
        [HttpGet]
        public List<ConsultationReportRepository> GetConsultationReportByDate(DateTime dtFromdate, DateTime dtTodate)
        {
            //DateTime dtFromdate = new DateTime(2015, 3, 1);
            //DateTime dtTodate = new DateTime(2015, 4, 1);
            return objPharmacy.GetConsultationReportByDate(dtFromdate, dtTodate);
        }
        [Route("api/PharmacyReports/GetPatientSalesReportByDate/{dtFromdate},{dtTodate}")]
        [HttpGet]
        public List<PatientSalesReportRepository> GetPatientSalesReportByDate(DateTime dtFromdate, DateTime dtTodate)
        {
            //DateTime dtFromdate = new DateTime(2016, 3, 1);
            //DateTime dtTodate = new DateTime(2016, 5, 1);
            return objPharmacy.GetPatientSalesReportByDate(dtFromdate, dtTodate);
        }
        [Route("api/PharmacyReports/GetPatientSalesReportByMedicineName/{MedicineName}")]
        [HttpGet]
        public List<PatientSalesReportRepository> GetPatientSalesReportByMedicineName(string MedicineName)
        {
            return objPharmacy.GetPatientSalesReportByMedicineName(MedicineName);
        }
        [Route("api/PharmacyReports/GetPatientSalesReportByPatientName/{PatientName}")]
        [HttpGet]
        public List<PatientSalesReportRepository> GetPatientSalesReportByPatientName(string PatientName)
        {
            return objPharmacy.GetPatientSalesReportByPatientName(PatientName);
        }

        public List<MedicineSalesandStockReportRepository> GetMedicineSalesandStockReportByName(string MedicineName)
        {
            return objPharmacy.GetMedicineSalesandStockReportByName(MedicineName);
        }
        [Route("api/PharmacyReports/GetMedicineStockReportt/{PstrMedicineName},{PintQuantityGreater},{PintQuantityLessthan}")]
        [HttpGet]
        public List<MedicineStockReportRepository> GetMedicineStockReport(string PstrMedicineName, int? PintQuantityGreater, int? PintQuantityLessthan)
        {
            return objPharmacy.GetMedicineStockReport(PstrMedicineName, PintQuantityGreater, PintQuantityLessthan);
        }

        #endregion
    }
}
