﻿using HMISAPI.Domain.Interfaces;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HMISAPI.Services.API.Controllers
{
     [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MedicineTypesController : ApiController
    {
         IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();
         
         public List<MedicineTypeRepository> Get()
        {
            return objPharmacy.GetMedicineType();
        }          
         public MedicineTypeRepository Post([FromBody]MedicineTypeRepository PobjMedicineType)
        {
            return objPharmacy.PostMedicineType(PobjMedicineType) ;
        }
         public bool Put(int id, [FromBody]MedicineTypeRepository PobjMedicineType)
         {
             return objPharmacy.PutMedicineType(id, PobjMedicineType);
         }
        public bool Delete(int id)
        {
            return objPharmacy.DeleteMedicineType(id);
        }

         [Route("api/MedicineTypes/Search/{name}")]
         [HttpGet]
        public List<MedicineTypeRepository> Search(string name)
        {
            return objPharmacy.GetMedicineTypeBySearch(name);
        }
         public List<MedicineTypeRepository> Get(int id)
         {
             return objPharmacy.GetMedicineTypeById(id);
         }     
    }
    
}
