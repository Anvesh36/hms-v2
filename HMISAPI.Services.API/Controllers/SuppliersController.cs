﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Domain.Interfaces;
namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SuppliersController : ApiController
    {
        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();
        // GET api/Suppliers
        public List<SupplierRepository> Get()
        {
            return objPharmacy.GetSuppliers();
        }

        public List<SupplierRepository> Get(int id)
        {
            return objPharmacy.GetSuppliersById(id);
        }

       
        public SupplierRepository Post([FromBody]SupplierRepository LobjSuppliers)
        {


            return objPharmacy.PostSupplier(LobjSuppliers);
        }
        // PUT api/manufacturers/5
        public bool Put(int id, [FromBody]SupplierRepository LobjSupplier)
        {

            return objPharmacy.PutSupplier(id, LobjSupplier);
        }
        public bool Delete(int id)
        {
            return objPharmacy.DeleteSupplier(id);
        }

    }
}