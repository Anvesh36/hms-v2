﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers.Pharmacy
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/pharmacy/masters")]
    public class MastersController : ApiController
    {
        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();
        //[Route("manufactures")]
        //[HttpGet]
        //public List<ManufacturerRepository> GetManufactures()
        //{
        //    return objPharmacy.GetManufacturersList();
        //}

        //[Route("manufactures/all")]
        //[HttpGet]
        //public List<ManufacturerRepository> GetAllManufactures()
        //{
        //    return objPharmacy.GetManufacturersList(false);
        //}
        //[Route("manufactures")]
        //[HttpPost]
        //public ManufacturerRepository CreateManufacturer([FromBody]ManufacturerRepository LobjManufacturer)
        //{
        //    return objPharmacy.PostManufacturer(LobjManufacturer);
        //}

        //[Route("manufactures/{id}")]
        //[HttpPut]
        //public bool UpdateManufacturer(int id, [FromBody]ManufacturerRepository LobjManufacturer)
        //{
        //    return objPharmacy.PutManufacturer(id, LobjManufacturer);
        //}

        //[Route("manufactures/{id}")]
        //[HttpDelete]
        //public bool DeleteManufacturer(int id)
        //{
        //    return objPharmacy.DeleteManufacturer(id);
        //}
    }
}
