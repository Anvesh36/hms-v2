﻿using HMISAPI.Domain.Interfaces;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HMISAPI.Services.API.Controllers
{
       [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MedicineGroupsController : ApiController
    {
           IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();
            public List<MedicineGroupRepository> Get()
            {
                return objPharmacy.GetMedicineGroup();
            }
            public MedicineGroupRepository Post([FromBody]MedicineGroupRepository PobjMedicneGroup)
            {
                return objPharmacy.PostMedicineGroup(PobjMedicneGroup);
            }

            public bool Put(int id, [FromBody]MedicineGroupRepository PobjMedicineGroup)
            {
                return objPharmacy.PutMedicineGroup(id, PobjMedicineGroup);
            }
            public bool Delete(int id)
            {
                return objPharmacy.DeleteMedicineGroup(id);
            }

            [Route("api/MedicineGroups/Search/{name}")]
            [HttpGet]
            public List<MedicineGroupRepository> Search(string name)
            {
                return objPharmacy.GetMedicineGroupBySearch(name);
            }
            public List<MedicineGroupRepository> Get(int id)
            {
                return objPharmacy.GetMedicineGroupById(id);
            }
  
        }
}
