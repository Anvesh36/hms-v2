﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProductsController : ApiController
    {
        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();
        // GET: Products
        #region Product

        public List<ProductsRepository> Get()
        {
            return objPharmacy.GetProduct();
        }
        public ProductsRepository Post(ProductsRepository PobjProductDetails)
        {
            return objPharmacy.PostProduct(PobjProductDetails);
        }
        public bool Put(int id, ProductsRepository PobjProduct)
        {
            return objPharmacy.PutProduct(id, PobjProduct);
        }

        public bool Delete(int id)
        {
            return objPharmacy.deleteProduct(id);

        }

        [Route("api/Products/GetProductBySearch/{type},{name}")]
        [HttpGet]
        public List<ProductsRepository> GetProductBySearch(string type, string name)
        {
            return objPharmacy.GetProductBySearch(type, name);
        }

        [Route("api/Products/GetProductByName/{name}")]
        [HttpGet]
        public List<ProductsRepository> GetProductByName(string name)
        {
            return objPharmacy.GetProductByName(name);
        }
        #endregion
    }
}