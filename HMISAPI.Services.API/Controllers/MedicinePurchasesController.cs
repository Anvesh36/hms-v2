﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MedicinePurchasesController : ApiController
    {
        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();

        #region MedicinePurchase
        public MedicinePurchaseRepository Post([FromBody] MedicinePurchaseRepository LobjMedicinePur) //MedicinePurchaseRepository PobjMedicinePurchase)
        {
            
            return objPharmacy.PostMedicinePurchase(LobjMedicinePur);
        }

        #endregion
    }
}