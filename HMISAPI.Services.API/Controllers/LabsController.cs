﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Web.Http.Cors;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LabsController : ApiController
    {
        ILab objlab = new HMISAPI.Infrastructure.Data.Lab();

        [Route("api/Labs/GetInvestigationsBySearch/{Pstrterm}")]
        [HttpGet]
        public List<DiagnosisRepository> GetInvestigationsBySearch(string Pstrterm)
        {
            return objlab.GetInvestigationsBySearch(Pstrterm);
        }
        [Route("api/Labs/PostInvestigations")]
        [HttpPost]
        public DiagnosisRepository PostInvestigations(DiagnosisRepository PobjDiagRep)
        {
            return objlab.PostInvestigations(PobjDiagRep);
        }
        [Route("api/Labs/GetPatientsByID/{id}")]
        [HttpGet]
        public List<DiagnosisRequisitionRepository> GetPatientsByID(int id)
        {
            return objlab.GetPatientsByID(id);
        }
        [Route("api/Labs/PostDiagnosisRequisition")]
        [HttpPost]
        public DiagnosisRequisitionRepository PostDiagnosisRequisition([FromBody]DiagnosisRequisitionRepository pobjdiagrequisition)
        {
            return objlab.PostDiagnosisRequisition(pobjdiagrequisition);
        }
        [Route("api/Labs/PostOpRequest")]
        [HttpPost]
        public DiagnosisRequisitionRepository PostOpRequest([FromBody]DiagnosisRequisitionRepository pobjdiagrequisition)
        {
            return objlab.PostOpRequest(pobjdiagrequisition);
        }
        [Route("api/Labs/GetDailyOpCollectionReport/{dtMonthDate}")]
        [HttpGet]
        public List<OpCollectionsReport> GetDailyOpCollectionReport(DateTime dtMonthDate)
        {
            return objlab.GetDailyOpCollectionReport(dtMonthDate);
        }
        [Route("api/Labs/OpCollectionReportByDate/{dtFromDate},{dtToDate}")]
        [HttpGet]

        public List<OpCollectionsReport> OpCollectionReportByDate(DateTime dtFromDate, DateTime dtToDate)
        {
            return objlab.OpCollectionReportByDate(dtFromDate, dtToDate);
        }
        [Route("api/Labs/OpCollectionReportByMonth/{Date}")]
        [HttpGet]
        public List<OpCollectionsReport> OpCollectionReportByMonth(DateTime Date)
        {
            return objlab.OpCollectionReportByMonth(Date);
        }
        [Route("api/Labs/GetDailyLabCollection/{date}")]
        [HttpGet]
        public List<DiagnosisRequisitionRepository> GetDailyLabCollection(DateTime date)
        {          
            return objlab.GetDailyLabCollection(date);
        }
        [Route("api/Labs/GetLabCollectionByDate/{frmdate},{Todate}")]
        [HttpGet]
        public List<DiagnosisRequisitionRepository> GetLabCollectionByDate(DateTime Frmdate, DateTime Todate)
        {

            return objlab.GetLabCollectionByDate(Frmdate, Todate);
        }
        [Route("api/Labs/GetMonthlyLabCollection/{dtyear}")]
        [HttpGet]
        public List<DiagnosisRequisitionRepository> GetMonthlyLabCollection(DateTime dtyear)
        {
            return objlab.GetMonthlyLabCollection(dtyear);
        }
        [Route("api/Labs/GetRequstSummary")]
        [HttpGet]
        public List<RequestSummaryRepository> GetRequestSummary()
        {
            return objlab.GetRequestSummary();
        }
        [Route("api/Labs/GetTemplates/{pstrsearch}")]
        [HttpGet]
        public List<DiagnosisRepository> GetTemplates(string pstrsearch)
        {
            return objlab.GetTemplates(pstrsearch);
        }
        [Route("api/Labs/PostTemplate")]
        [HttpPost]
        public DiagnosisComponentsRepository PostTemplate(DiagnosisComponentsRepository PobjCompValue)
        {
            return objlab.PostTemplate(PobjCompValue);
        }

        [Route("api/Labs/PostDiagnosis/")]
        [HttpPost]
        public DiagnosisRepository PostDiagnosis(DiagnosisRepository PobjDiagRep)
        {
            return objlab.PostDiagnosis(PobjDiagRep);
        }

        [Route("api/Labs/PutDiagnosis/{id}")]
        [HttpPost]
        public bool PutDiagnosis(int Id, DiagnosisRepository PobjDiagRep)
        {
            return objlab.PutDiagnosis(Id, PobjDiagRep);
        }
        [Route("api/Labs/PostDiagResults/")]
        [HttpPost]
        public DiagRequisitionInfoResultRepository PostDiagResults(DiagRequisitionInfoResultRepository PobjDiagReqResult)
        {
            return objlab.PostDiagResults(PobjDiagReqResult);
        }
        [Route("api/Labs/PutDiagResults/{id}")]
        [HttpPost]
        public bool PutDiagResults(int Id,DiagRequisitionInfoResultRepository PobjDiagReqResult)
        {
            return objlab.PutDiagResults(Id,PobjDiagReqResult);
        }


        [Route("api/Labs/PostVocherPayment")]
        [HttpPost]
        public TransactionRepository PostVocherPayment([FromBody]TransactionRepository pobjtransaction)
        {
            return objlab.PostVocherPayment(pobjtransaction);
        }
        
        [Route("api/Labs/GetVocherPayment")]
        [HttpGet]
        public List<TransactionRepository> GetVocherPayment()
        {
            return objlab.GetVocherPayment();  
        }
        [Route("api/Labs/PutVocherPayment/{id}")]
        [HttpPost]
        public bool PutVocherPayment(int id, [FromBody]TransactionRepository pobjtransaction)
        {
            return objlab.PutVocherPayment(id,pobjtransaction);
        }
        [Route("api/Labs/GetDiagnosisReport")]
        [HttpGet]
        public List<GetDiagnosisReportRepository> GetDiagnosisReport()
        {
            return objlab.GetDiagnosisReport();
        }

        
        [Route("api/Labs/PostDiagnosisComponents")]
        [HttpPost]
        public DiagnosisRepository PostDiagnosisComponents([FromBody]DiagnosisRepository PobjDiagnosisComponent)
        {
            return objlab.PostDiagnosisComponents(PobjDiagnosisComponent);

        }


    }
}