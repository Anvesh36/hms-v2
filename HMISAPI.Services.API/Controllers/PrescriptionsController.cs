﻿using HMISAPI.Domain.Interfaces;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PrescriptionsController : ApiController
    {
        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();

        [Route("api/Prescriptions/GetDrugPrescription")]
        [HttpGet]
        public List<DrugPrescriptionRepository> GetDrugPrescription()
        {
            return objPharmacy.GetDrugPrescriptions();
        }

        [Route("api/Prescriptions/GetDrugPrescription/{id}")]
        [HttpGet]
        public DrugPrescriptionRepository GetDrugPrescription(int id)
        {
            return objPharmacy.GetDrugPrescriptions(id);
        }
        public DrugPrescriptionRepository Post(DrugPrescriptionRepository PobjDrugPrescription)
        {
            return objPharmacy.PostDrugPrescription(PobjDrugPrescription);
        }
        public bool Put(int id, [FromBody]DrugPrescriptionRepository PobjDrugPrescription)
        {
            return objPharmacy.PutDrugPrescription(id, PobjDrugPrescription);
        }
        [Route("api/Prescriptions/GetLabPrescriptions")]
        [HttpGet]
        public List<LabPrescriptionsRepository> GetLabPrescriptions()
        {
            return objPharmacy.GetLabPrescriptions();
        }

        [Route("api/Prescriptions/GetLabPrescriptionsById/{id}")]
        [HttpGet]
        public LabPrescriptionsRepository GetLabPrescriptions(int id)
        {
            return objPharmacy.GetLabPrescriptions(id);
        }

        [Route("api/Prescriptions/PostLabPrescription")]
        [HttpPost]
        public LabPrescriptionsRepository PostLabPrescription(LabPrescriptionsRepository PobjLabPrescription)
        {
            return objPharmacy.PostLabPrescription(PobjLabPrescription);
        }
        [Route("api/Prescriptions/PutLabPrescription/{id}")]
        [HttpPost]
        public bool PutLabPrescription(int id,LabPrescriptionsRepository PobjLabPrescription)
        {
            return objPharmacy.PutLabPrescription(id,PobjLabPrescription);
        }
    }
}
