﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using System.Data;
using System.Net.Http;
using System.Web.Http;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Web.Http.Cors;
using HMIS.Domain.Interfaces;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class CentralStoresController : ApiController
    {
        ICentralStores objCentralStores = new HMISAPI.Infrastructure.Data.CentralStores();

        #region Equipment
        [Route("api/CentralStores/GetEquipment/{id}")]
        [HttpGet]
        public EquipmentRepository GetEquipment(int id)
        {
            return objCentralStores.GetEquipment(id);

        }

        [Route("api/CentralStores/GetEquipment/")]
        [HttpGet]
        public List<EquipmentRepository> GetEquipment()
        {
            return objCentralStores.GetEquipment();
        }

        [Route("api/CentralStores/PostEquipment/")]
        [HttpPost]
        public EquipmentRepository PostEquipment(EquipmentRepository PobjEquip)
        {
            return objCentralStores.PostEquipment(PobjEquip);
        }

        [Route("api/CentralStores/PutEquipment/{id}")]
        [HttpPost]
        public bool PutEquipment(int id, EquipmentRepository PobjEquip)
        {
            return objCentralStores.PutEquipment(id, PobjEquip);
        }

        [Route("api/CentralStores/DeleteEquipment/{id}")]
        [HttpPost]
        public bool DeleteEquipment(int id)
        {
            return objCentralStores.DeleteEquipment(id);
        }
        #endregion

        #region store

        [Route("api/CentralStores/GetStore/{id}")]
        [HttpGet]
        public StoreRepository GetStore(int id)
        {
            return objCentralStores.GetStore(id);
        }

        [Route("api/CentralStores/GetStore/")]
        [HttpGet]
        public List<StoreRepository> GetStore()
        {
            return objCentralStores.GetStore();
        }

        [Route("api/CentralStores/PostStore")]
        [HttpPost]
        public StoreRepository PostStore(StoreRepository PobjStore)
        {
            return objCentralStores.PostStore(PobjStore);
        }

        [Route("api/CentralStores/PutStore/{id}")]
        [HttpPost]
        public bool PutStore(int id, StoreRepository PobjStore)
        {
            return objCentralStores.PutStore(id, PobjStore);
        }

        [Route("api/CentralStores/DeleteStore/{id}")]
        [HttpPost]
        public bool DeleteStore(int id)
        {
            return objCentralStores.DeleteStore(id);
        }

        #endregion

        #region ChemicalUsed

        [Route("api/CentralStores/GetChemicalUsed/{id}")]
        [HttpGet]
        public ChemicalUsedRepository GetChemicalUsed(int id)
        {
            return objCentralStores.GetChemicalUsed(id);
        }

        [Route("api/CentralStores/GetChemicalUsed")]
        [HttpGet]
        public List<ChemicalUsedRepository> GetChemicalUsed()
        {
            return objCentralStores.GetChemicalUsed();

        }

        [Route("api/CentralStores/PostChemicalUsed")]
        [HttpPost]
        public ChemicalUsedRepository PostChemicalUsed(ChemicalUsedRepository PobjChemUsed)
        {
            return objCentralStores.PostChemicalUsed(PobjChemUsed);
        }

        [Route("api/CentralStores/PutChemicalUsed/{id}")]
        [HttpPost]
        public bool PutChemicalUsed(int id, ChemicalUsedRepository PobjChemUsed)
        {
            return objCentralStores.PutChemicalUsed(id, PobjChemUsed);
        }

        [Route("api/CentralStores/DeleteChemicalUsed/{id}")]
        [HttpPost]
        public bool DeleteChemicalUsed(int id)
        {
            return objCentralStores.DeleteChemicalUsed(id);
        }

        #endregion

        #region ChemicalPurchase

        [Route("api/CentralStores/GetChemicalPurchase/{id}")]
        [HttpGet]
        public ChemicalPurchaseRepository GetChemicalPurchase(int id)
        {
            return objCentralStores.GetChemicalPurchase(id);

        }

        [Route("api/CentralStores/GetChemicalPurchase")]
        [HttpGet]
        public List<ChemicalPurchaseRepository> GetChemicalPurchase()
        {
            return objCentralStores.GetChemicalPurchase();

        }

        [Route("api/CentralStores/PostChemicalPurchase")]
        [HttpPost]
        public ChemicalPurchaseRepository PostChemicalPurchase(ChemicalPurchaseRepository pobjChemPur)
        {
            return objCentralStores.PostChemicalPurchase(pobjChemPur);
        }

        [Route("api/CentralStores/PutChemicalPurchase/{id}")]
        [HttpPost]
        public bool PutChemicalPurchase(int id, ChemicalPurchaseRepository pobjChemPur)
        {
            return objCentralStores.PutChemicalPurchase(id, pobjChemPur);
        }

        [Route("api/CentralStores/DeleteChemicalPurchase/{id}")]
        [HttpPost]
        public bool DeleteChemicalPurchase(int id)
        {
            return objCentralStores.DeleteChemicalPurchase(id);
        }


        #endregion
    }
}