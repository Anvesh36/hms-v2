﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Net.Http;
using System.Web.Http;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Web.Http.Cors;
using HMIS.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class HospitalsController : ApiController
    {
        IHospitals hospitalContext = new Hospitals();

        #region Floor



        [Route("api/Hospitals/GetFloors/{towerid}")]
        [HttpGet]
        public List<FloorRepository> GetFloors(int towerid)
        {
            return hospitalContext.GetFloors(towerid);
        }

        [Route("api/Hospitals/PostFloor/")]
        [HttpPost]
        public FloorRepository PostFloor(FloorRepository PobjFloor)
        {
            return hospitalContext.AddFloor(PobjFloor);
        }

        [Route("api/Hospitals/PutFloor/{id}")]
        [HttpPost]
        public bool PutFloor(int id, FloorRepository PobjFloor)
        {
            return hospitalContext.UpdateFloor(id, PobjFloor);
        }

        [Route("api/Hospitals/DeleteFloor/{id}")]
        [HttpPost]
        public bool DeleteFloor(int id)
        {
            return hospitalContext.DeleteFloor(id);
        }

        #endregion

        #region tower
        [Route("api/Hospitals/GetTowers/{Hospitalid}")]
        [HttpGet]
        public List<HospitalTowerRepository> GetTowers(int Hospitalid)
        {
            return hospitalContext.GetTowers(Hospitalid);
        }

        [Route("api/Hospitals/AddTowers")]
        [HttpPost]
        public HospitalTowerRepository AddTowers(HospitalTowerRepository PobjTower)
        {
            return hospitalContext.AddTowers(PobjTower);
        }

        [Route("api/Hospitals/UpdateTower/{id}")]
        [HttpPost]
        public bool UpdateTower(int id, HospitalTowerRepository PobjTower)
        {
            return hospitalContext.UpdateTowers(id, PobjTower);
        }

        [Route("api/Hospitals/DeleteTower/{id}")]
        [HttpPost]
        public bool DeleteTower(int id)
        {
            return hospitalContext.DeleteTowers(id);
        }

        #endregion

        #region bed

        [Route("api/Hospitals/GetBeds/{roomid}")]
        [HttpGet]
        public List<BedRepository> GetBeds(int roomid)
        {
            return hospitalContext.GetBeds(roomid);
        }

        [Route("api/Hospitals/PostBed")]
        [HttpPost]
        public BedRepository PostBed(BedRepository PobjBed)
        {
            return hospitalContext.AddBed(PobjBed);
        }

        [Route("api/Hospitals/PutBed/{id}")]
        [HttpPost]
        public bool PutBed(int id, BedRepository PobjBed)
        {
            return hospitalContext.UpdateBed(id, PobjBed);
        }

        [Route("api/Hospitals/DeleteBed/{id}")]
        [HttpPost]
        public bool DeleteBed(int id)
        {
            return hospitalContext.DeleteBed(id);
        }
        #endregion

        #region room

        [Route("api/Hospitals/PostRoom/")]
        [HttpPost]
        public RoomRepository PostRoom(RoomRepository PobjRoom)
        {
            return hospitalContext.AddRoom(PobjRoom);
        }

        [Route("api/Hospitals/GetRooms/{Towerid}")]
        [HttpGet]
        public List<RoomRepository> GetRooms(int Towerid)
        {
            return hospitalContext.GetRooms(Towerid);
        }

        [Route("api/Hospitals/PutRoom/{id}")]
        [HttpPost]
        public bool PutRoom(int id, RoomRepository PobjRoom)
        {
            return hospitalContext.UpdateRoom(id, PobjRoom);
        }

        [Route("api/Hospitals/DeleteRoom/{id}")]
        [HttpPost]
        public bool DeleteRoom(int id)
        {
            return hospitalContext.DeleteRoom(id);
        }
        #endregion
        #region Hospital

        [Route("api/Hospitals/GetHospital")]
        [HttpGet]
        public List<HospitalRepository> GetHospital()
        {
            return hospitalContext.GetHospital();
        }


        [Route("api/Hospitals/GetHospitalById/{id}")]
        [HttpGet]
        public HospitalRepository GetHospital(int id)
        {
            return hospitalContext.GetHospital(id);
        }

        [Route("api/Hospitals/PostHospital")]
        [HttpPost]
        public HospitalRepository PostHospital(HospitalRepository PobjHospital)
        {
            return hospitalContext.PostHospital(PobjHospital);
        }

        [Route("api/Hospitals/PutHospital/{id}")]
        [HttpPost]
        public bool PutHospital(int id, HospitalRepository PobjHospital)
        {
            return hospitalContext.PutHospital(id, PobjHospital);
        }

        [Route("api/Hospitals/DeleteHospital/{id}")]
        [HttpPost]
        public bool DeleteHospital(int id)
        {
            return hospitalContext.DeleteHospital(id);
        }
#endregion

        //[Route("Beds/Allocate")]
        //[HttpPost]
        //public bool AllocateBed(BedAllocationModel model)
        //{
        //    return true;
        //}
        //[Route("{hospitalId:int}/Beds/Occupancy")]
        //[HttpGet]
        //public void GetBedOccupancy(int hospitalId)
        //{

        //}
    }
}
