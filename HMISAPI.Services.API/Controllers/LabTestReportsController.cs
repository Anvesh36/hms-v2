﻿using HMISAPI.Domain.Interfaces;
using HMISAPI.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HMISAPI.Infrastructure.Repository;

namespace HMISAPI.Services.API.Controllers
{
     [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LabTestReportsController : ApiController
    {
         IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();
            public List<LabTestReportRepository> Get()
            {
                return objPharmacy.GetLabTestReport();
            }
            public LabTestReportRepository Post(LabTestReportRepository PobjPrescription)
            {
                return objPharmacy.PostLabTest (PobjPrescription);
            }
        

       

    }
}
