﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
   
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PharmacyController : ApiController
    {
        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();

       
        //#region Manufacturer
        //public List<ManufacturerRepository> GetManufacturer()
        //{
        //    return objPharmacy.GetManufacturers();
        //}

        //public List<ManufacturerRepository> GetManufacturerBySearch()
        //{
        //    return objPharmacy.GetManufacturerBySearch("m");
        //}

        //public List<ManufacturerRepository> GetManufacturerByName()
        //{
        //    return objPharmacy.GetManufacturerByName("m");
        //}

        //[AcceptVerbs("GET", "POST")]
        //public string PostManufacturer(ManufacturerRepository LobjManufacturer)
        //{
        //    //ManufacturerRepository LobjManufacturer = new ManufacturerRepository();
        //    //LobjManufacturer.MfgName = "Volini";
        //    //LobjManufacturer.AgentName = "Krishna";
        //    //LobjManufacturer.AgentContactNumber = "859745628";
        //    //LobjManufacturer.IsActive = true;
        //    return objPharmacy.PostManufacturer(LobjManufacturer);
        //}

       // [AcceptVerbs("GET", "POST")]
        //public string PutManufacturer(ManufacturerRepository LobjManufacturer)
        //{
        //    //ManufacturerRepository LobjManufacturer = new ManufacturerRepository();
        //    //LobjManufacturer.MfgID = 323;
        //    //LobjManufacturer.MfgName = "Volini";
        //    //LobjManufacturer.AgentName = "Krishna";
        //    //LobjManufacturer.AgentContactNumber = "859745628";
        //    //LobjManufacturer.IsActive = true;
        //    return objPharmacy.PutManufacturer(LobjManufacturer);
        //}
       // #endregion

        //#region MedicinePurchase
        //[AcceptVerbs("GET", "POST")]
        //public string PostMedicinePurchase() //MedicinePurchaseRepository PobjMedicinePurchase)
        //{
        //    MedicinePurchaseRepository LobjMedicinePur = new MedicinePurchaseRepository();
        //    LobjMedicinePur.SupplierName = "Hyd Enterprices";
        //    LobjMedicinePur.PurchaseDate = DateTime.Now;
        //    LobjMedicinePur.BillNo = "87955";
        //    List<MedicinePurchaseInfoRepository> LobjlstMedicinePurInfo = new List<MedicinePurchaseInfoRepository>();
        //    MedicinePurchaseInfoRepository LobjMedicinePurInfo = new MedicinePurchaseInfoRepository();
        //    ProductsRepository LobjProducts = new ProductsRepository();
        //    LobjProducts.ProductID = 2;
        //    LobjMedicinePurInfo.Product = LobjProducts;
        //    LobjMedicinePurInfo.BatchNo = "Mdy8598";
        //    LobjMedicinePurInfo.MfgDate = new DateTime(2015, 8, 20);
        //    LobjMedicinePurInfo.ExpDate = new DateTime(2017, 8, 20);
        //    LobjMedicinePurInfo.Quantity = 100;
        //    LobjMedicinePurInfo.FreeQuantity = 10;
        //    LobjMedicinePurInfo.PackQuantity = 10;
        //    LobjMedicinePurInfo.PackPurchaseCost = 80;
        //    LobjMedicinePurInfo.PurchaseCostPerUnit = 8.30m;
        //    LobjMedicinePurInfo.SellingCostPerUnit = 15;
        //    LobjMedicinePurInfo.VatPercentage = 14;
        //    LobjMedicinePurInfo.IsActive = true;
        //    LobjlstMedicinePurInfo.Add(LobjMedicinePurInfo);
        //    MedicinePurchaseInfoRepository LobjMedicinePurInfo1 = new MedicinePurchaseInfoRepository();
        //    ProductsRepository LobjProducts1 = new ProductsRepository();
        //    LobjProducts1.ProductID = 5;
        //    LobjMedicinePurInfo.Product = LobjProducts1;
        //    LobjMedicinePurInfo1.BatchNo = "JU9850";
        //    LobjMedicinePurInfo1.MfgDate = new DateTime(2015, 10, 10);
        //    LobjMedicinePurInfo1.ExpDate = new DateTime(2017, 9, 26);
        //    LobjMedicinePurInfo1.Quantity = 100;
        //    LobjMedicinePurInfo1.FreeQuantity = 10;
        //    LobjMedicinePurInfo1.PackQuantity = 5;
        //    LobjMedicinePurInfo1.PackPurchaseCost = 90;
        //    LobjMedicinePurInfo1.PurchaseCostPerUnit = 17.18m;
        //    LobjMedicinePurInfo1.SellingCostPerUnit = 22;
        //    LobjMedicinePurInfo1.VatPercentage = 5;
        //    LobjMedicinePurInfo1.IsActive = true;
        //    LobjlstMedicinePurInfo.Add(LobjMedicinePurInfo1);
        //    LobjMedicinePur.lstMedicinePurchaseInfo = LobjlstMedicinePurInfo;
        //    return objPharmacy.PostMedicinePurchase(LobjMedicinePur);
        //}

        //#endregion

        //#region Alerts
        //[AcceptVerbs("GET", "POST")]
        //public List<ExpiredRepository> GetAllExpired()
        //{
        //    return objPharmacy.GetAllExpired();
        //}
        //[AcceptVerbs("GET", "POST")]
        //public List<EndOfStockRepository> EndOfStock()
        //{
        //    return objPharmacy.EndOfStock();
        //}
        //#endregion

        //#region Reports
        //[Route("api/Pharmacy/GetCollectionReportByMonth/{dtMonthDate}")]
        //[HttpPost]
        //public List<CollectionReportRepository> GetCollectionReportByMonth(DateTime dtMonthDate)
        //{
        //    //DateTime dtMonthDate = new DateTime(2016, 3, 1);
        //    return objPharmacy.GetCollectionReportByMonth(dtMonthDate);
        //}
        // [Route("api/Pharmacy/GetCollectionReportByDate/{dtMonthDate}")]
        //public List<CollectionReportRepository> GetCollectionReportByDate(DateTime dtFromdate, DateTime dtTodate)
        //{
        //    //DateTime dtFromdate = new DateTime(2016, 3, 1);
        //    //DateTime dtTodate = new DateTime(2016, 4, 1);
        //    return objPharmacy.GetCollectionReportByDate(dtFromdate, dtTodate);
        //}
        //public List<DailySalesReportRepository> GetDailySalesReport()
        //{
        //    return objPharmacy.GetDailySalesReport();
        //}
        //public List<SalesReportRepository> GetSalesReportByMonth()
        //{
        //    DateTime dtMonthDate = new DateTime(2016, 4, 1);
        //    return objPharmacy.GetSalesReportByMonth(dtMonthDate);
        //}
        //public List<SalesReportRepository> GetSalesReportByDate()
        //{
        //    DateTime dtFromdate = new DateTime(2015, 3, 1);
        //    DateTime dtTodate = new DateTime(2016, 4, 6);
        //    return objPharmacy.GetSalesReportByDate(dtFromdate, dtTodate);
        //}
        //public List<SalesReturnReportRepository> GetSalesReturnReportByMonth()
        //{
        //    DateTime dtMonthDate = new DateTime(2016, 4, 1);
        //    return objPharmacy.GetSalesReturnReportByMonth(dtMonthDate);
        //}
        //public List<SalesReturnReportRepository> GetSalesReturnReportByDate()
        //{
        //    DateTime dtFromdate = new DateTime(2016, 4, 1);
        //    DateTime dtTodate = new DateTime(2016, 11, 1);
        //    return objPharmacy.GetSalesReturnReportByDate(dtFromdate, dtTodate);
        //}
        //public List<StockReturnReportRepository> GetStockReturnReportByMonth()
        //{
        //    DateTime dtMonthDate = new DateTime(2016, 4, 1);
        //    return objPharmacy.GetStockReturnReportByMonth(dtMonthDate);
        //}
        //public List<StockReturnReportRepository> GetStockReturnReportByDate()
        //{
        //    DateTime dtFromdate = new DateTime(2016, 3, 1);
        //    DateTime dtTodate = new DateTime(2016, 12, 2);
        //    return objPharmacy.GetStockReturnReportByDate(dtFromdate, dtTodate);
        //}
        //public List<PatientInvoiceReportRepository> GetPatientInvoiceReportByDate()
        //{
        //    DateTime dtFromdate = new DateTime(2016, 3, 1);
        //    DateTime dtTodate = new DateTime(2016, 5, 2);
        //    return objPharmacy.GetPatientInvoiceReportByDate(dtFromdate, dtTodate);
        //}

        //public List<PatientInvoiceReportRepository> GetPatientInvoiceReportByPatientName()
        //{
        //    return objPharmacy.GetPatientInvoiceReportByPatientName("Rajesh R");
        //}
        //public List<ConsultationReportRepository> GetConsultationReportByMonth()
        //{
        //    DateTime dtMonthDate = new DateTime(2015, 3, 1);
        //    return objPharmacy.GetConsultationReportByMonth(dtMonthDate);
        //}

        //public List<ConsultationReportRepository> GetConsultationReportByDate()
        //{
        //    DateTime dtFromdate = new DateTime(2015, 3, 1);
        //    DateTime dtTodate = new DateTime(2015, 4, 1);
        //    return objPharmacy.GetConsultationReportByDate(dtFromdate, dtTodate);
        //}
        //public List<PatientSalesReportRepository> GetPatientSalesReportByDate()
        //{
        //    DateTime dtFromdate = new DateTime(2016, 3, 1);
        //    DateTime dtTodate = new DateTime(2016, 5, 1);
        //    return objPharmacy.GetPatientSalesReportByDate(dtFromdate, dtTodate);
        //}
        //public List<PatientSalesReportRepository> GetPatientSalesReportByMedicineName()
        //{
        //    return objPharmacy.GetPatientSalesReportByMedicineName("CIPLAR 10 MG");
        //}
        //public List<PatientSalesReportRepository> GetPatientSalesReportByPatientName()
        //{
        //    return objPharmacy.GetPatientSalesReportByPatientName("Rajesh R");
        //}
        ////public List<MedicineStockReportRepository> GetMedicineStockReport()
        ////{
        ////    return objPharmacy.GetMedicineStockReport("CIPLAR 10 MG", 25);
        ////}

        //#endregion
    }
}