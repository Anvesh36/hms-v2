﻿using HMIS.Domain.Interfaces;
using HMISAPI.Domain.Interfaces;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HMISAPI.Services.API.Controllers
{
   [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class FrontOfficeController : ApiController
    {
        IFrontOffice objFrontOffice = new FrontOffice();

        [Route("api/FrontOffice/PostCallRegister")]
        [HttpPost]
        public CallRegisterRepository PostCallRegister(CallRegisterRepository PobjCallRegister)
        {
            return objFrontOffice.PostCallRegister(PobjCallRegister);
        }
        [Route("api/FrontOffice/PutCallRegister/{id}")]
        [HttpPost]
        public bool PutCallRegister(int id,CallRegisterRepository PobjCallRegister)
        {
            return objFrontOffice.PutCallRegister(id,PobjCallRegister);     
        }

        [Route("api/FrontOffice/GetCallRegInfo")]
        [HttpGet]
        public List<CallRegisterRepository> GetCallRegInfo()
        {

            return objFrontOffice.GetCallRegInfo();
        }

        [Route("api/FrontOffice/GetCallRegInfo/{id}")]
        [HttpGet]
        public CallRegisterRepository GetCallRegInfo(int id)
        {

            return objFrontOffice.GetCallRegInfo(id);
        }


        [Route("api/FrontOffice/DeleteCallRegInfo/{id}")]
        [HttpPost]
        public bool DeleteCallRegInfo(int id)
        {
            return objFrontOffice.DeleteCallRegInfo(id);
        }
    }
}
