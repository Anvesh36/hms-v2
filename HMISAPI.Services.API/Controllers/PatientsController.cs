﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Web.Http.Cors;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PatientsController : ApiController
    {
        Patient ObjPatientDetails = new Patient();
        // GET: api/Patient
        //Content-Type: application/json
        public List<Patientinfo> Get()
        {
            IPatient ObjPatientDetails = new Patient();
            return ObjPatientDetails.GetPatientList();

        }

        // [HttpGet]
        // GET: api/Patient
        //   [Route("/api/Patient/GetLookups")]

        //public LookUpDetails GetLookups()
        //{
        //    Patient objlookup = new Patient();
        //    return objlookup.GetLookups();

        //}

        // GET: api/Patient/5
        public List<Patientinfo> Get(int id)
        {

            IPatient Lobjpatientgetbyid = new Patient();
            return Lobjpatientgetbyid.GetPatientDetailsByID(id);
        }

        // POST: api/Patient
        public Patientinfo Post([FromBody]Patientinfo PobjPatientDetails)
        {

            IPatient LobjPatient = new Patient();
            return LobjPatient.RegisterPatient(PobjPatientDetails);
        }

        // PUT: api/Patient/5
        public bool Put(int id, [FromBody]Patientinfo objPatientDemographics)
        {
            //Patientinfo objUpdate = new Patientinfo();
            //objUpdate.LastName = "ss";
            //objUpdate.PatientID = 4;
            IPatient lobUpdate = new Patient();
            return lobUpdate.PatientUpdate(id, objPatientDemographics);
        }
        // DELETE: api/Patient/5
        public bool Delete(int id)
        {
            //Patientinfo objDelete = new Patientinfo();
            //objDelete.PatientID = 4;
            IPatient lobDelete = new Patient();
            return lobDelete.PatientDelete(id);
        }
        [Route("api/Patients/GetPatients/{type},{name}")]
        [HttpGet]
        public List<Patientinfo> GetPatients(string type, string name)
        {
            IPatient objPatientDtails = new Patient();
            return objPatientDtails.GetPatients(type, name);
        }
        [Route("api/Patients/PostVitalsigns")]
        [HttpPost]
        public TreatmentRepository PostVitalsigns(TreatmentRepository PobjTreatment)
        {
            IPatient ObjPatientDetails = new Patient();
            return ObjPatientDetails.PosttreatmentVitalsigns(PobjTreatment);
        }
        [Route("api/Patients/GetPatientProfile")]
        [HttpGet]
        public List<Patientinfo> GetPatientProfile()
        {
            Patient objGetPatientProfile = new Patient();
            return objGetPatientProfile.GetPatientProfile();

        }
        [Route("api/Patients/PostOpReview")]
        [HttpPost]
        public Patientinfo PostOpReview(Patientinfo PobOpReview)
        {
           IPatient opreview = new Patient();
           return opreview.PostOpReview(PobOpReview);

        }
        [Route("api/Patients/GetOpLstReview/{treatdate}")]
        [HttpGet]
        public List<TreatmentRepository> GetOpLstReview(DateTime treatdate)
        {
            IPatient opgetreview = new Patient();
            return opgetreview.GetOpLstReview(treatdate);
        }
        [Route("api/Patients/GetBilling/{id}")]
        [HttpGet]
        public List<Patientinfo> GetBilling(int id)
        {
            return ObjPatientDetails.GetBillingDetails(id);
        }
        
        [Route("api/Patients/Getippatients/")]
        [HttpGet]
        public List<IpAdmissionRepository> Getippatients(IpAdmissionRepository PobjIpAdmission)
        {
            return ObjPatientDetails.GetIpPatients(PobjIpAdmission);
        }
        [Route("api/Patients/GetPatientVisits/{id}")]
        [HttpGet]
        public List<Patientinfo> GetPatientVisits(int id)
        {
            return ObjPatientDetails.GetPatientVisits(id);
        }
    }
}
