using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ManufacturersController : ApiController
    {
        // GET api/manufacturers

        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();
        public List<ManufacturerRepository> Get()
        {
            return objPharmacy.GetManufacturers();
        }

        [Route("api/manufacturers/all")]
        [HttpGet]
        public List<ManufacturerRepository> GetAll()
        {
            return objPharmacy.GetManufacturers(false);
        }

        public ManufacturerRepository Get(int id)
        {
            return objPharmacy.GetManufacturesById(id);
        }


        // POST api/manufacturers
        public ManufacturerRepository Post([FromBody]ManufacturerRepository LobjManufacturer)
        {
            return objPharmacy.PostManufacturer(LobjManufacturer);
        }

        // PUT api/manufacturers/5
        public bool Put(int id, [FromBody]ManufacturerRepository LobjManufacturer)
        {
            return objPharmacy.PutManufacturer(id, LobjManufacturer);
        }

        // DELETE api/manufacturers/5
        public bool Delete(int id)
        {
            return objPharmacy.DeleteManufacturer(id);
        }
    }
}
