﻿using HMISAPI.Domain.Interfaces;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MedicineInvoiceController : ApiController
    {
        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();

        [Route("api/MedicineInvoice/GetBatchNoByProductId/{Id}")]
        [HttpGet]
        public List<MedicineBatchRepository> GetBatchNoByProductId(int id)
        {
            return objPharmacy.GetBatchNoByProductId(id);
        }
        public MedicineInvoiceRepository Post([FromBody]MedicineInvoiceRepository PobjMedicineInvoice)
        {
            return objPharmacy.PostMedicineInvoice(PobjMedicineInvoice);
        }       
    }
}
