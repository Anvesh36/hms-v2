﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Net.Http;
using System.Web.Http;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Web.Http.Cors;
using HMISAPI.Domain.Interfaces;
using HMIS.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserAccessController : ApiController
    {
        IUserAccess objHrms = new HMISAPI.Infrastructure.Data.UserAccess();

        #region Roles

        
        //Post Role
        [Route("api/UserAccess/PostRole")]
        [HttpPost]
        public RolesRepository PostRole(RolesRepository PobjPostRole)
        {
            return objHrms.PostRole(PobjPostRole);
        }

        //Get Roles

        [Route("api/UserAccess/GetRoles")]
        [HttpGet]
        public List<RolesRepository> GetRoles()
        {
            return objHrms.GetRoles();
        }

        //Get Role By Id
        [Route("api/UserAccess/GetRoles/{id}")]
        [HttpGet]
        public RolesRepository GetRoles(int id)
        {
            return objHrms.GetRoles(id);
        }

        //Put Role
        [Route("api/UserAccess/PutRole/{id}")]
        [HttpPost]
        public bool PutRole(int id, [FromBody]RolesRepository LobjPutRole)
        {
            return objHrms.PutRole(id, LobjPutRole);
        }

        //Delete Role
        [Route("api/UserAccess/DeleteRole/{id}")]
        [HttpDelete]
        public bool DeleteRole(int id)
        {
            return objHrms.DeleteRole(id);
        }
        #endregion

        #region UserInfo
        //Post Role
        [Route("api/UserAccess/PostUser")]
        [HttpPost]
        public UserInfoRepository PostUser(UserInfoRepository PobjPostUser)
        {
            return objHrms.PostUser(PobjPostUser);
        }

        //Get Roles

        [Route("api/UserAccess/GetUsers")]
        [HttpGet]
        public List<UserInfoRepository> GetUsers()
        {
            return objHrms.GetUsers();
        }
        //Get By UserId
        [Route("api/UserAccess/GetUsers/{id}")]
        [HttpGet]
        public UserInfoRepository GetUsers(int id)
        {
            return objHrms.GetUsers(id);
        }

        //Put Role
        [Route("api/UserAccess/PutUser/{id}")]
        [HttpPost]
        public bool PutUser(int id, [FromBody]UserInfoRepository LobjPutUser)
        {
            return objHrms.PutUser(id, LobjPutUser);
        }

        //Delete Role
        [Route("api/UserAccess/DeleteUser/{id}")]
        [HttpDelete]
        public bool DeleteUser(int id)
        {
            return objHrms.DeleteUser(id);
        }
        #endregion

        #region Menu

        //Post Menus
        [Route("api/UserAccess/PostMenu")]
        [HttpPost]
        public MenuRepository PostMenu(MenuRepository PobjPostMenu)
        {
            return objHrms.PostMenu(PobjPostMenu);
        }
        //Get Menus
        [Route("api/UserAccess/GetMenu")]
        [HttpGet]
        public List<MenuRepository> GetMenu()
        {
            return objHrms.GetMenu();
        }

        //Get Menus By Id
        [Route("api/UserAccess/GetMenu/{id}")]
        [HttpGet]
        public MenuRepository GetMenu(int id)
        {
            return objHrms.GetMenu(id);
        }

        //Put Menus
        [Route("api/UserAccess/PutMenu/{id}")]
        [HttpPost]
        public bool PutMenu(int id, [FromBody]MenuRepository LobjPutMenu)
        {
            return objHrms.PutMenu(id, LobjPutMenu);
        }

        //Delete Menus
        [Route("api/UserAccess/DeleteMenu/{id}")]
        [HttpDelete]
        public bool DeleteMenu(int id)
        {
            return objHrms.DeleteMenu(id);
        }



        #endregion

        #region RoleMenu
        //Post Menus
        [Route("api/UserAccess/PostRoleMenu")]
        [HttpPost]
        public RoleMenuRepository PostRoleMenu(RoleMenuRepository PobjPostRoleMenu)
        {
            return objHrms.PostRoleMenu(PobjPostRoleMenu);
        }
        //Get Menus
        [Route("api/UserAccess/GetRoleMenu")]
        [HttpGet]
        public List<RoleMenuRepository> GetRoleMenu()
        {
            return objHrms.GetRoleMenu();
        }

        //Get RoleMenus
        [Route("api/UserAccess/GetRoleMenu/{id}")]
        [HttpGet]
        public RoleMenuRepository GetRoleMenu(int id)
        {
            return objHrms.GetRoleMenu(id);
        }

        //Put RoleMenus
        [Route("api/UserAccess/PutRoleMenu/{id}")]
        [HttpPost]
        public bool PutRoleMenu(int id, [FromBody]RoleMenuRepository LobjPutRoleMenu)
        {
            return objHrms.PutRoleMenu(id, LobjPutRoleMenu);
        }

        //Delete Menus
        [Route("api/UserAccess/DeleteRoleMenu/{id}")]
        [HttpDelete]
        public bool DeleteRoleMenu(int id)
        {
            return objHrms.DeleteRoleMenu(id);
        }
        #endregion


    }
}
