﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Net.Http;
using System.Web.Http;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Web.Http.Cors;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AppointmentsController : ApiController
    {
        IPatient objPatient = new Patient();

        
        [Route("api/Appointments/PostAppointments")]
        [HttpPost]
        public Patientinfo PostAppointments([FromBody]Patientinfo PobjAppointments)
        {
            return objPatient.PostAppointments(PobjAppointments);
        }
        
        [Route("api/Appointments/GetAppointments")]
        [HttpGet]
        public List<AppointmentRepository> GetAppointments()
        {
            return objPatient.GetAppointments();
        }

        [Route("api/Appointments/CancelAppointments/{id}")]
        [HttpPost]
        public bool CancelAppointments(int id)
        {
            return objPatient.DeleteAppointment(id);
        }
        [Route("api/Appointments/PutAppointment/{id}")]
        [HttpPost]
        public bool PutAppointment(int id, AppointmentRepository PobjAppointments)
        {
            return objPatient.PutAppointment(id, PobjAppointments);
        }

        [Route("api/Appointments/GenerateTokens/{aptType},{aptdate},{aptid}")]
        [HttpPost]
        public AppointmentRepository GenerateTokens(string aptType, DateTime aptdate, int aptid)
        {
            return objPatient.GenerateTokens(aptType, aptdate, aptid);
        }

        [Route("api/Appointments/PostDoctorSchedule")]
        [HttpPost]
        public DoctorScheduleRepository PostDoctorSchedule(DoctorScheduleRepository PobjDoctorSchedule)
        {
            return objPatient.PostDoctorSchedule(PobjDoctorSchedule);
        }

        [Route("api/Appointments/GetDoctorVisits/{id},{date}")]
        [HttpGet]
        public List<EmployeesRepository> GetDoctorVisits(int? id, DateTime? date)
        {
            return objPatient.GetDoctorVisits(id, date);
        }
        [Route("api/Appointments/GetOPReview")]
        [HttpGet]
        public List<Patientinfo> GetOPReview()
        {
            return objPatient.GetOpReview();
        }
        [Route("api/Appointments/GetDoctorsList")]
        [HttpGet]
        public List<EmployeesRepository> GetDoctorsList()
        {
            return objPatient.GetDoctorsList();
        }
        [Route("api/Appointments/PostDoctor")]
        [HttpPost]
        public EmployeesRepository PostDoctor(EmployeesRepository Pobjemployees)
        {
                 return objPatient.PostDoctor(Pobjemployees);
        }
        [Route("api/Appointments/PutDoctor/{id}")]
        [HttpPost]
        public bool PutDoctor(int id,EmployeesRepository Pobjemployees)
        {
            return objPatient.PutDoctor(id,Pobjemployees);
        }

        [Route("api/Appointments/GetAppointMentsBYType/{type}")]
        [HttpGet]
        public List<AppointmentRepository> GetAppointMentsBYType(string type)
        {
            return objPatient.GetAppointMentsBYType(type);

        }

        [Route("api/Appointments/GetAppointMentsByDoctorID/{DoctorID}")]
        [HttpGet]
        public List<AppointmentRepository> GetAppointMentsByDoctorID(int DoctorID)
        {
            return objPatient.GetAppointMentsByDoctorID(DoctorID);

        }

        [Route("api/Appointments/GetDrSettings")]
        [HttpGet]
        public List<DrsettingsRepository> GetDrSettings()
        {
            return objPatient.GetDrSettings();
        }

        [Route("api/Appointments/GetDrSettingsById/{id}")]
        [HttpGet]
        public DrsettingsRepository GetDrSettings(int id)
        {
            return objPatient.GetDrSettings(id);
        }

        [Route("api/Appointments/PostDrSettings")]
        [HttpPost]
        public DrsettingsRepository PostDrSettings(DrsettingsRepository PobjDrsettings)
        {
            return objPatient.PostDrSettings(PobjDrsettings);
        }

        [Route("api/Appointments/PutDrSettings/{id}")]
        [HttpPost]
        public bool PutDrSettings(int id, DrsettingsRepository PobjDrsettings)
        {
            return objPatient.PutDrSettings(id, PobjDrsettings);
        }
        [Route("api/Appointments/DeleteDrSettings/{id}")]
        [HttpPost]
        public bool DeleteDrSettings(int id)
        {
            return objPatient.DeleteDrSettings(id);
        }

        [Route("api/Appointments/GetDoctorAppointments/{DoctorId},{date}")]
        [HttpGet]
        public List<AppointmentRepository> GetDoctorAppointments(int DoctorId, DateTime date)
        {
            return objPatient.GetDoctorAppointments(DoctorId, date);
        }

        [Route("api/Appointments/GetDoctorPatients/{DoctorId},{PatientType}")]
        [HttpGet]
        public List<PatientDetailsRepository> GetDoctorPatients(int DoctorId, string PatientType)
        {
            return objPatient.GetDoctorPatients(DoctorId, PatientType);
        }

        [Route("api/Appointments/GetTokenSummary/{date},{id}")]
        [HttpGet]
        public List<AppointmentRepository> GetTokenSummary(DateTime date, int? id)
        {
            return objPatient.GetTokenSummary(date, id);
        }
    }
}