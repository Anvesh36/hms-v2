﻿using HMISAPI.Domain.Interfaces;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LabOpRequestController : ApiController
    {
        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();
        public List<LabOpRequestReportRepository> Get()
        {
            return objPharmacy.GetLabOpRequestReport();
        }
        public LabOpRequestReportRepository Post(LabOpRequestReportRepository PobjOpRequest)
        {
            return objPharmacy.PostLabOpRequest(PobjOpRequest);
        }
    }
}
