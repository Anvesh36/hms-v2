﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Domain.Interfaces;
namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StockReturnsController : ApiController
    {
        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();
        // GET: StockReturn
        [Route("api/StockReturn/GetStockReturnSuppliersByMedicineId")]
        [HttpPost]
        public List<StockReturnMedicineDetailsRepository> GetStockReturnSuppliersByMedicineId(StockReturnMedicineDetailsRepository Pstrsearch)
        {
            return objPharmacy.GetStockReturnSuppliersByMedicineId(Pstrsearch);
        }
        [Route("api/StockReturn/GetStockReturnBatchesBySupplier")]
        [HttpPost]
        public List<StockReturnMedicineDetailsRepository> GetStockReturnBatchesBySupplier(StockReturnMedicineDetailsRepository Pstrsearch)
        {
            return objPharmacy.GetStockReturnBatchesBySupplier(Pstrsearch);

        }
        [Route("api/StockReturn/GetStockReturnMedicineDetailsByBatchNo")]
        [HttpPost]
        public List<StockReturnMedicineDetailsRepository> GetStockReturnMedicineDetailsByBatchNo(StockReturnMedicineDetailsRepository Pstrsearch)
        {

            return objPharmacy.GetStockReturnMedicineDetailsByBatchNo(Pstrsearch);
        }

        public StockReturnMedicineDetailsRepository Post(StockReturnMedicineDetailsRepository PobjStockReturnMedicines)
        {
            return objPharmacy.PostStockReturn(PobjStockReturnMedicines);
        }
    }
}