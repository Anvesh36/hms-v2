﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using HMISAPI.Infrastructure.Data;
using HMISAPI.Domain.Interfaces;

namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SalesReturnsController : ApiController
    {
        IPharmacy objPharmacy = new HMISAPI.Infrastructure.Data.Pharmacy();
        // GET: SalesReturn
        public salesReturnRepository Post(salesReturnRepository PobjSalesReturn)
        {


            return objPharmacy.PostSalesReturn(PobjSalesReturn);
        }


        public List<MedicineInvoiceRepository> Get(int id)
        {
            return objPharmacy.GetSalesReturn(id);
        }
    }
}