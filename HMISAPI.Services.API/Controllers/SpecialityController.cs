﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Web.Http.Cors;
using HMISAPI.Domain.Interfaces;
using HMIS.Domain.Interfaces;
namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SpecialityController : ApiController
    {
        ISpeciality objSpeciality = new Speciality();

        [Route("api/Speciality/PostSpeciality")]
        [HttpPost]
        public SpecialityRepository PostSpeciality(SpecialityRepository PobjSpe)
        {
            return objSpeciality.PostSpeciality(PobjSpe);
        }

        [Route("api/Speciality/GetSpeciality")]
        [HttpGet]
        public List<SpecialityRepository> GetSpeciality()
        {
            return objSpeciality.GetSpeciality();
        }

        [Route("api/Speciality/GetSpeciality/{id}")]
        [HttpGet]
        public SpecialityRepository GetSpeciality(int id)
        {

            return objSpeciality.GetSpeciality(id);
        }

        [Route("api/Speciality/PutSpecialityDetail/{id}")]
        [HttpPost]
        public bool PutSpecialityDetail(int id, SpecialityRepository PobjSpe)
        {
            return objSpeciality.PutSpecialityDetail(id, PobjSpe);
        }

        [Route("api/Speciality/DeleteSpecialityDetail/{id}")]
        [HttpPost]
        public bool Delete(int id)
        {
            return objSpeciality.DeleteSpecialityDetail(id);
        }

    }
}
