﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Net.Http;
using System.Web.Http;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Web.Http.Cors;
using HMISAPI.Domain.Interfaces;
using HMIS.Domain.Interfaces;


namespace HMISAPI.Services.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BloodBankController : ApiController
    {
        IBloodBank objBloodBank = new BloodBank();

        #region Donor
        [Route("api/BloodBank/PostDonor")]
        [HttpPost]
        public BBDonorRepository PostDonor(BBDonorRepository PobjBBDonor)
        {
            return objBloodBank.PostDonor(PobjBBDonor);
        }

        [Route("api/BloodBank/GetDonor")]
        [HttpGet]
        public List<BBDonorRepository> GetDonor()
        {
            return objBloodBank.GetDonor();
        }

        [Route("api/BloodBank/GetDonor/{id}")]
        [HttpGet]
        public BBDonorRepository GetDonor(int id)
        {
            return objBloodBank.GetDonor(id);
        }

        [Route("api/BloodBank/PutDonor/{id}")]
        [HttpPost]
        public bool PutDonor(int id, BBDonorRepository PobjBBDonor)
        {
            return objBloodBank.PutDonor(id, PobjBBDonor);
        }

        #endregion

        #region PhysicalExam

        [Route("api/BloodBank/PostPhyExam")]
        [HttpPost]
        public BBPhysicalExaminationRepository PostPhyExam(BBPhysicalExaminationRepository Pobjphyex)
        {
            return objBloodBank.PostPhyExam(Pobjphyex);
        }

        [Route("api/BloodBank/PutPhyExam/{id}")]
        [HttpPost]
        public bool PutPhyExam(int id, BBPhysicalExaminationRepository Pobjphyex)
        {
            return objBloodBank.PutPhyExam(id, Pobjphyex);
        }

        [Route("api/BloodBank/GetPhyExam")]
        [HttpGet]
        public List<BBPhysicalExaminationRepository> GetPhyExam()
        {
            return objBloodBank.GetPhyExam();
        }

        [Route("api/BloodBank/GetPhyExam/{id}")]
        [HttpGet]
        public BBPhysicalExaminationRepository GetPhyExam(int id)
        {
            return objBloodBank.GetPhyExam(id);
        }
        #endregion

        #region refrigerator

        [Route("api/BloodBank/GetRefrigerator/{id}")]
        [HttpGet]
        public List<BBRefrigeratorRepository> GetRefrigerator(int? id)
        {
            return objBloodBank.GetRefrigerator(id);
        }

        [Route("api/BloodBank/PostRefrigerator")]
        [HttpPost]
        public BBRefrigeratorRepository PostRefrigerator(BBRefrigeratorRepository Pobjref)
        {
            return objBloodBank.PostRefrigerator(Pobjref);
        }

        [Route("api/BloodBank/PutRefrigerator/{id}")]
        [HttpPost]
        public bool PutRefrigerator(int id, BBRefrigeratorRepository Pobjref)
        {
            return objBloodBank.PutRefrigerator(id, Pobjref);
        }

        #endregion

        #region CellGrDetail
        [Route("api/Bloodbank/PostCellGrDetail")]
        [HttpPost]
        public BBGroupingCellRepository PostCellGrDetail(BBGroupingCellRepository PobjgrCell)
        {
            return objBloodBank.PostCellGrDetail(PobjgrCell);
        }

        [Route("api/Bloodbank/GetCellGroup/{id}")]
        [HttpGet]
        public BBGroupingCellRepository GetCellGroup(int id)
        {

            return objBloodBank.GetCellGroup(id);
        }
        [Route("api/Bloodbank/GetCellGroup")]
        [HttpGet]
        public List<BBGroupingCellRepository> GetCellGroup()
        {

            return objBloodBank.GetCellGroup();
        }

        [Route("api/Bloodbank/PutCellGrDetail/{id}")]
        [HttpPost]
        public bool PutCellGrDetail(int id, BBGroupingCellRepository PobjgrCell)
        {
            return objBloodBank.PutCellGrDetail(id, PobjgrCell);
        }

        #endregion

        #region BBserumGroup

        [Route("api/Bloodbank/PostBBSerumGroup")]
        [HttpPost]
        public BBGroupingSerumRepository PostBBSerumGroup(BBGroupingSerumRepository PobjgrSerum)
        {
            return objBloodBank.PostBBSerumGroup(PobjgrSerum);
        }

        [Route("api/Bloodbank/GetBBSerumGroup/{id}")]
        [HttpGet]
        public BBGroupingSerumRepository GetBBSerumGroup(int id)
        {
            return objBloodBank.GetBBSerumGroup(id);
        }

        [Route("api/Bloodbank/GetBBSerumGroup")]
        [HttpGet]
        public List<BBGroupingSerumRepository> GetBBSerumGroup()
        {
            return objBloodBank.GetBBSerumGroup();
        }

        [Route("api/Bloodbank/PutSerumGrDetail/{id}")]
        [HttpPost]
        public bool PutSerumGrDetail(int id, BBGroupingSerumRepository PobjgrSerum)
        {
            return objBloodBank.PutSerumGrDetail(id, PobjgrSerum);
        }
        #endregion

        #region Questionnaire

        [Route("api/Bloodbank/PostQuestionnaire")]
        [HttpPost]
        public BBQuestionnaireRepository PostQuestionnaire(BBQuestionnaireRepository PobjQue)
        {
            return objBloodBank.PostQuestionnaire(PobjQue);
        }

        [Route("api/Bloodbank/GetQuestionnaire")]
        [HttpGet]
        public List<BBQuestionnaireRepository> GetQuestionnaire()
        {
            return objBloodBank.GetBBQuestionnaire();
        }

        [Route("api/Bloodbank/GetBBQuestionnaire/{id}")]
        [HttpGet]
        public BBQuestionnaireRepository GetBBQuestionnaire(int id)
        {

            return objBloodBank.GetBBQuestionnaire(id);
        }




        [Route("api/Bloodbank/PutQuestionnaire/{id}")]
        [HttpPost]
        public bool PutQuestionnaire(int id, BBQuestionnaireRepository PobjQue)
        {
            return objBloodBank.PutQuestionnaire(id, PobjQue);
        }

        #endregion

        #region BloodRequest
        [Route("api/Bloodbank/PostTransfusionReq")]
        [HttpPost]
        public BBTransfusionRequestRepository PostTransfusionReq(BBTransfusionRequestRepository PobjTreq)
        {
            return objBloodBank.PostTransfusionReq(PobjTreq);
        }

        [Route("api/Bloodbank/GetTransfusionReq/{id}")]
        [HttpGet]
        public BBTransfusionRequestRepository GetTransfusionReq(int id)
        {
            return objBloodBank.GetTransfusionReq(id);
        }
        [Route("api/Bloodbank/GetTransfusionReq")]
        [HttpGet]
        public List<BBTransfusionRequestRepository> GetTransfusionReq()
        {
            return objBloodBank.GetTransfusionReq();
        }
        [Route("api/Bloodbank/PutTransfusionReq/{id}")]
        [HttpPost]
        public bool PutTransfusionReq(int id, BBTransfusionRequestRepository PobjTreq)
        {
            return objBloodBank.PutTransfusionReq(id, PobjTreq);
        }
        [Route("api/Bloodbank/DeleteBBTransfusionReq/{id}")]
        [HttpPost]
        public bool DeleteBBTransfusionReq(int id)
        {
            return objBloodBank.DeleteBBTransfusionReq(id);
        }

        #endregion

        #region BBSettings
        [Route("api/Bloodbank/PostBBSetting")]
        [HttpPost]
        public BBSettingRepository PostBBSetting(BBSettingRepository PobjBBSet)
        {
            return objBloodBank.PostBBSetting(PobjBBSet);
        }
        [Route("api/Bloodbank/GetBBSetting")]
        [HttpGet]
        public List<BBSettingRepository> GetBBSetting()
        {
            return objBloodBank.GetBBSetting();
        }
        [Route("api/Bloodbank/GetBBSetting/{id}")]
        [HttpGet]
        public BBSettingRepository GetBBSetting(int id)
        {
            return objBloodBank.GetBBSetting(id);
        }
        [Route("api/Bloodbank/PutBBSetting/{id}")]
        [HttpPost]
        public bool PutBBSetting(int id, BBSettingRepository PobjBBSet)
        {
            return objBloodBank.PutBBSetting(id, PobjBBSet);
        }

        [Route("api/Bloodbank/DeleteBBSetting/{id}")]
        [HttpPost]
        public bool DeleteBBSetting(int id)
        {
            return objBloodBank.DeleteBBSetting(id);
        }

        #endregion

        #region BBMandatoryTest

        [Route("api/BloodBank/GetBBMandatoryTest")]
        [HttpGet]
        public List<BBMandatoryTestrRepository> GetBBMandatoryTest()
        {
            return objBloodBank.GetBBMandatoryTest();
        }
        [Route("api/BloodBank/PostBBMandatoryTest")]
        [HttpPost]
        public BBMandatoryTestrRepository PostBBMandatoryTest(BBMandatoryTestrRepository PobjBBMandatoryTest)
        {
            return objBloodBank.PostBBMandatoryTest(PobjBBMandatoryTest);
        }
        [Route("api/BloodBank/PutBBMandatoryTest/{id}")]
        [HttpPost]
        public bool PutBBMandatoryTest(int id, BBMandatoryTestrRepository PobjBBMandatoryTest)
        {
            return objBloodBank.PutBBMandatoryTest(id, PobjBBMandatoryTest);
        }

        #endregion

        #region HaemotologyTest

        [Route("api/BloodBank/GetBBHaemotologyTest")]
        [HttpGet]
        public List<BBHaemotologyTestRepository> GetBBHaemotologyTest()
        {
            return objBloodBank.GetBBHaemotologyTest();
        }

        [Route("api/BloodBank/PostBBHaemotologyTest")]
        [HttpPost]
        public BBHaemotologyTestRepository PostBBHaemotologyTest(BBHaemotologyTestRepository PobjBBHaemotologyTest)
        {
            return objBloodBank.PostBBHaemotologyTest(PobjBBHaemotologyTest);
        }

        [Route("api/BloodBank/PutBBHaemotologyTest/{id}")]
        [HttpPost]
        public bool PutBBHaemotologyTest(int id, BBHaemotologyTestRepository PobjBBHaemotologyTest)
        {
            return objBloodBank.PutBBHaemotologyTest(id, PobjBBHaemotologyTest);
        }

        #endregion

        #region ScreeningBiology

        [Route("api/BloodBank/GetBBScreeningBiology")]
        [HttpGet]
        public List<BBScreeningBiologyRepository> GetBBScreeningBiology()
        {
            return objBloodBank.GetBBScreeningBiology();
        }

        [Route("api/BloodBank/PostBBScreeningBiology")]
        [HttpPost]
        public BBScreeningBiologyRepository PostBBScreeningBiology(BBScreeningBiologyRepository PobjBBScreeningBiology)
        {
            return objBloodBank.PostBBScreeningBiology(PobjBBScreeningBiology);
        }

        [Route("api/BloodBank/PutBBScreeningBiology/{id}")]
        [HttpPost]
        public bool PutBBScreeningBiology(int id, BBScreeningBiologyRepository PobjBBScreeningBiology)
        {
            return objBloodBank.PutBBScreeningBiology(id, PobjBBScreeningBiology);
        }

        #endregion

        #region Discard

        [Route("api/BloodBank/GetBBDiscard")]
        [HttpGet]
        public List<BBDiscardRepository> GetBBDiscard()
        {
            return objBloodBank.GetBBDiscard();
        }

        [Route("api/BloodBank/GetBBDiscardById/{id}")]
        [HttpGet]
        public BBDiscardRepository GetBBDiscard(int id)
        {
            return objBloodBank.GetBBDiscard(id);
        }
        [Route("api/BloodBank/PostBBDiscard")]
        [HttpPost]
        public BBDiscardRepository PostBBDiscard(BBDiscardRepository PobjBBDiscard)
        {
            return objBloodBank.PostBBDiscard(PobjBBDiscard);
        }

        [Route("api/BloodBank/PutBBDiscard/{id}")]
        [HttpPost]
        public bool PutBBDiscard(int id, BBDiscardRepository PobjBBDiscard)
        {
            return objBloodBank.PutBBDiscard(id, PobjBBDiscard);
        }
        [Route("api/BloodBank/DeleteBBDiscard/{id}")]
        [HttpPost]
        public bool PutBBDiscard(int id)
        {
            return objBloodBank.DeleteBBDiscard(id);
        }

        #endregion

        #region BLoodBank
        [Route("api/BloodBank/GetBloodBank")]
        [HttpGet]
        public List<BloodBankRepository> GetBloodBank()
        {
            return objBloodBank.GetBloodBank();

        }

        [Route("api/BloodBank/GetBloodBank/{id}")]
        [HttpGet]
        public BloodBankRepository GetBloodBank(int id)
        {
            return objBloodBank.GetBloodBank(id);

        }

        [Route("api/BloodBank/PostBloodBank")]
        [HttpPost]
        public BloodBankRepository PostBloodBank(BloodBankRepository PobjBB)
        {
            return objBloodBank.PostBloodBank(PobjBB);
        }
        [Route("api/BloodBank/PutBloodBank/{id}")]
        [HttpPost]
        public bool PutBloodBank(int id, BloodBankRepository PobjBB)
        {
            return objBloodBank.PutBloodBank(id, PobjBB);
        }

        [Route("api/BloodBank/DeleteBloodBank/{id}")]
        [HttpPost]
        public bool DeleteBloodBank(int id)
        {
            return objBloodBank.DeleteBloodBank(id);
        }
        #endregion

        #region BBDonation
        [Route("api/BloodBank/PostBBDonation")]
        [HttpPost]

        public BBDonationRepository PostBBDonation(BBDonationRepository PobjBBDonation)
        {
            return objBloodBank.PostBBDonation(PobjBBDonation);
        }

        [Route("api/BloodBank/GetBBDonation")]
        [HttpGet]
        public List<BBDonationRepository> GetBBDonation()
        {
            return objBloodBank.GetBBDonation();
        }
        [Route("api/BloodBank/GetBBDonation/{id}")]
        [HttpGet]
        public BBDonationRepository GetBBDonation(int id)
        {
            return objBloodBank.GetBBDonation(id);
        }
        [Route("api/BloodBank/PutBBDonation/{id}")]
        [HttpPost]
        public bool PutBBDonation(int id, [FromBody]BBDonationRepository LobjBBdonation)
        {
            return objBloodBank.PutBBDonation(id, LobjBBdonation);
        }

        [Route("api/BloodBank/DeleteBBDonation/{id}")]
        [HttpDelete]
        public bool DeleteBBDonation(int id)
        {
            return objBloodBank.DeleteBBDonation(id);
        }
        #endregion

        #region CampRegistration
        [Route("api/BloodBank/PostCampRegistration")]
        [HttpPost]

        public BBCampRegistrationRepository PostCampRegistration(BBCampRegistrationRepository PobjCampregistn)
        {
            return objBloodBank.PostCampRegistration(PobjCampregistn);
        }
        [Route("api/BloodBank/GetCampRegistration")]
        [HttpGet]
        public List<BBCampRegistrationRepository> GetCampRegistration()
        {
            return objBloodBank.GetCampRegistration();
        }
        [Route("api/BloodBank/GetCampRegistration/{id}")]
        [HttpGet]
        public BBCampRegistrationRepository GetCampRegistration(int id)
        {
            return objBloodBank.GetCampRegistration(id);
        }
        [Route("api/BloodBank/PutCampRegistration/{id}")]
        [HttpPost]
        public bool PutCampRegistration(int id, [FromBody]BBCampRegistrationRepository LobjCampRegistrn)
        {
            return objBloodBank.PutCampRegistration(id, LobjCampRegistrn);
        }

        [Route("api/BloodBank/DeleteCampRegistration/{id}")]
        [HttpDelete]
        public bool DeleteCampRegistration(int id)
        {
            return objBloodBank.DeleteCampRegistration(id);
        }
        #endregion

        #region  BBIssue
        [Route("api/BloodBank/GetBBIssue")]
        [HttpGet]
        public List<BBIssueRepository> GetBBIssue()
        {
            return objBloodBank.GetBBIssue();
        }

        [Route("api/BloodBank/GetBBIssueById/{id}")]
        [HttpGet]
        public BBIssueRepository GetBBIssue(int id)
        {
            return objBloodBank.GetBBIssue(id);
        }

        [Route("api/BloodBank/PostBBIssue")]
        [HttpPost]
        public BBIssueRepository PostBBIssue(BBIssueRepository PobjBBIssue)
        {
            return objBloodBank.PostBBIssue(PobjBBIssue);
        }
        [Route("api/BloodBank/PutBBIssue/{id}")]
        [HttpPost]
        public bool PutBBIssue(int id, BBIssueRepository PobjBBIssue)
        {
            return objBloodBank.PutBBIssue(id, PobjBBIssue);
        }
        [Route("api/BloodBank/DeleteBBIssue/{id}")]
        [HttpPost]
        public bool DeleteBBIssue(int id)
        {
            return objBloodBank.DeleteBBIssue(id);
        }
        #endregion

        [Route("api/Bloodbank/GetBloodMatching/{strbloodgroup},{isCrossMatch},{RequiredOn},{component}")]
         [HttpGet]
         public List<BBTransfusionRequestRepository> GetBloodMatching(string strbloodgroup, bool isCrossMatch, DateTime RequiredOn, string component)
         {
             return objBloodBank.GetBloodMatching(strbloodgroup, isCrossMatch, RequiredOn, component);

         }

        [Route("api/Bloodbank/PostTransReqConfirm")]
        [HttpPost]
        public BBTransfusionRequestRepository PostTransReqConfirm([FromBody]BBTransfusionRequestRepository PobjTreqConfirm)
        {
            return objBloodBank.PostTransReqConfirm(PobjTreqConfirm);
        }
    }
}