USE [HospitalTenant]
GO
/****** Object:  Schema [aarushiTenant]    Script Date: 09/30/2016 15:00:08 ******/
CREATE SCHEMA [aarushiTenant] AUTHORIZATION [dbo]
GO
/****** Object:  Table [dbo].[tblSpeciality]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSpeciality](
	[SpID] [int] IDENTITY(1,1) NOT NULL,
	[DrSettingsID] [int] NOT NULL,
	[SpName] [varchar](200) NOT NULL,
	[Description] [varchar](500) NULL,
	[Summary] [varchar](50) NULL,
	[EstFrom] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblSpecalization] PRIMARY KEY CLUSTERED 
(
	[SpID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblSpeciality] ON
INSERT [dbo].[tblSpeciality] ([SpID], [DrSettingsID], [SpName], [Description], [Summary], [EstFrom], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, 1, N'General Physician', NULL, NULL, NULL, 1, 1, CAST(0x0000A53600000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblSpeciality] ([SpID], [DrSettingsID], [SpName], [Description], [Summary], [EstFrom], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, 1, N'Physiotherapist', N'Bone Repair', N'jjjjjjj', CAST(0x0000A65700000000 AS DateTime), 1, 1, CAST(0x0000A657010E24FF AS DateTime), NULL, NULL)
INSERT [dbo].[tblSpeciality] ([SpID], [DrSettingsID], [SpName], [Description], [Summary], [EstFrom], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, 1, N'Cardiologist', N'Heart Related', N'jjjjjjj', CAST(0x0000A65700000000 AS DateTime), 1, 1, CAST(0x0000A657010FDE0D AS DateTime), NULL, NULL)
INSERT [dbo].[tblSpeciality] ([SpID], [DrSettingsID], [SpName], [Description], [Summary], [EstFrom], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (4, 1, N'Neurologist', N'NerveSystem', N'Nnnnn', CAST(0x0000A48D00000000 AS DateTime), 1, 1, CAST(0x0000A65701243438 AS DateTime), 1, CAST(0x0000A65800B96E20 AS DateTime))
INSERT [dbo].[tblSpeciality] ([SpID], [DrSettingsID], [SpName], [Description], [Summary], [EstFrom], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (5, 1, N'Dentist', N'Teeth', N'rrrrr', CAST(0x0000A48D00000000 AS DateTime), 1, 1, CAST(0x0000A65800B47CD1 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblSpeciality] OFF
/****** Object:  Table [dbo].[tblSpecializationField]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSpecializationField](
	[SpFieldID] [int] IDENTITY(1,1) NOT NULL,
	[SpID] [int] NULL,
	[SpFieldName] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblSpeclizationField] PRIMARY KEY CLUSTERED 
(
	[SpFieldID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblSpecializationField] ON
INSERT [dbo].[tblSpecializationField] ([SpFieldID], [SpID], [SpFieldName], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, 1, N'Obesity', 1, 1, CAST(0x0000A53600000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblSpecializationField] OFF
/****** Object:  Table [dbo].[tblSpecializationFieldValues]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSpecializationFieldValues](
	[SpFieldValueID] [int] IDENTITY(1,1) NOT NULL,
	[SpFieldID] [int] NOT NULL,
	[Value] [varchar](300) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblSpeclizationFieldValues] PRIMARY KEY CLUSTERED 
(
	[SpFieldValueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblSpecializationFieldValues] ON
INSERT [dbo].[tblSpecializationFieldValues] ([SpFieldValueID], [SpFieldID], [Value], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 2, N'Fat', 1, 1, CAST(0x0000A53600000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblSpecializationFieldValues] ([SpFieldValueID], [SpFieldID], [Value], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 2, N'Thin', 1, 1, CAST(0x0000A53600000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblSpecializationFieldValues] ([SpFieldValueID], [SpFieldID], [Value], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 2, N'Fit', 1, 1, CAST(0x0000A53600000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblSpecializationFieldValues] OFF
/****** Object:  Table [dbo].[tblSpecializationFieldLookUp]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSpecializationFieldLookUp](
	[SpFieldLookUpID] [int] NOT NULL,
	[SpID] [int] NOT NULL,
	[SpFieldID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAddress]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAddress](
	[AddressID] [int] IDENTITY(1,1) NOT NULL,
	[AddressLine1] [varchar](200) NOT NULL,
	[AddressLine2] [varchar](200) NULL,
	[City] [varchar](100) NOT NULL,
	[State] [varchar](100) NOT NULL,
	[Country] [varchar](100) NOT NULL,
	[ZipCode] [varchar](20) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblAddress] PRIMARY KEY CLUSTERED 
(
	[AddressID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblAddress] ON
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'hYD', N'Rangareddy', N'Balanagar', N'TP', N'India', N'12354', 1, 1, CAST(0x0000A5E300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'dfdg', N'bagss', N'pune', N'Telangana', N'India', N'523371', 1, 1, CAST(0x0000A6020105A5FD AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'89/B, KTR Park,', N'Krishnapur, sanathnagar', N'Hyderabad', N'Ap', N'India', NULL, 1, 1, CAST(0x0000A60A011A0CFD AS DateTime), 1, CAST(0x0000A60A01219757 AS DateTime))
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'89/B, KTR Park,', N'Krishna Nagar, Borabanda', N'1', N'ap', N'india', NULL, 1, 1, CAST(0x0000A60D0122307C AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, N'srt-157', NULL, N'Hyderabad', N'Telangana', N'India', NULL, 1, 1, CAST(0x0000A61901048B1F AS DateTime), 1, CAST(0x0000A61901055DED AS DateTime))
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, N'srt-156', NULL, N'vskp', N'ap', N'india', NULL, 1, 1, CAST(0x0000A61B00B42FF6 AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, N'srt-156', NULL, N'vskp', N'ap', N'india', NULL, 1, 1, CAST(0x0000A61B00BEB393 AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, N'srt-156', NULL, N'vskp', N'ap', N'india', NULL, 1, 1, CAST(0x0000A61B00C1553F AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, N'srt-156', NULL, N'vskp', N'ap', N'india', NULL, 1, 1, CAST(0x0000A61B00C38121 AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, N'89/B, KTR Park,', N'Krishna Nagar, Borabanda', N'1', N'ap', N'india', NULL, 1, 1, CAST(0x0000A61B011E9F0A AS DateTime), 2, CAST(0x0000A61B01308F1A AS DateTime))
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, N'89/B, KTR Park,', N'Krishna Nagar, Borabanda', N'gg', N'ap', N'india', NULL, 1, 0, CAST(0x0000A61B01280BCB AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (19, N'srt-156', NULL, N'vskp', N'ap', N'india', NULL, 1, 1, CAST(0x0000A61B014207DE AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, N'Sr nagar', N'Hyd', N'Hyderabad', N'Telangana', N'India', N'434343', 1, 1, CAST(0x0000A62800F3B763 AS DateTime), 2, CAST(0x0000A6280105AB7A AS DateTime))
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (22, N'srt-128', NULL, N'Hyderabad', N'Telangana', N'India', NULL, 1, 1, CAST(0x0000A63B0046F110 AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (27, N'KPHP', N'Hyd', N'Hyderabad', N'Telangana', N'India', N'434343', 1, 1, CAST(0x0000A64801142EA5 AS DateTime), 2, CAST(0x0000A64801248C77 AS DateTime))
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (28, N'sr nagar', N'hyd', N'hyderabad', N'Telangana', N'India', N'434343', 1, 1, CAST(0x0000A649011004C7 AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (29, N'Chennai', N'madiwala', N'chennai', N'tamilnadu', N'India', NULL, 1, 1, CAST(0x0000A64A01161D6B AS DateTime), NULL, NULL)
INSERT [dbo].[tblAddress] ([AddressID], [AddressLine1], [AddressLine2], [City], [State], [Country], [ZipCode], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (30, N'Chennai', N'sanathnagar', N'Hyderabad', N'Andhrapradesh', N'India', NULL, 1, 1, CAST(0x0000A64F00DA4F94 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblAddress] OFF
/****** Object:  Table [dbo].[tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPatient](
	[PatientId] [int] IDENTITY(1,1) NOT NULL,
	[HospitalBranchId] [int] NOT NULL,
	[Title] [varchar](50) NULL,
	[FirstName] [varchar](100) NOT NULL,
	[LastName] [varchar](50) NULL,
	[CallName] [varchar](50) NULL,
	[DOB] [date] NULL,
	[Age] [decimal](5, 2) NULL,
	[Gender] [varchar](50) NULL,
	[ContactNumber] [varchar](20) NULL,
	[GuardianName] [varchar](100) NULL,
	[GuardianContactNumber] [varchar](20) NULL,
	[GuardianRelation] [varchar](50) NULL,
	[PatientCode] [varchar](20) NULL,
	[Email] [varchar](100) NULL,
	[AadhaarNo] [varchar](50) NULL,
	[BloodGroup] [varchar](50) NULL,
	[MaritalStatus] [varchar](50) NULL,
	[Occupation] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime2](0) NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime2](0) NULL,
	[Comments] [varchar](1000) NULL,
	[AddressID] [int] NULL,
	[PatientType] [varchar](2) NULL,
 CONSTRAINT [PK_tblPatient] PRIMARY KEY CLUSTERED 
(
	[PatientId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_tblPatient_PatientCode] UNIQUE NONCLUSTERED 
(
	[PatientId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblPatient] ON
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (5, 1, N'MR', N'sreenu', N'gnata', NULL, CAST(0x173B0B00 AS Date), CAST(12.00 AS Decimal(5, 2)), N'Male', N'8522977322', N'sdf', N'12345', N'Son', NULL, N's@gmail.com', N'456', N'O+', N'23', N'sadf', 1, 1, CAST(0x0093F900223B0B0000 AS DateTime2), NULL, NULL, NULL, 1, N'OP')
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (6, 1, N'MR', N'shankar', N'ch', NULL, CAST(0x183B0B00 AS Date), CAST(22.00 AS Decimal(5, 2)), N'Male', N'9094380830', N'ss', N'234', N'Father', NULL, N'sh@gmail.com', N'12345', N'O-', N'23', N'se', 1, 1, CAST(0x00A2C800253B0B0000 AS DateTime2), NULL, NULL, NULL, 6, N'IP')
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (7, 1, N'MR', N'Rajesh', N'R', NULL, CAST(0x183B0B00 AS Date), CAST(22.00 AS Decimal(5, 2)), N'Male', N'984812345', N's', N'1234', N'Father', NULL, N's@g', N'f', N'O-', N'23', N'sdf', 1, 1, CAST(0x008DCA00253B0B0000 AS DateTime2), NULL, NULL, NULL, 7, N'OP')
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (8, 1, N'MR', N'Bharath', N'w', NULL, CAST(0x1A3B0B00 AS Date), CAST(17.00 AS Decimal(5, 2)), N'Male', N'23', N'sd', N'234567', N'Father', NULL, N'w@gmail.com', N'234', N'O-', N'23', N'd', 1, 1, CAST(0x0074D000253B0B0000 AS DateTime2), NULL, NULL, NULL, 1, N'OP')
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (9, 1, N'MR', N'sravani', N's', NULL, CAST(0x1F3B0B00 AS Date), CAST(21.00 AS Decimal(5, 2)), N'Male', N'23456', N'gg', N'666', N'0', NULL, N'p@gmail.com', N'123', N'AB+', N'68', N'ww', 1, 1, CAST(0x003CD300253B0B0000 AS DateTime2), NULL, NULL, NULL, NULL, N'OP')
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (10, 1, N'MR', N'raju', N'R', NULL, CAST(0x663B0B00 AS Date), CAST(30.00 AS Decimal(5, 2)), N'Male', NULL, NULL, NULL, N'Mother', NULL, NULL, NULL, N'0', N'2', N'farmer', 1, 1, CAST(0x00EDD400663B0B0000 AS DateTime2), NULL, NULL, NULL, 4, NULL)
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (11, 1, N'Mrs', N'rani', N'R', NULL, CAST(0x663B0B00 AS Date), CAST(30.00 AS Decimal(5, 2)), N'Male', NULL, NULL, NULL, N'Mother', NULL, NULL, NULL, N'0', N'2', N'farmer', 1, 1, CAST(0x00D6D600663B0B0000 AS DateTime2), NULL, NULL, NULL, 4, NULL)
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (12, 1, N'MR', N'shankar', N'sankararo', NULL, NULL, NULL, N'Male', NULL, NULL, NULL, N'Son', NULL, NULL, NULL, N'0', N'7', NULL, 1, 1, CAST(0x00EEB000683B0B0000 AS DateTime2), NULL, NULL, NULL, 1, NULL)
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (14, 1, N'MR', N'vani', N'rew4ew', NULL, CAST(0x5E160B00 AS Date), CAST(25.00 AS Decimal(5, 2)), N'Male', N'9090903234', NULL, NULL, N'Son', NULL, N'sss@gmail.com', N'345678', N'A-', N'23', N'farmer', 1, 1, CAST(0x000000006A3B0B0000 AS DateTime2), NULL, CAST(0x00614F01693B0B0000 AS DateTime2), NULL, 4, NULL)
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (20, 1, N'MR', N'anvesh', N'ani', NULL, NULL, CAST(25.00 AS Decimal(5, 2)), N'Male', N'984812348', NULL, NULL, NULL, NULL, N'anvesh1234@gmail.com', NULL, N'O+', NULL, NULL, 1, 1, CAST(0x009808016B3B0B0000 AS DateTime2), NULL, NULL, NULL, 1, N'OP')
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (26, 1, NULL, N'shankar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x00C39900763B0B0000 AS DateTime2), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (27, 1, NULL, N'shankar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x00BDA200763B0B0000 AS DateTime2), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (28, 1, NULL, N'shankar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x00FBA400763B0B0000 AS DateTime2), NULL, NULL, NULL, 15, NULL)
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (29, 1, NULL, N'shankar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x00D5A600763B0B0000 AS DateTime2), NULL, NULL, NULL, 16, NULL)
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (30, 1, NULL, N'shankar', N'shanku', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x00CD1201763B0B0000 AS DateTime2), 2, NULL, NULL, 19, NULL)
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (32, 1, NULL, N'narendra', N'sani', NULL, NULL, CAST(30.00 AS Decimal(5, 2)), N'male', N'9992221467', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x00A0F700783B0B0000 AS DateTime2), NULL, NULL, NULL, NULL, N'OP')
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (34, 1, NULL, N'jhanu', N'jaga', NULL, NULL, CAST(25.00 AS Decimal(5, 2)), NULL, N'9845678321', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x00553E00963B0B0000 AS DateTime2), NULL, NULL, NULL, NULL, N'OP')
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (35, 1, NULL, N'nari', N'syamala', NULL, NULL, CAST(25.00 AS Decimal(5, 2)), NULL, N'9845678321', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0032B500A23B0B0000 AS DateTime2), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tblPatient] ([PatientId], [HospitalBranchId], [Title], [FirstName], [LastName], [CallName], [DOB], [Age], [Gender], [ContactNumber], [GuardianName], [GuardianContactNumber], [GuardianRelation], [PatientCode], [Email], [AadhaarNo], [BloodGroup], [MaritalStatus], [Occupation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [AddressID], [PatientType]) VALUES (36, 1, N'mr', N'sreenu', NULL, NULL, NULL, CAST(12.00 AS Decimal(5, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x000BB000C73B0B0000 AS DateTime2), NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblPatient] OFF
/****** Object:  Table [dbo].[tblAssessment]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAssessment](
	[AssessmentID] [int] IDENTITY(1,1) NOT NULL,
	[PatientID] [int] NOT NULL,
	[SpecialityHistoryID] [int] NOT NULL,
	[pcpname] [varchar](50) NULL,
	[visitRecommedBy] [varchar](50) NULL,
	[mainSymptom] [varchar](50) NULL,
	[Height] [decimal](3, 2) NULL,
	[Weight] [decimal](5, 2) NULL,
	[IsMedication] [bit] NULL,
	[IsPrgenant] [bit] NULL,
	[Others] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblAssessment] PRIMARY KEY CLUSTERED 
(
	[AssessmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblAssessmentSurgical]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAssessmentSurgical](
	[AsseSurgicalId] [int] IDENTITY(1,1) NOT NULL,
	[AssessmentID] [int] NOT NULL,
	[SurgicalName] [varchar](50) NULL,
	[Answer] [varchar](50) NULL,
	[Comments] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblAssessmentSurgical] PRIMARY KEY CLUSTERED 
(
	[AsseSurgicalId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblAssessmentSpecialityHistory]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAssessmentSpecialityHistory](
	[AsseSpeciHisId] [int] IDENTITY(1,1) NOT NULL,
	[AssessmentID] [int] NOT NULL,
	[SpecilitySectioName] [varchar](50) NULL,
	[Question] [varchar](50) NULL,
	[Answer] [varchar](50) NULL,
	[Remarks] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblAssessmentSpecialityHistory] PRIMARY KEY CLUSTERED 
(
	[AsseSpeciHisId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblAssessmentSocial]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAssessmentSocial](
	[AsseSocialID] [int] IDENTITY(1,1) NOT NULL,
	[AssessmentID] [int] NOT NULL,
	[Question] [varchar](100) NULL,
	[Answer] [varchar](50) NULL,
	[Comments] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblAssessmentSocial] PRIMARY KEY CLUSTERED 
(
	[AsseSocialID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblAssessmentOthers]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAssessmentOthers](
	[AsseOtherId] [int] IDENTITY(1,1) NOT NULL,
	[AssessmentID] [int] NOT NULL,
	[Question] [varchar](100) NULL,
	[Answer] [varchar](50) NULL,
	[Comments] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblAssessmentOthers] PRIMARY KEY CLUSTERED 
(
	[AsseOtherId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblAssessmentFamily]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAssessmentFamily](
	[AsseFamilyId] [int] IDENTITY(1,1) NOT NULL,
	[AssessmentID] [int] NOT NULL,
	[Question] [varchar](50) NULL,
	[Answer] [varchar](50) NULL,
	[Comments] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblAssessmentFamily] PRIMARY KEY CLUSTERED 
(
	[AsseFamilyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblAsseMedications]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAsseMedications](
	[MediCode] [int] IDENTITY(1,1) NOT NULL,
	[MediName] [varchar](50) NULL,
	[MediDesicrption] [varchar](200) NULL,
	[Comments] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblSpMedications] PRIMARY KEY CLUSTERED 
(
	[MediCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblAsseMedications] ON
INSERT [dbo].[tblAsseMedications] ([MediCode], [MediName], [MediDesicrption], [Comments], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'Antibiotics', N'A drug used to treat bacterial infections', N' external or internal infections', 1, 1, CAST(0x0000A65C010D6768 AS DateTime), NULL, NULL)
INSERT [dbo].[tblAsseMedications] ([MediCode], [MediName], [MediDesicrption], [Comments], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'Codeine', N'Codeine is a narcotic analgesic, prescribed for pain and cough.', N' pain and cough ', 1, 1, CAST(0x0000A65C011B1B34 AS DateTime), 1, CAST(0x0000A65C0140165F AS DateTime))
INSERT [dbo].[tblAsseMedications] ([MediCode], [MediName], [MediDesicrption], [Comments], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, N'Benzonatate', N'A drug used to treat bacterial infections', N' depressing the cough ', 1, 1, CAST(0x0000A65C012A8147 AS DateTime), NULL, NULL)
INSERT [dbo].[tblAsseMedications] ([MediCode], [MediName], [MediDesicrption], [Comments], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (4, N'PAND', N'prescribed for Gastritis.', N' pain in stomach ', 1, 1, CAST(0x0000A65C012B74D0 AS DateTime), 1, CAST(0x0000A65C013311C9 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblAsseMedications] OFF
/****** Object:  Table [dbo].[tblAssessmentMedication]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAssessmentMedication](
	[AsseMedicalId] [int] IDENTITY(1,1) NOT NULL,
	[AssessmentID] [int] NOT NULL,
	[Medicode] [int] NOT NULL,
	[Reaction] [varchar](50) NULL,
	[Comments] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblAssessmentMedication] PRIMARY KEY CLUSTERED 
(
	[AsseMedicalId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblAsseFood]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAsseFood](
	[FoodCode] [int] IDENTITY(1,1) NOT NULL,
	[FoodName] [varchar](50) NULL,
	[FoodDescription] [varchar](200) NULL,
	[OtherDetials] [varchar](200) NULL,
	[Commets] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblSpFood] PRIMARY KEY CLUSTERED 
(
	[FoodCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblAsseFood] ON
INSERT [dbo].[tblAsseFood] ([FoodCode], [FoodName], [FoodDescription], [OtherDetials], [Commets], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'banana', N'taking one daily', N'sgrtgr', NULL, 1, 1, CAST(0x0000A65C010993E6 AS DateTime), NULL, NULL)
INSERT [dbo].[tblAsseFood] ([FoodCode], [FoodName], [FoodDescription], [OtherDetials], [Commets], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'apple', N'daily', N'dfghj', NULL, 1, 1, CAST(0x0000A65C0109CE93 AS DateTime), 2, CAST(0x0000A65C01239FDB AS DateTime))
SET IDENTITY_INSERT [dbo].[tblAsseFood] OFF
/****** Object:  Table [dbo].[tblAssessmentAllergies]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAssessmentAllergies](
	[AsseAllergyId] [int] IDENTITY(1,1) NOT NULL,
	[AssessmentID] [int] NOT NULL,
	[Foodcode] [int] NOT NULL,
	[Reaction] [varchar](50) NULL,
	[Comments] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModofoedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblAssessmentAllergies] PRIMARY KEY CLUSTERED 
(
	[AsseAllergyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblAsseDisorder]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAsseDisorder](
	[DisorderCode] [int] IDENTITY(1,1) NOT NULL,
	[DisorderName] [varchar](50) NULL,
	[DisorderDesc] [varchar](200) NULL,
	[Comments] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblSpDisorder] PRIMARY KEY CLUSTERED 
(
	[DisorderCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblAsseDisorder] ON
INSERT [dbo].[tblAsseDisorder] ([DisorderCode], [DisorderName], [DisorderDesc], [Comments], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, NULL, N'its very dangiours ill', NULL, 1, 1, CAST(0x0000A65C013C1F6C AS DateTime), 2, CAST(0x0000A65C013DEF0C AS DateTime))
SET IDENTITY_INSERT [dbo].[tblAsseDisorder] OFF
/****** Object:  Table [dbo].[tblAssessmentGenMedical]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAssessmentGenMedical](
	[AsseGenMId] [int] IDENTITY(1,1) NOT NULL,
	[AssessmentID] [int] NOT NULL,
	[DisorderCode] [int] NOT NULL,
	[Answer] [varchar](50) NULL,
	[Comments] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblAssessmentGenMedical] PRIMARY KEY CLUSTERED 
(
	[AsseGenMId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblBBRefrigerators]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBRefrigerators](
	[Refrigeratorid] [int] IDENTITY(1,1) NOT NULL,
	[RefrigratorName] [varchar](50) NULL,
	[ProductSize] [int] NULL,
	[Capacity] [int] NULL,
	[NoofRacks] [int] NULL,
	[Totalpackets] [float] NULL,
	[ElectricalReq] [varchar](50) NULL,
	[Price] [float] NULL,
	[Purchasedon] [datetime] NULL,
	[WarrantyTilldate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblBBRefrigerators] PRIMARY KEY CLUSTERED 
(
	[Refrigeratorid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBRefrigerators] ON
INSERT [dbo].[tblBBRefrigerators] ([Refrigeratorid], [RefrigratorName], [ProductSize], [Capacity], [NoofRacks], [Totalpackets], [ElectricalReq], [Price], [Purchasedon], [WarrantyTilldate], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'BplLarge', 50, 30, 200, 2, NULL, 500, CAST(0x0000A6E700000000 AS DateTime), CAST(0x0000AC8F00000000 AS DateTime), 1, 1, CAST(0x0000A64A00000000 AS DateTime), 1, CAST(0x0000A6520008E6F0 AS DateTime))
INSERT [dbo].[tblBBRefrigerators] ([Refrigeratorid], [RefrigratorName], [ProductSize], [Capacity], [NoofRacks], [Totalpackets], [ElectricalReq], [Price], [Purchasedon], [WarrantyTilldate], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'LG Large', 50, 30, 200, 2, NULL, 500, CAST(0x0000A6E700000000 AS DateTime), CAST(0x0000AC8F00000000 AS DateTime), 1, 1, CAST(0x0000A64B011CBE50 AS DateTime), 1, CAST(0x0000A64B01394AC9 AS DateTime))
INSERT [dbo].[tblBBRefrigerators] ([Refrigeratorid], [RefrigratorName], [ProductSize], [Capacity], [NoofRacks], [Totalpackets], [ElectricalReq], [Price], [Purchasedon], [WarrantyTilldate], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'kdid', 50, 30, 20, 2, NULL, 500, CAST(0x0000A6E700000000 AS DateTime), CAST(0x0000AC8F00000000 AS DateTime), 1, 1, CAST(0x0000A652000417A3 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblBBRefrigerators] OFF
/****** Object:  Table [dbo].[tblChemicalPurchase]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblChemicalPurchase](
	[ChemicalID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[Code] [varchar](100) NULL,
	[ManufacturedBy] [varchar](100) NULL,
	[ManufacturedDate] [datetime] NULL,
	[Expirydate] [datetime] NOT NULL,
	[PurchaseCostPerQuantity] [money] NULL,
	[Quantity] [int] NULL,
	[SalesCostPerQuantity] [money] NULL,
	[TotalCost] [money] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[Comments] [varchar](1000) NULL,
	[BranchID] [int] NULL,
 CONSTRAINT [PK_ChemicalPurchase] PRIMARY KEY CLUSTERED 
(
	[ChemicalID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblCCAllocate]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCCAllocate](
	[CC_AllocatedId] [int] IDENTITY(1,1) NOT NULL,
	[BedId] [int] NOT NULL,
	[StDatetime] [datetime] NOT NULL,
	[PtId] [int] NOT NULL,
	[PtName] [varchar](50) NOT NULL,
	[TenDatetime] [datetime] NULL,
	[EndDatetime] [datetime] NULL,
	[TrStatus] [varchar](20) NOT NULL,
	[Status] [varchar](20) NULL,
 CONSTRAINT [PK_tblCCAllocate] PRIMARY KEY CLUSTERED 
(
	[CC_AllocatedId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblCareInstruction]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCareInstruction](
	[CareInstructionID] [int] IDENTITY(1,1) NOT NULL,
	[TreatmentID] [int] NULL,
	[Instruction] [varchar](50) NULL,
	[InstructionType] [varchar](50) NULL,
	[DoctorID] [int] NULL,
	[StartDate] [datetime] NULL,
	[Duration] [varchar](50) NULL,
	[EndDate] [varchar](50) NULL,
	[Frequency] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblCareInstruction] PRIMARY KEY CLUSTERED 
(
	[CareInstructionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblDepartments]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDepartments](
	[DeptID] [int] IDENTITY(1,1) NOT NULL,
	[DeptName] [varchar](100) NOT NULL,
	[DeptHead] [int] NULL,
	[IsActive] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](250) NULL,
 CONSTRAINT [PK_tblDepartments] PRIMARY KEY CLUSTERED 
(
	[DeptID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblDepartments] ON
INSERT [dbo].[tblDepartments] ([DeptID], [DeptName], [DeptHead], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (1, N'Cardiac Surgery', NULL, 1, 1, CAST(0x0000A5DC00000000 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblDepartments] ([DeptID], [DeptName], [DeptHead], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (2, N' Cardiology', NULL, 1, 1, CAST(0x0000A5DE00000000 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblDepartments] ([DeptID], [DeptName], [DeptHead], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (3, N'Diabetology', NULL, 1, 1, CAST(0x0000A5DE00000000 AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblDepartments] OFF
/****** Object:  Table [dbo].[tblBBCampRegistration]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBCampRegistration](
	[CampID] [int] IDENTITY(1,1) NOT NULL,
	[BBCode] [varchar](50) NULL,
	[District] [varchar](50) NULL,
	[Mandal] [varchar](50) NULL,
	[CityorVillage] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
	[CampName] [varchar](50) NULL,
	[DateOfCamp] [varchar](50) NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[Incharge] [varchar](50) NULL,
	[InchargeCtNo] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblBBCampRegistration] PRIMARY KEY CLUSTERED 
(
	[CampID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBCampRegistration] ON
INSERT [dbo].[tblBBCampRegistration] ([CampID], [BBCode], [District], [Mandal], [CityorVillage], [Location], [CampName], [DateOfCamp], [StartDateTime], [EndDateTime], [Incharge], [InchargeCtNo], [IsActive], [CreatedOn], [CreatedBy], [ModifiedBy], [ModifiedDate]) VALUES (1, N'31223', N'prakasam', N'markapur', N'goguladinne', N'markapur', N'hjkjkj', N'2016-07-23', CAST(0x0000A64A00000000 AS DateTime), CAST(0x0000A65F00000000 AS DateTime), N'venkat', N'9090380830', 1, CAST(0x0000A64C00000000 AS DateTime), 1, NULL, NULL)
INSERT [dbo].[tblBBCampRegistration] ([CampID], [BBCode], [District], [Mandal], [CityorVillage], [Location], [CampName], [DateOfCamp], [StartDateTime], [EndDateTime], [Incharge], [InchargeCtNo], [IsActive], [CreatedOn], [CreatedBy], [ModifiedBy], [ModifiedDate]) VALUES (2, N'143', N'Rangareddy', N'RR', N'Chandanagar', N'Opp-Rsbrothers', N'Help', N'2016-07-26', CAST(0x0000A64F00000000 AS DateTime), CAST(0x0000A64F00000000 AS DateTime), N'ss', N'93939393', 0, CAST(0x0000A64F012D0C16 AS DateTime), 1, 2, CAST(0x0000A64F012FF66B AS DateTime))
SET IDENTITY_INSERT [dbo].[tblBBCampRegistration] OFF
/****** Object:  Table [dbo].[tblEquipment]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEquipment](
	[EquipmentID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[Code] [varchar](100) NULL,
	[ManufacturedBy] [varchar](200) NULL,
	[ManufacturedDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_tblEquipment] PRIMARY KEY CLUSTERED 
(
	[EquipmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblEquipment] ON
INSERT [dbo].[tblEquipment] ([EquipmentID], [Name], [Code], [ManufacturedBy], [ManufacturedDate], [ExpireDate], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy], [IsActive], [Comments]) VALUES (1, N'sanku', N'12354', NULL, NULL, NULL, CAST(0x0000A65F01109CDC AS DateTime), 1, CAST(0x0000A65F01174151 AS DateTime), 3, 0, NULL)
INSERT [dbo].[tblEquipment] ([EquipmentID], [Name], [Code], [ManufacturedBy], [ManufacturedDate], [ExpireDate], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy], [IsActive], [Comments]) VALUES (2, N'sank', N'123', NULL, NULL, CAST(0x0000A6DA00000000 AS DateTime), CAST(0x0000A65F0116C033 AS DateTime), 1, NULL, NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[tblEquipment] OFF
/****** Object:  Table [dbo].[tblDiagnosis]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDiagnosis](
	[DiagnosisID] [int] IDENTITY(1,1) NOT NULL,
	[DiagDepartmentName] [varchar](50) NOT NULL,
	[DiagName] [varchar](100) NOT NULL,
	[Comments] [varchar](1000) NULL,
	[Amount] [decimal](10, 2) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblDiagnosis] PRIMARY KEY CLUSTERED 
(
	[DiagnosisID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblDiagnosis] ON
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Diabetology', N'RBS', NULL, CAST(300.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A5010125AF6D AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Diabetology', N'URINE', NULL, CAST(400.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A5010125D14E AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'Diabetology', N'TYROID CAPSULE', NULL, CAST(500.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A50101260DAE AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'Cardiac Surgery', N'ESR', NULL, CAST(350.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A5010126429A AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'Cardiac Surgery', N'LFT', NULL, CAST(400.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A50101265E4F AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, N' Cardiology', N'HIV', NULL, CAST(300.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A5010126EFB0 AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, N' Cardiology', N'NASAL SCERATIONS', NULL, CAST(500.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A50101271EBD AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, N'Cardiac Surgery', N'Complete Blood Picture11', NULL, CAST(400.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0102DAC0 AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, N' Cardiology', N'HIV I and 2', NULL, CAST(480.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B010CF469 AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, N'Diabetology', N'Bleeding Time and Clotting Time', NULL, CAST(500.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B010D3474 AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, N'Cardiac Surgery', N'Renal function test', NULL, CAST(450.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01165A38 AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, N' Cardiology', N'HBS AG Australian Antigen', NULL, CAST(300.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01274443 AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, N'Diabetology', N'Gram Stan', NULL, CAST(450.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0128594B AS DateTime))
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, N'Cardiology', N'OPR', NULL, CAST(500.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A61F00D0215F AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagnosis] ([DiagnosisID], [DiagDepartmentName], [DiagName], [Comments], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, N'Brain', N'OMR Scan', NULL, CAST(500.00 AS Decimal(10, 2)), 1, 1, CAST(0x0000A647011A6E8C AS DateTime), 1, CAST(0x0000A647011C2EC2 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblDiagnosis] OFF
/****** Object:  Table [dbo].[tblLabTest]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLabTest](
	[LabTestID] [int] IDENTITY(1,1) NOT NULL,
	[Invs_Name] [varchar](50) NULL,
	[Invs_Code] [int] NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[ReferAmount] [decimal](18, 2) NOT NULL,
	[Category] [varchar](50) NULL,
	[Description] [varchar](50) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_dbo.tblLabTest] PRIMARY KEY CLUSTERED 
(
	[LabTestID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblLabTest] ON
INSERT [dbo].[tblLabTest] ([LabTestID], [Invs_Name], [Invs_Code], [Price], [ReferAmount], [Category], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'cardiology', 222, CAST(3000.00 AS Decimal(18, 2)), CAST(400.00 AS Decimal(18, 2)), N'XRY', NULL, 1, CAST(0x0000A63200000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLabTest] ([LabTestID], [Invs_Name], [Invs_Code], [Price], [ReferAmount], [Category], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Radiography', 240, CAST(330.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), N'XRY', NULL, 1, CAST(0x0000A61300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLabTest] ([LabTestID], [Invs_Name], [Invs_Code], [Price], [ReferAmount], [Category], [Description], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'Cytology', 300, CAST(450.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), N'XRY', NULL, 1, CAST(0x0000A61300EE54AD AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblLabTest] OFF
/****** Object:  Table [dbo].[tblHospitalSettings]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblHospitalSettings](
	[HospitalID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Value] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblHospitalBranchDetails]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblHospitalBranchDetails](
	[HospitalBranchID] [int] IDENTITY(1,1) NOT NULL,
	[HospitalID] [int] NOT NULL,
	[BranchName] [varchar](100) NOT NULL,
	[AddressID] [int] NULL,
	[PriContactNumber] [varchar](20) NULL,
	[SecContactNumber2] [varchar](20) NULL,
	[PriContactPerson] [varchar](100) NULL,
	[PriContactPersonNumber] [varchar](20) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](200) NULL,
 CONSTRAINT [PK_tblHospitalBranchDetails] PRIMARY KEY CLUSTERED 
(
	[HospitalBranchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblHospitalBranchDetails] ON
INSERT [dbo].[tblHospitalBranchDetails] ([HospitalBranchID], [HospitalID], [BranchName], [AddressID], [PriContactNumber], [SecContactNumber2], [PriContactPerson], [PriContactPersonNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (1, 4, N'kukatpally branch', 1, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A5C000000000 AS DateTime), NULL, CAST(0x0000A5C0013CFF53 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tblHospitalBranchDetails] OFF
/****** Object:  Table [dbo].[tblLocation]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLocation](
	[LID] [int] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](100) NULL,
	[LOCATIONTYPE] [int] NULL,
	[PARENTID] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK__tblLocat__C6555721C8B8DBB5] PRIMARY KEY CLUSTERED 
(
	[LID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblLocation] ON
INSERT [dbo].[tblLocation] ([LID], [NAME], [LOCATIONTYPE], [PARENTID], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'India', 81, 0, 1, 2, CAST(0x0000A5C600000000 AS DateTime), NULL, CAST(0x0000A5C600CC5783 AS DateTime))
INSERT [dbo].[tblLocation] ([LID], [NAME], [LOCATIONTYPE], [PARENTID], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'Andhra Pradesh', 82, 1, 1, 2, CAST(0x0000A5C600000000 AS DateTime), NULL, CAST(0x0000A5C600CC801E AS DateTime))
INSERT [dbo].[tblLocation] ([LID], [NAME], [LOCATIONTYPE], [PARENTID], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, N'Hyderabad', 83, 2, 1, 2, CAST(0x0000A5C600000000 AS DateTime), NULL, CAST(0x0000A5C600CCAD94 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblLocation] OFF
/****** Object:  Table [dbo].[tblLeavesCredit]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLeavesCredit](
	[EID] [int] NOT NULL,
	[Month] [int] NULL,
	[Year] [int] NULL,
	[LeavesCredit] [float] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_tblLeavesCredit] PRIMARY KEY CLUSTERED 
(
	[EID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblManufacturers]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblManufacturers](
	[MfgID] [int] IDENTITY(1,1) NOT NULL,
	[MfgName] [varchar](100) NOT NULL,
	[AgentName] [varchar](50) NULL,
	[AgentContactNumber] [varchar](20) NULL,
	[AddressLine1] [varchar](250) NULL,
	[AddressLine2] [varchar](250) NULL,
	[City] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Location] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblCompany] PRIMARY KEY CLUSTERED 
(
	[MfgID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblManufacturers] ON
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (1, N'GLAXO', N'dgd', N'1023456789', NULL, NULL, N'Hyderabad', 1, 0, 1, CAST(0x0000A56700CE5A44 AS DateTime), 2, CAST(0x0000A60400E4FBE4 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (2, N'NICHOLAS', N'hifff', N'5678912345', NULL, NULL, N'Pune', 1, 0, 1, CAST(0x0000A56700CE9688 AS DateTime), 2, CAST(0x0000A6040167A9B0 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (3, N'BIOLOGICAL LTD', NULL, NULL, NULL, NULL, N'Delhi', 1, 0, 1, CAST(0x0000A56700CE969A AS DateTime), NULL, CAST(0x0000A56700CE969A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (4, N'BOEHRINGS', NULL, NULL, NULL, NULL, N'Kailam', 1, 0, 1, CAST(0x0000A56700CE96A9 AS DateTime), NULL, CAST(0x0000A56700CE96A9 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (5, N'BAYER', NULL, NULL, NULL, NULL, N'Vzm', 1, 0, 1, CAST(0x0000A56700CE96BD AS DateTime), NULL, CAST(0x0000A56700CE96BD AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (6, N'HIMALAYA', NULL, NULL, NULL, NULL, N'Vsp', 1, 0, 1, CAST(0x0000A56700CE96DE AS DateTime), NULL, CAST(0x0000A56700CE96DE AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (7, N'Cipla', NULL, NULL, NULL, NULL, N'Rangareddy', 1, 0, 1, CAST(0x0000A56700CE96ED AS DateTime), NULL, CAST(0x0000A56700CE96ED AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (8, N'ABBOTT', NULL, NULL, NULL, NULL, N'Santhnagar', 1, 0, 1, CAST(0x0000A56700CE9708 AS DateTime), NULL, CAST(0x0000A56700CE9708 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (9, N'AJANTA PHARMA ', NULL, NULL, NULL, NULL, N'Erragadda', 1, 1, 1, CAST(0x0000A56700CE9739 AS DateTime), NULL, CAST(0x0000A56700CE9739 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (10, N'BLUE CROSS', NULL, NULL, NULL, NULL, N'Esi', 1, 0, 1, CAST(0x0000A56700CE976A AS DateTime), NULL, CAST(0x0000A56700CE976A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (11, N'RANBAXY', NULL, NULL, NULL, NULL, N'SrNagar', 1, 0, 1, CAST(0x0000A56700CE9791 AS DateTime), NULL, CAST(0x0000A56700CE9791 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (12, N'UNICHEM', NULL, NULL, NULL, NULL, N'Ameerpet', 1, 0, 1, CAST(0x0000A56700CE979D AS DateTime), NULL, CAST(0x0000A56700CE979D AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (13, N'SANOFI INDIA LIMITED', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE97BD AS DateTime), NULL, CAST(0x0000A56700CE97BD AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (14, N'TORRENT', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE97DC AS DateTime), NULL, CAST(0x0000A56700CE97DC AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (15, N'MERCK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9823 AS DateTime), NULL, CAST(0x0000A56700CE9823 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (16, N'ALEMBIC', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE99A0 AS DateTime), NULL, CAST(0x0000A56700CE99A0 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (17, N'MANKIND', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE99B2 AS DateTime), NULL, CAST(0x0000A56700CE99B2 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (18, N'Lupin', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE99F4 AS DateTime), NULL, CAST(0x0000A56700CE99F4 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (19, N'Sun Pharma', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9A21 AS DateTime), NULL, CAST(0x0000A56700CE9A21 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (20, N'LUNDBECK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9A99 AS DateTime), NULL, CAST(0x0000A56700CE9A99 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (21, N'ARISTO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9B08 AS DateTime), NULL, CAST(0x0000A56700CE9B08 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (22, N'REDDY', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9B19 AS DateTime), NULL, CAST(0x0000A56700CE9B19 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (23, N'WALLACE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9B25 AS DateTime), NULL, CAST(0x0000A56700CE9B25 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (24, N'CIPLA GENERIC ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9B73 AS DateTime), NULL, CAST(0x0000A56700CE9B73 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (25, N'PANACEA BIOTEC', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9B86 AS DateTime), NULL, CAST(0x0000A56700CE9B86 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (26, N'SUN ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9BA7 AS DateTime), NULL, CAST(0x0000A56700CE9BA7 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (27, N'ZYDUS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9C01 AS DateTime), NULL, CAST(0x0000A56700CE9C01 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (28, N'INTAS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9C67 AS DateTime), NULL, CAST(0x0000A56700CE9C67 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (29, N'SARABHAI', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9CC4 AS DateTime), NULL, CAST(0x0000A56700CE9CC4 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (30, N'GERMAN REMIDIES', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9CEB AS DateTime), NULL, CAST(0x0000A56700CE9CEB AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (31, N'SVIZERA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9CFA AS DateTime), NULL, CAST(0x0000A56700CE9CFA AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (32, N'SERDIA ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9D10 AS DateTime), NULL, CAST(0x0000A56700CE9D10 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (33, N'PFIZER', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9D51 AS DateTime), NULL, CAST(0x0000A56700CE9D51 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (34, N'MICROLAB', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9D5F AS DateTime), NULL, CAST(0x0000A56700CE9D5F AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (35, N'GLENMARCK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9DB6 AS DateTime), NULL, CAST(0x0000A56700CE9DB6 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (36, N'WYETH LTD', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9DEC AS DateTime), NULL, CAST(0x0000A56700CE9DEC AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (37, N'SVIZERA HEALTHCARE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9DFD AS DateTime), NULL, CAST(0x0000A56700CE9DFD AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (38, N'MICRO ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9E28 AS DateTime), NULL, CAST(0x0000A56700CE9E28 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (39, N'BILOGICAL E LTD', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9E34 AS DateTime), NULL, CAST(0x0000A56700CE9E34 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (40, N'BOEHRINGER INGELHEIM ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9E67 AS DateTime), NULL, CAST(0x0000A56700CE9E67 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (41, N'GERMAN REMEDIES ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9E7F AS DateTime), NULL, CAST(0x0000A56700CE9E7F AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (42, N'MOREPEN', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9E8C AS DateTime), NULL, CAST(0x0000A56700CE9E8C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (43, N'ALCHEM PHYTOCEUTICAL ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9EC2 AS DateTime), NULL, CAST(0x0000A56700CE9EC2 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (44, N'HIMALAYA DRUGS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9EE9 AS DateTime), NULL, CAST(0x0000A56700CE9EE9 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (45, N'MERCK PHARMA ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CE9F41 AS DateTime), NULL, CAST(0x0000A56700CE9F41 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (46, N'ICON ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA13B AS DateTime), NULL, CAST(0x0000A56700CEA13B AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (47, N'GLENMARK ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA141 AS DateTime), NULL, CAST(0x0000A56700CEA141 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (48, N'ALKEM', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA152 AS DateTime), NULL, CAST(0x0000A56700CEA152 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (49, N'SHALAKS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA15F AS DateTime), NULL, CAST(0x0000A56700CEA15F AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (50, N'PHARMED LTD', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA19A AS DateTime), NULL, CAST(0x0000A56700CEA19A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (51, N'CROSLANDS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA1BF AS DateTime), NULL, CAST(0x0000A56700CEA1BF AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (52, N'WOCKHARDT', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA1C4 AS DateTime), NULL, CAST(0x0000A56700CEA1C4 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (53, N'MEDLEY', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA1DC AS DateTime), NULL, CAST(0x0000A56700CEA1DC AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (54, N'MARK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA20D AS DateTime), NULL, CAST(0x0000A56700CEA20D AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (55, N'PRORIENT ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA215 AS DateTime), NULL, CAST(0x0000A56700CEA215 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (56, N'NOVARTIS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA21C AS DateTime), NULL, CAST(0x0000A56700CEA21C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (57, N'AEON PHARMA ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA223 AS DateTime), NULL, CAST(0x0000A56700CEA223 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (58, N'FULFORD', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA298 AS DateTime), NULL, CAST(0x0000A56700CEA298 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (59, N'ALLERCAN', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA2A6 AS DateTime), NULL, CAST(0x0000A56700CEA2A6 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (60, N'RPG LIFESCIENCE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA2C1 AS DateTime), NULL, CAST(0x0000A56700CEA2C1 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (61, N'SH PHARMA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA2E2 AS DateTime), NULL, CAST(0x0000A56700CEA2E2 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (62, N'AVENTIS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA329 AS DateTime), NULL, CAST(0x0000A56700CEA329 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (63, N'SANOFI', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA330 AS DateTime), NULL, CAST(0x0000A56700CEA330 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (64, N'FEMCARE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA36D AS DateTime), NULL, CAST(0x0000A56700CEA36D AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (65, N'Ranbaxy Labs', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA390 AS DateTime), NULL, CAST(0x0000A56700CEA390 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (66, N'FDL LTD', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA438 AS DateTime), NULL, CAST(0x0000A56700CEA438 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (67, N'CITADEL ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA45C AS DateTime), NULL, CAST(0x0000A56700CEA45C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (68, N'ORGANON', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA466 AS DateTime), NULL, CAST(0x0000A56700CEA466 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (69, N'APEX', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA49A AS DateTime), NULL, CAST(0x0000A56700CEA49A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (70, N'SHREYA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA4AD AS DateTime), NULL, CAST(0x0000A56700CEA4AD AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (71, N'UCB', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA4D7 AS DateTime), NULL, CAST(0x0000A56700CEA4D7 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (72, N'AJANTHA ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA54B AS DateTime), NULL, CAST(0x0000A56700CEA54B AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (73, N'ARMAC ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA5A0 AS DateTime), NULL, CAST(0x0000A56700CEA5A0 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (74, N'OZONE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA5A6 AS DateTime), NULL, CAST(0x0000A56700CEA5A6 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (75, N'INDI PHARMA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA5AD AS DateTime), NULL, CAST(0x0000A56700CEA5AD AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (76, N'IPCA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA5CA AS DateTime), NULL, CAST(0x0000A56700CEA5CA AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (77, N'ASTRA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA769 AS DateTime), NULL, CAST(0x0000A56700CEA769 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (78, N'LIVA ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA786 AS DateTime), NULL, CAST(0x0000A56700CEA786 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (79, N'ALEM', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA7D0 AS DateTime), NULL, CAST(0x0000A56700CEA7D0 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (80, N'Dr.Reddy''s', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA7D7 AS DateTime), NULL, CAST(0x0000A56700CEA7D7 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (81, N'UNITED BIOTECH', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA832 AS DateTime), NULL, CAST(0x0000A56700CEA832 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (82, N'ERIS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA845 AS DateTime), NULL, CAST(0x0000A56700CEA845 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (83, N'MSD', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA868 AS DateTime), NULL, CAST(0x0000A56700CEA868 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (84, N'NULIFE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA87F AS DateTime), NULL, CAST(0x0000A56700CEA87F AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (85, N'MODI OMEGA ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA8A0 AS DateTime), NULL, CAST(0x0000A56700CEA8A0 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (86, N'VASU', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA8DF AS DateTime), NULL, CAST(0x0000A56700CEA8DF AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (87, N'ALKEM ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA956 AS DateTime), NULL, CAST(0x0000A56700CEA956 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (88, N'FEM', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA969 AS DateTime), NULL, CAST(0x0000A56700CEA969 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (89, N'CADILA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA97C AS DateTime), NULL, CAST(0x0000A56700CEA97C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (90, N'MACLEODS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA989 AS DateTime), NULL, CAST(0x0000A56700CEA989 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (91, N'RBI', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA99C AS DateTime), NULL, CAST(0x0000A56700CEA99C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (92, N'J B CHEMICALS ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA9A8 AS DateTime), NULL, CAST(0x0000A56700CEA9A8 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (93, N'FRANCO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA9B4 AS DateTime), NULL, CAST(0x0000A56700CEA9B4 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (94, N'EMCURE ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA9C6 AS DateTime), NULL, CAST(0x0000A56700CEA9C6 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (95, N'ERIS LIFE SCIENCES', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA9E3 AS DateTime), NULL, CAST(0x0000A56700CEA9E3 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (96, N'USV ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEA9F5 AS DateTime), NULL, CAST(0x0000A56700CEA9F5 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (97, N'ZUVENTUS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAA53 AS DateTime), NULL, CAST(0x0000A56700CEAA53 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (98, N'BAYER ZYDUS PHARMA ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAA59 AS DateTime), NULL, CAST(0x0000A56700CEAA59 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (99, N'FOURTS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAA7D AS DateTime), NULL, CAST(0x0000A56700CEAA7D AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (100, N'COMED', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAA98 AS DateTime), NULL, CAST(0x0000A56700CEAA98 AS DateTime), NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (101, N'DR REDDYS ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAB33 AS DateTime), NULL, CAST(0x0000A56700CEAB33 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (102, N'FRANCO INDIAN ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAC0A AS DateTime), NULL, CAST(0x0000A56700CEAC0A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (103, N'AEOMEDS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAC8F AS DateTime), NULL, CAST(0x0000A56700CEAC8F AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (104, N'LABORATOIRE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAD73 AS DateTime), NULL, CAST(0x0000A56700CEAD73 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (105, N'ELDER', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAD8A AS DateTime), NULL, CAST(0x0000A56700CEAD8A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (106, N'SANDOZ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEADBB AS DateTime), NULL, CAST(0x0000A56700CEADBB AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (107, N'ENCORE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEADC1 AS DateTime), NULL, CAST(0x0000A56700CEADC1 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (108, N'SIDMAK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAE26 AS DateTime), NULL, CAST(0x0000A56700CEAE26 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (109, N'VERITAZ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAE9E AS DateTime), NULL, CAST(0x0000A56700CEAE9E AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (110, N'BIONOVA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAF89 AS DateTime), NULL, CAST(0x0000A56700CEAF89 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (111, N'SWISSKEM', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEAF90 AS DateTime), NULL, CAST(0x0000A56700CEAF90 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (112, N'ATOZ LIFESCIENCE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB00D AS DateTime), NULL, CAST(0x0000A56700CEB00D AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (113, N'TRIDOSS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB012 AS DateTime), NULL, CAST(0x0000A56700CEB012 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (114, N'MARS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB01D AS DateTime), NULL, CAST(0x0000A56700CEB01D AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (115, N'CURE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB02A AS DateTime), NULL, CAST(0x0000A56700CEB02A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (116, N'HETERO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB03F AS DateTime), NULL, CAST(0x0000A56700CEB03F AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (117, N'ANGLO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB045 AS DateTime), NULL, CAST(0x0000A56700CEB045 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (118, N'SOLVAY', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB091 AS DateTime), NULL, CAST(0x0000A56700CEB091 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (119, N'CHEMINNOVA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB09C AS DateTime), NULL, CAST(0x0000A56700CEB09C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (120, N'SYNTHELAB', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB0D2 AS DateTime), NULL, CAST(0x0000A56700CEB0D2 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (121, N'SIDMARK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB124 AS DateTime), NULL, CAST(0x0000A56700CEB124 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (122, N'RUBRA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB1F2 AS DateTime), NULL, CAST(0x0000A56700CEB1F2 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (123, N'PROTEC', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB208 AS DateTime), NULL, CAST(0x0000A56700CEB208 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (124, N'ENDEABOUR', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB26B AS DateTime), NULL, CAST(0x0000A56700CEB26B AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (125, N'SERVIER', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB2B3 AS DateTime), NULL, CAST(0x0000A56700CEB2B3 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (126, N'SOLVARY', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB334 AS DateTime), NULL, CAST(0x0000A56700CEB334 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (127, N'PULSE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB340 AS DateTime), NULL, CAST(0x0000A56700CEB340 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (128, N'WALTER', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB34C AS DateTime), NULL, CAST(0x0000A56700CEB34C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (129, N'SURIEN PHARMA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB358 AS DateTime), NULL, CAST(0x0000A56700CEB358 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (130, N'OCHOA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB365 AS DateTime), NULL, CAST(0x0000A56700CEB365 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (131, N'MODI-MUNDI', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB391 AS DateTime), NULL, CAST(0x0000A56700CEB391 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (132, N'RECKITT', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB3D1 AS DateTime), NULL, CAST(0x0000A56700CEB3D1 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (133, N'LES LAB', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB3D9 AS DateTime), NULL, CAST(0x0000A56700CEB3D9 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (134, N'INNOVA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB3DF AS DateTime), NULL, CAST(0x0000A56700CEB3DF AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (135, N'ASTALIFE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB44C AS DateTime), NULL, CAST(0x0000A56700CEB44C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (136, N'ENDEAVOUR', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB519 AS DateTime), NULL, CAST(0x0000A56700CEB519 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (137, N'ANGLO-FRENCH', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB52B AS DateTime), NULL, CAST(0x0000A56700CEB52B AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (138, N'RAVENBHEL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB553 AS DateTime), NULL, CAST(0x0000A56700CEB553 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (139, N'EMUCURE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB585 AS DateTime), NULL, CAST(0x0000A56700CEB585 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (140, N'LIFE-STAR MANKIND', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB594 AS DateTime), NULL, CAST(0x0000A56700CEB594 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (141, N'GENO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB5B4 AS DateTime), NULL, CAST(0x0000A56700CEB5B4 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (142, N'COSME', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB5BF AS DateTime), NULL, CAST(0x0000A56700CEB5BF AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (143, N'INDICO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB5D0 AS DateTime), NULL, CAST(0x0000A56700CEB5D0 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (144, N'FDC LTD', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB5ED AS DateTime), NULL, CAST(0x0000A56700CEB5ED AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (145, N'MERCURY', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB68C AS DateTime), NULL, CAST(0x0000A56700CEB68C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (146, N'MARAL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB76C AS DateTime), NULL, CAST(0x0000A56700CEB76C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (147, N'LIFE-STAR ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB777 AS DateTime), NULL, CAST(0x0000A56700CEB777 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (148, N'SAIMIRRA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB7F8 AS DateTime), NULL, CAST(0x0000A56700CEB7F8 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (149, N'BERGEN', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB803 AS DateTime), NULL, CAST(0x0000A56700CEB803 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (150, N'WINMEDICAL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB87A AS DateTime), NULL, CAST(0x0000A56700CEB87A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (151, N'LESSAC', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB8E3 AS DateTime), NULL, CAST(0x0000A56700CEB8E3 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (152, N'OKASA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB8F6 AS DateTime), NULL, CAST(0x0000A56700CEB8F6 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (153, N'OCHAA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB915 AS DateTime), NULL, CAST(0x0000A56700CEB915 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (154, N'JAGSONPAL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB964 AS DateTime), NULL, CAST(0x0000A56700CEB964 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (155, N'INDOCO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEB997 AS DateTime), NULL, CAST(0x0000A56700CEB997 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (156, N'VAPICARE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEBB4E AS DateTime), NULL, CAST(0x0000A56700CEBB4E AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (157, N'PHARM', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEBBDA AS DateTime), NULL, CAST(0x0000A56700CEBBDA AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (158, N'BOBAY', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEBCFE AS DateTime), NULL, CAST(0x0000A56700CEBCFE AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (159, N'ZANDU', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEBD07 AS DateTime), NULL, CAST(0x0000A56700CEBD07 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (160, N'KALINDI', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEBD27 AS DateTime), NULL, CAST(0x0000A56700CEBD27 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (161, N'VANCE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEBD60 AS DateTime), NULL, CAST(0x0000A56700CEBD60 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (162, N'RPG', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEBDCD AS DateTime), NULL, CAST(0x0000A56700CEBDCD AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (163, N'PHARMACIA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEBE42 AS DateTime), NULL, CAST(0x0000A56700CEBE42 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (164, N'FERRING', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC12C AS DateTime), NULL, CAST(0x0000A56700CEC12C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (165, N'GATES BYBY', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC143 AS DateTime), NULL, CAST(0x0000A56700CEC143 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (166, N'BLUECROSS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC14D AS DateTime), NULL, CAST(0x0000A56700CEC14D AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (167, N'JB CHEM', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC16F AS DateTime), NULL, CAST(0x0000A56700CEC16F AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (168, N'WINMEDICARE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC1D1 AS DateTime), NULL, CAST(0x0000A56700CEC1D1 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (169, N'INGA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC21A AS DateTime), NULL, CAST(0x0000A56700CEC21A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (170, N'SURIEN', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC235 AS DateTime), NULL, CAST(0x0000A56700CEC235 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (171, N'IND-SWIFT', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC35B AS DateTime), NULL, CAST(0x0000A56700CEC35B AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (172, N'MEDOPHAR', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC3AE AS DateTime), NULL, CAST(0x0000A56700CEC3AE AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (173, N'ATOZ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC3BA AS DateTime), NULL, CAST(0x0000A56700CEC3BA AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (174, N'AIMIL PHARMA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC3EA AS DateTime), NULL, CAST(0x0000A56700CEC3EA AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (175, N'EIMIL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC3F5 AS DateTime), NULL, CAST(0x0000A56700CEC3F5 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (176, N'SYSTOPIC', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC418 AS DateTime), NULL, CAST(0x0000A56700CEC418 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (177, N'FDC', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC49C AS DateTime), NULL, CAST(0x0000A56700CEC49C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (178, N'PALSONS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC5CF AS DateTime), NULL, CAST(0x0000A56700CEC5CF AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (179, N'GENX', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC648 AS DateTime), NULL, CAST(0x0000A56700CEC648 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (180, N'INDLHENIE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC653 AS DateTime), NULL, CAST(0x0000A56700CEC653 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (181, N'TRISTAR', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC659 AS DateTime), NULL, CAST(0x0000A56700CEC659 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (182, N'TABLETS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC6E3 AS DateTime), NULL, CAST(0x0000A56700CEC6E3 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (183, N'ZUVISTA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC6EF AS DateTime), NULL, CAST(0x0000A56700CEC6EF AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (184, N'PHARMALINK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC6F5 AS DateTime), NULL, CAST(0x0000A56700CEC6F5 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (185, N'GERMAN', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC712 AS DateTime), NULL, CAST(0x0000A56700CEC712 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (186, N'WYETH', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC737 AS DateTime), NULL, CAST(0x0000A56700CEC737 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (187, N'VANGUARD', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC76A AS DateTime), NULL, CAST(0x0000A56700CEC76A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (188, N'NRJET', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC8AB AS DateTime), NULL, CAST(0x0000A56700CEC8AB AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (189, N'SHIELD', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC8C6 AS DateTime), NULL, CAST(0x0000A56700CEC8C6 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (190, N'REKVINA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC8FB AS DateTime), NULL, CAST(0x0000A56700CEC8FB AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (191, N'BEST', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEC913 AS DateTime), NULL, CAST(0x0000A56700CEC913 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (192, N'SEEKO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECA1B AS DateTime), NULL, CAST(0x0000A56700CECA1B AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (193, N'JOHNSON', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECA36 AS DateTime), NULL, CAST(0x0000A56700CECA36 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (194, N'UNI-SANKYO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECA84 AS DateTime), NULL, CAST(0x0000A56700CECA84 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (195, N'VANGUARO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECAA7 AS DateTime), NULL, CAST(0x0000A56700CECAA7 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (196, N'REXCIN', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECAF3 AS DateTime), NULL, CAST(0x0000A56700CECAF3 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (197, N'ETHPHARMA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECB21 AS DateTime), NULL, CAST(0x0000A56700CECB21 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (198, N'PHARMED', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECB83 AS DateTime), NULL, CAST(0x0000A56700CECB83 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (199, N'EXTRACARE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECBA7 AS DateTime), NULL, CAST(0x0000A56700CECBA7 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (200, N'VERTICA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECE04 AS DateTime), NULL, CAST(0x0000A56700CECE04 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (201, N'MEDSUN', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECE14 AS DateTime), NULL, CAST(0x0000A56700CECE14 AS DateTime), NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (202, N'CRESCENT', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECE1E AS DateTime), NULL, CAST(0x0000A56700CECE1E AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (203, N'DABUR', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECE41 AS DateTime), NULL, CAST(0x0000A56700CECE41 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (204, N'BESTON', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECE45 AS DateTime), NULL, CAST(0x0000A56700CECE45 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (205, N'BIODEAL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECE71 AS DateTime), NULL, CAST(0x0000A56700CECE71 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (206, N'SCHERING', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECEC0 AS DateTime), NULL, CAST(0x0000A56700CECEC0 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (207, N'MODIMUNDI', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECF0B AS DateTime), NULL, CAST(0x0000A56700CECF0B AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (208, N'MEYER', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECF33 AS DateTime), NULL, CAST(0x0000A56700CECF33 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (209, N'POLYMED', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CECF39 AS DateTime), NULL, CAST(0x0000A56700CECF39 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (210, N'LIFESTAR', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED03A AS DateTime), NULL, CAST(0x0000A56700CED03A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (211, N'REMIDEX ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED13E AS DateTime), NULL, CAST(0x0000A56700CED13E AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (212, N'CRESTICA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED1D2 AS DateTime), NULL, CAST(0x0000A56700CED1D2 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (213, N'NOVASTAR', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED255 AS DateTime), NULL, CAST(0x0000A56700CED255 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (214, N'ALRED', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED296 AS DateTime), NULL, CAST(0x0000A56700CED296 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (215, N'JBCHEM', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED325 AS DateTime), NULL, CAST(0x0000A56700CED325 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (216, N'GALDERMA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED3AF AS DateTime), NULL, CAST(0x0000A56700CED3AF AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (217, N'ZYG PHARMA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED3C1 AS DateTime), NULL, CAST(0x0000A56700CED3C1 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (218, N'SSL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED445 AS DateTime), NULL, CAST(0x0000A56700CED445 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (219, N'SAF', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED452 AS DateTime), NULL, CAST(0x0000A56700CED452 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (220, N'YASH', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED48C AS DateTime), NULL, CAST(0x0000A56700CED48C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (221, N'STIEFEL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED4C8 AS DateTime), NULL, CAST(0x0000A56700CED4C8 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (222, N'FEN', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED4CD AS DateTime), NULL, CAST(0x0000A56700CED4CD AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (223, N'H & H', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED4E8 AS DateTime), NULL, CAST(0x0000A56700CED4E8 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (224, N'HIMANI', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED4FE AS DateTime), NULL, CAST(0x0000A56700CED4FE AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (225, N'GLOWDERMA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED503 AS DateTime), NULL, CAST(0x0000A56700CED503 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (226, N'ICPA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED510 AS DateTime), NULL, CAST(0x0000A56700CED510 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (227, N'CURATIO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED528 AS DateTime), NULL, CAST(0x0000A56700CED528 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (228, N'MED', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED536 AS DateTime), NULL, CAST(0x0000A56700CED536 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (229, N'P&P', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED55D AS DateTime), NULL, CAST(0x0000A56700CED55D AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (230, N'LEKAR', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED653 AS DateTime), NULL, CAST(0x0000A56700CED653 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (231, N'UNIVERSAL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED661 AS DateTime), NULL, CAST(0x0000A56700CED661 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (232, N'GENX PHARMA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED693 AS DateTime), NULL, CAST(0x0000A56700CED693 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (233, N'ALBERT', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED6F1 AS DateTime), NULL, CAST(0x0000A56700CED6F1 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (234, N'UNIMARK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED6FD AS DateTime), NULL, CAST(0x0000A56700CED6FD AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (235, N'PERCOS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED71B AS DateTime), NULL, CAST(0x0000A56700CED71B AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (236, N'GRACEWELL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED764 AS DateTime), NULL, CAST(0x0000A56700CED764 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (237, N'WESTCOAST', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED7A8 AS DateTime), NULL, CAST(0x0000A56700CED7A8 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (238, N'APPLE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED7E9 AS DateTime), NULL, CAST(0x0000A56700CED7E9 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (239, N'ASTRAZENECA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED806 AS DateTime), NULL, CAST(0x0000A56700CED806 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (240, N'SUNPHARMA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED812 AS DateTime), NULL, CAST(0x0000A56700CED812 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (241, N'NESTLE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED831 AS DateTime), NULL, CAST(0x0000A56700CED831 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (242, N'NIHAL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED838 AS DateTime), NULL, CAST(0x0000A56700CED838 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (243, N'ASRISTO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED84A AS DateTime), NULL, CAST(0x0000A56700CED84A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (244, N'BIOLOGICAL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED867 AS DateTime), NULL, CAST(0x0000A56700CED867 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (245, N'GREESHA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED88C AS DateTime), NULL, CAST(0x0000A56700CED88C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (246, N'WARDEX', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED8C5 AS DateTime), NULL, CAST(0x0000A56700CED8C5 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (247, N'SHIVAHERBAL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED8F3 AS DateTime), NULL, CAST(0x0000A56700CED8F3 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (248, N'NOVORTIS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED906 AS DateTime), NULL, CAST(0x0000A56700CED906 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (249, N'GRIFFON', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED923 AS DateTime), NULL, CAST(0x0000A56700CED923 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (250, N'CARDEL PHARMA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED92A AS DateTime), NULL, CAST(0x0000A56700CED92A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (251, N'STRASSENBURG', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED980 AS DateTime), NULL, CAST(0x0000A56700CED980 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (252, N'PPC', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED992 AS DateTime), NULL, CAST(0x0000A56700CED992 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (253, N'THEMIS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CED9B7 AS DateTime), NULL, CAST(0x0000A56700CED9B7 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (254, N'NOVO NORDISK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDA27 AS DateTime), NULL, CAST(0x0000A56700CEDA27 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (255, N'LILLY', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDA33 AS DateTime), NULL, CAST(0x0000A56700CEDA33 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (256, N'BIOCON', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDA50 AS DateTime), NULL, CAST(0x0000A56700CEDA50 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (257, N'KHADELWAL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDB1E AS DateTime), NULL, CAST(0x0000A56700CEDB1E AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (258, N'TROIKAA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDB83 AS DateTime), NULL, CAST(0x0000A56700CEDB83 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (259, N'EAST INDIA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDBD9 AS DateTime), NULL, CAST(0x0000A56700CEDBD9 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (260, N'FULFORA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDBEB AS DateTime), NULL, CAST(0x0000A56700CEDBEB AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (261, N'UNIQUE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDC30 AS DateTime), NULL, CAST(0x0000A56700CEDC30 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (262, N'ALOEVERA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDCB9 AS DateTime), NULL, CAST(0x0000A56700CEDCB9 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (263, N'CHARAK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDD08 AS DateTime), NULL, CAST(0x0000A56700CEDD08 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (264, N'RAPTAKOS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDD3C AS DateTime), NULL, CAST(0x0000A56700CEDD3C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (265, N'HINDUSTAN', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDD6A AS DateTime), NULL, CAST(0x0000A56700CEDD6A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (266, N'VERTAGE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDDBF AS DateTime), NULL, CAST(0x0000A56700CEDDBF AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (267, N'PANCHA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDE07 AS DateTime), NULL, CAST(0x0000A56700CEDE07 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (268, N'PROMED', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDE19 AS DateTime), NULL, CAST(0x0000A56700CEDE19 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (269, N'MARCK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDE98 AS DateTime), NULL, CAST(0x0000A56700CEDE98 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (270, N'WINDLAS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDF2F AS DateTime), NULL, CAST(0x0000A56700CEDF2F AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (271, N'MEDIORALS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDF34 AS DateTime), NULL, CAST(0x0000A56700CEDF34 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (272, N'GALPHA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDF73 AS DateTime), NULL, CAST(0x0000A56700CEDF73 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (273, N'CREATIVE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDFC9 AS DateTime), NULL, CAST(0x0000A56700CEDFC9 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (274, N'GSK', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDFF1 AS DateTime), NULL, CAST(0x0000A56700CEDFF1 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (275, N'ADORE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDFF7 AS DateTime), NULL, CAST(0x0000A56700CEDFF7 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (276, N'SYNTHO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEDFFC AS DateTime), NULL, CAST(0x0000A56700CEDFFC AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (277, N'JAWA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE00F AS DateTime), NULL, CAST(0x0000A56700CEE00F AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (278, N'SGS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE015 AS DateTime), NULL, CAST(0x0000A56700CEE015 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (279, N'ALLERGAN', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE039 AS DateTime), NULL, CAST(0x0000A56700CEE039 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (280, N'ENTOD', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE044 AS DateTime), NULL, CAST(0x0000A56700CEE044 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (281, N'DEY''S', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE04A AS DateTime), NULL, CAST(0x0000A56700CEE04A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (282, N'UNIMED', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE05C AS DateTime), NULL, CAST(0x0000A56700CEE05C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (283, N'CENTAUR', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE074 AS DateTime), NULL, CAST(0x0000A56700CEE074 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (284, N'SKANT HEALTHCARE', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE0CB AS DateTime), NULL, CAST(0x0000A56700CEE0CB AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (285, N'AKUMS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE0D6 AS DateTime), NULL, CAST(0x0000A56700CEE0D6 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (286, N'ENDOVER', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE19F AS DateTime), NULL, CAST(0x0000A56700CEE19F AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (287, N'BANAGA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE274 AS DateTime), NULL, CAST(0x0000A56700CEE274 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (288, N'GYPSONA', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE287 AS DateTime), NULL, CAST(0x0000A56700CEE287 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (289, N'BECTON', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE29C AS DateTime), NULL, CAST(0x0000A56700CEE29C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (290, N'VISHNURAM', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE2A8 AS DateTime), NULL, CAST(0x0000A56700CEE2A8 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (291, N'SOMATICO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE2EA AS DateTime), NULL, CAST(0x0000A56700CEE2EA AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (292, N'BIOCHEM', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE328 AS DateTime), NULL, CAST(0x0000A56700CEE328 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (293, N'MANO', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEE346 AS DateTime), NULL, CAST(0x0000A56700CEE346 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (294, N'10ddg', N'kirsjfh', N'7799277957', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CEF338 AS DateTime), 0, CAST(0x0000A63B012F52F4 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (295, N'qwewe', N'ewew', N'9618797876', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CF241C AS DateTime), 0, CAST(0x0000A62700A87605 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (296, N'Appolo10', N'Appolo', N'9533818878', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CF2474 AS DateTime), 0, CAST(0x0000A62700A83F6F AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (297, N'HELIOS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CF24ED AS DateTime), NULL, CAST(0x0000A56700CF24ED AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (298, N'ESKAG', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CF24F7 AS DateTime), NULL, CAST(0x0000A56700CF24F7 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (299, N'KNOLL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CF2518 AS DateTime), NULL, CAST(0x0000A56700CF2518 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (300, N'XXXXX', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CF251D AS DateTime), NULL, CAST(0x0000A56700CF251D AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (301, N'CROS', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CF2547 AS DateTime), NULL, CAST(0x0000A56700CF2547 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (302, N'INGAL', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A56700CF2558 AS DateTime), NULL, CAST(0x0000A56700CF2558 AS DateTime), NULL)
GO
print 'Processed 300 total records'
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (303, N'VERITAZ HEALTHCARE ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB012FDC4D AS DateTime), NULL, CAST(0x0000A5BB012FDC4D AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (304, N'ENTOD PHARMA ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB012FE20C AS DateTime), NULL, CAST(0x0000A5BB012FE20C AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (305, N'ZUVENTUS HEALTHCARE LTD ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB012FE2DC AS DateTime), NULL, CAST(0x0000A5BB012FE2DC AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (306, N'P P C ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB012FE341 AS DateTime), NULL, CAST(0x0000A5BB012FE341 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (307, N'MENARINI ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB012FEC54 AS DateTime), NULL, CAST(0x0000A5BB012FEC54 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (308, N'LIVA HEALTHCARE ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB012FF9CC AS DateTime), NULL, CAST(0x0000A5BB012FF9CC AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (309, N'SHIVANI HEALTHCARE ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB012FFAE8 AS DateTime), NULL, CAST(0x0000A5BB012FFAE8 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (310, N'F D C ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB012FFB6D AS DateTime), NULL, CAST(0x0000A5BB012FFB6D AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (311, N'RAPROSS ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB012FFC4E AS DateTime), NULL, CAST(0x0000A5BB012FFC4E AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (312, N'SHREYA LABS ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB012FFE4A AS DateTime), NULL, CAST(0x0000A5BB012FFE4A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (313, N'ADCOCK INGRAM', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB013009C5 AS DateTime), NULL, CAST(0x0000A5BB013009C5 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (314, N'WALTER BUSHNELL ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB01300DD0 AS DateTime), NULL, CAST(0x0000A5BB01300DD0 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (316, N'ANGLO FRENCH ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB01300F86 AS DateTime), NULL, CAST(0x0000A5BB01300F86 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (319, N'AGRAWAL ', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5BB013016A6 AS DateTime), NULL, CAST(0x0000A5BB013016A6 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (320, N'reddys', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A5ED00CA3CE2 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (325, N'zoddu', N'ani', N'859745628', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A6020114484D AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (326, N'pp', N'll', N'859745628', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A602011E57D6 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (332, N'krishna', N'guntu', N'9618707028', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A60400D7D177 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (333, N'krishna', N'guntu', N'9533818878', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A60400DAE34B AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (338, N'krishna', N'guntu', N'9532145665', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A60400E1C462 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (339, N'krishna', N'guntu', N'9532145665', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A60400E1C462 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (340, N'krishna', N'guntu', N'454545455', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A60400E2677B AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (342, N'Srinadh', N'srikanth', N'3456789012', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A60400638F40 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (343, N'Srinadh', N'srikanth', N'3456789012', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A604006394B0 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (344, N'Kishore', N'naidu', N'9765432112', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A604015AD590 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (346, N'dbj', N'sjfhsj', N'jsfhsj', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A604015F1C67 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (350, N'hgh', N'hgh', N'8867564535', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A6040160C63A AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (353, N'hhf', N'ghg', N'9617756656', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A60401637798 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (359, N'sjfjshf', N'wfsj', N'86826284628', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A6040174F40D AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (360, N'fhsj', N'fjshf', N'8284728728', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A60401759F4B AS DateTime), 2, CAST(0x0000A6040175D327 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (363, N'krishna123', N'bfjdjd', N'9618707028', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A60800D01D64 AS DateTime), 2, CAST(0x0000A60800D05054 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (364, N'krishna123', N'bfjdjd', N'9618707028', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A60800D042C5 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (365, N'ss', N'Ravi', N'93937798', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A60D012CF96C AS DateTime), 2, CAST(0x0000A60D012DEC8A AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (366, N'krishna123', N'bfjdjd', N'9618707028', NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A61500BA1D28 AS DateTime), 2, CAST(0x0000A61500BAD4A0 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (367, N'krishna1234dfbgdh', N'bfjdjd', N'9618707028', NULL, NULL, N'82', 1, 0, 1, CAST(0x0000A61500BD3093 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (369, N'kfc', N'krishna guntu', N'9618707028', NULL, NULL, N'82', 1, 0, 1, CAST(0x0000A61601778CF9 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (370, N'Krishna', N'djbjdghjd', N'3463763746', NULL, NULL, N'83', 1, 0, 1, CAST(0x0000A61601795875 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (371, N'dbdnfsnD', N'dfbdnfb', N'8743874834', NULL, NULL, N'82', 1, 0, 1, CAST(0x0000A617013CB602 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (372, N'bfgf', N'hghgh', N'5656565656', NULL, NULL, N'82', 1, 0, 1, CAST(0x0000A617016E62A2 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (373, N'gfg', N'gfg', N'9753234565', NULL, NULL, N'81', 1, 0, 1, CAST(0x0000A61701703D89 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (374, N'hfhfhfh', N'ghgh', N'6767676767', NULL, NULL, N'83', 1, 0, 1, CAST(0x0000A61701714271 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (375, N'xfgdj', N'sdsjfgjs', N'7878787878', NULL, NULL, N'82', 1, 0, 1, CAST(0x0000A61701798F16 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (376, N'ggfg', N'hfhfh', N'5656656566', NULL, NULL, N'83', 1, 0, 1, CAST(0x0000A6170179CD1A AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (379, N'Srinivas', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A618013E3313 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (382, N'Anvesh', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A61B00F9C942 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (384, N'aurodinbo', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A61B011DC3CA AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (385, N'Rayolity1', N'ch', NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A61B012A14BC AS DateTime), 2, CAST(0x0000A61B012E21E6 AS DateTime), NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (386, N'marodinbo', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, CAST(0x0000A61D00FBCD15 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (387, N'Appolo', N'company', N'9533818878', NULL, NULL, NULL, 1, NULL, 1, CAST(0x0000A62700A7ED47 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (393, N'kdgdjghj', N'jdgfdjgdj', N'7878787878', NULL, NULL, NULL, 1, NULL, 1, CAST(0x0000A62D003FE198 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (394, N'djhgdj', N'jfghdjgh', N'3467436767', NULL, NULL, NULL, 1, NULL, 1, CAST(0x0000A62D00735835 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblManufacturers] ([MfgID], [MfgName], [AgentName], [AgentContactNumber], [AddressLine1], [AddressLine2], [City], [IsActive], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Location]) VALUES (395, N'Shankar', NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, CAST(0x0000A63A0181B50F AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblManufacturers] OFF
/****** Object:  Table [dbo].[tblLookupCategory]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLookupCategory](
	[LookupCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[LookupCategoryName] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](200) NULL,
	[LookupCategoryCode] [varchar](50) NULL,
 CONSTRAINT [PK_tblLookupCategory] PRIMARY KEY CLUSTERED 
(
	[LookupCategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblLookupCategory] ON
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (1, N'Title', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A012280E5 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (2, N'Gender', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A0122A26E AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (3, N'EmpType', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A0122FC6F AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (4, N'BloodGroup', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A01236D71 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (5, N'Designation', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A01239FBB AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (6, N'EmpAddressType', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A0123C7C1 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (7, N'GraduationType', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A01241415 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (8, N'RelationShip', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A0124C511 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (9, N'MaritalStatus', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A012507D6 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (10, N'BankAccountType', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A0125427F AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (11, N'RoomType', 1, 1, CAST(0x0000A4640134EFBC AS DateTime), NULL, CAST(0x0000A4640134EFBC AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (12, N'City', 1, 1, CAST(0x0000A4DD00000000 AS DateTime), NULL, CAST(0x0000A4DD00A5B85B AS DateTime), NULL, N'C')
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (16, N'PaymentMode', 1, 1, CAST(0x0000A50D00000000 AS DateTime), NULL, CAST(0x0000A50D00FC6256 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (17, N'MenuPermissionType', 1, 1, CAST(0x0000A53D00000000 AS DateTime), NULL, CAST(0x0000A53D012EB21A AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (18, N'DiagnosisStatus', 1, 1, CAST(0x0000A54B00000000 AS DateTime), NULL, CAST(0x0000A54B014305D2 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (19, N'ProductForm', 1, 1, CAST(0x0000A5C300000000 AS DateTime), NULL, CAST(0x0000A5C3010F19E9 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (20, N'ProductGroup', 1, 1, CAST(0x0000A5C300000000 AS DateTime), NULL, CAST(0x0000A5C3010F4840 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (21, N'AppointmentMode', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB013E819B AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (22, N'AppointmentStatus', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB013E819B AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (23, N'PaymentStatus', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB013E819B AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (24, N'DiagnosisDepartment', 1, 1, CAST(0x0000A5DE00000000 AS DateTime), NULL, CAST(0x0000A5DE012928E6 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (25, N'AppointmentType', 1, 1, CAST(0x0000A5DF00000000 AS DateTime), NULL, CAST(0x0000A5DF01155337 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (26, N'Sales', 1, 1, CAST(0x0000A5F500000000 AS DateTime), NULL, CAST(0x0000A5F5004EF890 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (27, N'Return', 1, 1, CAST(0x0000A5F500000000 AS DateTime), NULL, CAST(0x0000A5F5004F0EDB AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookupCategory] ([LookupCategoryId], [LookupCategoryName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCategoryCode]) VALUES (28, N'LkPatientTypeId', 1, 1, CAST(0x0000A61000000000 AS DateTime), NULL, CAST(0x0000A60F0036C8B0 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblLookupCategory] OFF
/****** Object:  Table [dbo].[tblUserLoginLog]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserLoginLog](
	[LID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[SessionID] [varchar](100) NOT NULL,
	[LoginTime] [datetime] NOT NULL,
	[LogoutTime] [datetime] NULL,
 CONSTRAINT [PK_tblLoginRegistry] PRIMARY KEY CLUSTERED 
(
	[LID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblUserInfo]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserInfo](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[LoginID] [varchar](100) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[RoleID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblUserInfo] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblUserInfo] ON
INSERT [dbo].[tblUserInfo] ([UID], [LoginID], [Password], [RoleID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'admin', N'aarushi', 2, 1, 1, CAST(0x0000A5E000000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblUserInfo] OFF
/****** Object:  Table [dbo].[tblMenu]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMenu](
	[MenuId] [int] IDENTITY(1,1) NOT NULL,
	[ParentID] [int] NOT NULL,
	[MenuName] [varchar](100) NOT NULL,
	[NavigateURL] [varchar](500) NULL,
	[MenuLogo] [varchar](100) NULL,
	[MenuOrder] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](max) NULL,
 CONSTRAINT [PK_tblMenu_1] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblMenu] ON
INSERT [dbo].[tblMenu] ([MenuId], [ParentID], [MenuName], [NavigateURL], [MenuLogo], [MenuOrder], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (1, 1, N'Adminstration', N'Modules/Adminstration.aspx', NULL, NULL, 1, 1, CAST(0x0000A658013A8C2E AS DateTime), 2, CAST(0x0000A658013E253F AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tblMenu] OFF
/****** Object:  Table [dbo].[tblPtOperations]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblPtOperations](
	[OperationID] [int] IDENTITY(1,1) NOT NULL,
	[AdmissionID] [int] NULL,
	[DoctorID] [int] NULL,
	[BedID] [int] NULL,
	[OperationName] [varchar](100) NULL,
	[Procedure] [varchar](1000) NULL,
	[Risk] [varchar](1000) NULL,
	[ExistingDiseases] [varchar](1000) NULL,
	[DiagnosisSummery] [varchar](1000) NULL,
	[PostOperationNotes] [varchar](1000) NULL,
	[UsedDrugs] [varchar](1000) NULL,
	[TreatmentSummery] [varchar](1000) NULL,
	[PatientStatus] [varchar](1000) NULL,
	[OperationStatus] [varchar](10) NULL,
	[Advices] [varchar](1000) NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NULL,
	[Description] [varchar](500) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_tblPtOperations] PRIMARY KEY CLUSTERED 
(
	[OperationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblRoles]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRoles](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleTitle] [varchar](100) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblRoles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblRoles] ON
INSERT [dbo].[tblRoles] ([RoleID], [RoleTitle], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'admin', N'aa', 1, 1, CAST(0x0000A61B00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoles] ([RoleID], [RoleTitle], [Description], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'SuperAdmin', N'Headof admin', 0, 1, CAST(0x0000A6550136F78A AS DateTime), 2, CAST(0x0000A655013878B4 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblRoles] OFF
/****** Object:  Table [dbo].[tblTransactions]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTransactions](
	[TransID] [int] IDENTITY(1,1) NOT NULL,
	[HospitalBranchID] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[MobileNo] [varchar](20) NULL,
	[Purpose] [varchar](250) NULL,
	[TransDate] [datetime] NOT NULL,
	[PatientID] [int] NULL,
	[EID] [int] NULL,
	[CashAmount] [float] NULL,
	[CardAmount] [float] NULL,
	[TotalAmount] [money] NOT NULL,
	[PaymentMode] [varchar](50) NULL,
	[TransType] [varchar](50) NULL,
	[PaidTo] [varchar](50) NULL,
	[TotalPaid] [float] NULL,
	[DueAmount] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED 
(
	[TransID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblTransactions] ON
INSERT [dbo].[tblTransactions] ([TransID], [HospitalBranchID], [Name], [MobileNo], [Purpose], [TransDate], [PatientID], [EID], [CashAmount], [CardAmount], [TotalAmount], [PaymentMode], [TransType], [PaidTo], [TotalPaid], [DueAmount], [CreatedDate], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (2, 1, N'sreenu', N'8522977322', N'SICK', CAST(0x0000A5F500000000 AS DateTime), 5, 5, NULL, NULL, 200.0000, NULL, N'sales', NULL, NULL, NULL, CAST(0x0000A5F500000000 AS DateTime), 1, CAST(0x0000A5F500000000 AS DateTime), NULL)
INSERT [dbo].[tblTransactions] ([TransID], [HospitalBranchID], [Name], [MobileNo], [Purpose], [TransDate], [PatientID], [EID], [CashAmount], [CardAmount], [TotalAmount], [PaymentMode], [TransType], [PaidTo], [TotalPaid], [DueAmount], [CreatedDate], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (3, 1, N'anvesh', N'9095493245', N'fever', CAST(0x0000A61E00000000 AS DateTime), 9, 6, NULL, NULL, 500.0000, NULL, N'sales', NULL, NULL, NULL, CAST(0x0000A61E00000000 AS DateTime), 1, CAST(0x0000A61D00000000 AS DateTime), NULL)
INSERT [dbo].[tblTransactions] ([TransID], [HospitalBranchID], [Name], [MobileNo], [Purpose], [TransDate], [PatientID], [EID], [CashAmount], [CardAmount], [TotalAmount], [PaymentMode], [TransType], [PaidTo], [TotalPaid], [DueAmount], [CreatedDate], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (4, 1, N'shankar', N'9848123456', N'cold', CAST(0x0000A65000000000 AS DateTime), 6, 5, NULL, NULL, 300.0000, NULL, N'sales', NULL, NULL, NULL, CAST(0x0000A63C00000000 AS DateTime), 1, CAST(0x0000A61E00000000 AS DateTime), NULL)
INSERT [dbo].[tblTransactions] ([TransID], [HospitalBranchID], [Name], [MobileNo], [Purpose], [TransDate], [PatientID], [EID], [CashAmount], [CardAmount], [TotalAmount], [PaymentMode], [TransType], [PaidTo], [TotalPaid], [DueAmount], [CreatedDate], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (6, 1, NULL, NULL, N'fever', CAST(0x0000A61F00000000 AS DateTime), NULL, NULL, NULL, NULL, 400.0000, NULL, N'sales', N'Patient', NULL, NULL, CAST(0x0000A61F01365E24 AS DateTime), 1, NULL, NULL)
INSERT [dbo].[tblTransactions] ([TransID], [HospitalBranchID], [Name], [MobileNo], [Purpose], [TransDate], [PatientID], [EID], [CashAmount], [CardAmount], [TotalAmount], [PaymentMode], [TransType], [PaidTo], [TotalPaid], [DueAmount], [CreatedDate], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (7, 1, NULL, NULL, NULL, CAST(0x0000A62000000000 AS DateTime), NULL, NULL, NULL, NULL, 350.0000, N'cash', N'ssss', NULL, NULL, NULL, CAST(0x0000A62000D62CBD AS DateTime), 1, CAST(0x0000A62000FD21C1 AS DateTime), NULL)
INSERT [dbo].[tblTransactions] ([TransID], [HospitalBranchID], [Name], [MobileNo], [Purpose], [TransDate], [PatientID], [EID], [CashAmount], [CardAmount], [TotalAmount], [PaymentMode], [TransType], [PaidTo], [TotalPaid], [DueAmount], [CreatedDate], [CreatedBy], [ModifiedOn], [ModifiedBy]) VALUES (9, 1, N'rakesh', N'984812345', N'cold', CAST(0x0000A64700000000 AS DateTime), 5, 6, NULL, NULL, 500.0000, N'card', NULL, N'patient', NULL, NULL, CAST(0x0000A64701289E6A AS DateTime), 1, CAST(0x0000A647012DC25C AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tblTransactions] OFF
/****** Object:  Table [dbo].[tblTransactionInfo]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTransactionInfo](
	[TransInfoID] [int] IDENTITY(1,1) NOT NULL,
	[TransID] [int] NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[Amount] [money] NOT NULL,
	[Discount] [money] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](250) NULL,
 CONSTRAINT [PK_tblTransactionDetails] PRIMARY KEY CLUSTERED 
(
	[TransInfoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblSupplier]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSupplier](
	[SupplierID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierName] [varchar](100) NOT NULL,
	[ContactPerson] [varchar](100) NULL,
	[ContactNo] [varchar](20) NULL,
	[TinNo] [varchar](20) NULL,
	[AddressID] [int] NULL,
	[Comments] [varchar](200) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblSupplier] PRIMARY KEY CLUSTERED 
(
	[SupplierID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblSupplier] ON
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'R. Enterprises', N'Ranga', N'8994482933', N'49029485889', NULL, NULL, 1, 2, CAST(0x0000A5C600000000 AS DateTime), NULL, CAST(0x0000A5C600CDB1C4 AS DateTime))
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Srinivasa Enterprises', N'Kiran', N'9803999229', N'YU983943903', NULL, NULL, 1, 2, CAST(0x0000A5C600000000 AS DateTime), NULL, CAST(0x0000A5C600CE4B3F AS DateTime))
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'dfddf enterprices', N'gaaa', N'56789234', N'123456', 4, NULL, 1, 2, CAST(0x0000A5C600F0EADA AS DateTime), 2, CAST(0x0000A63B00493484 AS DateTime))
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'Malikarjuna Enterprises', N'Krishna', N'859745628', NULL, NULL, NULL, 0, 2, CAST(0x0000A5CA00BD5AEC AS DateTime), 2, CAST(0x0000A63B004959DF AS DateTime))
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'Malikarjuna Enterprises', N'Krishna', N'859745628', NULL, NULL, NULL, 0, 2, CAST(0x0000A5CA00BD681B AS DateTime), NULL, NULL)
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'Hyd Enterprices', NULL, NULL, NULL, NULL, NULL, 1, 2, CAST(0x0000A5D700D240C4 AS DateTime), NULL, NULL)
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, N'Hyd Enterprices', NULL, NULL, NULL, NULL, NULL, 1, 2, CAST(0x0000A5D700D25075 AS DateTime), NULL, NULL)
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, N'Hyd Enterprices', NULL, NULL, NULL, NULL, NULL, 1, 2, CAST(0x0000A5D700D307F3 AS DateTime), NULL, NULL)
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, N'sreenivasa Enterprices', N'raju', N'8522977322', NULL, 4, NULL, 1, 2, CAST(0x0000A6020105A2E7 AS DateTime), 2, CAST(0x0000A60300C9D057 AS DateTime))
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, N'shankar', N'shanku', N'9394060318', N'321', 6, NULL, 1, 1, CAST(0x0000A60A011A0A08 AS DateTime), 2, CAST(0x0000A60A01219757 AS DateTime))
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, N'Malikarjuna', N'Krishna1', N'859745628', NULL, 7, NULL, 1, 1, CAST(0x0000A60D0122307B AS DateTime), NULL, NULL)
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, N'KGb', NULL, NULL, NULL, 8, NULL, 1, 1, CAST(0x0000A619010489D9 AS DateTime), 2, CAST(0x0000A61901055DED AS DateTime))
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, N'param', N'Krish', N'859745628', NULL, 17, NULL, 1, 1, CAST(0x0000A61B011E9F0A AS DateTime), 2, CAST(0x0000A61B01308F1A AS DateTime))
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, N'devi', N'parvathi', N'859745628', NULL, 18, NULL, 1, 1, CAST(0x0000A61B01280BCB AS DateTime), NULL, NULL)
INSERT [dbo].[tblSupplier] ([SupplierID], [SupplierName], [ContactPerson], [ContactNo], [TinNo], [AddressID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, N'KcGuntu', NULL, NULL, NULL, 22, NULL, 1, 0, CAST(0x0000A63B0046F110 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblSupplier] OFF
/****** Object:  Table [dbo].[tblStore]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblStore](
	[ItemNumber] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[ManufacturedBy] [varchar](50) NULL,
	[Quantity] [int] NULL,
	[Cost] [money] NULL,
	[SupplierName] [varchar](50) NULL,
	[InVoice] [int] NULL,
	[CreatedOn] [date] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedOn] [date] NULL,
	[ModifiedBy] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[Comments] [nvarchar](500) NULL,
	[BranchID] [int] NULL,
 CONSTRAINT [PK_tblStore] PRIMARY KEY CLUSTERED 
(
	[ItemNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblRoleMenu]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRoleMenu](
	[RoleMenuID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[MenuID] [int] NOT NULL,
	[LkAccessID] [varchar](50) NULL,
	[IsActive] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblRoleMenu] PRIMARY KEY CLUSTERED 
(
	[RoleMenuID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblRoleMenu] ON
INSERT [dbo].[tblRoleMenu] ([RoleMenuID], [RoleID], [MenuID], [LkAccessID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 1, NULL, 1, 1, CAST(0x0000A6580144C867 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoleMenu] ([RoleMenuID], [RoleID], [MenuID], [LkAccessID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 1, 1, NULL, 1, 1, CAST(0x0000A6580144D266 AS DateTime), NULL, NULL)
INSERT [dbo].[tblRoleMenu] ([RoleMenuID], [RoleID], [MenuID], [LkAccessID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 1, 1, NULL, 1, 1, CAST(0x0000A6580144D459 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblRoleMenu] OFF
/****** Object:  Table [dbo].[tblProducts]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblProducts](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [varchar](100) NOT NULL,
	[MfgID] [int] NOT NULL,
	[QuantityPerUnit] [varchar](20) NULL,
	[LicenceNo] [varchar](20) NULL,
	[IsDeleted] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[RackLocation] [varchar](200) NULL,
	[Comments] [varchar](500) NULL,
	[Schedule] [varchar](20) NULL,
	[Composition] [varchar](1000) NULL,
	[MinQuantity] [int] NULL,
	[FormName] [varchar](30) NULL,
	[GroupName] [varchar](20) NULL,
	[FormCategory] [varchar](20) NULL,
 CONSTRAINT [PK_tblMedicineCatalog] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_MedicineCatalog] UNIQUE NONCLUSTERED 
(
	[ProductName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblProducts] ON
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (1, N'BETNESOL FORTE', 1, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE5A4C AS DateTime), NULL, CAST(0x0000A63C017308C4 AS DateTime), NULL, NULL, NULL, N'Betamethasone 1 MG', NULL, N'tablets', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (2, N'BETNESOL ', 1, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE9672 AS DateTime), NULL, CAST(0x0000A63C0173B53B AS DateTime), NULL, NULL, NULL, N'Betamethasone 0.5 MG', NULL, N'injection', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (3, N'BECOZYM C FORTE', 2, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE9688 AS DateTime), NULL, CAST(0x0000A56700CE9688 AS DateTime), NULL, NULL, NULL, N'Biotin 0.15 MG+Calcium pantothenate 16.3 MG+Cyanocobalamin 10 MCG+Niacinamide 50 MG+Pyridoxine 3 MG+Riboflavine 10 MG+Thiamine mononitrate 10 MG+Vitamin C 150 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (4, N'BESTOZYME', 3, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE969C AS DateTime), NULL, CAST(0x0000A56700CE969C AS DateTime), NULL, NULL, NULL, N'Alpha amylase 100 MG+Papain 60 MG', NULL, N'CAP', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (5, N'BUSCOPAN', 4, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE96AA AS DateTime), NULL, CAST(0x0000A56700CE96AA AS DateTime), NULL, NULL, NULL, N'Hyoscine butylbromide 10 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (6, N'BAYCIP 500 MG', 5, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE96BD AS DateTime), NULL, CAST(0x0000A56700CE96BD AS DateTime), NULL, NULL, NULL, N'Ciprofloxacin 500 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (7, N'BAYCIP 250 MG', 5, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE96CB AS DateTime), NULL, CAST(0x0000A56700CE96CB AS DateTime), NULL, NULL, NULL, N'Ciprofloxacin 250 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (8, N'CYSTONE 60S TABLET1', 6, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE96DF AS DateTime), NULL, CAST(0x0000A62D007702CF AS DateTime), NULL, NULL, NULL, N'Ayurvedic -', NULL, N'tablets', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (9, N'CIPLAR 40 MG', 7, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE96F0 AS DateTime), NULL, CAST(0x0000A56700CE96F0 AS DateTime), NULL, NULL, NULL, N'Propranolol 40 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (10, N'CIPLAR 10 MG 56 23', 7, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE96FB AS DateTime), NULL, CAST(0x0000A62A003BD4C4 AS DateTime), NULL, NULL, NULL, N'Propranolol 10 MG', NULL, N'liquids', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (11, N'CREON 25000', 8, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE970C AS DateTime), NULL, CAST(0x0000A56700CE970C AS DateTime), NULL, NULL, NULL, N'Pancreatin 300 MG', NULL, N'CAP', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (12, N'CREON 10000', 8, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE9722 AS DateTime), NULL, CAST(0x0000A56700CE9722 AS DateTime), NULL, NULL, NULL, N'Pancreatin 150 MG', NULL, N'CAP', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (13, N'CAROFIT D', 9, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE973A AS DateTime), NULL, CAST(0x0000A56700CE973A AS DateTime), NULL, NULL, NULL, N'Beta-carotene 100 MG+Cyanocobalamin 15 MCG+Elemental chromium 200 MCG+Elemental selenium 100 MCG+Folic acid 1.5 MG+Pyridoxine 3 MG+Thiamine mononitrate 10 MG+Vitamin C 150 MG+Vitamin E 25 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (14, N'Morphine', 1, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE9748 AS DateTime), 1, CAST(0x0000A62A003D7E29 AS DateTime), NULL, NULL, NULL, N'Carvedilol(3.125 Mg)', NULL, N'capsules', N'PainKillers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (15, N'CARZEC 6.25 MG', 1, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE9759 AS DateTime), NULL, CAST(0x0000A56700CE9759 AS DateTime), NULL, NULL, NULL, N'Carvedilol 6.25 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (16, N'CEDON 100 DT', 10, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE976B AS DateTime), NULL, CAST(0x0000A56700CE976B AS DateTime), NULL, NULL, NULL, N'Cefpodoxime(100mg) , Lactobacillus Sporogenes(60 Million)', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (17, N'CEDON 200 DT', 10, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE977C AS DateTime), NULL, CAST(0x0000A56700CE977C AS DateTime), NULL, NULL, NULL, N'Cefpodoxime(200 Mg)', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (18, N'CEPODEM 200 MG', 11, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE9792 AS DateTime), NULL, CAST(0x0000A56700CE9792 AS DateTime), NULL, NULL, NULL, N'Cefpodoxime 200 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (19, N'CLODREL 75 MG', 12, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE97A1 AS DateTime), NULL, CAST(0x0000A56700CE97A1 AS DateTime), NULL, NULL, NULL, N'Clopidogrel 75 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (20, N'CLODREL PLUS', 12, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE97AF AS DateTime), NULL, CAST(0x0000A56700CE97AF AS DateTime), NULL, NULL, NULL, N'Clopidogrel 75 MG+Aspirin 75 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (21, N'CORDARONE - X', 13, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE97C6 AS DateTime), NULL, CAST(0x0000A56700CE97C6 AS DateTime), NULL, NULL, NULL, N'Amiodarone 200 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (22, N'CORDARONE 100MG TABLET', 13, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE97D1 AS DateTime), NULL, CAST(0x0000A56700CE97D1 AS DateTime), NULL, NULL, NULL, N'Amiodarone 100 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (23, N'CLONOTRIL 0.25 MG', 14, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE97E9 AS DateTime), NULL, CAST(0x0000A56700CE97E9 AS DateTime), NULL, NULL, NULL, N'Clonazepam 0.25 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (24, N'CLONOTRIL 0.5 MG', 14, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE9801 AS DateTime), NULL, CAST(0x0000A56700CE9801 AS DateTime), NULL, NULL, NULL, N'Clonazepam 0.5 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (25, N'CLONOTRIL 1MG TABLET', 14, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE980E AS DateTime), NULL, CAST(0x0000A56700CE980E AS DateTime), NULL, NULL, NULL, N'Clonazepam 1 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (26, N'CLONOTRIL PLUS', 14, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE9819 AS DateTime), NULL, CAST(0x0000A56700CE9819 AS DateTime), NULL, NULL, NULL, N'Escitalopram 10 MG+Clonazepam 0.5 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (27, N'CONCOR AM 5 MG', 15, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE9823 AS DateTime), NULL, CAST(0x0000A56700CE9823 AS DateTime), NULL, NULL, NULL, N'Bisoprolol 5 MG+Amlodipine 5 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (28, N'CONCOR COR 2.5 MG', 15, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE982E AS DateTime), NULL, CAST(0x0000A56700CE982E AS DateTime), NULL, NULL, NULL, N'Bisoprolol 2.5 MG+Amlodipine 5 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (29, N'CONCOR 5 MG', 15, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE983C AS DateTime), NULL, CAST(0x0000A56700CE983C AS DateTime), NULL, NULL, NULL, N'Bisoprolol 5 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (30, N'CONCOR 10 MG', 15, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE984A AS DateTime), NULL, CAST(0x0000A56700CE984A AS DateTime), NULL, NULL, NULL, N'Bisoprolol 10 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (31, N'CERUVIN A 75MG/75MG TABLET', 11, N'1', NULL, 0, 1, 1, CAST(0x0000A56700CE9868 AS DateTime), NULL, CAST(0x0000A56700CE9868 AS DateTime), NULL, NULL, NULL, N'Clopidogrel 75 MG+Aspirin 75 MG', NULL, N'TAB', N'painKillers', N'ProductGroup')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (32, N'CERUVIN AF 75+150MG TABLET', 11, N'1', NULL, NULL, 1, 1, CAST(0x0000A56700CE98E0 AS DateTime), NULL, CAST(0x0000A56700CE98E0 AS DateTime), NULL, NULL, NULL, N'Clopidogrel 75 MG+Aspirin 150 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (34, N'CERUVIN 150MG TABLET', 11, N'1', NULL, NULL, 1, 1, CAST(0x0000A56700CE996C AS DateTime), NULL, CAST(0x0000A56700CE996C AS DateTime), NULL, NULL, NULL, N'Clopidogrel 150 MG', NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (35, N'COBADEX - CZS', 1, N'1', NULL, 0, 0, 1, CAST(0x0000A56700CE997C AS DateTime), NULL, CAST(0x0000A56700CE997C AS DateTime), NULL, NULL, NULL, N'Chromium picolinate 250 MCG+Cyanocobalamin 15 MCG+Elemental selenium 100 MCG+Folic acid 1500 MCG+Niacinamide 100 MCG+Pyridoxine 3 MG+Zinc sulphate 61.8 MG', NULL, N'CAP', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (36, N'DSFDSF', 320, N'50', N'004', 0, 1, 1, CAST(0x0000A5ED00CA3DFF AS DateTime), 1, CAST(0x0000A61900C35820 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'saline', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (37, N'lonvki', 320, N'50', N'4', 0, 1, 1, CAST(0x0000A60A01354DB3 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'saline', N'Pain Killers', N'ProductGroup')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (38, N'ranbaxy', 320, N'50', N'4', 0, 0, 1, CAST(0x0000A60A013DB1C7 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'saline', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (43, N'limcee', 5, N'50', NULL, 0, 1, 1, CAST(0x0000A60B00000000 AS DateTime), NULL, CAST(0x0000A6180066991F AS DateTime), NULL, NULL, NULL, NULL, NULL, N'tablet', N'Cold & Fever', N'ProductGroup')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (44, N'Advil', 5, N'30', NULL, 0, 1, 1, CAST(0x0000A61800000000 AS DateTime), 1, CAST(0x0000A61900EDEA6F AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Tablet', N'Fever', N'ProductGroup')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (45, N'Lice', 6, N'10', N'123', 0, 1, 1, CAST(0x0000A618013E5ECB AS DateTime), 1, CAST(0x0000A63A01855F4F AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Solid Rock', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (49, N'Ibuprofen', 8, NULL, NULL, 0, 0, 1, CAST(0x0000A61900FC7C96 AS DateTime), NULL, CAST(0x0000A61B012E1A2B AS DateTime), NULL, NULL, NULL, NULL, NULL, N'TAB', N'Cold & Fever', N'ProductGroup')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (50, N'L Doper', 379, N'10', N'123', 0, 1, 1, CAST(0x0000A61A00C0DF96 AS DateTime), NULL, CAST(0x0000A63A018B0F47 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'Solid', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (51, N'Ani', 382, N'10', N'321', 0, 1, 1, CAST(0x0000A61B00F9CF1E AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Liquid', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (52, N'Levorphanol', 5, NULL, NULL, 0, 1, 1, CAST(0x0000A61B011BA163 AS DateTime), NULL, CAST(0x0000A61B0123A38C AS DateTime), NULL, NULL, NULL, NULL, NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (53, N'Mcleen', 384, N'10', N'423', 0, 1, 0, CAST(0x0000A61B011DC5E3 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'syrup', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (54, N'ice', 384, N'20', N'224', 0, 0, 0, CAST(0x0000A61B011FB1DE AS DateTime), NULL, CAST(0x0000A61B012042F9 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'perfume', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (55, N'Naproxen', 8, NULL, NULL, 0, 1, 1, CAST(0x0000A61B012D2E4B AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'TAB', N'Cold & Fever', N'ProductGroup')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (56, N'rice', 386, N'20', N'225', 0, 1, 1, CAST(0x0000A61D00FBCEAF AS DateTime), NULL, CAST(0x0000A61D01002067 AS DateTime), NULL, NULL, NULL, NULL, NULL, N'perfume', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (67, N'Levo', 5, NULL, NULL, 0, 1, 1, CAST(0x0000A63B00099E1A AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'TAB', N'Pain Killers', N'ProductForm')
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (70, N'maridon', 386, N'20', N'225', 0, 1, 1, CAST(0x0000A63B002A0821 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Capsule', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (71, N'ssrwr', 386, N'32', N'jssjdhdsj', 0, 1, 1, CAST(0x0000A63B002D81DB AS DateTime), NULL, CAST(0x0000A63B002DC3CF AS DateTime), N'jfhdsjhfdsj', N'sdiuhdsjghjdshgsjdkghjshj', N'djshfdjsgh', N'gsjsgjs', NULL, N'injection', NULL, NULL)
INSERT [dbo].[tblProducts] ([ProductID], [ProductName], [MfgID], [QuantityPerUnit], [LicenceNo], [IsDeleted], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [RackLocation], [Comments], [Schedule], [Composition], [MinQuantity], [FormName], [GroupName], [FormCategory]) VALUES (74, N'argo', 386, N'50', N'123', 0, 1, 1, CAST(0x0000A63B0104BE89 AS DateTime), NULL, NULL, N'3rd desk', N'None', N'daily 3 times', N'tesxt', NULL, N'liquid', N'Cap', N'ProductGroup')
SET IDENTITY_INSERT [dbo].[tblProducts] OFF
/****** Object:  Table [dbo].[tblPharmacyDetails]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPharmacyDetails](
	[PharmacyID] [int] IDENTITY(1,1) NOT NULL,
	[HospitalBranchID] [int] NOT NULL,
	[PharmacyName] [varchar](100) NOT NULL,
	[ContactNumber1] [varchar](20) NULL,
	[ContactNumber2] [varchar](50) NULL,
	[PharmacyContactPerson] [varchar](100) NULL,
	[ContactMobileNumber] [varchar](20) NULL,
	[ServiceTax] [decimal](5, 2) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](200) NULL,
 CONSTRAINT [PK_tblPharmacyDetails] PRIMARY KEY CLUSTERED 
(
	[PharmacyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblPharmacyDetails] ON
INSERT [dbo].[tblPharmacyDetails] ([PharmacyID], [HospitalBranchID], [PharmacyName], [ContactNumber1], [ContactNumber2], [PharmacyContactPerson], [ContactMobileNumber], [ServiceTax], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (1, 1, N'My Pharmacy', N'9866985589', NULL, N'Siva', N'8560254586', CAST(2.00 AS Decimal(5, 2)), 1, 1, CAST(0x0000A5D000000000 AS DateTime), NULL, CAST(0x0000A5D400F99784 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tblPharmacyDetails] OFF
/****** Object:  Table [dbo].[tblChemicalUsed]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblChemicalUsed](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NULL,
	[TransactionType] [varchar](100) NULL,
	[ChemicalID] [int] NOT NULL,
	[PurchaseCostPerQuantity] [money] NULL,
	[Quantity] [int] NULL,
	[SalesCostPerQuantity] [money] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblChemicalUsed] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblLookup]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLookup](
	[LookupId] [varchar](50) NOT NULL,
	[LookupCategoryId] [int] NOT NULL,
	[LookupName] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](200) NULL,
	[LookupCode] [varchar](50) NULL,
 CONSTRAINT [PK_tblLookup] PRIMARY KEY CLUSTERED 
(
	[LookupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'1', 1, N'Mr', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A0127C404 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'10', 3, N'Permenant', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A012B06DA AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'101', 19, N'Injections', 1, 1, CAST(0x0000A5C600000000 AS DateTime), NULL, CAST(0x0000A5C600FC549D AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'102', 20, N'Pain Killers', 1, 1, CAST(0x0000A5C600000000 AS DateTime), NULL, CAST(0x0000A5C600FC7E11 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'103', 20, N'Cold and Fever Tablet', 1, 1, CAST(0x0000A5C600000000 AS DateTime), NULL, CAST(0x0000A5C600FCA185 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'11', 3, N'Contract', 1, 1, CAST(0x0000A43A01280D06 AS DateTime), NULL, CAST(0x0000A43A012B4516 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'110', 19, N'Capsules', 0, 1, CAST(0x0000A5CA012965FE AS DateTime), 2, CAST(0x0000A60100E88C08 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'111', 20, N'D-Vitamin', 0, 1, CAST(0x0000A5CA0135E882 AS DateTime), 2, CAST(0x0000A5DF0132D077 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'112', 19, N'TAB', 1, 1, CAST(0x0000A56700CE5A46 AS DateTime), NULL, CAST(0x0000A56700CE5A46 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'113', 19, N'CAP', 1, 1, CAST(0x0000A56700CE969A AS DateTime), NULL, CAST(0x0000A56700CE969A AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'114', 19, N'SYRUP', 1, 1, CAST(0x0000A56700CE9E35 AS DateTime), NULL, CAST(0x0000A56700CE9E35 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'115', 19, N'DROPS', 1, 1, CAST(0x0000A56700CE9E4E AS DateTime), NULL, CAST(0x0000A56700CE9E4E AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'116', 19, N'INJECTION', 1, 1, CAST(0x0000A56700CE9E59 AS DateTime), NULL, CAST(0x0000A56700CE9E59 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'117', 19, N'TABLET', 1, 1, CAST(0x0000A56700CE9E99 AS DateTime), NULL, CAST(0x0000A56700CE9E99 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'118', 19, N'SUPPOSITORY', 1, 1, CAST(0x0000A56700CE9EA6 AS DateTime), NULL, CAST(0x0000A56700CE9EA6 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'119', 19, N'CAPSULE', 1, 1, CAST(0x0000A56700CE9F0F AS DateTime), NULL, CAST(0x0000A56700CE9F0F AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'120', 19, N'CREAM', 1, 1, CAST(0x0000A56700CE9F1B AS DateTime), NULL, CAST(0x0000A56700CE9F1B AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'121', 19, N'GEL', 1, 1, CAST(0x0000A56700CEA0C2 AS DateTime), NULL, CAST(0x0000A56700CEA0C2 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'122', 19, N'CRM', 1, 1, CAST(0x0000A56700CEA160 AS DateTime), NULL, CAST(0x0000A56700CEA160 AS DateTime), NULL, N'h')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'123', 19, N'SYP', 1, 1, CAST(0x0000A56700CEA17D AS DateTime), NULL, CAST(0x0000A56700CEA17D AS DateTime), NULL, N'i')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'124', 19, N'SACHET', 1, 1, CAST(0x0000A56700CEA191 AS DateTime), NULL, CAST(0x0000A56700CEA191 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'125', 19, N'Capsules', 1, 1, CAST(0x0000A56700CEA1AE AS DateTime), NULL, CAST(0x0000A56700CEA1AE AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'126', 19, N'Injections', 1, 1, CAST(0x0000A56700CEA1B9 AS DateTime), NULL, CAST(0x0000A56700CEA1B9 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'127', 19, N'POW', 1, 1, CAST(0x0000A56700CEA1BF AS DateTime), NULL, CAST(0x0000A56700CEA1BF AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'128', 19, N'SOAP', 1, 1, CAST(0x0000A56700CEA20D AS DateTime), NULL, CAST(0x0000A56700CEA20D AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'129', 19, N'INH', 1, 1, CAST(0x0000A56700CEA23F AS DateTime), NULL, CAST(0x0000A56700CEA23F AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'130', 19, N'ROTA', 1, 1, CAST(0x0000A56700CEA246 AS DateTime), NULL, CAST(0x0000A56700CEA246 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'131', 19, N'E/D', 0, 1, CAST(0x0000A56700CEA2A6 AS DateTime), 1, CAST(0x0000A60C00F68D4F AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'132', 19, N'ANITBIOTIC', 1, 1, CAST(0x0000A56700CEA7D0 AS DateTime), NULL, CAST(0x0000A56700CEA7D0 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'133', 19, N'Liquid', 1, 1, CAST(0x0000A56700CEA7D7 AS DateTime), NULL, CAST(0x0000A56700CEA7D7 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'134', 19, N'Inhalers', 1, 1, CAST(0x0000A56700CEA7E9 AS DateTime), NULL, CAST(0x0000A56700CEA7E9 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'135', 19, N'ROTACAPS', 1, 1, CAST(0x0000A56700CEA7F0 AS DateTime), NULL, CAST(0x0000A56700CEA7F0 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'136', 19, N'MULTIVITAMIN', 1, 1, CAST(0x0000A56700CEA827 AS DateTime), NULL, CAST(0x0000A56700CEA827 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'137', 19, N'FUNGAL', 1, 1, CAST(0x0000A56700CEA832 AS DateTime), NULL, CAST(0x0000A56700CEA832 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'138', 19, N'NASAL SPRAY', 1, 1, CAST(0x0000A56700CEA868 AS DateTime), NULL, CAST(0x0000A56700CEA868 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'139', 19, N'VIAL', 1, 1, CAST(0x0000A56700CEA876 AS DateTime), NULL, CAST(0x0000A56700CEA876 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'140', 19, N'BONE STREN', 1, 1, CAST(0x0000A56700CEA87F AS DateTime), NULL, CAST(0x0000A56700CEA87F AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'15', 3, N'Temporary', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A012B9D5C AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'153', 21, N'Walk IN', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB01412E28 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'154', 21, N'Pre Booking', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB01414F0B AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'155', 21, N'Online', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB0141647F AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'157', 22, N'Scheduled', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB014214EB AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'159', 22, N'Rescheduled', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB01425B6C AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'16', 3, N'Training', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A012BCDB9 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'160', 22, N'Cancelled', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB014279A7 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'161', 22, N'Arrived', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB0142A500 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'162', 23, N'Paid', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB0142EC63 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'163', 23, N'Pending', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DB014306AA AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'164', 22, N'Visited', 1, 1, CAST(0x0000A5DB00000000 AS DateTime), NULL, CAST(0x0000A5DC01257117 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'165', 25, N'IP-Consultation', 1, 1, CAST(0x0000A5DF00000000 AS DateTime), NULL, CAST(0x0000A5DF01158ABD AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'166', 25, N'OP-Consultation', 1, 1, CAST(0x0000A5DF00000000 AS DateTime), NULL, CAST(0x0000A5DF0115ACA1 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'167', 25, N'Operation', 1, 1, CAST(0x0000A5DF00000000 AS DateTime), NULL, CAST(0x0000A5DF0115C5AD AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'168', 25, N'Self', 1, 1, CAST(0x0000A5DF00000000 AS DateTime), NULL, CAST(0x0000A5DF0115D959 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'17', 3, N'Consulting', 1, 1, CAST(0x0000A43A01280D06 AS DateTime), NULL, CAST(0x0000A43A012BF3AA AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'170', 20, N'B-Complex', 1, 1, CAST(0x0000A5DF012FFD40 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'171', 19, N'saline', 1, 1, CAST(0x0000A5ED00CA3937 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'176', 26, N'Sales', 1, 1, CAST(0x0000A5F500000000 AS DateTime), NULL, CAST(0x0000A5F5004F6495 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'177', 27, N'Return', 1, 1, CAST(0x0000A5F500000000 AS DateTime), NULL, CAST(0x0000A5F5004F80F1 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'18', 4, N'A+', 1, 1, CAST(0x0000A43A01280D06 AS DateTime), NULL, CAST(0x0000A43A012D0D5C AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'187', 19, N'LIMCEE', 1, 1, CAST(0x0000A60C00F23D32 AS DateTime), 1, CAST(0x0000A60C00F48C07 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'188', 20, N'Paracetmol', 1, 1, CAST(0x0000A60D011E6A83 AS DateTime), 2, CAST(0x0000A60D0120C057 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'189', 28, N'InPatient', 1, 1, CAST(0x0000A61000000000 AS DateTime), NULL, CAST(0x0000A60F00370C59 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'19', 4, N'A-', 1, 1, CAST(0x0000A43A01280D06 AS DateTime), NULL, CAST(0x0000A43A012D1CA9 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'190', 28, N'OutPatient', 1, 1, CAST(0x0000A61000000000 AS DateTime), NULL, CAST(0x0000A60F0037255B AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'191', 28, N'Customer', 1, 1, CAST(0x0000A61000000000 AS DateTime), NULL, CAST(0x0000A60F00374572 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'2', 1, N'Mrs', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A0127DDF4 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'20', 4, N'B+', 1, 1, CAST(0x0000A43A01280D06 AS DateTime), NULL, CAST(0x0000A43A012D2FC5 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'24', 4, N'B-', 1, 1, CAST(0x0000A43A01280D06 AS DateTime), NULL, CAST(0x0000A43A012D4EA1 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'25', 4, N'AB+', 1, 1, CAST(0x0000A43A01280D06 AS DateTime), NULL, CAST(0x0000A43A012D5DFD AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'26', 4, N'AB-', 1, 1, CAST(0x0000A43A01280D06 AS DateTime), NULL, CAST(0x0000A43A012D6C47 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'27', 4, N'O+', 1, 1, CAST(0x0000A43A01280D06 AS DateTime), NULL, CAST(0x0000A43A012D7DC4 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'28', 4, N'O-', 1, 1, CAST(0x0000A43A01280D06 AS DateTime), NULL, CAST(0x0000A43A012DA844 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'29', 5, N'Admin', 1, 1, CAST(0x0000A43B00000000 AS DateTime), NULL, CAST(0x0000A43B00B29068 AS DateTime), NULL, N'AD')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'3', 1, N'Baby', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A0127FB2F AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'30', 5, N'Sr.Doctor', 1, 1, CAST(0x0000A43B00B29068 AS DateTime), NULL, CAST(0x0000A43B00B2AE49 AS DateTime), NULL, N'D')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'31', 5, N'Jr.Doctor', 1, 1, CAST(0x0000A43B00B29068 AS DateTime), NULL, CAST(0x0000A43B00B2E427 AS DateTime), NULL, N'D')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'33', 5, N'Staff', 1, 1, CAST(0x0000A43B00B29068 AS DateTime), NULL, CAST(0x0000A43B00B33C3F AS DateTime), NULL, N'S')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'34', 5, N'Patient', 1, 1, CAST(0x0000A43B00B29068 AS DateTime), NULL, CAST(0x0000A43B00B4A83E AS DateTime), NULL, N'P')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'35', 5, N'Supervisor', 1, 1, CAST(0x0000A43B00B29068 AS DateTime), NULL, CAST(0x0000A43B00B4D80B AS DateTime), NULL, N'SP')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'36', 5, N'Reception', 1, 1, CAST(0x0000A43B00B29068 AS DateTime), NULL, CAST(0x0000A43B00B4FAC1 AS DateTime), NULL, N'R')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'37', 5, N'Accountant ', 1, 1, CAST(0x0000A43B00000000 AS DateTime), NULL, CAST(0x0000A43B00B554CE AS DateTime), NULL, N'AC')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'39', 5, N'Nurse', 1, 1, CAST(0x0000A43B00B2E427 AS DateTime), NULL, CAST(0x0000A43B00B59182 AS DateTime), NULL, N'N')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'4', 1, N'Master', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A01280D06 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'40', 5, N'Trainee', 1, 1, CAST(0x0000A43B00000000 AS DateTime), NULL, CAST(0x0000A43B00B612F5 AS DateTime), NULL, N'T')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'45', 6, N'Permanent Address', 1, 1, CAST(0x0000A43B00000000 AS DateTime), NULL, CAST(0x0000A43B00B68D06 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'46', 6, N'Current Address', 1, 1, CAST(0x0000A43B00000000 AS DateTime), NULL, CAST(0x0000A43B00B6E026 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'47', 7, N'Primary Education', 1, 1, CAST(0x0000A43B00000000 AS DateTime), NULL, CAST(0x0000A43B00B93A72 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'48', 7, N'Secondary', 1, 1, CAST(0x0000A43B00B93A72 AS DateTime), NULL, CAST(0x0000A43B00B9656D AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'50', 7, N'Under Graduation', 1, 1, CAST(0x0000A43B00B93A72 AS DateTime), NULL, CAST(0x0000A43B00BA03D8 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'54', 7, N'Graduation', 1, 1, CAST(0x0000A43B00B93A72 AS DateTime), NULL, CAST(0x0000A43B00BAB0FA AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'55', 7, N'Post Graduation', 1, 1, CAST(0x0000A43B00B93A72 AS DateTime), NULL, CAST(0x0000A43B00BACB85 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'57', 7, N'Phd', 1, 1, CAST(0x0000A43B00000000 AS DateTime), NULL, CAST(0x0000A43B00BD0146 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'58', 7, N'Special Course', 1, 1, CAST(0x0000A43B00000000 AS DateTime), NULL, CAST(0x0000A43B00BDB51D AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'59', 8, N'Father', 1, 1, CAST(0x0000A43B00000000 AS DateTime), NULL, CAST(0x0000A43B00BDE50B AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'6', 1, N'Baby of', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A0128494B AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'60', 8, N'Mother', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00BDF955 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'61', 8, N'Sister', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00BE0EBA AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'62', 8, N'Brother', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00BE2372 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'63', 8, N'Friend', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00BE5D46 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'64', 8, N'Son', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00BE820E AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'65', 8, N'Daughter', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00BEA613 AS DateTime), NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'66', 8, N'Uncle', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00BF0583 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'67', 8, N'Aunty', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00BF2EFC AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'68', 9, N'Married', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00BFC988 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'69', 9, N'Un Married', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00BFEB27 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'7', 2, N'Male', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A01286E78 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'70', 10, N'Current Account', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00C1021F AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'71', 10, N'Savings Account', 1, 1, CAST(0x0000A43B00BDB51D AS DateTime), NULL, CAST(0x0000A43B00C12975 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'73', 11, N'AC Room', 1, 1, CAST(0x0000A46400000000 AS DateTime), NULL, CAST(0x0000A464013577B6 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'74', 11, N'Deluxe Room', 1, 1, CAST(0x0000A464013577B6 AS DateTime), NULL, CAST(0x0000A4640135CA02 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'75', 11, N'Operation Theatre', 1, 1, CAST(0x0000A464013577B6 AS DateTime), NULL, CAST(0x0000A46401361196 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'76', 11, N'ICU', 1, 1, CAST(0x0000A464013577B6 AS DateTime), NULL, CAST(0x0000A46401364336 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'78', 11, N'Normal Room', 1, 1, CAST(0x0000A464013577B6 AS DateTime), NULL, CAST(0x0000A46401366401 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'79', 11, N'General Ward', 1, 1, CAST(0x0000A464013577B6 AS DateTime), NULL, CAST(0x0000A4640136AA99 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'8', 2, N'Female', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A012883C2 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'80', 1, N'Dr', 1, 1, CAST(0x0000A4DD00000000 AS DateTime), NULL, CAST(0x0000A4DD00B053EF AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'81', 12, N'Vijayawada', 1, 1, CAST(0x0000A50D00000000 AS DateTime), NULL, CAST(0x0000A50D00FD40E2 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'82', 12, N'Hyderabad', 1, 1, CAST(0x0000A50D00000000 AS DateTime), NULL, CAST(0x0000A50D00FD890A AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'83', 12, N'Secundrabad', 1, 1, CAST(0x0000A50D00000000 AS DateTime), NULL, CAST(0x0000A50D00FDA8AB AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'85', 16, N'Cash', 1, 1, CAST(0x0000A50D00000000 AS DateTime), NULL, CAST(0x0000A50D00FDDDEC AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'86', 16, N'Credit Card', 1, 1, CAST(0x0000A50D00000000 AS DateTime), NULL, CAST(0x0000A50D00FE7786 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'89', 16, N'Cheque', 1, 1, CAST(0x0000A50D00000000 AS DateTime), NULL, CAST(0x0000A50D00FED3C5 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'9', 2, N'Other', 1, 1, CAST(0x0000A43A00000000 AS DateTime), NULL, CAST(0x0000A43A012893AB AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'90', 16, N'Debit Card', 1, 1, CAST(0x0000A50D00000000 AS DateTime), NULL, CAST(0x0000A50D00FF8364 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'91', 16, N'Online', 1, 1, CAST(0x0000A50D00000000 AS DateTime), NULL, CAST(0x0000A50D00FFAA56 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'92', 17, N'Add New', 1, 1, CAST(0x0000A53D00000000 AS DateTime), NULL, CAST(0x0000A53D012EE506 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'93', 17, N'Edit', 1, 1, CAST(0x0000A53D00000000 AS DateTime), NULL, CAST(0x0000A53D012F059A AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'94', 17, N'Delete', 1, 1, CAST(0x0000A53D00000000 AS DateTime), NULL, CAST(0x0000A53D012F1C04 AS DateTime), NULL, NULL)
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'95', 18, N'Sample Collected', 1, 1, CAST(0x0000A54B00000000 AS DateTime), NULL, CAST(0x0000A54B01436A3F AS DateTime), NULL, N'1')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'96', 18, N'Report Generated', 1, 1, CAST(0x0000A54B00000000 AS DateTime), NULL, CAST(0x0000A54B014389E0 AS DateTime), NULL, N'2')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'97', 18, N'Report Printed', 1, 1, CAST(0x0000A54B00000000 AS DateTime), NULL, CAST(0x0000A54B0143C1D5 AS DateTime), NULL, N'3')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'98', 18, N'Report Cancelled', 1, 1, CAST(0x0000A54B00000000 AS DateTime), NULL, CAST(0x0000A54B01440771 AS DateTime), NULL, N'4')
INSERT [dbo].[tblLookup] ([LookupId], [LookupCategoryId], [LookupName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments], [LookupCode]) VALUES (N'99', 19, N'Inhalers', 1, 1, CAST(0x0000A5C600000000 AS DateTime), NULL, CAST(0x0000A5C600FBC74A AS DateTime), NULL, NULL)
/****** Object:  Table [dbo].[tblHospital]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblHospital](
	[HospitalId] [int] IDENTITY(1,1) NOT NULL,
	[HospitalName] [varchar](100) NOT NULL,
	[ParentHospitalId] [int] NULL,
	[HospitalCode] [varchar](20) NOT NULL,
	[AddressID] [int] NOT NULL,
	[PrimaryContactNo] [decimal](10, 0) NOT NULL,
	[SecodaryContactNo] [decimal](10, 0) NULL,
	[OtherContactNo] [decimal](10, 0) NULL,
	[PriContactPersonName] [varchar](100) NOT NULL,
	[PriMobileNumber] [decimal](10, 0) NOT NULL,
	[RegistrationNo] [varchar](20) NULL,
	[TIN] [varchar](20) NULL,
	[VAT] [varchar](20) NULL,
	[PAN] [varchar](20) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](1000) NULL,
 CONSTRAINT [PK_tblHospitalDetails] PRIMARY KEY CLUSTERED 
(
	[HospitalId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblHospital] ON
INSERT [dbo].[tblHospital] ([HospitalId], [HospitalName], [ParentHospitalId], [HospitalCode], [AddressID], [PrimaryContactNo], [SecodaryContactNo], [OtherContactNo], [PriContactPersonName], [PriMobileNumber], [RegistrationNo], [TIN], [VAT], [PAN], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (4, N'Yashoda', NULL, N'ysd', 1, CAST(7799277977 AS Decimal(10, 0)), CAST(7799277977 AS Decimal(10, 0)), CAST(7799277977 AS Decimal(10, 0)), N'Guru', CAST(7799277977 AS Decimal(10, 0)), N'Reg12345', N'Tin12345', N'Vat12345', N'APK21214', 1, 1, CAST(0x0000A5FF00000000 AS DateTime), NULL, CAST(0x0000A60900B670BB AS DateTime), NULL)
INSERT [dbo].[tblHospital] ([HospitalId], [HospitalName], [ParentHospitalId], [HospitalCode], [AddressID], [PrimaryContactNo], [SecodaryContactNo], [OtherContactNo], [PriContactPersonName], [PriMobileNumber], [RegistrationNo], [TIN], [VAT], [PAN], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (1003, N'appolo', NULL, N'aplo', 4, CAST(9094380830 AS Decimal(10, 0)), CAST(8522977322 AS Decimal(10, 0)), CAST(984812345 AS Decimal(10, 0)), N'ramu', CAST(984812345 AS Decimal(10, 0)), N'aplo3456', N'Tin1456', N'Vat3878', N'BGYPG555ON', 1, 1, CAST(0x0000A5A600000000 AS DateTime), NULL, CAST(0x0000A6560013FD9C AS DateTime), NULL)
INSERT [dbo].[tblHospital] ([HospitalId], [HospitalName], [ParentHospitalId], [HospitalCode], [AddressID], [PrimaryContactNo], [SecodaryContactNo], [OtherContactNo], [PriContactPersonName], [PriMobileNumber], [RegistrationNo], [TIN], [VAT], [PAN], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (1005, N'dfg', NULL, N'gh', 1, CAST(78087654 AS Decimal(10, 0)), CAST(345678 AS Decimal(10, 0)), CAST(1234567890 AS Decimal(10, 0)), N'gfhj', CAST(3456789 AS Decimal(10, 0)), N'34567', N'qwertyui', N'ewrtyu', N'adsfghj', 1, 1, CAST(0x0000A63C00000000 AS DateTime), NULL, CAST(0x0000A65600151090 AS DateTime), NULL)
INSERT [dbo].[tblHospital] ([HospitalId], [HospitalName], [ParentHospitalId], [HospitalCode], [AddressID], [PrimaryContactNo], [SecodaryContactNo], [OtherContactNo], [PriContactPersonName], [PriMobileNumber], [RegistrationNo], [TIN], [VAT], [PAN], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (1009, N'rainbow', NULL, N'rainbow', 7, CAST(8106054326 AS Decimal(10, 0)), NULL, NULL, N'rakesh', CAST(8522977322 AS Decimal(10, 0)), N'567890', NULL, NULL, NULL, 1, 1, CAST(0x0000A5C300000000 AS DateTime), NULL, CAST(0x0000A656002620B7 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tblHospital] OFF
/****** Object:  Table [dbo].[tblLabDetails]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLabDetails](
	[LabID] [int] IDENTITY(1,1) NOT NULL,
	[HospitalBranchID] [int] NOT NULL,
	[LabName] [varchar](100) NOT NULL,
	[ContactNumber1] [varchar](20) NULL,
	[ContactNumber2] [varchar](50) NOT NULL,
	[DiagnosisContactPerson] [varchar](100) NULL,
	[ContactMobileNumber] [varchar](20) NULL,
	[ServiceTax] [decimal](5, 2) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](200) NULL,
 CONSTRAINT [PK_tblDiagnosisDetails] PRIMARY KEY CLUSTERED 
(
	[LabID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmployee](
	[EID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NULL,
	[FirstName] [varchar](100) NOT NULL,
	[LastName] [varchar](100) NOT NULL,
	[DisplayName] [varchar](50) NULL,
	[DOB] [date] NULL,
	[Age] [decimal](5, 2) NOT NULL,
	[Gender] [varchar](50) NOT NULL,
	[DOJ] [date] NOT NULL,
	[EmploymentType] [varchar](50) NOT NULL,
	[EmployeeId] [varchar](30) NULL,
	[EmployeeType] [varchar](50) NULL,
	[Qualification] [varchar](50) NOT NULL,
	[Experience] [decimal](5, 1) NULL,
	[DesignationName] [varchar](50) NULL,
	[DepartmentId] [int] NOT NULL,
	[WorkLocation] [int] NULL,
	[HomeBranch] [int] NULL,
	[ManagerId] [int] NULL,
	[Salary] [decimal](15, 2) NULL,
	[Achievements] [varchar](1000) NULL,
	[MobileNo] [varchar](20) NULL,
	[LandLine] [varchar](20) NULL,
	[Email] [varchar](100) NULL,
	[EmerContactName] [varchar](100) NULL,
	[EmerContactNo] [varchar](20) NULL,
	[EmrRelation] [varchar](50) NULL,
	[PersAddressID] [int] NULL,
	[ProfAddressID] [int] NOT NULL,
	[AadhaarNo] [varchar](50) NULL,
	[PanNo] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](1000) NULL,
 CONSTRAINT [PK_tblEmployee] PRIMARY KEY CLUSTERED 
(
	[EID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblEmployee] ON
INSERT [dbo].[tblEmployee] ([EID], [Title], [FirstName], [LastName], [DisplayName], [DOB], [Age], [Gender], [DOJ], [EmploymentType], [EmployeeId], [EmployeeType], [Qualification], [Experience], [DesignationName], [DepartmentId], [WorkLocation], [HomeBranch], [ManagerId], [Salary], [Achievements], [MobileNo], [LandLine], [Email], [EmerContactName], [EmerContactNo], [EmrRelation], [PersAddressID], [ProfAddressID], [AadhaarNo], [PanNo], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (5, N'mr', N'Rajesh', N'Rahut', N'Rajeshkanna', CAST(0xCD060B00 AS Date), CAST(50.00 AS Decimal(5, 2)), N'Male', CAST(0x27240B00 AS Date), N'Permanent', N'too1', N'doctor', N'Post Graduation', CAST(2.6 AS Decimal(5, 1)), N'Sr.Doctor', 1, 1, 1, 2, CAST(10000.00 AS Decimal(15, 2)), N'no', N'9493849437', N'no', N'rr@gmail.com', NULL, NULL, N'father', NULL, 1, N'1234', N'654689', 1, 1, CAST(0x0000A67700000000 AS DateTime), NULL, CAST(0x0000A5E3004D3D7E AS DateTime), NULL)
INSERT [dbo].[tblEmployee] ([EID], [Title], [FirstName], [LastName], [DisplayName], [DOB], [Age], [Gender], [DOJ], [EmploymentType], [EmployeeId], [EmployeeType], [Qualification], [Experience], [DesignationName], [DepartmentId], [WorkLocation], [HomeBranch], [ManagerId], [Salary], [Achievements], [MobileNo], [LandLine], [Email], [EmerContactName], [EmerContactNo], [EmrRelation], [PersAddressID], [ProfAddressID], [AadhaarNo], [PanNo], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (6, N'MR', N'Mahesh', N'babu', N'maheshbabu', CAST(0x8CDD0A00 AS Date), CAST(66.00 AS Decimal(5, 2)), N'Male', CAST(0xC73A0B00 AS Date), N'Contract', N'abc1', N'doctor', N'Post Graduation', CAST(23.0 AS Decimal(5, 1)), N'Sr.Doctor', 2, 1, 1, 1, CAST(1000000.00 AS Decimal(15, 2)), N'yes', N'9989899899', N'255393', N'mb@mail.com', N'ambulance', N'100', N'Mother', NULL, 1, N'56541', N'bbe9g7633r', 1, 1, CAST(0x0000A61200000000 AS DateTime), NULL, CAST(0x0000A612001E10F6 AS DateTime), NULL)
INSERT [dbo].[tblEmployee] ([EID], [Title], [FirstName], [LastName], [DisplayName], [DOB], [Age], [Gender], [DOJ], [EmploymentType], [EmployeeId], [EmployeeType], [Qualification], [Experience], [DesignationName], [DepartmentId], [WorkLocation], [HomeBranch], [ManagerId], [Salary], [Achievements], [MobileNo], [LandLine], [Email], [EmerContactName], [EmerContactNo], [EmrRelation], [PersAddressID], [ProfAddressID], [AadhaarNo], [PanNo], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (7, N'MR', N'Anvesh', N'g', N'Anvesh', CAST(0xC3150B00 AS Date), CAST(26.00 AS Decimal(5, 2)), N'Male', CAST(0xDB3A0B00 AS Date), N'Temporary', N'111', N'Doctor', N'Post Graduation', CAST(3.0 AS Decimal(5, 1)), N'Jr.Doctor', 3, 1, 1, 1, CAST(20000.00 AS Decimal(15, 2)), N'no', N'8899889989', N'123456', N'ani@gmail.com', NULL, NULL, N'Son', NULL, 1, N'123', N'123', 1, 1, CAST(0x0000A60400000000 AS DateTime), NULL, CAST(0x0000A612001F8F73 AS DateTime), NULL)
INSERT [dbo].[tblEmployee] ([EID], [Title], [FirstName], [LastName], [DisplayName], [DOB], [Age], [Gender], [DOJ], [EmploymentType], [EmployeeId], [EmployeeType], [Qualification], [Experience], [DesignationName], [DepartmentId], [WorkLocation], [HomeBranch], [ManagerId], [Salary], [Achievements], [MobileNo], [LandLine], [Email], [EmerContactName], [EmerContactNo], [EmrRelation], [PersAddressID], [ProfAddressID], [AadhaarNo], [PanNo], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (8, N'Mr', N'srinu', N'ganta', N'sree', CAST(0x69160B00 AS Date), CAST(26.00 AS Decimal(5, 2)), N'Male', CAST(0xFB390B00 AS Date), N'Permanent', N'1456', N'Doctor', N'Post Graduate', CAST(5.0 AS Decimal(5, 1)), N'Sr.Doctor', 1, 1, 1, NULL, CAST(50000.00 AS Decimal(15, 2)), NULL, N'8522977344', NULL, NULL, NULL, NULL, NULL, NULL, 20, N'180023457', NULL, 1, 1, CAST(0x0000A62800F3B828 AS DateTime), 2, CAST(0x0000A6280105A8E2 AS DateTime), NULL)
INSERT [dbo].[tblEmployee] ([EID], [Title], [FirstName], [LastName], [DisplayName], [DOB], [Age], [Gender], [DOJ], [EmploymentType], [EmployeeId], [EmployeeType], [Qualification], [Experience], [DesignationName], [DepartmentId], [WorkLocation], [HomeBranch], [ManagerId], [Salary], [Achievements], [MobileNo], [LandLine], [Email], [EmerContactName], [EmerContactNo], [EmrRelation], [PersAddressID], [ProfAddressID], [AadhaarNo], [PanNo], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (13, N'Mr', N'ravikumar reddy', N'kandi', N'ravi', CAST(0x69160B00 AS Date), CAST(26.00 AS Decimal(5, 2)), N'Male', CAST(0xFB390B00 AS Date), N'Permanent', N'1456', N'Doctor', N'Post Graduate', CAST(5.0 AS Decimal(5, 1)), N'Sr.Doctor', 1, 1, 1, NULL, CAST(50000.00 AS Decimal(15, 2)), NULL, N'8522977344', NULL, NULL, NULL, NULL, NULL, NULL, 18, N'180023457', NULL, 1, 1, CAST(0x0000A64801142EA5 AS DateTime), 2, CAST(0x0000A648012489E1 AS DateTime), NULL)
INSERT [dbo].[tblEmployee] ([EID], [Title], [FirstName], [LastName], [DisplayName], [DOB], [Age], [Gender], [DOJ], [EmploymentType], [EmployeeId], [EmployeeType], [Qualification], [Experience], [DesignationName], [DepartmentId], [WorkLocation], [HomeBranch], [ManagerId], [Salary], [Achievements], [MobileNo], [LandLine], [Email], [EmerContactName], [EmerContactNo], [EmrRelation], [PersAddressID], [ProfAddressID], [AadhaarNo], [PanNo], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (16, N'Mr', N'Shree', N'Bal', N'sree', CAST(0x823B0B00 AS Date), CAST(25.00 AS Decimal(5, 2)), N'FmMale', CAST(0xFB390B00 AS Date), N'Permanent', N'1456', N'Doctor', N'Post Graduate', CAST(5.0 AS Decimal(5, 1)), N'Sr.Doctor', 1, 1, 1, NULL, CAST(50000.00 AS Decimal(15, 2)), NULL, N'8522977344', NULL, NULL, NULL, NULL, NULL, NULL, 19, N'543211', NULL, 1, 1, CAST(0x0000A6490110056A AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblEmployee] ([EID], [Title], [FirstName], [LastName], [DisplayName], [DOB], [Age], [Gender], [DOJ], [EmploymentType], [EmployeeId], [EmployeeType], [Qualification], [Experience], [DesignationName], [DepartmentId], [WorkLocation], [HomeBranch], [ManagerId], [Salary], [Achievements], [MobileNo], [LandLine], [Email], [EmerContactName], [EmerContactNo], [EmrRelation], [PersAddressID], [ProfAddressID], [AadhaarNo], [PanNo], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (19, N'Mr', N'Shree', N'Bal', N'sree', CAST(0x823B0B00 AS Date), CAST(25.00 AS Decimal(5, 2)), N'FmMale', CAST(0xFB390B00 AS Date), N'Permanent', N'1456', N'Doctor', N'Post Graduate', CAST(5.0 AS Decimal(5, 1)), N'Sr.Doctor', 1, 1, 1, NULL, CAST(50000.00 AS Decimal(15, 2)), NULL, N'8522977344', NULL, NULL, NULL, NULL, NULL, NULL, 4, N'543211', NULL, 1, 1, CAST(0x0000A649011A4409 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblEmployee] ([EID], [Title], [FirstName], [LastName], [DisplayName], [DOB], [Age], [Gender], [DOJ], [EmploymentType], [EmployeeId], [EmployeeType], [Qualification], [Experience], [DesignationName], [DepartmentId], [WorkLocation], [HomeBranch], [ManagerId], [Salary], [Achievements], [MobileNo], [LandLine], [Email], [EmerContactName], [EmerContactNo], [EmrRelation], [PersAddressID], [ProfAddressID], [AadhaarNo], [PanNo], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (20, N'Mr', N'Soubhagya', N'Reddyl', N'SR', CAST(0x4A050B00 AS Date), CAST(38.00 AS Decimal(5, 2)), N'Male', CAST(0xFB390B00 AS Date), N'Permanent', N'1456', N'Doctor', N'Post Graduate', CAST(5.0 AS Decimal(5, 1)), N'Sr.Doctor', 1, 1, 1, NULL, CAST(50000.00 AS Decimal(15, 2)), NULL, N'9875623456', NULL, NULL, NULL, NULL, NULL, NULL, 7, N'8967gh78', NULL, 1, 1, CAST(0x0000A649013013D4 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblEmployee] ([EID], [Title], [FirstName], [LastName], [DisplayName], [DOB], [Age], [Gender], [DOJ], [EmploymentType], [EmployeeId], [EmployeeType], [Qualification], [Experience], [DesignationName], [DepartmentId], [WorkLocation], [HomeBranch], [ManagerId], [Salary], [Achievements], [MobileNo], [LandLine], [Email], [EmerContactName], [EmerContactNo], [EmrRelation], [PersAddressID], [ProfAddressID], [AadhaarNo], [PanNo], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (21, N'MR', N'shankar', N'ch', NULL, NULL, CAST(26.00 AS Decimal(5, 2)), N'Male', CAST(0xAA3B0B00 AS Date), N'Contract', NULL, NULL, N'Betch', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, CAST(0x0000A64F010A2A23 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblEmployee] ([EID], [Title], [FirstName], [LastName], [DisplayName], [DOB], [Age], [Gender], [DOJ], [EmploymentType], [EmployeeId], [EmployeeType], [Qualification], [Experience], [DesignationName], [DepartmentId], [WorkLocation], [HomeBranch], [ManagerId], [Salary], [Achievements], [MobileNo], [LandLine], [Email], [EmerContactName], [EmerContactNo], [EmrRelation], [PersAddressID], [ProfAddressID], [AadhaarNo], [PanNo], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (22, N'MR', N'anves', N'g', N'ani', NULL, CAST(26.00 AS Decimal(5, 2)), N'Male', CAST(0xAA3B0B00 AS Date), N'Contract', NULL, NULL, N'Betch', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1, CAST(0x0000A64F010CC379 AS DateTime), 2, CAST(0x0000A64F010F0B77 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tblEmployee] OFF
/****** Object:  Table [dbo].[tblDrSettings]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDrSettings](
	[DrSettingsID] [int] IDENTITY(1,1) NOT NULL,
	[DoctorID] [int] NULL,
	[SpecialityID] [int] NULL,
	[OPFee] [decimal](18, 2) NULL,
	[IPFee] [decimal](18, 2) NULL,
	[Qualification] [varchar](200) NULL,
	[IsHead] [varchar](50) NULL,
	[Membership] [varchar](1000) NULL,
	[Awards] [varchar](1000) NULL,
	[CertificationDate] [datetime] NULL,
	[CertificationExpires] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[OpSlotTime] [time](7) NULL,
	[AvgSlotTime] [time](7) NULL,
 CONSTRAINT [PK_tblDrSettings] PRIMARY KEY CLUSTERED 
(
	[DrSettingsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblDrSettings] ON
INSERT [dbo].[tblDrSettings] ([DrSettingsID], [DoctorID], [SpecialityID], [OPFee], [IPFee], [Qualification], [IsHead], [Membership], [Awards], [CertificationDate], [CertificationExpires], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [OpSlotTime], [AvgSlotTime]) VALUES (1, 5, 1, CAST(456.00 AS Decimal(18, 2)), CAST(230.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A5A600000000 AS DateTime), NULL, NULL, CAST(0x070008D6E8290000 AS Time), CAST(0x070040230E430000 AS Time))
INSERT [dbo].[tblDrSettings] ([DrSettingsID], [DoctorID], [SpecialityID], [OPFee], [IPFee], [Qualification], [IsHead], [Membership], [Awards], [CertificationDate], [CertificationExpires], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [OpSlotTime], [AvgSlotTime]) VALUES (2, 7, 1, CAST(500.00 AS Decimal(18, 2)), CAST(200.00 AS Decimal(18, 2)), N'MBBS', NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A65700D8B864 AS DateTime), NULL, NULL, CAST(0x0700A8E76F4B0000 AS Time), CAST(0x070010ACD1530000 AS Time))
INSERT [dbo].[tblDrSettings] ([DrSettingsID], [DoctorID], [SpecialityID], [OPFee], [IPFee], [Qualification], [IsHead], [Membership], [Awards], [CertificationDate], [CertificationExpires], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [OpSlotTime], [AvgSlotTime]) VALUES (3, 8, 1, CAST(496.00 AS Decimal(18, 2)), CAST(100.00 AS Decimal(18, 2)), N'MBBS', N'hghhnj', N'Yes', NULL, CAST(0x0000A65700000000 AS DateTime), CAST(0x0000A65800000000 AS DateTime), 1, 1, CAST(0x0000A65700DEF62E AS DateTime), 2, CAST(0x0000A6580116A9F1 AS DateTime), CAST(0x0700D088C3100000 AS Time), CAST(0x070068C461080000 AS Time))
INSERT [dbo].[tblDrSettings] ([DrSettingsID], [DoctorID], [SpecialityID], [OPFee], [IPFee], [Qualification], [IsHead], [Membership], [Awards], [CertificationDate], [CertificationExpires], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [OpSlotTime], [AvgSlotTime]) VALUES (4, 5, 1, CAST(200.00 AS Decimal(18, 2)), CAST(500.00 AS Decimal(18, 2)), N'RMP', NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A65801185203 AS DateTime), 2, CAST(0x0000A6580123F22A AS DateTime), CAST(0x070068C461080000 AS Time), CAST(0x07009CA6920C0000 AS Time))
SET IDENTITY_INSERT [dbo].[tblDrSettings] OFF
/****** Object:  Table [dbo].[tblDiagnosisComponent]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDiagnosisComponent](
	[ComponentID] [int] IDENTITY(1,1) NOT NULL,
	[DiagnosisID] [int] NOT NULL,
	[CompName] [varchar](100) NOT NULL,
	[Units] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblDiagnosisComponent] PRIMARY KEY CLUSTERED 
(
	[ComponentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [Uk_tblDiagnosisComponent] UNIQUE NONCLUSTERED 
(
	[DiagnosisID] ASC,
	[CompName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblDiagnosisComponent] ON
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 3, N'QUANTITY', N'ml', 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A50101278409 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 3, N'COLOUR', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A5010127A3B9 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 3, N'APPEARANCE', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A5010127CEB9 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 3, N'REACTION', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A5010127F434 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 3, N'SP GRAVITY', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A5010128198D AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 3, N'ALBUMIN', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A501012841F8 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 3, N'SUGAR', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A50101285B54 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 3, N'BIL SALTS', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A50101287BF5 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, 3, N'BILE PIGMNTS', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A50101289B7A AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 3, N'PUS CELLS', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A5010128C478 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, 3, N'RBC', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A5010128E13D AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, 3, N'EPITHELIAL CELLS', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A50101290835 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, 3, N'CASTS', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A50101292886 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, 3, N'CRYSTALS', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A50101294853 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, 3, N'OTHERS', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A50101296474 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, 9, N'P.AMIKACIN', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A501012A89A7 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, 9, N'P.GENTAMICIN', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A501012AAB7F AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (22, 9, N'p.CIPROFLOXACIN', NULL, 1, 1, CAST(0x0000A50000000000 AS DateTime), NULL, CAST(0x0000A5010147C74F AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (24, 9, N'P.OFLAXACIN', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200D64CB4 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (26, 9, N'P.CO TRIMOXAZOLE', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200D6A8A8 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (27, 9, N'P.CEFTAZIDIME', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200D6C902 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (28, 9, N'P.CEFEPIME', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200D6E610 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (29, 9, N'P.CEFOPERAZONE', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200D70DDF AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (30, 9, N'P.CEFOTAXIME', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200D7460A AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (31, 9, N'P.PIPERACILLIN TAZOBACTUM', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200D7A972 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (32, 9, N'P.IMIPENEM', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200D7C112 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (33, 9, N'P.MEROPENEM', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200D7D7A2 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (34, 9, N'P.AMOXICILLIN CLAVULANIC ACID', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200D9C106 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (35, 9, N'P.AZITHRONYCIN', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200DAA75F AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (38, 9, N'P.CEFAZOLIN', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200DB04E1 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40, 9, N'P.CEFEXIME', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200DB66D4 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (41, 9, N'P.LINAZOLID', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200DB826D AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (42, 9, N'P.VANCOMYCIN', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200DBA6EB AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (47, 9, N'S.AMIKACIN', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200DC1407 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (48, 9, N'S.GENTAMICIN', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E0C489 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (49, 9, N'S.CIPROFLOXACIN', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E0FBBE AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (50, 9, N'S.OFLAXACIN', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E1308C AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (51, 9, N'S.COTRIMOXAZOLE', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E156A1 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (53, 9, N'S.CEFTAZIDIME', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E19D0F AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (54, 9, N'S.CEFEPIME', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E1BB6E AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (55, 9, N'S.CEFOPERAZONE', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E1DF86 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (57, 9, N'S.CEFOTAXIME', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E21F5A AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (58, 9, N'S.PIPERACILLIN TAZOBACTUM', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E24998 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (59, 9, N'S.IMIPENEM', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E265C5 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (60, 9, N'S.MEROPENEM', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E28C7E AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (61, 9, N'S.AMOXICILLIN CLAVULANIC ACID', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E2B5D0 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (62, 9, N'S.AZITHRONYCIN', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E2CE22 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (63, 9, N'S.CEFAZOLIN', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E2EC34 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (65, 9, N'S.CEFEXIME', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E307DE AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (66, 9, N'S.LINAZOLID', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E3286D AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (67, 9, N'S.VANCOMYCIN', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E33E25 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (68, 5, N'HAEMOGLOBIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00ACFD55 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (71, 5, N'RBC COUNT', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00AD481C AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (72, 5, N'PCV', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00ADA3FF AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (73, 5, N'MCV', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00ADC8BA AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (74, 5, N'MCH', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00AE0914 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (75, 5, N'MCHC', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00AE5392 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (76, 5, N'TOTAL WBC COUNT', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00AFB6B5 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (77, 5, N'PLOYMORPHS', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00AFF3B9 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (82, 5, N'LYMPHOCYTES', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B04A1D AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (83, 5, N'EOSINOPHILS', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B07EB7 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (84, 5, N'MONOCYTES', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B09866 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (85, 5, N'BASOPHILS', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B0C87A AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (86, 5, N'ABSOLUTE EOSINOPHIL COUNT', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B119C7 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (87, 5, N'BLEEDING TIME', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B13EB0 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (92, 5, N'CLOTTING TIME', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B19C5F AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (93, 5, N'PLATELET SMEAR', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B1CAA0 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (94, 5, N'PERIPHERAL SMEAR', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B1FDCC AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (95, 5, N'ESR', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B21CF7 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (96, 7, N'R.B.S', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B37C0E AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (97, 7, N'SERUM CREATININE', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B3AAE6 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (98, 7, N'BLOOD UREA', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B3C948 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (99, 7, N'TOTAL BILIRUBIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B3FFF2 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (100, 7, N'DIRECT BILIRUBIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B42583 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (101, 7, N'IN DIRECT BILIRUBIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B4464A AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (102, 7, N'S.G.P.T', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B45A39 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (103, 7, N'S.G.O.T', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B473D7 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (104, 7, N'ALKALINE PHOSPHATASE', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B4A182 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (105, 7, N'TOTAL PROTEINS', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B4C009 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (106, 7, N'ALBUMIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B4D9B8 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (107, 7, N'GLOBULIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B50097 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (108, 7, N'A/G RATIO', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B56C15 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (109, 2, N'RANDOM BLOOD SUGAR', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B7D31B AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (110, 2, N'SERUM CREATINNE', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B7F82C AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (111, 2, N'TOTAL', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B80E87 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (112, 2, N'DIRECT', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B8260C AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (116, 11, N'HAEMOGLOBIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0103E847 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (118, 11, N'RBC COUNT', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01042A11 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (119, 11, N'PCV', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0104492B AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (120, 11, N'MCV', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B010464E3 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (121, 11, N'MCH', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01047BC6 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (122, 11, N'MCHC', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01049DAE AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (123, 11, N'TOTAL WBC COUNT', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0104B6AE AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (124, 11, N'PLOYMORPHS', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0104CDCB AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (125, 11, N'LYMPHOCYTES', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0104E758 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (126, 11, N'EOSINOPHILS', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01050306 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (127, 11, N'MONOCYTES', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0105242F AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (128, 11, N'BASOPHILS', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01053D23 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (129, 11, N'ABSOLUTE EOSINOPHIL COUNT', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01055980 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (130, 11, N'BLEEDING TIME', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B010574D9 AS DateTime))
GO
print 'Processed 100 total records'
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (131, 11, N'CLOTTING TIME', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01058B89 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (132, 11, N'PLATELET SMEAR', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0105A37B AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (133, 11, N'PERIPHERAL SMEAR', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0105BC19 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (135, 11, N'ESR', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0105D67E AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (136, 14, N'HAEMOGLOBIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B010FEB29 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (137, 14, N'RBC COUNT', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01100526 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (138, 14, N'PCV', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01101B17 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (139, 14, N'MCV', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0110411A AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (140, 14, N'MCH', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011055AC AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (141, 14, N'MCHC', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01106A0E AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (142, 14, N'TOTAL WBC COUNT', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011086C7 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (143, 14, N'PLOYMORPHS', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01109A9C AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (145, 14, N'LYMPHOCYTES', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0110B1EB AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (146, 14, N'EOSINOPHILS', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0110C8D6 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (147, 14, N'MONOCYTES', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0110E1FA AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (148, 14, N'BASOPHILS', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0110FA67 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (149, 14, N'ABSOLUTE EOSINOPHIL COUNT', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01111418 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (150, 14, N'BLEEDING TIME', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01112A65 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (151, 14, N'CLOTTING TIME', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01114058 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (153, 14, N'PLATELET SMEAR', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01115994 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (154, 14, N'PERIPHERAL SMEAR', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01116FFA AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (155, 14, N'ESR', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011184DA AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (156, 15, N'R.B.S', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0118598D AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (157, 15, N'SERUM CREATININE', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01186FBF AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (158, 15, N'BLOOD UREA', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01188951 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (159, 15, N'TOTAL BILIRUBIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01189D42 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (161, 15, N'DIRECT BILIRUBIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0118B268 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (162, 15, N'IN DIRECT BILIRUBIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0118C528 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (163, 15, N'S.G.P.T', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0118DAEB AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (164, 15, N'S.G.O.T', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0118F0C5 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (165, 15, N'ALKALINE PHOSPHATASE', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0119070D AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (166, 15, N'TOTAL PROTEINS', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01191B19 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (167, 15, N'ALBUMIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01192F1C AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (168, 15, N'GLOBULIN', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011953F8 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (169, 15, N'A/G RATIO', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01196843 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (170, 13, N'HIV 1 and 2', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01268BF0 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (171, 13, N'Serum Immunoglobulins-IgE', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0126E6C4 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (173, 16, N'HIV-1 AND HIV-2', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01278942 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (175, 16, N'HBs-Ag', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0127C3A1 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (176, 17, N'Sputum for Gram Stain', NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01288AE2 AS DateTime))
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (177, 18, N'P.Fin', NULL, 1, 1, CAST(0x0000A61F00D0215F AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (178, 18, N'Sodium', N'ml/ml', 1, 1, CAST(0x0000A622011DADE5 AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (179, 18, N'Potasim', N'ml/ml', 1, 1, CAST(0x0000A622011DADE6 AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (183, 18, N'Sodium1', N'ml/ml', 1, 1, CAST(0x0000A6220130FD18 AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (184, 18, N'Potasim1', N'ml/ml', 1, 1, CAST(0x0000A6220130FD5E AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (186, 18, N'Sodium2', N'ml/ml', 1, 1, CAST(0x0000A62201358180 AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagnosisComponent] ([ComponentID], [DiagnosisID], [CompName], [Units], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (187, 18, N'Potasim2', N'ml/ml', 1, 1, CAST(0x0000A622013581C5 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblDiagnosisComponent] OFF
/****** Object:  Table [dbo].[tblBBDonor]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBDonor](
	[DonorId] [int] IDENTITY(1,1) NOT NULL,
	[DonorName] [varchar](50) NULL,
	[Gender] [varchar](20) NOT NULL,
	[DOB] [date] NULL,
	[Age] [decimal](5, 2) NOT NULL,
	[Occupation] [varchar](50) NULL,
	[MaritalStatus] [varchar](50) NULL,
	[MobileNo] [varchar](20) NULL,
	[EmergencyCtName] [varchar](50) NULL,
	[EmergencyCtno] [varchar](20) NULL,
	[Email] [varchar](30) NULL,
	[ContactMode] [varchar](50) NULL,
	[DonationType] [varchar](50) NOT NULL,
	[BloodGroup] [nvarchar](50) NOT NULL,
	[DonorSource] [varchar](50) NOT NULL,
	[OldRefNo] [varchar](50) NULL,
	[AddressID] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblbbDonor] PRIMARY KEY CLUSTERED 
(
	[DonorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBDonor] ON
INSERT [dbo].[tblBBDonor] ([DonorId], [DonorName], [Gender], [DOB], [Age], [Occupation], [MaritalStatus], [MobileNo], [EmergencyCtName], [EmergencyCtno], [Email], [ContactMode], [DonationType], [BloodGroup], [DonorSource], [OldRefNo], [AddressID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'swati', N'female', NULL, CAST(26.00 AS Decimal(5, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Blood', N'O+', N'test', NULL, 1, 1, 1, CAST(0x0000A66000000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBDonor] ([DonorId], [DonorName], [Gender], [DOB], [Age], [Occupation], [MaritalStatus], [MobileNo], [EmergencyCtName], [EmergencyCtno], [Email], [ContactMode], [DonationType], [BloodGroup], [DonorSource], [OldRefNo], [AddressID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Ram', N'male', NULL, CAST(26.00 AS Decimal(5, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Voluntary', N'O+', N'direct', NULL, NULL, 1, 1, CAST(0x0000A64A0113CA3D AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBDonor] ([DonorId], [DonorName], [Gender], [DOB], [Age], [Occupation], [MaritalStatus], [MobileNo], [EmergencyCtName], [EmergencyCtno], [Email], [ContactMode], [DonationType], [BloodGroup], [DonorSource], [OldRefNo], [AddressID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'kijl', N'male', NULL, CAST(28.00 AS Decimal(5, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Blood', N'O+', N'direct', NULL, NULL, 1, 1, CAST(0x0000A64A01161D6B AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBDonor] ([DonorId], [DonorName], [Gender], [DOB], [Age], [Occupation], [MaritalStatus], [MobileNo], [EmergencyCtName], [EmergencyCtno], [Email], [ContactMode], [DonationType], [BloodGroup], [DonorSource], [OldRefNo], [AddressID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'Sree', N'female', NULL, CAST(26.00 AS Decimal(5, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Blood', N'O+', N'test', NULL, NULL, 1, 1, CAST(0x0000A64F00DA4F94 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblBBDonor] OFF
/****** Object:  Table [dbo].[tblAppointment]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAppointment](
	[AppointmentID] [int] IDENTITY(1,1) NOT NULL,
	[PatientID] [int] NOT NULL,
	[DoctorID] [int] NOT NULL,
	[BranchID] [int] NULL,
	[DeptID] [int] NOT NULL,
	[TokenNo] [varchar](10) NULL,
	[PayStatus] [varchar](10) NULL,
	[AppointmentMode] [varchar](50) NOT NULL,
	[TokenStatus] [varchar](50) NULL,
	[AppointmentStatus] [varchar](50) NULL,
	[AppointmentType] [varchar](10) NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL,
	[ConsultFee] [decimal](10, 0) NOT NULL,
	[Discount] [decimal](10, 0) NULL,
	[PayMode] [varchar](20) NULL,
	[IsWaiveOff] [bit] NULL,
	[ReviewID] [int] NULL,
	[Comments] [varchar](250) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[TransID] [int] NULL,
 CONSTRAINT [PK_tblPatientAppointment] PRIMARY KEY CLUSTERED 
(
	[AppointmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblAppointment] ON
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (1, 5, 5, 1, 1, N'2', NULL, N'online', NULL, N'scheduled', N'Post', CAST(0x00009FD600000000 AS DateTime), CAST(0x0000A06100000000 AS DateTime), CAST(50 AS Decimal(10, 0)), NULL, N'cash', NULL, NULL, NULL, 1, 1, CAST(0x0000A61100000000 AS DateTime), NULL, CAST(0x0000A611005F8615 AS DateTime), NULL)
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (4, 6, 6, 1, 2, N'5', NULL, N'walkin', NULL, N'scheduled', N'Current', CAST(0x00009FD900000000 AS DateTime), CAST(0x00009FDA00000000 AS DateTime), CAST(50 AS Decimal(10, 0)), NULL, N'cash', NULL, NULL, NULL, 1, 1, CAST(0x0000A61200000000 AS DateTime), NULL, CAST(0x0000A6110177F86E AS DateTime), NULL)
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (5, 6, 13, 1, 3, N'6', NULL, N'walkin', NULL, N'waitlist', N'Past', CAST(0x00009FD900000000 AS DateTime), CAST(0x00009FF800000000 AS DateTime), CAST(100 AS Decimal(10, 0)), NULL, N'cash', NULL, NULL, NULL, 1, 1, CAST(0x0000A61200000000 AS DateTime), NULL, CAST(0x0000A611017841D7 AS DateTime), NULL)
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (11, 5, 5, 1, 2, NULL, NULL, N'online', NULL, N'expired', N'Current', CAST(0x00009FD900000000 AS DateTime), CAST(0x00009FF500000000 AS DateTime), CAST(100 AS Decimal(10, 0)), NULL, N'cash', NULL, NULL, NULL, 1, 1, CAST(0x0000A61200000000 AS DateTime), NULL, CAST(0x0000A612005DB031 AS DateTime), NULL)
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (15, 20, 7, 1, 1, NULL, NULL, N'153', NULL, NULL, N'Past', CAST(0x00009FD900000000 AS DateTime), CAST(0x00009FF500000000 AS DateTime), CAST(100 AS Decimal(10, 0)), NULL, N'cash', NULL, NULL, NULL, 1, 1, CAST(0x0000A61100000000 AS DateTime), NULL, CAST(0x0000A616006E19D8 AS DateTime), NULL)
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (16, 7, 7, 1, 3, NULL, N'paid', N'Walkin', NULL, NULL, N'Post', CAST(0x0000A62600000000 AS DateTime), CAST(0x0000A6DA00000000 AS DateTime), CAST(150 AS Decimal(10, 0)), NULL, N'cash', NULL, NULL, NULL, 1, 1, CAST(0x0000A61900EB2A4B AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (18, 20, 6, NULL, 1, NULL, N'Due', N'Online', NULL, NULL, NULL, CAST(0x0000A56C00000000 AS DateTime), CAST(0x0000A56D00000000 AS DateTime), CAST(300 AS Decimal(10, 0)), NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A61B013D78C3 AS DateTime), 3, CAST(0x0000A6260140D6D3 AS DateTime), NULL)
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (19, 20, 7, 2, 2, NULL, N'paid', N'Online', NULL, NULL, N'Post', CAST(0x0000A6DA00000000 AS DateTime), CAST(0x0000A6DA00000000 AS DateTime), CAST(150 AS Decimal(10, 0)), NULL, N'1', NULL, NULL, NULL, 1, 1, CAST(0x0000A61D010493B3 AS DateTime), NULL, CAST(0x0000A66E00C7EB90 AS DateTime), NULL)
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (21, 32, 8, 1, 3, N'O001', N'paid', N'Online', NULL, NULL, N'Past', CAST(0x0000A62600000000 AS DateTime), CAST(0x0000A6E400000000 AS DateTime), CAST(220 AS Decimal(10, 0)), NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A62600000000 AS DateTime), NULL, CAST(0x0000A66E00FCAC7A AS DateTime), NULL)
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (22, 7, 7, 1, 1, N'O001', N'paid', N'online', NULL, NULL, NULL, CAST(0x0000A61C00000000 AS DateTime), CAST(0x0000A6DA00000000 AS DateTime), CAST(150 AS Decimal(10, 0)), NULL, N'cash', NULL, NULL, NULL, 1, 1, CAST(0x0000A66100D20CA6 AS DateTime), NULL, CAST(0x0000A66E0122F1F7 AS DateTime), NULL)
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (23, 7, 7, 1, 1, N'W015', N'paid', N'walkin', NULL, NULL, NULL, CAST(0x0000A62600000000 AS DateTime), CAST(0x0000A6DA00000000 AS DateTime), CAST(150 AS Decimal(10, 0)), NULL, N'cash', NULL, NULL, NULL, 1, 1, CAST(0x0000A66100D60D8A AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblAppointment] ([AppointmentID], [PatientID], [DoctorID], [BranchID], [DeptID], [TokenNo], [PayStatus], [AppointmentMode], [TokenStatus], [AppointmentStatus], [AppointmentType], [StartDateTime], [EndDateTime], [ConsultFee], [Discount], [PayMode], [IsWaiveOff], [ReviewID], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TransID]) VALUES (30, 36, 7, 1, 1, N'W016', N'paid', N'Walkin', NULL, NULL, NULL, CAST(0x0000A62600000000 AS DateTime), CAST(0x0000A6DA00000000 AS DateTime), CAST(150 AS Decimal(10, 0)), NULL, N'cash', NULL, NULL, NULL, 1, 1, CAST(0x0000A66C00CE4F6C AS DateTime), NULL, CAST(0x0000A66E0126B0A1 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tblAppointment] OFF
/****** Object:  Table [dbo].[tblBBTransfusionRequest]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBTransfusionRequest](
	[RequestID] [int] IDENTITY(1,1) NOT NULL,
	[Patientid] [int] NOT NULL,
	[PatientType] [varchar](50) NULL,
	[BloodGroup] [nvarchar](50) NULL,
	[Component] [varchar](50) NULL,
	[NoOfUnits] [varchar](150) NULL,
	[Attachments] [varchar](250) NULL,
	[isCrossMatch] [bit] NULL,
	[RequestedBy] [varchar](50) NULL,
	[AssignTo] [varchar](50) NULL,
	[RequiredOn] [datetime] NULL,
	[ReqStatus] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblBBTranfusionRequest_1] PRIMARY KEY CLUSTERED 
(
	[RequestID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBTransfusionRequest] ON
INSERT [dbo].[tblBBTransfusionRequest] ([RequestID], [Patientid], [PatientType], [BloodGroup], [Component], [NoOfUnits], [Attachments], [isCrossMatch], [RequestedBy], [AssignTo], [RequiredOn], [ReqStatus], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, 5, N'permanent', N'O-', NULL, N'18', NULL, 1, N'2', N'VASU', CAST(0x0000A65A00000000 AS DateTime), N'Requested', 1, 1, CAST(0x0000A5A600000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBTransfusionRequest] ([RequestID], [Patientid], [PatientType], [BloodGroup], [Component], [NoOfUnits], [Attachments], [isCrossMatch], [RequestedBy], [AssignTo], [RequiredOn], [ReqStatus], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, 20, N'OP', N'O+', N'rrrrrr', N'16', N'Bill', 1, N'dfg', N'Ramya', CAST(0x0000A65A00000000 AS DateTime), N'PartiallyFulfilled', 1, 1, CAST(0x0000A65601290926 AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBTransfusionRequest] ([RequestID], [Patientid], [PatientType], [BloodGroup], [Component], [NoOfUnits], [Attachments], [isCrossMatch], [RequestedBy], [AssignTo], [RequiredOn], [ReqStatus], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, 20, N'OP', N'A-', N'rrrrrr', N'12', N'Bill', NULL, N'dfg', N'Ramya', CAST(0x0000A65A00000000 AS DateTime), N'Fulfilled', 1, 1, CAST(0x0000A6560129AD9D AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBTransfusionRequest] ([RequestID], [Patientid], [PatientType], [BloodGroup], [Component], [NoOfUnits], [Attachments], [isCrossMatch], [RequestedBy], [AssignTo], [RequiredOn], [ReqStatus], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (4, 8, NULL, N'A+', NULL, N'8', NULL, 1, N'vasu', NULL, CAST(0x0000A65A00000000 AS DateTime), N'Requested', 1, 1, CAST(0x0000A63D00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBTransfusionRequest] ([RequestID], [Patientid], [PatientType], [BloodGroup], [Component], [NoOfUnits], [Attachments], [isCrossMatch], [RequestedBy], [AssignTo], [RequiredOn], [ReqStatus], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (6, 7, NULL, N'B-', NULL, N'12', NULL, 1, NULL, NULL, CAST(0x0000A65A00000000 AS DateTime), N'Requested', 1, 1, CAST(0x0000A61F00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBTransfusionRequest] ([RequestID], [Patientid], [PatientType], [BloodGroup], [Component], [NoOfUnits], [Attachments], [isCrossMatch], [RequestedBy], [AssignTo], [RequiredOn], [ReqStatus], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (7, 6, NULL, N'B+', NULL, N'20', NULL, 1, NULL, NULL, CAST(0x0000A64C00000000 AS DateTime), N'Requested', 1, 1, CAST(0x0000A61F00000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblBBTransfusionRequest] OFF
/****** Object:  Table [dbo].[tblCallRegister]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCallRegister](
	[CallInteractionID] [int] IDENTITY(1,1) NOT NULL,
	[CallerName] [varchar](50) NULL,
	[ContactNo] [varchar](20) NULL,
	[CallingFrom] [varchar](50) NOT NULL,
	[Area] [varchar](50) NOT NULL,
	[CallType] [varchar](50) NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[CallDetails] [varchar](50) NULL,
	[CallStartDt] [datetime] NULL,
	[CallEndDt] [datetime] NULL,
	[PatientId] [int] NOT NULL,
	[Remarks] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblCallRegister] PRIMARY KEY CLUSTERED 
(
	[CallInteractionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblCallRegister] ON
INSERT [dbo].[tblCallRegister] ([CallInteractionID], [CallerName], [ContactNo], [CallingFrom], [Area], [CallType], [DepartmentId], [CallDetails], [CallStartDt], [CallEndDt], [PatientId], [Remarks], [Status], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'rajesh', N'90943806670', N'hyderabad', N'sanat nagar', N'emergency', 2, NULL, CAST(0x0000A65500000000 AS DateTime), CAST(0x0000A65500000000 AS DateTime), 5, NULL, NULL, 1, 1, CAST(0x0000A65501030AEB AS DateTime), 2, CAST(0x0000A656012A306C AS DateTime))
INSERT [dbo].[tblCallRegister] ([CallInteractionID], [CallerName], [ContactNo], [CallingFrom], [Area], [CallType], [DepartmentId], [CallDetails], [CallStartDt], [CallEndDt], [PatientId], [Remarks], [Status], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (7, N'RAJU', N'90943806670', N'hyderabad', N'KPHP', N'emergency', 3, NULL, CAST(0x0000A65500000000 AS DateTime), CAST(0x0000A65500000000 AS DateTime), 6, NULL, NULL, 1, 1, CAST(0x0000A6550132B325 AS DateTime), 2, CAST(0x0000A6550135400F AS DateTime))
INSERT [dbo].[tblCallRegister] ([CallInteractionID], [CallerName], [ContactNo], [CallingFrom], [Area], [CallType], [DepartmentId], [CallDetails], [CallStartDt], [CallEndDt], [PatientId], [Remarks], [Status], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (12, N'shankar', N'987654233', N'chennai', N'T nagar', N'emergency', 2, NULL, NULL, NULL, 6, NULL, NULL, 1, 1, CAST(0x0000A6560123DE96 AS DateTime), NULL, NULL)
INSERT [dbo].[tblCallRegister] ([CallInteractionID], [CallerName], [ContactNo], [CallingFrom], [Area], [CallType], [DepartmentId], [CallDetails], [CallStartDt], [CallEndDt], [PatientId], [Remarks], [Status], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (13, N'shankar', N'987654233', N'chennai', N'T nagar', N'emergency', 2, NULL, NULL, NULL, 6, NULL, NULL, 1, 1, CAST(0x0000A6560128FA4F AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblCallRegister] OFF
/****** Object:  Table [dbo].[tblCustomer]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCustomer](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[PatientID] [int] NULL,
	[FirstName] [varchar](80) NOT NULL,
	[LastName] [varchar](20) NULL,
	[MobileNumber] [varchar](15) NULL,
	[Age] [decimal](5, 2) NULL,
	[Gender] [int] NULL,
	[AddressID] [int] NULL,
	[IsPatient] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblCustomer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblCustomer] ON
INSERT [dbo].[tblCustomer] ([CustomerID], [PatientID], [FirstName], [LastName], [MobileNumber], [Age], [Gender], [AddressID], [IsPatient], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 5, N'Ravi', N'gora', N'7895664231', CAST(23.00 AS Decimal(5, 2)), 7, NULL, 0, 1, 1, CAST(0x0000A5CA00000000 AS DateTime), NULL, CAST(0x0000A5CB0102F908 AS DateTime))
INSERT [dbo].[tblCustomer] ([CustomerID], [PatientID], [FirstName], [LastName], [MobileNumber], [Age], [Gender], [AddressID], [IsPatient], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 6, N'srinuvas', N'ganta', N'8522977322', CAST(25.00 AS Decimal(5, 2)), 7, NULL, 0, 1, 1, CAST(0x0000A5CA00000000 AS DateTime), NULL, CAST(0x0000A5CB01033993 AS DateTime))
INSERT [dbo].[tblCustomer] ([CustomerID], [PatientID], [FirstName], [LastName], [MobileNumber], [Age], [Gender], [AddressID], [IsPatient], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 8, N'venkat', N'syamala', N'8374271251', CAST(29.00 AS Decimal(5, 2)), 7, NULL, 0, 1, 1, CAST(0x0000A5CA00000000 AS DateTime), NULL, CAST(0x0000A5CB0103B855 AS DateTime))
INSERT [dbo].[tblCustomer] ([CustomerID], [PatientID], [FirstName], [LastName], [MobileNumber], [Age], [Gender], [AddressID], [IsPatient], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, 9, N'subbareddy', N'kandi', N'9154149943', NULL, 7, NULL, NULL, 1, 1, CAST(0x0000A5DC00BC4CCF AS DateTime), NULL, NULL)
INSERT [dbo].[tblCustomer] ([CustomerID], [PatientID], [FirstName], [LastName], [MobileNumber], [Age], [Gender], [AddressID], [IsPatient], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, 7, N'shankar', N'chukka', N'984812348', CAST(33.00 AS Decimal(5, 2)), 7, NULL, NULL, 1, 1, CAST(0x0000A5DC00CA368C AS DateTime), NULL, NULL)
INSERT [dbo].[tblCustomer] ([CustomerID], [PatientID], [FirstName], [LastName], [MobileNumber], [Age], [Gender], [AddressID], [IsPatient], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (19, 9, N'ramakrishna', N'ganta', N'9154149943', CAST(70.00 AS Decimal(5, 2)), 8, NULL, 1, 1, 1, CAST(0x0000A5ED00D6EA2B AS DateTime), NULL, NULL)
INSERT [dbo].[tblCustomer] ([CustomerID], [PatientID], [FirstName], [LastName], [MobileNumber], [Age], [Gender], [AddressID], [IsPatient], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, 8, N'padmavathi', N'kandi', N'8522988344', CAST(23.00 AS Decimal(5, 2)), NULL, NULL, 0, 1, 1, CAST(0x0000A60A01149F21 AS DateTime), NULL, NULL)
INSERT [dbo].[tblCustomer] ([CustomerID], [PatientID], [FirstName], [LastName], [MobileNumber], [Age], [Gender], [AddressID], [IsPatient], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (28, 10, N'narayana', N'syamala', N'8106057476', CAST(26.00 AS Decimal(5, 2)), 7, NULL, 1, 1, 1, CAST(0x0000A60D0138BD77 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblCustomer] OFF
/****** Object:  Table [dbo].[tblComponentValues]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblComponentValues](
	[CmpValID] [int] IDENTITY(1,1) NOT NULL,
	[ComponentID] [int] NULL,
	[ValName] [varchar](100) NULL,
	[Comments] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblComponentValues] PRIMARY KEY CLUSTERED 
(
	[CmpValID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [Uk_tblComponentValues] UNIQUE NONCLUSTERED 
(
	[ComponentID] ASC,
	[ValName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblComponentValues] ON
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 17, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E405D6 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 17, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E423DC AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 18, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E440EF AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 18, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E45B67 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 22, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E4D8FB AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, 22, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E530F9 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, 24, N'SENSITIVE', N'NULL ', 1, 1, CAST(0x0000A50200000000 AS DateTime), NULL, CAST(0x0000A50200E571A5 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, 24, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E70AE8 AS DateTime), NULL, CAST(0x0000A50200E70AE8 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, 26, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8775B AS DateTime), NULL, CAST(0x0000A50200E8775B AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, 26, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8775B AS DateTime), NULL, CAST(0x0000A50200E8775B AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, 27, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8775B AS DateTime), NULL, CAST(0x0000A50200E8775B AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, 27, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8775B AS DateTime), NULL, CAST(0x0000A50200E8775B AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, 28, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8775B AS DateTime), NULL, CAST(0x0000A50200E8775B AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (19, 28, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8775B AS DateTime), NULL, CAST(0x0000A50200E8775B AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, 29, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8775B AS DateTime), NULL, CAST(0x0000A50200E8775B AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (21, 29, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8775B AS DateTime), NULL, CAST(0x0000A50200E8775B AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (22, 30, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8775B AS DateTime), NULL, CAST(0x0000A50200E8775B AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (23, 30, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8775B AS DateTime), NULL, CAST(0x0000A50200E8775B AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (24, 31, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8A4D8 AS DateTime), NULL, CAST(0x0000A50200E8A4D8 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (25, 31, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8A4D8 AS DateTime), NULL, CAST(0x0000A50200E8A4D8 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (26, 32, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8A4D8 AS DateTime), NULL, CAST(0x0000A50200E8A4D8 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (27, 32, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8A4D8 AS DateTime), NULL, CAST(0x0000A50200E8A4D8 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (28, 33, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8A4D8 AS DateTime), NULL, CAST(0x0000A50200E8A4D8 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (29, 33, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8A4D8 AS DateTime), NULL, CAST(0x0000A50200E8A4D8 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (30, 34, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8A4D8 AS DateTime), NULL, CAST(0x0000A50200E8A4D8 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (31, 34, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8A4D8 AS DateTime), NULL, CAST(0x0000A50200E8A4D8 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (32, 35, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8A4D8 AS DateTime), NULL, CAST(0x0000A50200E8A4D8 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (33, 35, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8A4D8 AS DateTime), NULL, CAST(0x0000A50200E8A4D8 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (34, 38, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8FF9E AS DateTime), NULL, CAST(0x0000A50200E8FF9E AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (35, 38, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8FF9E AS DateTime), NULL, CAST(0x0000A50200E8FF9E AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (36, 40, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8FF9E AS DateTime), NULL, CAST(0x0000A50200E8FF9E AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (37, 40, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8FF9E AS DateTime), NULL, CAST(0x0000A50200E8FF9E AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (38, 41, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8FF9E AS DateTime), NULL, CAST(0x0000A50200E8FF9E AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (39, 41, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8FF9E AS DateTime), NULL, CAST(0x0000A50200E8FF9E AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40, 42, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8FF9E AS DateTime), NULL, CAST(0x0000A50200E8FF9E AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (41, 42, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8FF9E AS DateTime), NULL, CAST(0x0000A50200E8FF9E AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (42, 47, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E8FF9E AS DateTime), NULL, CAST(0x0000A50200E8FF9E AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (43, 47, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E8FF9E AS DateTime), NULL, CAST(0x0000A50200E8FF9E AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (44, 48, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9497F AS DateTime), NULL, CAST(0x0000A50200E9497F AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (45, 48, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9497F AS DateTime), NULL, CAST(0x0000A50200E9497F AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (46, 49, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9497F AS DateTime), NULL, CAST(0x0000A50200E9497F AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (47, 49, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9497F AS DateTime), NULL, CAST(0x0000A50200E9497F AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (48, 50, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9497F AS DateTime), NULL, CAST(0x0000A50200E9497F AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (49, 50, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9497F AS DateTime), NULL, CAST(0x0000A50200E9497F AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (50, 51, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9497F AS DateTime), NULL, CAST(0x0000A50200E9497F AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (51, 51, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9497F AS DateTime), NULL, CAST(0x0000A50200E9497F AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (52, 53, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9497F AS DateTime), NULL, CAST(0x0000A50200E9497F AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (53, 53, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9497F AS DateTime), NULL, CAST(0x0000A50200E9497F AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (54, 54, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E98205 AS DateTime), NULL, CAST(0x0000A50200E98205 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (55, 54, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E98205 AS DateTime), NULL, CAST(0x0000A50200E98205 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (56, 55, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E98205 AS DateTime), NULL, CAST(0x0000A50200E98205 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (57, 55, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E98205 AS DateTime), NULL, CAST(0x0000A50200E98205 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (58, 57, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E98205 AS DateTime), NULL, CAST(0x0000A50200E98205 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (59, 57, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E98205 AS DateTime), NULL, CAST(0x0000A50200E98205 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (60, 58, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E98205 AS DateTime), NULL, CAST(0x0000A50200E98205 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (61, 58, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E98205 AS DateTime), NULL, CAST(0x0000A50200E98205 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (62, 59, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E98205 AS DateTime), NULL, CAST(0x0000A50200E98205 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (63, 59, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E98205 AS DateTime), NULL, CAST(0x0000A50200E98205 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (64, 60, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9C043 AS DateTime), NULL, CAST(0x0000A50200E9C043 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (65, 60, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9C043 AS DateTime), NULL, CAST(0x0000A50200E9C043 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (66, 61, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9C043 AS DateTime), NULL, CAST(0x0000A50200E9C043 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (67, 61, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9C043 AS DateTime), NULL, CAST(0x0000A50200E9C043 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (68, 62, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9C043 AS DateTime), NULL, CAST(0x0000A50200E9C043 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (69, 62, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9C043 AS DateTime), NULL, CAST(0x0000A50200E9C043 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (70, 63, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9C043 AS DateTime), NULL, CAST(0x0000A50200E9C043 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (71, 63, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9C043 AS DateTime), NULL, CAST(0x0000A50200E9C043 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (72, 65, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9C043 AS DateTime), NULL, CAST(0x0000A50200E9C043 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (73, 65, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9C043 AS DateTime), NULL, CAST(0x0000A50200E9C043 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (74, 66, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9E747 AS DateTime), NULL, CAST(0x0000A50200E9E747 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (75, 66, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9E747 AS DateTime), NULL, CAST(0x0000A50200E9E747 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (76, 67, N'SENSITIVE', NULL, 1, 1, CAST(0x0000A50200E9E747 AS DateTime), NULL, CAST(0x0000A50200E9E747 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (77, 67, N'REACTIVE', NULL, 1, 1, CAST(0x0000A50200E9E747 AS DateTime), NULL, CAST(0x0000A50200E9E747 AS DateTime))
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (78, 11, N'Sensitive', NULL, 1, 1, CAST(0x0000A61F011DEE59 AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (79, 177, N'+Ve', N'test', 1, 1, CAST(0x0000A62201217F8D AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (80, 186, N'-Ve', N'test', 1, 1, CAST(0x0000A62201358182 AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (81, 186, N'+Ve', NULL, 1, 1, CAST(0x0000A62201358182 AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (82, 187, N'+Ve', N'test', 1, 1, CAST(0x0000A622013581C5 AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentValues] ([CmpValID], [ComponentID], [ValName], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (83, 187, N'-Ve', NULL, 1, 1, CAST(0x0000A622013581C5 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblComponentValues] OFF
/****** Object:  Table [dbo].[tblComponentStandardRanges]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblComponentStandardRanges](
	[CmpStdID] [int] IDENTITY(1,1) NOT NULL,
	[ComponentID] [int] NOT NULL,
	[Gender] [varchar](50) NULL,
	[MinAge] [int] NULL,
	[MaxAge] [int] NULL,
	[StandardRange] [varchar](max) NOT NULL,
	[MinRange] [decimal](12, 2) NULL,
	[MaxRange] [decimal](12, 2) NULL,
	[Comments] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblComponentStandardRange] PRIMARY KEY CLUSTERED 
(
	[CmpStdID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblComponentStandardRanges] ON
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 109, NULL, NULL, NULL, N'80-160 mg/dl', CAST(80.00 AS Decimal(12, 2)), CAST(160.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B89B48 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 110, N'male', NULL, NULL, N'0.7-1.3 mg/dl', CAST(0.70 AS Decimal(12, 2)), CAST(1.30 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B8E5B2 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 110, N'female', NULL, NULL, N'0.6-1.1 mg/dl', CAST(0.60 AS Decimal(12, 2)), CAST(1.10 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00B91E8F AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 111, NULL, NULL, NULL, N'01-1.0 mg/dl', CAST(1.00 AS Decimal(12, 2)), CAST(1.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00BAF1F5 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 55, NULL, NULL, NULL, N'80-160 mg/dl', CAST(80.00 AS Decimal(12, 2)), CAST(160.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00EDBDFD AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, 55, N'male', NULL, NULL, N'0.5-1.3 mg/dl', CAST(0.50 AS Decimal(12, 2)), CAST(1.30 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00EE085C AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 11, N'female', NULL, NULL, N'0.6-1.1 mg/dl', CAST(0.60 AS Decimal(12, 2)), CAST(1.10 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00EE706C AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, 98, NULL, NULL, NULL, N'15-40mg/dl', CAST(15.00 AS Decimal(12, 2)), CAST(40.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00EEAEEE AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, 112, NULL, NULL, NULL, N'upto 0.2 mg/dl', CAST(0.00 AS Decimal(12, 2)), CAST(0.20 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00F02570 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, 99, NULL, NULL, NULL, N'0.1-1.0 mg/dl', CAST(0.10 AS Decimal(12, 2)), CAST(1.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00F0795A AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, 100, NULL, NULL, NULL, N'upto 0.2 mg/dl', CAST(0.00 AS Decimal(12, 2)), CAST(0.20 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00F0ADD1 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (19, 102, NULL, NULL, NULL, N'10 to 40 IU/L', CAST(10.00 AS Decimal(12, 2)), CAST(40.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00F4D590 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, 103, NULL, NULL, NULL, N'8 to 20 IU/L', CAST(8.00 AS Decimal(12, 2)), CAST(20.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00F567E5 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (21, 104, NULL, NULL, NULL, N'35-150 IU/L', CAST(35.00 AS Decimal(12, 2)), CAST(150.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00F598EE AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (22, 105, NULL, NULL, NULL, N'6.4-7.8 gm/dl', CAST(6.40 AS Decimal(12, 2)), CAST(7.80 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00F5CD3B AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (23, 106, NULL, NULL, NULL, N'3.5-5.2 gm/dl', CAST(3.50 AS Decimal(12, 2)), CAST(5.20 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00F5F219 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (24, 107, NULL, NULL, NULL, N'2.3-3.5 gm/dl', CAST(2.30 AS Decimal(12, 2)), CAST(3.50 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00F623B2 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (25, 68, N'male', NULL, NULL, N'14.0-16.0gms%', CAST(14.00 AS Decimal(12, 2)), CAST(16.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00F8689E AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (26, 68, N'female', NULL, NULL, N'12.0-14.0gms%', CAST(12.00 AS Decimal(12, 2)), CAST(14.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00F8B406 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (35, 71, N'male', NULL, NULL, N'4.5-6.0 milli/Cu.mm', CAST(4.50 AS Decimal(12, 2)), CAST(6.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FA5CFE AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (36, 71, N'female', NULL, NULL, N'4.0-5.5 milli/Cu.mm', CAST(4.00 AS Decimal(12, 2)), CAST(5.50 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FA818C AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (37, 72, N'male', NULL, NULL, N'40-50%', CAST(40.00 AS Decimal(12, 2)), CAST(50.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FAB7AE AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (38, 72, N'female', NULL, NULL, N'36-47%', CAST(36.00 AS Decimal(12, 2)), CAST(47.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FAD23D AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (39, 73, NULL, NULL, NULL, N'76-96fl', CAST(76.00 AS Decimal(12, 2)), CAST(96.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FB1B5B AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40, 74, NULL, NULL, NULL, N'27-37 pg', CAST(27.00 AS Decimal(12, 2)), CAST(37.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FB4F49 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (41, 75, NULL, NULL, NULL, N'32-36%', CAST(32.00 AS Decimal(12, 2)), CAST(36.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FB8183 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (42, 76, NULL, NULL, NULL, N'4,000-11,000/Cu.mm', CAST(4000.00 AS Decimal(12, 2)), CAST(11000.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FBC153 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (43, 77, NULL, NULL, NULL, N'50-70%', CAST(50.00 AS Decimal(12, 2)), CAST(70.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FBE15B AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (44, 82, NULL, NULL, NULL, N'20-40%', CAST(20.00 AS Decimal(12, 2)), CAST(40.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FC0480 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (45, 83, NULL, NULL, NULL, N'01-04%', CAST(1.00 AS Decimal(12, 2)), CAST(4.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FC3320 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (46, 84, NULL, NULL, NULL, N'02-08%', CAST(2.00 AS Decimal(12, 2)), CAST(8.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FC58D2 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (47, 85, NULL, NULL, NULL, N'00-01%', CAST(0.00 AS Decimal(12, 2)), CAST(1.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FC7E4F AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (48, 86, NULL, NULL, NULL, N'40-440 cells', CAST(40.00 AS Decimal(12, 2)), CAST(440.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FC9DB4 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (50, 87, NULL, NULL, NULL, N'Upto - 5 min', CAST(0.00 AS Decimal(12, 2)), CAST(5.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FCE1C4 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (51, 92, NULL, NULL, NULL, N'2-8 min', CAST(2.00 AS Decimal(12, 2)), CAST(8.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FD13D1 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (52, 95, NULL, NULL, NULL, N'Upto 07 min', CAST(0.00 AS Decimal(12, 2)), CAST(7.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B00FD5197 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (53, 116, N'male', NULL, NULL, N'14.0-16.0gms%', CAST(14.00 AS Decimal(12, 2)), CAST(16.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B010763AC AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (54, 116, N'female', NULL, NULL, N'12.0-14.0gms%', CAST(12.00 AS Decimal(12, 2)), CAST(14.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01079932 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (55, 118, N'male', NULL, NULL, N'4.5-6.0 milli/Cu.mm', CAST(4.50 AS Decimal(12, 2)), CAST(6.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0107D5F4 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (56, 118, N'female', NULL, NULL, N'4.0-5.5 milli/Cu.mm', CAST(4.00 AS Decimal(12, 2)), CAST(5.50 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0107F481 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (57, 119, N'male', NULL, NULL, N'40-50%', CAST(40.00 AS Decimal(12, 2)), CAST(50.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01081DB2 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (58, 119, N'female', NULL, NULL, N'36-47%', CAST(36.00 AS Decimal(12, 2)), CAST(47.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01083762 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (59, 120, NULL, NULL, NULL, N'76-96fl', CAST(76.00 AS Decimal(12, 2)), CAST(96.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01089FFF AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (60, 121, NULL, NULL, NULL, N'27-37 pg', CAST(27.00 AS Decimal(12, 2)), CAST(37.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0108BC1D AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (61, 122, NULL, NULL, NULL, N'32-36%', CAST(32.00 AS Decimal(12, 2)), CAST(36.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0108D644 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (62, 123, NULL, NULL, NULL, N'4,000-11,000/Cu.mm', CAST(4000.00 AS Decimal(12, 2)), CAST(11000.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0108F78A AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (63, 124, NULL, NULL, NULL, N'50-70%', CAST(50.00 AS Decimal(12, 2)), CAST(70.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0109449C AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (64, 125, NULL, NULL, NULL, N'20-40%', CAST(20.00 AS Decimal(12, 2)), CAST(40.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01095F02 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (65, 126, NULL, NULL, NULL, N'01-04%', CAST(1.00 AS Decimal(12, 2)), CAST(4.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01097784 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (66, 127, NULL, NULL, NULL, N'02-08%', CAST(2.00 AS Decimal(12, 2)), CAST(8.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01099197 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (67, 128, NULL, NULL, NULL, N'00-01%', CAST(0.00 AS Decimal(12, 2)), CAST(1.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0109A6E9 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (68, 129, NULL, NULL, NULL, N'40-440 cells', CAST(40.00 AS Decimal(12, 2)), CAST(440.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0109BFC1 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (69, 130, NULL, NULL, NULL, N'Upto - 5 min', CAST(0.00 AS Decimal(12, 2)), CAST(5.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0109D791 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (70, 131, NULL, NULL, NULL, N'2-8 min', CAST(2.00 AS Decimal(12, 2)), CAST(8.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B010BB76C AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (71, 135, NULL, NULL, NULL, N'Upto 07 min', CAST(0.00 AS Decimal(12, 2)), CAST(7.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B010C0B93 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (73, 136, N'male', NULL, NULL, N'14.0-16.0gms%', CAST(14.00 AS Decimal(12, 2)), CAST(16.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01125221 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (74, 136, N'female', NULL, NULL, N'12.0-14.0gms%', CAST(12.00 AS Decimal(12, 2)), CAST(14.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01127774 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (76, 137, N'male', NULL, NULL, N'4.5-6.0 milli/Cu.mm', CAST(4.50 AS Decimal(12, 2)), CAST(6.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0112A0CB AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (77, 137, N'female', NULL, NULL, N'4.0-5.5 milli/Cu.mm', CAST(4.00 AS Decimal(12, 2)), CAST(5.50 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0112BA0F AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (78, 138, N'male', NULL, NULL, N'40-50%', CAST(40.00 AS Decimal(12, 2)), CAST(50.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0112E696 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (79, 138, N'female', NULL, NULL, N'36-47%', CAST(36.00 AS Decimal(12, 2)), CAST(47.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0112FF5E AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (81, 139, NULL, NULL, NULL, N'76-96fl', CAST(76.00 AS Decimal(12, 2)), CAST(96.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0113396B AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (82, 140, NULL, NULL, NULL, N'27-37 pg', CAST(27.00 AS Decimal(12, 2)), CAST(37.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01135449 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (83, 141, NULL, NULL, NULL, N'32-36%', CAST(32.00 AS Decimal(12, 2)), CAST(36.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01136C8E AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (85, 142, NULL, NULL, NULL, N'4,000-11,000/Cu.mm', CAST(4000.00 AS Decimal(12, 2)), CAST(11000.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01139669 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (86, 143, NULL, NULL, NULL, N'50-70%', CAST(50.00 AS Decimal(12, 2)), CAST(70.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0113AF6C AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (87, 145, NULL, NULL, NULL, N'20-40%', CAST(20.00 AS Decimal(12, 2)), CAST(40.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01141EC4 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (88, 146, NULL, NULL, NULL, N'01-04%', CAST(1.00 AS Decimal(12, 2)), CAST(4.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01145126 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (89, 147, NULL, NULL, NULL, N'02-08%', CAST(2.00 AS Decimal(12, 2)), CAST(8.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0114688E AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (90, 148, NULL, NULL, NULL, N'00-01%', CAST(0.00 AS Decimal(12, 2)), CAST(1.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011481DB AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (91, 149, NULL, NULL, NULL, N'40-440 cells', CAST(40.00 AS Decimal(12, 2)), CAST(440.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0114BA8E AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (92, 150, NULL, NULL, NULL, N'Upto - 5 min', CAST(0.00 AS Decimal(12, 2)), CAST(5.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B0114E10B AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (93, 151, NULL, NULL, NULL, N'2-8 min', CAST(2.00 AS Decimal(12, 2)), CAST(8.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011505A7 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (94, 155, NULL, NULL, NULL, N'Upto 07 min', CAST(0.00 AS Decimal(12, 2)), CAST(7.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B01154DAA AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (95, 156, NULL, NULL, NULL, N'80-160 mg/dl', CAST(80.00 AS Decimal(12, 2)), CAST(160.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011AA740 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (96, 157, N'male', NULL, NULL, N'0.5-1.3 mg/dl', CAST(0.50 AS Decimal(12, 2)), CAST(1.30 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011AD8DC AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (97, 157, N'female', NULL, NULL, N'0.6-1.1 mg/dl', CAST(0.60 AS Decimal(12, 2)), CAST(1.10 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011AF629 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (98, 158, NULL, NULL, NULL, N'15-40mg/dl', CAST(15.00 AS Decimal(12, 2)), CAST(40.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011B2AE1 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (100, 159, NULL, NULL, NULL, N'0.1-1.0 mg/dl', CAST(0.10 AS Decimal(12, 2)), CAST(1.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011B7653 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (103, 161, NULL, NULL, NULL, N'upto 0.2 mg/dl', CAST(0.00 AS Decimal(12, 2)), CAST(0.20 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011C2BC2 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (104, 163, NULL, NULL, NULL, N'10 to 40 IU/L', CAST(10.00 AS Decimal(12, 2)), CAST(40.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011D2B6D AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (106, 164, NULL, NULL, NULL, N'10 to 40 IU/L', CAST(10.00 AS Decimal(12, 2)), CAST(40.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011DC74D AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (107, 165, NULL, NULL, NULL, N'35-150 IU/L', CAST(35.00 AS Decimal(12, 2)), CAST(150.00 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011E4650 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (108, 166, NULL, NULL, NULL, N'6.4-7.8 gm/dl', CAST(6.40 AS Decimal(12, 2)), CAST(7.80 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011E64F1 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (109, 167, NULL, NULL, NULL, N'3.5-5.2 gm/dl', CAST(3.50 AS Decimal(12, 2)), CAST(5.20 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011EB3F0 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (110, 168, NULL, NULL, NULL, N'2.3-3.5 gm/dl', CAST(2.30 AS Decimal(12, 2)), CAST(3.50 AS Decimal(12, 2)), NULL, 1, 1, CAST(0x0000A51B00000000 AS DateTime), NULL, CAST(0x0000A51B011ED823 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (128, 168, NULL, NULL, NULL, N'1-2', NULL, NULL, NULL, 1, 1, CAST(0x0000A68900000000 AS DateTime), NULL, CAST(0x0000A6220062BB11 AS DateTime))
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (129, 183, NULL, NULL, NULL, N'1-2', NULL, NULL, NULL, 1, 1, CAST(0x0000A6220130FD1A AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (130, 183, NULL, NULL, NULL, N'1-2', NULL, NULL, NULL, 1, 1, CAST(0x0000A6220130FD1A AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (131, 184, NULL, NULL, NULL, N'1-2', NULL, NULL, NULL, 1, 1, CAST(0x0000A6220130FD5E AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (132, 184, NULL, NULL, NULL, N'1-2', NULL, NULL, NULL, 1, 1, CAST(0x0000A6220130FD5E AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (133, 186, NULL, NULL, NULL, N'1-2', NULL, NULL, NULL, 1, 1, CAST(0x0000A62201358184 AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (134, 186, NULL, NULL, NULL, N'1-2', NULL, NULL, NULL, 1, 1, CAST(0x0000A62201358184 AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (135, 187, NULL, NULL, NULL, N'1-2', NULL, NULL, NULL, 1, 1, CAST(0x0000A622013581C5 AS DateTime), NULL, NULL)
INSERT [dbo].[tblComponentStandardRanges] ([CmpStdID], [ComponentID], [Gender], [MinAge], [MaxAge], [StandardRange], [MinRange], [MaxRange], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (136, 187, NULL, NULL, NULL, N'1-2', NULL, NULL, NULL, 1, 1, CAST(0x0000A622013581C5 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblComponentStandardRanges] OFF
/****** Object:  Table [dbo].[tblEmpFamily]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmpFamily](
	[FamilyID] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NOT NULL,
	[Title] [varchar](20) NULL,
	[FirstName] [varchar](100) NOT NULL,
	[LastName] [varchar](100) NOT NULL,
	[MiddleName] [varchar](100) NULL,
	[Gender] [varchar](50) NOT NULL,
	[DOB] [date] NULL,
	[PhoneNo] [varchar](20) NULL,
	[MobileNo] [varchar](20) NULL,
	[Address1] [varchar](max) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Relationship] [varchar](50) NULL,
	[Age] [tinyint] NULL,
	[BloodGroup] [varchar](50) NOT NULL,
	[IsDependent] [bit] NULL,
	[IsNominee] [bit] NULL,
	[Occupation] [varchar](100) NULL,
	[IsDeath] [bit] NULL,
	[IsEmergencyContact] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblEmpFamily] PRIMARY KEY CLUSTERED 
(
	[FamilyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblEmpFamily] ON
INSERT [dbo].[tblEmpFamily] ([FamilyID], [EID], [Title], [FirstName], [LastName], [MiddleName], [Gender], [DOB], [PhoneNo], [MobileNo], [Address1], [City], [State], [Country], [Relationship], [Age], [BloodGroup], [IsDependent], [IsNominee], [Occupation], [IsDeath], [IsEmergencyContact], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (6, 5, N'mrs', N'Ani', N'g', NULL, N'male', CAST(0x7A080B00 AS Date), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'O+', NULL, NULL, NULL, NULL, NULL, 0, 1, CAST(0x0000A64200B99E04 AS DateTime), NULL, CAST(0x0000A64800F043C1 AS DateTime))
INSERT [dbo].[tblEmpFamily] ([FamilyID], [EID], [Title], [FirstName], [LastName], [MiddleName], [Gender], [DOB], [PhoneNo], [MobileNo], [Address1], [City], [State], [Country], [Relationship], [Age], [BloodGroup], [IsDependent], [IsNominee], [Occupation], [IsDeath], [IsEmergencyContact], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (7, 6, N'mr', N'shanku', N'f', NULL, N'female', CAST(0x20250B00 AS Date), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'ab', NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A6DA00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblEmpFamily] ([FamilyID], [EID], [Title], [FirstName], [LastName], [MiddleName], [Gender], [DOB], [PhoneNo], [MobileNo], [Address1], [City], [State], [Country], [Relationship], [Age], [BloodGroup], [IsDependent], [IsNominee], [Occupation], [IsDeath], [IsEmergencyContact], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (8, 7, N'hj', N'Anuska', N'Sharma', NULL, N'Famale', CAST(0xE7310B00 AS Date), N'34567819', N'9861034567', N'Maharastra', N'fff', N'ssss', N'INDIA', NULL, NULL, N'O+', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(0x0000A6470130CD4B AS DateTime), NULL, NULL)
INSERT [dbo].[tblEmpFamily] ([FamilyID], [EID], [Title], [FirstName], [LastName], [MiddleName], [Gender], [DOB], [PhoneNo], [MobileNo], [Address1], [City], [State], [Country], [Relationship], [Age], [BloodGroup], [IsDependent], [IsNominee], [Occupation], [IsDeath], [IsEmergencyContact], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (9, 8, N'mk', N'hjj', N'das', NULL, N'male', CAST(0x87080B00 AS Date), N'0672345687', N'9734256234', N'Chennai', N'kkk', N'ssss', N'INDIA', NULL, NULL, N'AB+', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(0x0000A6470132805A AS DateTime), NULL, NULL)
INSERT [dbo].[tblEmpFamily] ([FamilyID], [EID], [Title], [FirstName], [LastName], [MiddleName], [Gender], [DOB], [PhoneNo], [MobileNo], [Address1], [City], [State], [Country], [Relationship], [Age], [BloodGroup], [IsDependent], [IsNominee], [Occupation], [IsDeath], [IsEmergencyContact], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (10, 5, N'mr', N'srinu', N'g', NULL, N'male', CAST(0x7A080B00 AS Date), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'O+', NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A64800EE43F9 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblEmpFamily] OFF
/****** Object:  Table [dbo].[tblEmpEducation]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmpEducation](
	[EduID] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NOT NULL,
	[GraduationType] [varchar](50) NULL,
	[EducationName] [varchar](100) NULL,
	[Branch] [varchar](100) NULL,
	[InstitutionName] [varchar](200) NOT NULL,
	[University] [varchar](200) NULL,
	[PassoutYear] [int] NOT NULL,
	[Percentage] [decimal](5, 2) NULL,
	[IsSpecialist] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblEmpEducation] PRIMARY KEY CLUSTERED 
(
	[EduID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblEmpBankDetails]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmpBankDetails](
	[EmpBankId] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NOT NULL,
	[BankId] [varchar](50) NOT NULL,
	[AccountNo] [varchar](20) NOT NULL,
	[Address1] [varchar](max) NULL,
	[City] [int] NULL,
	[IFSCode] [varchar](20) NULL,
	[Purpose] [varchar](100) NULL,
	[IsVarified] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](1000) NULL,
 CONSTRAINT [PK_tblEmpBankDetails] PRIMARY KEY CLUSTERED 
(
	[EmpBankId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblDiagRequisition]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDiagRequisition](
	[DiagReqID] [int] IDENTITY(1,1) NOT NULL,
	[PatientID] [int] NULL,
	[ReportDate] [datetime] NOT NULL,
	[DispatchedOn] [datetime] NULL,
	[ReportStatus] [varchar](50) NULL,
	[PaymentStatus] [varchar](50) NULL,
	[ReferredDoctor] [varchar](100) NULL,
	[Comments] [varchar](250) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[TotalAmount] [decimal](15, 2) NULL,
	[PaymentType] [varchar](50) NULL,
	[LabID] [int] NULL,
	[TreatmentID] [int] NULL,
 CONSTRAINT [PK_tblPatientRequisition] PRIMARY KEY CLUSTERED 
(
	[DiagReqID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblDiagRequisition] ON
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (3, 5, CAST(0x0000A50E00000000 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A51000000000 AS DateTime), NULL, CAST(0x0000A51100C8DA35 AS DateTime), NULL, N'card', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (4, 5, CAST(0x0000A50F00000000 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A51000000000 AS DateTime), NULL, CAST(0x0000A51100C9B9BE AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (5, 6, CAST(0x0000A51B010EABA3 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A51B010EABA3 AS DateTime), NULL, CAST(0x0000A51B010EABA3 AS DateTime), NULL, N'card', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (6, 8, CAST(0x0000A5220125320F AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A5220125320F AS DateTime), NULL, CAST(0x0000A5220125320F AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (7, 8, CAST(0x0000A523013BE21A AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013BE21A AS DateTime), NULL, CAST(0x0000A523013BE21A AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (8, 20, CAST(0x0000A523013F11C3 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013F11C3 AS DateTime), NULL, CAST(0x0000A523013F11C3 AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (9, 20, CAST(0x0000A523014BD19D AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014BD19D AS DateTime), NULL, CAST(0x0000A523014BD19D AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (10, 14, CAST(0x0000A523014C137E AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014C137E AS DateTime), NULL, CAST(0x0000A523014C137E AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (11, 5, CAST(0x0000A523014DB571 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014DB571 AS DateTime), NULL, CAST(0x0000A523014DB571 AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (12, 5, CAST(0x0000A5230152172A AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A5230152172A AS DateTime), NULL, CAST(0x0000A5230152172A AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (13, 6, CAST(0x0000A52400D387C4 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52400D387C4 AS DateTime), NULL, CAST(0x0000A52400D387C4 AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (14, 8, CAST(0x0000A52400D6E396 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52400D6E396 AS DateTime), NULL, CAST(0x0000A52400D6E396 AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (15, 14, CAST(0x0000A52400FD3B70 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52400FD3B70 AS DateTime), NULL, CAST(0x0000A52400FD3B70 AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (16, 20, CAST(0x0000A52600CE9E00 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52600CE9E00 AS DateTime), NULL, CAST(0x0000A52600CE9E00 AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (17, 20, CAST(0x0000A52600FDE349 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52600FDE349 AS DateTime), NULL, CAST(0x0000A52600FDE349 AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (18, 20, CAST(0x0000A52F007CAA59 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CAA59 AS DateTime), NULL, CAST(0x0000A52F007CAA59 AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (19, 8, CAST(0x0000A52F007CB03E AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB03E AS DateTime), NULL, CAST(0x0000A52F007CB03E AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (20, 6, CAST(0x0000A52F007CB095 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB095 AS DateTime), NULL, CAST(0x0000A52F007CB095 AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (21, 5, CAST(0x0000A52F007CB2DE AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB2DE AS DateTime), NULL, CAST(0x0000A52F007CB2DE AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (22, 5, CAST(0x0000A52F007CB3D5 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB3D5 AS DateTime), NULL, CAST(0x0000A52F007CB3D5 AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (23, 14, CAST(0x0000A52F007CB4BD AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB4BD AS DateTime), NULL, CAST(0x0000A52F007CB4BD AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (1018, 20, CAST(0x0000A5340034846D AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A5340034846D AS DateTime), NULL, CAST(0x0000A5340034846D AS DateTime), NULL, N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (2018, 8, CAST(0x0000A55C00F67223 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A55C00F67223 AS DateTime), NULL, CAST(0x0000A55C00F67223 AS DateTime), CAST(400.00 AS Decimal(15, 2)), N'card', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (2022, 6, CAST(0x0000A55C010672CF AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A55C010672CF AS DateTime), NULL, CAST(0x0000A55C010672CF AS DateTime), CAST(750.00 AS Decimal(15, 2)), N'card', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (2023, 5, CAST(0x0000A56000F9EA09 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A56000F9EA09 AS DateTime), NULL, CAST(0x0000A56000F9EA09 AS DateTime), CAST(300.00 AS Decimal(15, 2)), N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (2024, 6, CAST(0x0000A62B00000000 AS DateTime), NULL, NULL, NULL, N'ganga', NULL, 1, 1, CAST(0x0000A61A011A4484 AS DateTime), NULL, NULL, CAST(2500.00 AS Decimal(15, 2)), N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (2025, 7, CAST(0x0000A63000000000 AS DateTime), NULL, NULL, NULL, N'ramana', NULL, 1, 1, CAST(0x0000A61A01253E54 AS DateTime), NULL, NULL, CAST(2500.00 AS Decimal(15, 2)), N'cash', NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (2026, 20, CAST(0x0000A61D00000000 AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A61D01152DD5 AS DateTime), NULL, NULL, CAST(800.00 AS Decimal(15, 2)), NULL, NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (2027, 20, CAST(0x0000A61D00000000 AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A61D011669C5 AS DateTime), NULL, NULL, CAST(800.00 AS Decimal(15, 2)), NULL, NULL, NULL)
INSERT [dbo].[tblDiagRequisition] ([DiagReqID], [PatientID], [ReportDate], [DispatchedOn], [ReportStatus], [PaymentStatus], [ReferredDoctor], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TotalAmount], [PaymentType], [LabID], [TreatmentID]) VALUES (2029, 32, CAST(0x0000A61D00000000 AS DateTime), NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A61D01223154 AS DateTime), NULL, NULL, CAST(400.00 AS Decimal(15, 2)), N'cash', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblDiagRequisition] OFF
/****** Object:  Table [dbo].[tblDoctorSchedule]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDoctorSchedule](
	[DoctorSchID] [int] IDENTITY(1,1) NOT NULL,
	[DoctorID] [int] NOT NULL,
	[BranchID] [int] NOT NULL,
	[SlotDuration] [int] NOT NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[WorkingDays] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblDoctorSchedule] PRIMARY KEY CLUSTERED 
(
	[DoctorSchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblDoctorSchedule] ON
INSERT [dbo].[tblDoctorSchedule] ([DoctorSchID], [DoctorID], [BranchID], [SlotDuration], [StartDate], [EndDate], [WorkingDays], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 5, 2, 5, CAST(0x113A0B00 AS Date), CAST(0x303A0B00 AS Date), N'25', 1, 1, CAST(0x0000A6120116784B AS DateTime), NULL, NULL)
INSERT [dbo].[tblDoctorSchedule] ([DoctorSchID], [DoctorID], [BranchID], [SlotDuration], [StartDate], [EndDate], [WorkingDays], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 7, 2, 7, CAST(0x353C0B00 AS Date), CAST(0x353C0B00 AS Date), N'31', 1, 1, CAST(0x0000A61900F5F5DC AS DateTime), NULL, NULL)
INSERT [dbo].[tblDoctorSchedule] ([DoctorSchID], [DoctorID], [BranchID], [SlotDuration], [StartDate], [EndDate], [WorkingDays], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 6, 2, 4, CAST(0x4B3B0B00 AS Date), CAST(0x523B0B00 AS Date), N'22', 1, 1, CAST(0x0000A61B01401BE8 AS DateTime), NULL, CAST(0x0000A626014289B0 AS DateTime))
INSERT [dbo].[tblDoctorSchedule] ([DoctorSchID], [DoctorID], [BranchID], [SlotDuration], [StartDate], [EndDate], [WorkingDays], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 5, 2, 6, NULL, NULL, N'28', 1, 1, CAST(0x0000A61D0104F927 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblDoctorSchedule] OFF
/****** Object:  Table [dbo].[tblEmpPaySetting]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmpPaySetting](
	[PayID] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NOT NULL,
	[AccountNo] [varchar](30) NOT NULL,
	[BankName] [varchar](50) NOT NULL,
	[IFSCCode] [varchar](30) NOT NULL,
	[AccountType] [varchar](50) NOT NULL,
	[BranchName] [varchar](200) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifidBy] [int] NULL,
	[Purpose] [varchar](1000) NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblEmpPaySetting] PRIMARY KEY CLUSTERED 
(
	[PayID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblEmployeeBranchLog]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEmployeeBranchLog](
	[EmpBranchLogID] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NOT NULL,
	[HospitalBranchID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblEmployeeBranchLog] PRIMARY KEY CLUSTERED 
(
	[EmpBranchLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblEmployeeAccessbleBranchs]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEmployeeAccessbleBranchs](
	[EmpAccBranchID] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NOT NULL,
	[HospitalBranchID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblEmployeeAccessbleBranchs] PRIMARY KEY CLUSTERED 
(
	[EmpAccBranchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_tblEmployeeAccessbleBranchs] UNIQUE NONCLUSTERED 
(
	[EID] ASC,
	[HospitalBranchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblEmpSalarySettings]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEmpSalarySettings](
	[SID] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NOT NULL,
	[Salary] [money] NOT NULL,
	[FromMonth] [date] NOT NULL,
	[ToMonth] [date] NULL,
	[Basic] [money] NOT NULL,
	[HRA] [money] NOT NULL,
	[OtherAllowances] [money] NOT NULL,
	[Conveyance] [money] NOT NULL,
	[LTA] [money] NOT NULL,
	[ProfTax] [money] NOT NULL,
	[TDS] [money] NOT NULL,
	[PF] [money] NOT NULL,
	[Loan] [money] NOT NULL,
	[FoodCoupons] [money] NOT NULL,
	[TelephoneExpenses] [money] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[MedicalAllowances] [money] NULL,
 CONSTRAINT [PK_tblEmpSalarySettings] PRIMARY KEY CLUSTERED 
(
	[SID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblEmpProffesionalInfo]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmpProffesionalInfo](
	[ProffID] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NOT NULL,
	[Department] [varchar](50) NOT NULL,
	[Designation] [varchar](50) NOT NULL,
	[Qualification] [varchar](50) NULL,
	[Specialization] [varchar](50) NOT NULL,
	[Section] [varchar](50) NOT NULL,
	[Experience] [int] NULL,
	[EmpType] [varchar](50) NOT NULL,
	[EmployementType] [varchar](50) NULL,
	[WorkLocation] [varchar](50) NOT NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblProffesionalInfo] PRIMARY KEY CLUSTERED 
(
	[ProffID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblEmpProffesionalInfo] ON
INSERT [dbo].[tblEmpProffesionalInfo] ([ProffID], [EID], [Department], [Designation], [Qualification], [Specialization], [Section], [Experience], [EmpType], [EmployementType], [WorkLocation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 5, N'Ordinary', N'ASE', NULL, N'MS', N'hu', NULL, N'Nurse', N'Permanent', N'Hyd', 1, 1, CAST(0x0000A65B00000000 AS DateTime), NULL, CAST(0x0000A64800F2FC03 AS DateTime))
INSERT [dbo].[tblEmpProffesionalInfo] ([ProffID], [EID], [Department], [Designation], [Qualification], [Specialization], [Section], [Experience], [EmpType], [EmployementType], [WorkLocation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 6, N'Cardiology', N'SeniorDoctor', N'MBBS', N'Cardiologist', N'ASA', 12, N'Doctor', NULL, N'Hyd', 1, 1, CAST(0x0000A64800B7158E AS DateTime), NULL, NULL)
INSERT [dbo].[tblEmpProffesionalInfo] ([ProffID], [EID], [Department], [Designation], [Qualification], [Specialization], [Section], [Experience], [EmpType], [EmployementType], [WorkLocation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 6, N'Ordinary', N'ASE', NULL, N'MS', N'hu', NULL, N'Nurse', N'Permanent', N'Hyd', 1, 1, CAST(0x0000A64800F0E575 AS DateTime), NULL, NULL)
INSERT [dbo].[tblEmpProffesionalInfo] ([ProffID], [EID], [Department], [Designation], [Qualification], [Specialization], [Section], [Experience], [EmpType], [EmployementType], [WorkLocation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 19, N'Pedreatic', N'Sr.Doctor', N'MBBS', N'Cardiologist', N'ASA', 12, N'Doctor', NULL, N'Hyd', 1, 1, CAST(0x0000A649011A4409 AS DateTime), NULL, NULL)
INSERT [dbo].[tblEmpProffesionalInfo] ([ProffID], [EID], [Department], [Designation], [Qualification], [Specialization], [Section], [Experience], [EmpType], [EmployementType], [WorkLocation], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 20, N'Dermatologist', N'SeniorDoctor', N'MBBS', N'Cardiologist', N'ASA', 12, N'Doctor', NULL, N'Hyd', 1, 1, CAST(0x0000A649013012E3 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblEmpProffesionalInfo] OFF
/****** Object:  Table [dbo].[tblEmpPreviousExperience]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmpPreviousExperience](
	[ExpID] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NOT NULL,
	[CompanyName] [varchar](100) NOT NULL,
	[FromDate] [date] NOT NULL,
	[ToDate] [date] NOT NULL,
	[IsRelevantExp] [bit] NOT NULL,
	[Designation] [varchar](50) NULL,
	[ContactEmpName] [varchar](100) NULL,
	[ContactEmpNo] [varchar](20) NULL,
	[Salary] [decimal](10, 2) NOT NULL,
	[ReasonForLeaving] [varchar](200) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblEmpPreviousExperience] PRIMARY KEY CLUSTERED 
(
	[ExpID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblEmpPersonalInfo]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmpPersonalInfo](
	[PersID] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NOT NULL,
	[AddressLine] [varchar](200) NULL,
	[AddreesLine2] [varchar](200) NULL,
	[MobileNo] [varchar](20) NULL,
	[LandLine] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
	[Nationality] [varchar](50) NULL,
	[Religion] [varchar](50) NULL,
	[MaritalStatus] [varchar](50) NULL,
	[DateOfMarriage] [date] NULL,
	[City] [varchar](100) NOT NULL,
	[State] [varchar](100) NOT NULL,
	[Country] [varchar](100) NOT NULL,
	[Pincode] [varchar](20) NULL,
	[AadharNumber] [varchar](30) NULL,
	[PassPortNumber] [varchar](50) NULL,
	[PanCardNumber] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblEmpPersonalInfo] PRIMARY KEY CLUSTERED 
(
	[PersID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblEmpPersonalInfo] ON
INSERT [dbo].[tblEmpPersonalInfo] ([PersID], [EID], [AddressLine], [AddreesLine2], [MobileNo], [LandLine], [Email], [Nationality], [Religion], [MaritalStatus], [DateOfMarriage], [City], [State], [Country], [Pincode], [AadharNumber], [PassPortNumber], [PanCardNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 6, N'chennai', NULL, N'9094380830', NULL, N'srinu4use', N'Indian', N'Hindu', N'married', NULL, N'Chennai', N'TamilNadu', N'India', N'500043', N'608343212345', N'4567321', NULL, 1, 1, CAST(0x0000A642012F2499 AS DateTime), 2, CAST(0x0000A64201424133 AS DateTime))
INSERT [dbo].[tblEmpPersonalInfo] ([PersID], [EID], [AddressLine], [AddreesLine2], [MobileNo], [LandLine], [Email], [Nationality], [Religion], [MaritalStatus], [DateOfMarriage], [City], [State], [Country], [Pincode], [AadharNumber], [PassPortNumber], [PanCardNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 7, N'Pune', NULL, N'9861023456', N'06742470085', N'yyyy@gmail.com', N'Indian', NULL, N'UnMarried', NULL, N'Nelore', N'Chennai', N'INDI', N'500050', N'gyui678', N'NMTY56RT', N'BNGH678E', 1, 1, CAST(0x0000A6430138D5BF AS DateTime), 2, CAST(0x0000A64801103C8A AS DateTime))
INSERT [dbo].[tblEmpPersonalInfo] ([PersID], [EID], [AddressLine], [AddreesLine2], [MobileNo], [LandLine], [Email], [Nationality], [Religion], [MaritalStatus], [DateOfMarriage], [City], [State], [Country], [Pincode], [AadharNumber], [PassPortNumber], [PanCardNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 5, N'Bangolore', NULL, N'984812345', NULL, N'srinu4tgn@gamil.com', N'Indian', N'Hindu', N'married', CAST(0x4E2B0B00 AS Date), N'Hyederabad', N'Telangana', N'India', N'500042', N'608343212345', NULL, NULL, 1, 1, CAST(0x0000A64800C043D0 AS DateTime), 2, CAST(0x0000A64800C6335C AS DateTime))
INSERT [dbo].[tblEmpPersonalInfo] ([PersID], [EID], [AddressLine], [AddreesLine2], [MobileNo], [LandLine], [Email], [Nationality], [Religion], [MaritalStatus], [DateOfMarriage], [City], [State], [Country], [Pincode], [AadharNumber], [PassPortNumber], [PanCardNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, 8, N'Chennai', NULL, NULL, NULL, N'gggh@gmail.com', N'Indian', NULL, NULL, NULL, N'Chennai', N'TamilNadu', N'India', N'500043', N'5671234590', NULL, NULL, 1, 1, CAST(0x0000A64800000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblEmpPersonalInfo] ([PersID], [EID], [AddressLine], [AddreesLine2], [MobileNo], [LandLine], [Email], [Nationality], [Religion], [MaritalStatus], [DateOfMarriage], [City], [State], [Country], [Pincode], [AadharNumber], [PassPortNumber], [PanCardNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 5, N'Chennai', NULL, N'9861023456', N'06742470085', N'yyyy@gmail.com', N'Indian', NULL, N'UnMarried', CAST(0x373A0B00 AS Date), N'Nelore', N'Chennai', N'INDIA', N'500050', N'gyui678', N'NMTY56RT', N'BNGH678E', 1, 1, CAST(0x0000A648010139E8 AS DateTime), 2, NULL)
INSERT [dbo].[tblEmpPersonalInfo] ([PersID], [EID], [AddressLine], [AddreesLine2], [MobileNo], [LandLine], [Email], [Nationality], [Religion], [MaritalStatus], [DateOfMarriage], [City], [State], [Country], [Pincode], [AadharNumber], [PassPortNumber], [PanCardNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, 8, N'Chennai', NULL, N'9861023456', N'06742470085', N'yyyy@gmail.com', N'Indian', NULL, N'UnMarried', CAST(0x373A0B00 AS Date), N'Nelore', N'Chennai', N'INDIA', N'500050', N'gyui678', N'NMTY56RT', N'BNGH678E', 1, 1, CAST(0x0000A64801023A4F AS DateTime), 2, NULL)
INSERT [dbo].[tblEmpPersonalInfo] ([PersID], [EID], [AddressLine], [AddreesLine2], [MobileNo], [LandLine], [Email], [Nationality], [Religion], [MaritalStatus], [DateOfMarriage], [City], [State], [Country], [Pincode], [AadharNumber], [PassPortNumber], [PanCardNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, 6, N'Maesore', NULL, N'9861023456', N'06742470085', N'yyyy@gmail.com', N'Indian', NULL, N'Married', CAST(0x8A310B00 AS Date), N'Manglore', N'Karnatak', N'INDIA', N'500050', N'GHYU8678', N'GHTYU789	', N'HJIER56GHY', 1, 1, CAST(0x0000A64801055B02 AS DateTime), 2, NULL)
INSERT [dbo].[tblEmpPersonalInfo] ([PersID], [EID], [AddressLine], [AddreesLine2], [MobileNo], [LandLine], [Email], [Nationality], [Religion], [MaritalStatus], [DateOfMarriage], [City], [State], [Country], [Pincode], [AadharNumber], [PassPortNumber], [PanCardNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, 13, N'HYD', NULL, N'984812345', NULL, N'srinu4tgn@gamil.com', N'Indian', N'Hindu', N'married', NULL, N'Hyederabad', N'Telangana', N'India', N'500050', N'345678', NULL, NULL, 1, 1, CAST(0x0000A65600FEA17E AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblEmpPersonalInfo] OFF
/****** Object:  Table [dbo].[tblHospitalTowers]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblHospitalTowers](
	[TowerId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Hospitalid] [int] NOT NULL,
	[isActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](50) NULL,
 CONSTRAINT [PK_tblHospitalTowers] PRIMARY KEY CLUSTERED 
(
	[TowerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblHospitalTowers] ON
INSERT [dbo].[tblHospitalTowers] ([TowerId], [Name], [Type], [Hospitalid], [isActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (1, N'Ani', N'block', 4, 1, 1, CAST(0x0000A6DA00000000 AS DateTime), 1, CAST(0x0000A63900E754A4 AS DateTime), NULL)
INSERT [dbo].[tblHospitalTowers] ([TowerId], [Name], [Type], [Hospitalid], [isActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (2, N'jani', N'block', 4, 1, 1, CAST(0x0000A63900E56C26 AS DateTime), 1, CAST(0x0000A64F00BB86EC AS DateTime), NULL)
INSERT [dbo].[tblHospitalTowers] ([TowerId], [Name], [Type], [Hospitalid], [isActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (3, N'Anvesh', N'block', 4, 1, 1, CAST(0x0000A63900E9163B AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblHospitalTowers] ([TowerId], [Name], [Type], [Hospitalid], [isActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (4, N'sankar', N'block', 4, 1, 1, CAST(0x0000A64800EA3ED8 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblHospitalTowers] ([TowerId], [Name], [Type], [Hospitalid], [isActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (5, N'srinu', N'block', 4, 1, 1, CAST(0x0000A64F00B944EC AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblHospitalTowers] OFF
/****** Object:  Table [dbo].[tblLabSettings]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLabSettings](
	[LabValueID] [int] IDENTITY(1,1) NOT NULL,
	[LabID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Value] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblLabSettings] PRIMARY KEY CLUSTERED 
(
	[LabValueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblLeave]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLeave](
	[LID] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NULL,
	[Date] [datetime] NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[NumberOfDays] [float] NULL,
	[Reason] [nvarchar](500) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_tblLeave] PRIMARY KEY CLUSTERED 
(
	[LID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMedicinePurchase]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMedicinePurchase](
	[PurchaseID] [int] IDENTITY(1,1) NOT NULL,
	[SupplierID] [int] NOT NULL,
	[PurchaseDate] [date] NOT NULL,
	[BillNo] [varchar](20) NULL,
	[PharmacyID] [int] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[PurchaseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblMedicinePurchase] ON
INSERT [dbo].[tblMedicinePurchase] ([PurchaseID], [SupplierID], [PurchaseDate], [BillNo], [PharmacyID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 2, CAST(0x2B3B0B00 AS Date), N'12450', 1, 1, CAST(0x0000A5D000000000 AS DateTime), NULL, CAST(0x0000A5D400F9D2D8 AS DateTime))
INSERT [dbo].[tblMedicinePurchase] ([PurchaseID], [SupplierID], [PurchaseDate], [BillNo], [PharmacyID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 2, CAST(0x4D3B0B00 AS Date), N'12451', 1, 1, CAST(0x0000A5F200000000 AS DateTime), NULL, CAST(0x0000A5F200000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblMedicinePurchase] OFF
/****** Object:  Table [dbo].[tblPharmacySettings]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPharmacySettings](
	[PharmacyValueID] [int] IDENTITY(1,1) NOT NULL,
	[PharmacyID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Value] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblPharmacySettings] PRIMARY KEY CLUSTERED 
(
	[PharmacyValueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblWard]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblWard](
	[RoomId] [int] IDENTITY(1,1) NOT NULL,
	[RoomTypeId] [varchar](50) NOT NULL,
	[RoomName] [varchar](50) NOT NULL,
	[Cost] [money] NOT NULL,
	[BlockId] [int] NOT NULL,
	[HospitalBranchId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](1000) NULL,
 CONSTRAINT [PK_tblRooms] PRIMARY KEY CLUSTERED 
(
	[RoomId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblWard] ON
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (9, N'79', N'ward7', 2000.0000, 3, 4, 1, 0, CAST(0x0000A61500D19D54 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (10, N'78', N'ward6', 1000.0000, 3, 4, 1, 0, CAST(0x0000A61500D1B7BE AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (12, N'76', N'ward4', 1200.0000, 3, 4, 1, 0, CAST(0x0000A61500D1D40D AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (13, N'75', N'ward5', 1300.0000, 3, 4, 1, 0, CAST(0x0000A61500D1E255 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (14, N'74', N'ward2', 1400.0000, 3, 4, 1, 0, CAST(0x0000A61500D1F0BD AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (15, N'73', N'ward1', 1500.0000, 3, 4, 1, 0, CAST(0x0000A61500D1FF09 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (16, N'73', N'ward1', 1500.0000, 4, 4, 1, 0, CAST(0x0000A61500D21150 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (17, N'73', N'ward1', 1500.0000, 5, 4, 1, 0, CAST(0x0000A61500D21988 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (18, N'73', N'ward1', 1500.0000, 6, 4, 1, 0, CAST(0x0000A61500D2212A AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (19, N'73', N'ward1', 1500.0000, 7, 4, 1, 0, CAST(0x0000A61500D228EF AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (20, N'74', N'ward2', 1300.0000, 7, 4, 1, 0, CAST(0x0000A61500D262BA AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (21, N'74', N'ward2', 1300.0000, 6, 4, 1, 0, CAST(0x0000A61500D26985 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (22, N'74', N'ward2', 1300.0000, 5, 4, 1, 0, CAST(0x0000A61500D26F6F AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (23, N'74', N'ward2', 1300.0000, 4, 4, 1, 0, CAST(0x0000A61500D2758B AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (26, N'73', N'ward3', 1400.0000, 3, 4, 1, 0, CAST(0x0000A61500D3EE22 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (27, N'73', N'ward3', 1400.0000, 4, 4, 1, 0, CAST(0x0000A61500D3FA2D AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (28, N'73', N'ward3', 1400.0000, 5, 4, 1, 0, CAST(0x0000A61500D40386 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (29, N'73', N'ward3', 1400.0000, 6, 4, 1, 0, CAST(0x0000A61500D40978 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (30, N'73', N'ward3', 1400.0000, 7, 4, 1, 0, CAST(0x0000A61500D411FA AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (31, N'74', N'ward4', 1500.0000, 4, 4, 1, 0, CAST(0x0000A61500D42ABA AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (32, N'74', N'ward4', 1500.0000, 5, 4, 1, 0, CAST(0x0000A61500D43094 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (33, N'74', N'ward4', 1500.0000, 6, 4, 1, 0, CAST(0x0000A61500D43673 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (34, N'74', N'ward4', 1500.0000, 7, 4, 1, 0, CAST(0x0000A61500D46F13 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (35, N'75', N'ward5', 1600.0000, 4, 4, 1, 0, CAST(0x0000A61500D482B3 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (36, N'75', N'ward5', 1600.0000, 5, 4, 1, 0, CAST(0x0000A61500D48846 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (37, N'75', N'ward5', 1600.0000, 6, 4, 1, 0, CAST(0x0000A61500D48EF6 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (38, N'75', N'ward5', 1600.0000, 7, 4, 1, 0, CAST(0x0000A61500D494C1 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (39, N'76', N'ward6', 1800.0000, 4, 4, 1, 0, CAST(0x0000A61500D4A795 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (40, N'76', N'ward6', 1800.0000, 5, 4, 1, 0, CAST(0x0000A61500D4AD24 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (41, N'76', N'ward6', 1800.0000, 6, 4, 1, 0, CAST(0x0000A61500D4B2E6 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (42, N'76', N'ward6', 1800.0000, 7, 4, 1, 0, CAST(0x0000A61500D4B859 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (47, N'78', N'ward8', 2500.0000, 5, 4, 1, 0, CAST(0x0000A61500D4FF11 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (48, N'78', N'ward8', 2500.0000, 6, 4, 1, 0, CAST(0x0000A61500D50553 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (49, N'78', N'ward8', 2500.0000, 7, 4, 1, 0, CAST(0x0000A61500D50CA1 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (50, N'79', N'ward9', 2200.0000, 4, 4, 1, 0, CAST(0x0000A61500D51CA1 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (51, N'79', N'ward9', 2200.0000, 5, 4, 1, 0, CAST(0x0000A61500D52230 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (52, N'79', N'ward9', 2200.0000, 6, 4, 1, 0, CAST(0x0000A61500D5287D AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblWard] ([RoomId], [RoomTypeId], [RoomName], [Cost], [BlockId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (53, N'79', N'ward9', 2200.0000, 7, 4, 1, 0, CAST(0x0000A61500D52EDF AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblWard] OFF
/****** Object:  Table [dbo].[tblUser]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUser](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[RoleID] [int] NOT NULL,
	[EID] [int] NULL,
	[SecurityQuestion1] [varchar](200) NULL,
	[SecurityAnswer1] [varchar](100) NULL,
	[IsActive] [bit] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_tblUser] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblUser] ON
INSERT [dbo].[tblUser] ([UserID], [Name], [Password], [RoleID], [EID], [SecurityQuestion1], [SecurityAnswer1], [IsActive], [CreatedOn], [CreatedBy], [ModifiedOn], [ModifiedBy], [Comments]) VALUES (1, N'shankar', N'123', 1, 5, NULL, NULL, 1, CAST(0x0000A6590143D385 AS DateTime), 1, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblUser] OFF
/****** Object:  Table [dbo].[tblMedicineInvoice]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMedicineInvoice](
	[InvoiceID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceDate] [datetime2](0) NOT NULL,
	[ReferredDoctor] [varchar](100) NULL,
	[PatientID] [int] NOT NULL,
	[PharmacyID] [int] NULL,
	[TotalAmount] [decimal](10, 2) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime2](0) NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime2](0) NULL,
	[Consultation] [decimal](6, 2) NULL,
	[MedicineSuggestedID] [int] NULL,
 CONSTRAINT [PK_tblPatientInvoiceInfo] PRIMARY KEY CLUSTERED 
(
	[InvoiceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblMedicineInvoice] ON
INSERT [dbo].[tblMedicineInvoice] ([InvoiceID], [InvoiceDate], [ReferredDoctor], [PatientID], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Consultation], [MedicineSuggestedID]) VALUES (3, CAST(0x00241101C7390B0000 AS DateTime2), N'sreenu', 5, 1, CAST(60.00 AS Decimal(10, 2)), 1, CAST(0x00241101C7390B0000 AS DateTime2), NULL, CAST(0x00241101C7390B0000 AS DateTime2), NULL, NULL)
INSERT [dbo].[tblMedicineInvoice] ([InvoiceID], [InvoiceDate], [ReferredDoctor], [PatientID], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Consultation], [MedicineSuggestedID]) VALUES (7, CAST(0x00241101C7390B0000 AS DateTime2), N'anvesh', 6, 1, CAST(198.00 AS Decimal(10, 2)), 1, CAST(0x00241101C7390B0000 AS DateTime2), NULL, CAST(0x00EDA600343B0B0000 AS DateTime2), NULL, NULL)
INSERT [dbo].[tblMedicineInvoice] ([InvoiceID], [InvoiceDate], [ReferredDoctor], [PatientID], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Consultation], [MedicineSuggestedID]) VALUES (8, CAST(0x00241101C7390B0000 AS DateTime2), N'vasu', 8, 1, CAST(31.00 AS Decimal(10, 2)), 1, CAST(0x00000000C7390B0000 AS DateTime2), NULL, CAST(0x00000000C7390B0000 AS DateTime2), NULL, NULL)
INSERT [dbo].[tblMedicineInvoice] ([InvoiceID], [InvoiceDate], [ReferredDoctor], [PatientID], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Consultation], [MedicineSuggestedID]) VALUES (21, CAST(0x00F10B013A3B0B0000 AS DateTime2), N'sheshu', 9, 1, CAST(40.00 AS Decimal(10, 2)), 1, CAST(0x00310C013A3B0B0000 AS DateTime2), NULL, NULL, NULL, NULL)
INSERT [dbo].[tblMedicineInvoice] ([InvoiceID], [InvoiceDate], [ReferredDoctor], [PatientID], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Consultation], [MedicineSuggestedID]) VALUES (23, CAST(0x0023F900483B0B0000 AS DateTime2), N'sheshu', 10, 1, CAST(50.00 AS Decimal(10, 2)), 1, CAST(0x0023F900483B0B0000 AS DateTime2), NULL, NULL, NULL, NULL)
INSERT [dbo].[tblMedicineInvoice] ([InvoiceID], [InvoiceDate], [ReferredDoctor], [PatientID], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Consultation], [MedicineSuggestedID]) VALUES (24, CAST(0x000BA6005E3B0B0000 AS DateTime2), N'sheshu', 11, 1, CAST(40.00 AS Decimal(10, 2)), 1, CAST(0x000EA6005E3B0B0000 AS DateTime2), NULL, NULL, NULL, NULL)
INSERT [dbo].[tblMedicineInvoice] ([InvoiceID], [InvoiceDate], [ReferredDoctor], [PatientID], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Consultation], [MedicineSuggestedID]) VALUES (38, CAST(0x005B3E00963B0B0000 AS DateTime2), N'jhanu', 34, 1, CAST(60.00 AS Decimal(10, 2)), 1, CAST(0x005B3E00963B0B0000 AS DateTime2), NULL, NULL, NULL, NULL)
INSERT [dbo].[tblMedicineInvoice] ([InvoiceID], [InvoiceDate], [ReferredDoctor], [PatientID], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Consultation], [MedicineSuggestedID]) VALUES (51, CAST(0x007FD200A23B0B0000 AS DateTime2), N'naresh', 34, 1, CAST(80.00 AS Decimal(10, 2)), 1, CAST(0x0086D200A23B0B0000 AS DateTime2), NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblMedicineInvoice] OFF
/****** Object:  Table [dbo].[tblMedicineSalesReturn]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMedicineSalesReturn](
	[SalesReturnID] [int] IDENTITY(1,1) NOT NULL,
	[ReturnDate] [datetime] NOT NULL,
	[PaymentType] [int] NULL,
	[PharmacyID] [int] NULL,
	[TotalAmount] [money] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[SalesReturnID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblMedicineSalesReturn] ON
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, CAST(0x0000A5E000C4ED72 AS DateTime), 1, 1, 50.0000, 1, CAST(0x0000A5E000C4EEAB AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, CAST(0x0000A6A4012547E0 AS DateTime), 1, 1, 1250.0000, 1, CAST(0x0000A5ED012548B5 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, CAST(0x0000A60B00CA4735 AS DateTime), 1, 1, 1250.0000, 1, CAST(0x0000A60B00CA4955 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, CAST(0x0000A60B00D0DB85 AS DateTime), 1, 1, 1250.0000, 1, CAST(0x0000A60B00D0DB85 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, CAST(0x0000A60B01186116 AS DateTime), 1, 1, 500.0000, 1, CAST(0x0000A60B01186771 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, CAST(0x0000A60B011BD1AF AS DateTime), 1, 1, 500.0000, 1, CAST(0x0000A60B011BD1AF AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, CAST(0x0000A610014B2F1C AS DateTime), NULL, 1, 250.0000, 1, CAST(0x0000A610014B2F1C AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (21, CAST(0x0000A610014C6080 AS DateTime), 1, 1, 250.0000, 1, CAST(0x0000A610014C6080 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (22, CAST(0x0000A610014E79D6 AS DateTime), 1, 1, 250.0000, 1, CAST(0x0000A610014E7AEA AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (23, CAST(0x0000A61100A99726 AS DateTime), 1, 1, 250.0000, 1, CAST(0x0000A61100A9A950 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (24, CAST(0x0000A61900B8610D AS DateTime), NULL, 1, 333.0000, 1, CAST(0x0000A61900B86C04 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (27, CAST(0x0000A61B01248502 AS DateTime), NULL, 1, 44.0000, 1, CAST(0x0000A61B01248502 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturn] ([SalesReturnID], [ReturnDate], [PaymentType], [PharmacyID], [TotalAmount], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (28, CAST(0x0000A61D01028BE8 AS DateTime), NULL, 1, 44.0000, 1, CAST(0x0000A61D01028BE8 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblMedicineSalesReturn] OFF
/****** Object:  Table [dbo].[tblMedicineStockReturn]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMedicineStockReturn](
	[StockReturnID] [int] IDENTITY(1,1) NOT NULL,
	[ReturnDate] [datetime] NOT NULL,
	[PharmacyID] [int] NOT NULL,
	[TotalAmount] [money] NOT NULL,
	[IsActive] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblMedicineStockReturn] PRIMARY KEY CLUSTERED 
(
	[StockReturnID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblMedicineStockReturn] ON
INSERT [dbo].[tblMedicineStockReturn] ([StockReturnID], [ReturnDate], [PharmacyID], [TotalAmount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, CAST(0x0000A5E200000000 AS DateTime), 1, 500.0000, 1, 1, CAST(0x0000A30700000000 AS DateTime), NULL, CAST(0x0000A5E300513739 AS DateTime))
INSERT [dbo].[tblMedicineStockReturn] ([StockReturnID], [ReturnDate], [PharmacyID], [TotalAmount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, CAST(0x0000A6CF00000000 AS DateTime), 1, 600.0000, 1, 1, CAST(0x0000A5E300000000 AS DateTime), NULL, CAST(0x0000A5E300717C0C AS DateTime))
INSERT [dbo].[tblMedicineStockReturn] ([StockReturnID], [ReturnDate], [PharmacyID], [TotalAmount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, CAST(0x0000A60D01188120 AS DateTime), 1, 5000.0000, 1, 1, CAST(0x0000A60D01188120 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineStockReturn] ([StockReturnID], [ReturnDate], [PharmacyID], [TotalAmount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, CAST(0x0000A610013D4263 AS DateTime), 1, 25.0000, 1, 1, CAST(0x0000A610013D3DCF AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineStockReturn] ([StockReturnID], [ReturnDate], [PharmacyID], [TotalAmount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, CAST(0x0000A61100AD6F44 AS DateTime), 1, 25.0000, 1, 1, CAST(0x0000A61100AD6873 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineStockReturn] ([StockReturnID], [ReturnDate], [PharmacyID], [TotalAmount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, CAST(0x0000A61B01296295 AS DateTime), 1, 25.0000, 1, 0, CAST(0x0000A61B01296295 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineStockReturn] ([StockReturnID], [ReturnDate], [PharmacyID], [TotalAmount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, CAST(0x0000A61D0102F395 AS DateTime), 1, 25.0000, 1, 0, CAST(0x0000A61D0102F395 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblMedicineStockReturn] OFF
/****** Object:  Table [dbo].[tblOprAttndNurses]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblOprAttndNurses](
	[OprAttndNurseID] [int] IDENTITY(1,1) NOT NULL,
	[OperationID] [int] NOT NULL,
	[NurseID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblOprAttndNurses] PRIMARY KEY CLUSTERED 
(
	[OprAttndNurseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblOprAttndDoctors]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblOprAttndDoctors](
	[OprAttndDoctorID] [int] IDENTITY(1,1) NOT NULL,
	[OperationID] [int] NOT NULL,
	[DoctorID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblOprAttndDoctors] PRIMARY KEY CLUSTERED 
(
	[OprAttndDoctorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPatientInsurance]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPatientInsurance](
	[PatientInsuranceID] [int] IDENTITY(1,1) NOT NULL,
	[PatientID] [int] NOT NULL,
	[InsuranceName] [varchar](100) NOT NULL,
	[RelationToInsurd] [int] NULL,
	[CompanyName] [varchar](100) NOT NULL,
	[EffictiveFrom] [date] NOT NULL,
	[EffictiveTo] [date] NOT NULL,
	[Limit] [decimal](18, 0) NULL,
	[BankID] [int] NULL,
	[IFSCCode] [varchar](20) NULL,
	[AcctHolderName] [varchar](100) NULL,
	[AcctNumber] [varchar](50) NULL,
	[IsActive] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblPatientInsurance] PRIMARY KEY CLUSTERED 
(
	[PatientInsuranceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblPatientInsurance] ON
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 5, N'Life Insurance', NULL, N'sad', CAST(0x00000000 AS Date), CAST(0x00000000 AS Date), NULL, NULL, N'1234', N'sd', N'1234', 1, 1, CAST(0x0000A5C701247C5C AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 6, N'Insurance Name', NULL, N'sdf', CAST(0x00000000 AS Date), CAST(0x00000000 AS Date), NULL, NULL, N'df', N'12', N'123', 1, 1, CAST(0x0000A5CA00EB2119 AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 7, N'Insurance Name', NULL, N'sd', CAST(0x00000000 AS Date), CAST(0x00000000 AS Date), NULL, NULL, N'd', N'd', N'd', 1, 1, CAST(0x0000A5CA00ED605F AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 8, N'Life Insurance', NULL, N'sd', CAST(0x00000000 AS Date), CAST(0x00000000 AS Date), NULL, NULL, N'234', N'xcv', N'435678', 1, 1, CAST(0x0000A5CA00F44772 AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 9, N'Life Insurance', NULL, N'a', CAST(0x00000000 AS Date), CAST(0x00000000 AS Date), NULL, NULL, N'23', N'ss', N'23121', 1, 1, CAST(0x0000A5CA00F78A9B AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 10, N'LIC', 2, N'Aarushi', CAST(0x633B0B00 AS Date), CAST(0x633B0B00 AS Date), NULL, 22, NULL, NULL, NULL, 1, 1, CAST(0x0000A60B00F98CA2 AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 11, N'LIC', 2, N'Aarushi', CAST(0x633B0B00 AS Date), CAST(0x633B0B00 AS Date), NULL, 22, NULL, NULL, NULL, 1, 1, CAST(0x0000A60B00FBC5CE AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 12, N'LIC!', 1, N'Drama', CAST(0x683B0B00 AS Date), CAST(0x683B0B00 AS Date), NULL, 55, NULL, NULL, NULL, 1, 1, CAST(0x0000A60D00CF5848 AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, 26, N'pp', NULL, N'Relince', CAST(0x753B0B00 AS Date), CAST(0x773B0B00 AS Date), NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A61B00B43629 AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, 27, N'pp', NULL, N'Relince', CAST(0x753B0B00 AS Date), CAST(0x773B0B00 AS Date), NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A61B00BEBDC3 AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, 28, N'pp', NULL, N'Relince', CAST(0x753B0B00 AS Date), CAST(0x773B0B00 AS Date), NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A61B00C15AEE AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, 29, N'pp', NULL, N'Relince', CAST(0x753B0B00 AS Date), CAST(0x773B0B00 AS Date), NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A61B00C3858E AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientInsurance] ([PatientInsuranceID], [PatientID], [InsuranceName], [RelationToInsurd], [CompanyName], [EffictiveFrom], [EffictiveTo], [Limit], [BankID], [IFSCCode], [AcctHolderName], [AcctNumber], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, 30, N'pp', NULL, N'Relince', CAST(0x753B0B00 AS Date), CAST(0x773B0B00 AS Date), NULL, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A61B014207DE AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblPatientInsurance] OFF
/****** Object:  Table [dbo].[tblPatientDisease]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPatientDisease](
	[PatientDiseaseId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[DiseaseName] [varchar](20) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](1000) NULL,
 CONSTRAINT [PK_tblPatientDisease] PRIMARY KEY CLUSTERED 
(
	[PatientDiseaseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblPatientDisease] ON
INSERT [dbo].[tblPatientDisease] ([PatientDiseaseId], [PatientId], [DiseaseName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (2, 5, N'Malaria', 1, 1, CAST(0x0000A60C00000000 AS DateTime), NULL, CAST(0x0000A6110189CD8D AS DateTime), NULL)
INSERT [dbo].[tblPatientDisease] ([PatientDiseaseId], [PatientId], [DiseaseName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (3, 7, N'Malaria', 1, 1, CAST(0x0000A61200000000 AS DateTime), NULL, CAST(0x0000A6110189DBF8 AS DateTime), NULL)
INSERT [dbo].[tblPatientDisease] ([PatientDiseaseId], [PatientId], [DiseaseName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (4, 6, N'Cancer', 1, 1, CAST(0x0000A61200000000 AS DateTime), NULL, CAST(0x0000A611018A8233 AS DateTime), NULL)
INSERT [dbo].[tblPatientDisease] ([PatientDiseaseId], [PatientId], [DiseaseName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (5, 6, N'Malaria', 1, 1, CAST(0x0000A61200000000 AS DateTime), NULL, CAST(0x0000A611018A9132 AS DateTime), NULL)
INSERT [dbo].[tblPatientDisease] ([PatientDiseaseId], [PatientId], [DiseaseName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (6, 5, N'Cancer', 1, 1, CAST(0x0000A61200000000 AS DateTime), NULL, CAST(0x0000A61200256289 AS DateTime), NULL)
INSERT [dbo].[tblPatientDisease] ([PatientDiseaseId], [PatientId], [DiseaseName], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (8, 5, N'ColdFever', 1, 1, CAST(0x0000A61200000000 AS DateTime), NULL, CAST(0x0000A61200258424 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tblPatientDisease] OFF
/****** Object:  Table [dbo].[tblPatientContactInfo]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPatientContactInfo](
	[PatientContactInfoID] [int] IDENTITY(1,1) NOT NULL,
	[PatientID] [int] NOT NULL,
	[Address1] [varchar](250) NOT NULL,
	[Address2] [varchar](250) NULL,
	[City] [int] NOT NULL,
	[Landmark] [varchar](250) NULL,
	[Pincode] [varchar](20) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[AddressType] [int] NOT NULL,
	[IsActive] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblPatientContactInfo] PRIMARY KEY CLUSTERED 
(
	[PatientContactInfoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblPatientContactInfo] ON
INSERT [dbo].[tblPatientContactInfo] ([PatientContactInfoID], [PatientID], [Address1], [Address2], [City], [Landmark], [Pincode], [HomePhone], [WorkPhone], [AddressType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 5, N'sadf', N'dasf', 0, N'df', N'df', N'df', N'ddfs', 0, 1, 1, CAST(0x0000A5C701247A6F AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientContactInfo] ([PatientContactInfoID], [PatientID], [Address1], [Address2], [City], [Landmark], [Pincode], [HomePhone], [WorkPhone], [AddressType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 6, N'hyd', N'hyd1', 0, N'rsbrothers', N'', N'1234', N'1234', 0, 1, 1, CAST(0x0000A5CA00EB2119 AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientContactInfo] ([PatientContactInfoID], [PatientID], [Address1], [Address2], [City], [Landmark], [Pincode], [HomePhone], [WorkPhone], [AddressType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 7, N'f', N'f', 0, N'f', N'123', N'23', N'23', 0, 1, 1, CAST(0x0000A5CA00ED5F1B AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientContactInfo] ([PatientContactInfoID], [PatientID], [Address1], [Address2], [City], [Landmark], [Pincode], [HomePhone], [WorkPhone], [AddressType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 8, N'sdf', N'sdf', 0, N'sdf', N'sdf', N'df', N'df', 0, 1, 1, CAST(0x0000A5CA00F44772 AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientContactInfo] ([PatientContactInfoID], [PatientID], [Address1], [Address2], [City], [Landmark], [Pincode], [HomePhone], [WorkPhone], [AddressType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 9, N'hydebad', N'hhyd', 0, N'rr', N'3445', N'2344', N'234', 0, 1, 1, CAST(0x0000A5CA00F78A9B AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientContactInfo] ([PatientContactInfoID], [PatientID], [Address1], [Address2], [City], [Landmark], [Pincode], [HomePhone], [WorkPhone], [AddressType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 10, N'Hydrabad', N'Sanathnagar', 3, N'Near Rsbrothers', N'3333', N'6677889944', N'44444444', 3, 1, 1, CAST(0x0000A60B00F9880B AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientContactInfo] ([PatientContactInfoID], [PatientID], [Address1], [Address2], [City], [Landmark], [Pincode], [HomePhone], [WorkPhone], [AddressType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 11, N'Hydrabad', N'Sanathnagar', 3, N'Near Rsbrothers', N'3333', N'6677889944', N'44444444', 3, 1, 1, CAST(0x0000A60B00FBC420 AS DateTime), NULL, NULL)
INSERT [dbo].[tblPatientContactInfo] ([PatientContactInfoID], [PatientID], [Address1], [Address2], [City], [Landmark], [Pincode], [HomePhone], [WorkPhone], [AddressType], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 12, N'hyd1', N'rr', 1, NULL, NULL, NULL, NULL, 2, 1, 1, CAST(0x0000A60D00CF5775 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblPatientContactInfo] OFF
/****** Object:  Table [dbo].[tblTreatment]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTreatment](
	[TreatmentId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[ApntID] [int] NULL,
	[LabRequestID] [int] NULL,
	[AdmissionID] [int] NULL,
	[DoctorId] [int] NOT NULL,
	[TreatmentDate] [datetime] NULL,
	[DiscussionSummary] [varchar](1000) NULL,
	[Instruction] [varchar](1000) NOT NULL,
	[IsDiagnosisSuggested] [bit] NOT NULL,
	[IsMedicineSuggested] [bit] NOT NULL,
	[NextVisit] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](1000) NULL,
 CONSTRAINT [PK_tblTreatment] PRIMARY KEY CLUSTERED 
(
	[TreatmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblTreatment] ON
INSERT [dbo].[tblTreatment] ([TreatmentId], [PatientId], [ApntID], [LabRequestID], [AdmissionID], [DoctorId], [TreatmentDate], [DiscussionSummary], [Instruction], [IsDiagnosisSuggested], [IsMedicineSuggested], [NextVisit], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (2, 5, NULL, NULL, NULL, 5, CAST(0x0000A62400000000 AS DateTime), N'ghghj', N'daily', 1, 1, NULL, 1, 1, CAST(0x0000A62400000000 AS DateTime), NULL, CAST(0x0000A6240021C9BB AS DateTime), NULL)
INSERT [dbo].[tblTreatment] ([TreatmentId], [PatientId], [ApntID], [LabRequestID], [AdmissionID], [DoctorId], [TreatmentDate], [DiscussionSummary], [Instruction], [IsDiagnosisSuggested], [IsMedicineSuggested], [NextVisit], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (3, 6, NULL, NULL, NULL, 6, CAST(0x0000A62400000000 AS DateTime), N'ahfk', N'weekly', 1, 1, NULL, 1, 1, CAST(0x0000A62400000000 AS DateTime), NULL, CAST(0x0000A624002206F2 AS DateTime), NULL)
INSERT [dbo].[tblTreatment] ([TreatmentId], [PatientId], [ApntID], [LabRequestID], [AdmissionID], [DoctorId], [TreatmentDate], [DiscussionSummary], [Instruction], [IsDiagnosisSuggested], [IsMedicineSuggested], [NextVisit], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (17, 7, NULL, NULL, NULL, 7, CAST(0x0000A62400000000 AS DateTime), N'No dis', N'weekly', 1, 1, NULL, 1, 1, CAST(0x0000A625011449FE AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblTreatment] ([TreatmentId], [PatientId], [ApntID], [LabRequestID], [AdmissionID], [DoctorId], [TreatmentDate], [DiscussionSummary], [Instruction], [IsDiagnosisSuggested], [IsMedicineSuggested], [NextVisit], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (19, 8, NULL, NULL, NULL, 5, CAST(0x0000A62500000000 AS DateTime), N' dis', N'monthly', 1, 1, NULL, 1, 1, CAST(0x0000A6250122E1B3 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblTreatment] ([TreatmentId], [PatientId], [ApntID], [LabRequestID], [AdmissionID], [DoctorId], [TreatmentDate], [DiscussionSummary], [Instruction], [IsDiagnosisSuggested], [IsMedicineSuggested], [NextVisit], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (20, 5, NULL, NULL, NULL, 5, CAST(0x0000A62800000000 AS DateTime), N'discussion', N'instuction', 1, 1, NULL, 1, 1, CAST(0x0000A62800B56716 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblTreatment] ([TreatmentId], [PatientId], [ApntID], [LabRequestID], [AdmissionID], [DoctorId], [TreatmentDate], [DiscussionSummary], [Instruction], [IsDiagnosisSuggested], [IsMedicineSuggested], [NextVisit], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (21, 5, NULL, NULL, NULL, 5, CAST(0x0000A62800000000 AS DateTime), N'discussion', N'instuction', 1, 1, NULL, 1, 1, CAST(0x0000A62800B5675F AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblTreatment] ([TreatmentId], [PatientId], [ApntID], [LabRequestID], [AdmissionID], [DoctorId], [TreatmentDate], [DiscussionSummary], [Instruction], [IsDiagnosisSuggested], [IsMedicineSuggested], [NextVisit], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (22, 5, NULL, NULL, NULL, 5, CAST(0x0000A62800000000 AS DateTime), N'discussion1', N'instuction1', 1, 1, NULL, 1, 1, CAST(0x0000A62800B88739 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblTreatment] ([TreatmentId], [PatientId], [ApntID], [LabRequestID], [AdmissionID], [DoctorId], [TreatmentDate], [DiscussionSummary], [Instruction], [IsDiagnosisSuggested], [IsMedicineSuggested], [NextVisit], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (24, 5, NULL, NULL, NULL, 5, CAST(0x0000A62800000000 AS DateTime), N'give injction', N'instrct1', 1, 1, NULL, 1, 1, CAST(0x0000A6280101F4F2 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblTreatment] ([TreatmentId], [PatientId], [ApntID], [LabRequestID], [AdmissionID], [DoctorId], [TreatmentDate], [DiscussionSummary], [Instruction], [IsDiagnosisSuggested], [IsMedicineSuggested], [NextVisit], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (25, 5, NULL, NULL, NULL, 6, CAST(0x0000A64E00000000 AS DateTime), N'No dis', N'weekly', 1, 1, NULL, 1, 1, CAST(0x0000A64E00F56C13 AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblTreatment] OFF
/****** Object:  Table [dbo].[tblPatientSpecializationFieldResults]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPatientSpecializationFieldResults](
	[PatSpFieldResID] [int] IDENTITY(1,1) NOT NULL,
	[TreatmentID] [int] NOT NULL,
	[SpFieldID] [int] NOT NULL,
	[Result] [varchar](300) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblPatientSpecializationFieldResults] PRIMARY KEY CLUSTERED 
(
	[PatSpFieldResID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblDeptFieldResults]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblDeptFieldResults](
	[PatSpFieldResID] [int] IDENTITY(1,1) NOT NULL,
	[TreatmentID] [int] NOT NULL,
	[SpFieldID] [int] NOT NULL,
	[Result] [varchar](300) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblDeptFieldResults] PRIMARY KEY CLUSTERED 
(
	[PatSpFieldResID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblRooms]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblRooms](
	[RoomId] [int] IDENTITY(1,1) NOT NULL,
	[RoomTypeId] [varchar](50) NOT NULL,
	[RoomName] [varchar](50) NOT NULL,
	[Cost] [money] NOT NULL,
	[TowerId] [int] NOT NULL,
	[HospitalBranchId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](1000) NULL,
 CONSTRAINT [PK_tblRooms_1] PRIMARY KEY CLUSTERED 
(
	[RoomId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblRooms] ON
INSERT [dbo].[tblRooms] ([RoomId], [RoomTypeId], [RoomName], [Cost], [TowerId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (2, N'3', N'icu', 500.0000, 2, 1, 1, 1, CAST(0x0000A63900FDB36D AS DateTime), 1, CAST(0x0000A64F00C82A51 AS DateTime), NULL)
INSERT [dbo].[tblRooms] ([RoomId], [RoomTypeId], [RoomName], [Cost], [TowerId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (3, N'2', N'Test', 500.0000, 3, 1, 1, 1, CAST(0x0000A64300000000 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblRooms] ([RoomId], [RoomTypeId], [RoomName], [Cost], [TowerId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (5, N'4', N'emergency', 500.0000, 2, 1, 1, 1, CAST(0x0000A6150106D9F0 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblRooms] ([RoomId], [RoomTypeId], [RoomName], [Cost], [TowerId], [HospitalBranchId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (7, N'1', N'devil', 290.0000, 2, 1, 1, 1, CAST(0x0000A64F00BC710D AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblRooms] OFF
/****** Object:  Table [dbo].[tblPrescriptions]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPrescriptions](
	[PrescriptionID] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[MedicineName] [varchar](50) NULL,
	[MedicineType] [varchar](50) NULL,
	[Quantity] [nchar](10) NOT NULL,
	[Douse] [varchar](50) NULL,
	[Duration] [varchar](50) NULL,
	[Direction] [varchar](50) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblppp] PRIMARY KEY CLUSTERED 
(
	[PrescriptionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblPrescriptions] ON
INSERT [dbo].[tblPrescriptions] ([PrescriptionID], [AppointmentID], [ProductID], [MedicineName], [MedicineType], [Quantity], [Douse], [Duration], [Direction], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 5, N'BUSCOPAN', N'TAB', N'5         ', NULL, NULL, NULL, 1, CAST(0x0000A61300000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblPrescriptions] ([PrescriptionID], [AppointmentID], [ProductID], [MedicineName], [MedicineType], [Quantity], [Douse], [Duration], [Direction], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 4, 6, N'BAYCIP 500 MG', N'TAB', N'4         ', NULL, NULL, NULL, 1, CAST(0x0000A613012033D7 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblPrescriptions] OFF
/****** Object:  Table [dbo].[tblOpLabRequest]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblOpLabRequest](
	[OpLabRequestID] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentID] [int] NOT NULL,
	[LabTestID] [int] NOT NULL,
	[TestName] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblOplLabRequest] PRIMARY KEY CLUSTERED 
(
	[OpLabRequestID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblOpLabRequest] ON
INSERT [dbo].[tblOpLabRequest] ([OpLabRequestID], [AppointmentID], [LabTestID], [TestName], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 1, N'cardiology', NULL, 1, CAST(0x0001269900000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblOpLabRequest] ([OpLabRequestID], [AppointmentID], [LabTestID], [TestName], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 4, 3, N'Radiography', NULL, 1, CAST(0x0000A613012AD8A3 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblOpLabRequest] OFF
/****** Object:  Table [dbo].[tblMedicineSuggested]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMedicineSuggested](
	[MedicineSuggestedID] [int] IDENTITY(1,1) NOT NULL,
	[TreatmentID] [int] NOT NULL,
	[SuggestedDate] [datetime] NOT NULL,
	[Comments] [varchar](100) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblMedicineSuggested] PRIMARY KEY CLUSTERED 
(
	[MedicineSuggestedID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblMedicineStockReturnInfo]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMedicineStockReturnInfo](
	[StockReturnInfoID] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseID] [int] NOT NULL,
	[StockReturnID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[BatchNo] [varchar](20) NOT NULL,
	[Quantity] [int] NOT NULL,
	[Amount] [nchar](10) NOT NULL,
	[IsActive] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblMedicineStockReturnInfo] PRIMARY KEY CLUSTERED 
(
	[StockReturnInfoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblMedicineStockReturnInfo] ON
INSERT [dbo].[tblMedicineStockReturnInfo] ([StockReturnInfoID], [PurchaseID], [StockReturnID], [ProductID], [BatchNo], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 1, 1, 10, N'CY6789', 20, N'40        ', 1, 1, CAST(0x0000A30700000000 AS DateTime), 1, CAST(0x0000A5E300519FA7 AS DateTime))
INSERT [dbo].[tblMedicineStockReturnInfo] ([StockReturnInfoID], [PurchaseID], [StockReturnID], [ProductID], [BatchNo], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 1, 3, 8, N'PZ1517', 15, N'50        ', 1, 1, CAST(0x0000A6D700000000 AS DateTime), 1, CAST(0x0000A5E30071E8D8 AS DateTime))
INSERT [dbo].[tblMedicineStockReturnInfo] ([StockReturnInfoID], [PurchaseID], [StockReturnID], [ProductID], [BatchNo], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 1, 4, 5, N'PZ1517', 5, N'25        ', 1, 1, CAST(0x0000A60D01188DFB AS DateTime), 0, NULL)
INSERT [dbo].[tblMedicineStockReturnInfo] ([StockReturnInfoID], [PurchaseID], [StockReturnID], [ProductID], [BatchNo], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 1, 5, 8, N'PZ1517', 1, N'10        ', 1, 1, CAST(0x0000A610013DD140 AS DateTime), 0, NULL)
INSERT [dbo].[tblMedicineStockReturnInfo] ([StockReturnInfoID], [PurchaseID], [StockReturnID], [ProductID], [BatchNo], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, 1, 7, 10, N'CY6789', 1, N'10        ', 1, 0, CAST(0x0000A61B0129629C AS DateTime), 0, NULL)
INSERT [dbo].[tblMedicineStockReturnInfo] ([StockReturnInfoID], [PurchaseID], [StockReturnID], [ProductID], [BatchNo], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 1, 7, 8, N'PZ1517', 1, N'10        ', 1, 0, CAST(0x0000A61B0129629C AS DateTime), 0, NULL)
INSERT [dbo].[tblMedicineStockReturnInfo] ([StockReturnInfoID], [PurchaseID], [StockReturnID], [ProductID], [BatchNo], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, 1, 8, 10, N'CY6789', 1, N'10        ', 1, 0, CAST(0x0000A61D0102F395 AS DateTime), 0, NULL)
INSERT [dbo].[tblMedicineStockReturnInfo] ([StockReturnInfoID], [PurchaseID], [StockReturnID], [ProductID], [BatchNo], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, 1, 8, 8, N'PZ1517', 1, N'10        ', 1, 0, CAST(0x0000A61D0102F395 AS DateTime), 0, NULL)
SET IDENTITY_INSERT [dbo].[tblMedicineStockReturnInfo] OFF
/****** Object:  Table [dbo].[tblMedicineSalesReturnInfo]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMedicineSalesReturnInfo](
	[SalesReturnInfoID] [int] IDENTITY(1,1) NOT NULL,
	[SalesReturnID] [int] NOT NULL,
	[InvoiceID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[BatchNo] [varchar](20) NOT NULL,
	[PurchaseID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Amount] [money] NOT NULL,
	[IsActive] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblMedicineSalesReturnInfo] PRIMARY KEY CLUSTERED 
(
	[SalesReturnInfoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblMedicineSalesReturnInfo] ON
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 11, 3, 8, N'PZ1517', 1, 25, 1250.0000, 1, 1, CAST(0x0000A5E000C52CB3 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 13, 21, 10, N'CY6789', 1, 20, 1000.0000, 1, 1, CAST(0x0000A5ED01254FDF AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 14, 3, 8, N'PZ1517', 1, 25, 1250.0000, 1, 1, CAST(0x0000A60B00CA774A AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 15, 3, 8, N'PZ1517', 1, 25, 1250.0000, 1, 1, CAST(0x0000A60B00D0DBC9 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 16, 3, 8, N'PZ1517', 1, 25, 500.0000, 1, 1, CAST(0x0000A60B0118EBD9 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, 17, 3, 8, N'PZ1517', 1, 10, 500.0000, 1, 1, CAST(0x0000A60B011BD277 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, 20, 7, 8, N'PZ1517', 1, 10, 25.0000, 1, 1, CAST(0x0000A610014B319E AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, 21, 7, 8, N'PZ1517', 1, 16, 25.0000, 1, 1, CAST(0x0000A610014C6080 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, 22, 7, 8, N'PZ1517', 1, 16, 25.0000, 1, 1, CAST(0x0000A610014EA72A AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, 27, 8, 7, N'CY6789', 1, 321, 59.0000, 1, 0, CAST(0x0000A61B01248502 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, 27, 8, 9, N'PZ1517', 1, 450, 100.0000, 1, 0, CAST(0x0000A61B01248502 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (19, 28, 8, 7, N'CY6789', 1, 321, 59.0000, 1, 0, CAST(0x0000A61D01028BE8 AS DateTime), NULL, NULL)
INSERT [dbo].[tblMedicineSalesReturnInfo] ([SalesReturnInfoID], [SalesReturnID], [InvoiceID], [ProductID], [BatchNo], [PurchaseID], [Quantity], [Amount], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, 28, 8, 9, N'PZ1517', 1, 450, 100.0000, 1, 0, CAST(0x0000A61D01028BE8 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblMedicineSalesReturnInfo] OFF
/****** Object:  Table [dbo].[tblMedicinePurchaseInfo]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMedicinePurchaseInfo](
	[PurchaseInfoID] [int] IDENTITY(1,1) NOT NULL,
	[PurchaseID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[BatchNo] [varchar](20) NOT NULL,
	[Quantity] [int] NOT NULL,
	[FreeQuantity] [int] NOT NULL,
	[PackQuantity] [int] NULL,
	[PurchaseCostPerUnit] [decimal](10, 2) NULL,
	[SellingCostPerUnit] [decimal](10, 2) NULL,
	[PackPurchaseCost] [decimal](18, 2) NULL,
	[VatPercentage] [decimal](5, 2) NULL,
	[Discount] [decimal](18, 2) NULL,
	[ManufactureDate] [date] NOT NULL,
	[ExpiryDate] [date] NOT NULL,
	[IsActive] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblMedicinePurchaseInfo] PRIMARY KEY CLUSTERED 
(
	[PurchaseInfoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_tblMedicinePurchaseInfo] UNIQUE NONCLUSTERED 
(
	[PurchaseID] ASC,
	[ProductID] ASC,
	[BatchNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblMedicinePurchaseInfo] ON
INSERT [dbo].[tblMedicinePurchaseInfo] ([PurchaseInfoID], [PurchaseID], [ProductID], [BatchNo], [Quantity], [FreeQuantity], [PackQuantity], [PurchaseCostPerUnit], [SellingCostPerUnit], [PackPurchaseCost], [VatPercentage], [Discount], [ManufactureDate], [ExpiryDate], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 8, N'PZ1517', 25, 1, 10, CAST(25.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(10, 2)), CAST(250.00 AS Decimal(18, 2)), NULL, NULL, CAST(0x86390B00 AS Date), CAST(0x1D3D0B00 AS Date), 1, 1, CAST(0x0000A5D000000000 AS DateTime), NULL, CAST(0x0000A5D900EBFD19 AS DateTime))
INSERT [dbo].[tblMedicinePurchaseInfo] ([PurchaseInfoID], [PurchaseID], [ProductID], [BatchNo], [Quantity], [FreeQuantity], [PackQuantity], [PurchaseCostPerUnit], [SellingCostPerUnit], [PackPurchaseCost], [VatPercentage], [Discount], [ManufactureDate], [ExpiryDate], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 1, 10, N'CY6789', 10, 2, 5, CAST(10.00 AS Decimal(10, 2)), CAST(18.00 AS Decimal(10, 2)), CAST(50.00 AS Decimal(18, 2)), CAST(14.00 AS Decimal(5, 2)), NULL, CAST(0x423A0B00 AS Date), CAST(0xE03C0B00 AS Date), 1, 1, CAST(0x0000A5D000000000 AS DateTime), NULL, CAST(0x0000A5D401059A6B AS DateTime))
INSERT [dbo].[tblMedicinePurchaseInfo] ([PurchaseInfoID], [PurchaseID], [ProductID], [BatchNo], [Quantity], [FreeQuantity], [PackQuantity], [PurchaseCostPerUnit], [SellingCostPerUnit], [PackPurchaseCost], [VatPercentage], [Discount], [ManufactureDate], [ExpiryDate], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 1, 14, N'HUE8902', 20, 2, 10, CAST(8.00 AS Decimal(10, 2)), CAST(15.00 AS Decimal(10, 2)), CAST(80.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(5, 2)), NULL, CAST(0xE6390B00 AS Date), CAST(0x543B0B00 AS Date), 1, 1, CAST(0x0000A5D000000000 AS DateTime), NULL, CAST(0x0000A5D4010655F3 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblMedicinePurchaseInfo] OFF
/****** Object:  Table [dbo].[tblTreatmentVitalSigns]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTreatmentVitalSigns](
	[VitalsignsID] [int] IDENTITY(1,1) NOT NULL,
	[TreatmentID] [int] NOT NULL,
	[VitalSignType] [varchar](50) NOT NULL,
	[Value1] [float] NOT NULL,
	[Value2] [float] NULL,
	[Note] [varchar](500) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tblTreatmentVitalSigns] PRIMARY KEY CLUSTERED 
(
	[VitalsignsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblTreatmentVitalSigns] ON
INSERT [dbo].[tblTreatmentVitalSigns] ([VitalsignsID], [TreatmentID], [VitalSignType], [Value1], [Value2], [Note], [CreatedBy], [CreatedDate]) VALUES (3, 2, N'bp', 120, 80, N'test', 1, CAST(0x0000A6D400000000 AS DateTime))
INSERT [dbo].[tblTreatmentVitalSigns] ([VitalsignsID], [TreatmentID], [VitalSignType], [Value1], [Value2], [Note], [CreatedBy], [CreatedDate]) VALUES (5, 24, N'1', 120, 80, N'df', 1, CAST(0x0000A6280101F4F8 AS DateTime))
INSERT [dbo].[tblTreatmentVitalSigns] ([VitalsignsID], [TreatmentID], [VitalSignType], [Value1], [Value2], [Note], [CreatedBy], [CreatedDate]) VALUES (6, 24, N'2', 170, 80, N'df', 1, CAST(0x0000A6280101F4F8 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblTreatmentVitalSigns] OFF
/****** Object:  Table [dbo].[tblTreatmentMedicine]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTreatmentMedicine](
	[TrMedicineID] [int] IDENTITY(1,1) NOT NULL,
	[TreatmentID] [int] NULL,
	[MedicineID] [int] NULL,
	[MedicineName] [varchar](100) NOT NULL,
	[Dose] [varchar](50) NULL,
	[Quantity] [int] NOT NULL,
	[NoofDays] [int] NULL,
	[Directions] [varchar](100) NULL,
	[Advice] [varchar](100) NULL,
	[Comments] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblMedicineSuggestedDetails] PRIMARY KEY CLUSTERED 
(
	[TrMedicineID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblTreatmentMedicine] ON
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 2, 14, N'Morphine', N'2drops', 2, 7, N'daily', NULL, NULL, 1, 1, CAST(0x0000A62400000000 AS DateTime), NULL, CAST(0x0000A6240024BB6D AS DateTime))
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 3, 43, N'limcee', N'5grams', 3, 7, N'weekly', NULL, NULL, 1, 1, CAST(0x0000A62400000000 AS DateTime), NULL, CAST(0x0000A62400255926 AS DateTime))
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, 17, 18, N'Paracetmol', N'3drops', 5, 7, N'daily', NULL, NULL, 1, 1, CAST(0x0000A62501144A78 AS DateTime), 2, CAST(0x0000A625011DB16F AS DateTime))
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, 20, NULL, N'M1', NULL, 1, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A62800B5671A AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, 20, NULL, N'Male', NULL, 2, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A62800B5671A AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (19, 21, NULL, N'M1', NULL, 1, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A62800B5675F AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, 21, NULL, N'Male', NULL, 2, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A62800B5675F AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (21, 22, NULL, N'M3', NULL, 1, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A62800B88739 AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (22, 22, NULL, N'M4', NULL, 2, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A62800B88739 AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (25, 24, NULL, N'paracetmaol', NULL, 1, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A6280101F4F9 AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (26, 24, NULL, N'sistalad', NULL, 2, NULL, NULL, NULL, NULL, 1, 1, CAST(0x0000A6280101F4FA AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentMedicine] ([TrMedicineID], [TreatmentID], [MedicineID], [MedicineName], [Dose], [Quantity], [NoofDays], [Directions], [Advice], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (27, 25, 9, N'Salsalate', N'5drops', 8, 5, N'daily', NULL, NULL, 0, 1, CAST(0x0000A64E00F56C7A AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblTreatmentMedicine] OFF
/****** Object:  Table [dbo].[tblTreatmentDiagnosis]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTreatmentDiagnosis](
	[TrDiagnosisID] [int] IDENTITY(1,1) NOT NULL,
	[TreatmentID] [int] NOT NULL,
	[DiagnosisID] [int] NULL,
	[DiagnosisName] [varchar](100) NOT NULL,
	[InMandatory] [bit] NULL,
	[Priority] [int] NULL,
	[Comments] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblDiagnosisSuggestedInfo] PRIMARY KEY CLUSTERED 
(
	[TrDiagnosisID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblTreatmentDiagnosis] ON
INSERT [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID], [TreatmentID], [DiagnosisID], [DiagnosisName], [InMandatory], [Priority], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 2, 2, N'RBS', 1, NULL, NULL, 1, 1, CAST(0x0000A62400000000 AS DateTime), NULL, CAST(0x0000A624002276E1 AS DateTime))
INSERT [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID], [TreatmentID], [DiagnosisID], [DiagnosisName], [InMandatory], [Priority], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 3, 8, N'HIV', 1, NULL, NULL, 1, 1, CAST(0x0000A62400000000 AS DateTime), NULL, CAST(0x0000A62400229C02 AS DateTime))
INSERT [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID], [TreatmentID], [DiagnosisID], [DiagnosisName], [InMandatory], [Priority], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 19, 4, N'TYROID CAPSULE', 1, NULL, NULL, 1, 1, CAST(0x0000A6250122E1EA AS DateTime), 2, CAST(0x0000A6250134CA7B AS DateTime))
INSERT [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID], [TreatmentID], [DiagnosisID], [DiagnosisName], [InMandatory], [Priority], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 20, NULL, N'u1', NULL, NULL, NULL, 0, 1, CAST(0x0000A62800B56718 AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID], [TreatmentID], [DiagnosisID], [DiagnosisName], [InMandatory], [Priority], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 20, NULL, N'union', NULL, NULL, NULL, 0, 1, CAST(0x0000A62800B56718 AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID], [TreatmentID], [DiagnosisID], [DiagnosisName], [InMandatory], [Priority], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 21, NULL, N'u1', NULL, NULL, NULL, 0, 1, CAST(0x0000A62800B5675F AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID], [TreatmentID], [DiagnosisID], [DiagnosisName], [InMandatory], [Priority], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 21, NULL, N'union', NULL, NULL, NULL, 0, 1, CAST(0x0000A62800B5675F AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID], [TreatmentID], [DiagnosisID], [DiagnosisName], [InMandatory], [Priority], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, 22, NULL, N'u2', NULL, NULL, NULL, 0, 1, CAST(0x0000A62800B88739 AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID], [TreatmentID], [DiagnosisID], [DiagnosisName], [InMandatory], [Priority], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 22, NULL, N'union3', NULL, NULL, NULL, 0, 1, CAST(0x0000A62800B88739 AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID], [TreatmentID], [DiagnosisID], [DiagnosisName], [InMandatory], [Priority], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, 24, NULL, N'union', NULL, NULL, NULL, 0, 1, CAST(0x0000A6280101F4F6 AS DateTime), NULL, NULL)
INSERT [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID], [TreatmentID], [DiagnosisID], [DiagnosisName], [InMandatory], [Priority], [Comments], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, 24, NULL, N'Thyriod', NULL, NULL, NULL, 0, 1, CAST(0x0000A6280101F4F6 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblTreatmentDiagnosis] OFF
/****** Object:  Table [dbo].[tblMedicineInvoicePayMode]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMedicineInvoicePayMode](
	[InvoicePayModeID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceID] [int] NOT NULL,
	[PaymentMode] [int] NOT NULL,
	[Amount] [money] NOT NULL,
	[IsActive] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblMedicineInvoicePayMode] PRIMARY KEY CLUSTERED 
(
	[InvoicePayModeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMedicineInvoiceInfo]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMedicineInvoiceInfo](
	[InvoiceInfoID] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[PurchaseID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[BatchNo] [varchar](20) NULL,
	[Usage] [varchar](200) NULL,
	[Discount] [decimal](10, 2) NULL,
	[SubTotal] [decimal](10, 2) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime2](0) NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime2](0) NULL,
 CONSTRAINT [PK_tblMedicineInvoiceInfo] PRIMARY KEY CLUSTERED 
(
	[InvoiceInfoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblMedicineInvoiceInfo] ON
INSERT [dbo].[tblMedicineInvoiceInfo] ([InvoiceInfoID], [InvoiceID], [ProductID], [PurchaseID], [Quantity], [BatchNo], [Usage], [Discount], [SubTotal], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 3, 8, 1, 1, N'PZ1517', NULL, NULL, CAST(20.00 AS Decimal(10, 2)), 1, CAST(0x00000000343B0B0000 AS DateTime2), NULL, CAST(0x0095A900343B0B0000 AS DateTime2))
INSERT [dbo].[tblMedicineInvoiceInfo] ([InvoiceInfoID], [InvoiceID], [ProductID], [PurchaseID], [Quantity], [BatchNo], [Usage], [Discount], [SubTotal], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 21, 10, 2, 1, N'CY6789', NULL, NULL, CAST(20.00 AS Decimal(10, 2)), 1, CAST(0x00340C013A3B0B0000 AS DateTime2), NULL, NULL)
INSERT [dbo].[tblMedicineInvoiceInfo] ([InvoiceInfoID], [InvoiceID], [ProductID], [PurchaseID], [Quantity], [BatchNo], [Usage], [Discount], [SubTotal], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (55, 51, 10, 1, 1, N'CY6789', NULL, NULL, CAST(40.00 AS Decimal(10, 2)), 1, CAST(0x0081D200A23B0B0000 AS DateTime2), NULL, NULL)
INSERT [dbo].[tblMedicineInvoiceInfo] ([InvoiceInfoID], [InvoiceID], [ProductID], [PurchaseID], [Quantity], [BatchNo], [Usage], [Discount], [SubTotal], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (56, 51, 8, 1, 1, N'PZ1517', NULL, NULL, CAST(40.00 AS Decimal(10, 2)), 1, CAST(0x0084D200A23B0B0000 AS DateTime2), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblMedicineInvoiceInfo] OFF
/****** Object:  Table [dbo].[tblDiagRequisitionInfo]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDiagRequisitionInfo](
	[DiagReqInfoID] [int] IDENTITY(1,1) NOT NULL,
	[DiagReqID] [int] NOT NULL,
	[DiagnosisID] [int] NOT NULL,
	[Discount] [decimal](10, 2) NULL,
	[Amount] [decimal](10, 2) NOT NULL,
	[IsDelivered] [bit] NULL,
	[DispatchedOn] [datetime] NULL,
	[IsSampleCollected] [bit] NULL,
	[SampleCollectedOn] [datetime] NULL,
	[IsReportReady] [bit] NULL,
	[ReportReadyOn] [datetime] NULL,
	[Comments] [varchar](max) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblPatientDiagnosis] PRIMARY KEY CLUSTERED 
(
	[DiagReqInfoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblDiagRequisitionInfo] ON
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 3, 2, CAST(10.00 AS Decimal(10, 2)), CAST(390.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A51000000000 AS DateTime), NULL, CAST(0x0000A51100CA5D3B AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 3, 4, CAST(20.00 AS Decimal(10, 2)), CAST(380.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A51000000000 AS DateTime), NULL, CAST(0x0000A51100CA8799 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 3, 5, CAST(30.00 AS Decimal(10, 2)), CAST(370.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A51000000000 AS DateTime), NULL, CAST(0x0000A51100CAB6AB AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 5, 3, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A51B010EABA3 AS DateTime), NULL, CAST(0x0000A51B010EABA3 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 5, 4, CAST(0.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A51B010EABA3 AS DateTime), NULL, CAST(0x0000A51B010EABA3 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 5, 9, CAST(0.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A51B010EABA3 AS DateTime), NULL, CAST(0x0000A51B010EABA3 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 6, 14, CAST(0.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A5220125320F AS DateTime), NULL, CAST(0x0000A5220125320F AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, 6, 11, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52201253220 AS DateTime), NULL, CAST(0x0000A52201253220 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 6, 3, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52201253220 AS DateTime), NULL, CAST(0x0000A52201253220 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, 7, 2, CAST(50.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013BE21B AS DateTime), NULL, CAST(0x0000A523013BE21B AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, 7, 3, CAST(100.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013BE21B AS DateTime), NULL, CAST(0x0000A523013BE21B AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, 7, 4, CAST(0.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013BE21B AS DateTime), NULL, CAST(0x0000A523013BE21B AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, 7, 5, CAST(0.00 AS Decimal(10, 2)), CAST(350.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013BE21B AS DateTime), NULL, CAST(0x0000A523013BE21B AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, 7, 7, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013BE21C AS DateTime), NULL, CAST(0x0000A523013BE21C AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, 7, 8, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013BE21C AS DateTime), NULL, CAST(0x0000A523013BE21C AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, 7, 11, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013BE21C AS DateTime), NULL, CAST(0x0000A523013BE21C AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, 7, 14, CAST(0.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013BE21C AS DateTime), NULL, CAST(0x0000A523013BE21C AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (19, 8, 11, CAST(100.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013F11C3 AS DateTime), NULL, CAST(0x0000A523013F11C3 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, 8, 14, CAST(0.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523013F11C3 AS DateTime), NULL, CAST(0x0000A523013F11C3 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (21, 9, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014BD19D AS DateTime), NULL, CAST(0x0000A523014BD19D AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (22, 9, 5, CAST(50.00 AS Decimal(10, 2)), CAST(350.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014BD19D AS DateTime), NULL, CAST(0x0000A523014BD19D AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (23, 9, 14, CAST(100.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014BD19D AS DateTime), NULL, CAST(0x0000A523014BD19D AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (24, 10, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014C137E AS DateTime), NULL, CAST(0x0000A523014C137E AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (25, 10, 5, CAST(50.00 AS Decimal(10, 2)), CAST(350.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014C137E AS DateTime), NULL, CAST(0x0000A523014C137E AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (26, 10, 14, CAST(100.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014C137E AS DateTime), NULL, CAST(0x0000A523014C137E AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (27, 11, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014DB571 AS DateTime), NULL, CAST(0x0000A523014DB571 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (28, 11, 3, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014DB571 AS DateTime), NULL, CAST(0x0000A523014DB571 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (29, 11, 5, CAST(50.00 AS Decimal(10, 2)), CAST(350.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014DB571 AS DateTime), NULL, CAST(0x0000A523014DB571 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (30, 11, 7, CAST(100.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014DB571 AS DateTime), NULL, CAST(0x0000A523014DB571 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (31, 11, 17, CAST(50.00 AS Decimal(10, 2)), CAST(450.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014DB571 AS DateTime), NULL, CAST(0x0000A523014DB571 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (32, 11, 15, CAST(50.00 AS Decimal(10, 2)), CAST(450.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014DB571 AS DateTime), NULL, CAST(0x0000A523014DB571 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (33, 11, 14, CAST(100.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A523014DB571 AS DateTime), NULL, CAST(0x0000A523014DB571 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (34, 12, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A5230152172A AS DateTime), NULL, CAST(0x0000A5230152172A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (35, 12, 3, CAST(100.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A5230152172A AS DateTime), NULL, CAST(0x0000A5230152172A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (36, 12, 5, CAST(50.00 AS Decimal(10, 2)), CAST(350.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A5230152172A AS DateTime), NULL, CAST(0x0000A5230152172A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (37, 12, 7, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A5230152172A AS DateTime), NULL, CAST(0x0000A5230152172A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (38, 12, 11, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A5230152172A AS DateTime), NULL, CAST(0x0000A5230152172A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (39, 12, 13, CAST(80.00 AS Decimal(10, 2)), CAST(480.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A5230152172A AS DateTime), NULL, CAST(0x0000A5230152172A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40, 12, 14, CAST(0.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A5230152172A AS DateTime), NULL, CAST(0x0000A5230152172A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (41, 13, 15, CAST(0.00 AS Decimal(10, 2)), CAST(450.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52400D387C6 AS DateTime), NULL, CAST(0x0000A52400D387C6 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (42, 13, 11, CAST(100.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52400D387C7 AS DateTime), NULL, CAST(0x0000A52400D387C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (43, 14, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52400D6E396 AS DateTime), NULL, CAST(0x0000A52400D6E396 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (44, 14, 3, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52400D6E396 AS DateTime), NULL, CAST(0x0000A52400D6E396 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (45, 14, 5, CAST(0.00 AS Decimal(10, 2)), CAST(350.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52400D6E396 AS DateTime), NULL, CAST(0x0000A52400D6E396 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (46, 15, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52400FD3B72 AS DateTime), NULL, CAST(0x0000A52400FD3B72 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (47, 16, 4, CAST(100.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52600CE9E00 AS DateTime), NULL, CAST(0x0000A52600CE9E00 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (48, 16, 5, CAST(0.00 AS Decimal(10, 2)), CAST(350.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52600CE9E04 AS DateTime), NULL, CAST(0x0000A52600CE9E04 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (49, 17, 3, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52600FDE34D AS DateTime), NULL, CAST(0x0000A52600FDE34D AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (50, 18, 3, CAST(100.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CAA65 AS DateTime), NULL, CAST(0x0000A52F007CAA65 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (51, 18, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CAA65 AS DateTime), NULL, CAST(0x0000A52F007CAA65 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (52, 19, 3, CAST(100.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB03E AS DateTime), NULL, CAST(0x0000A52F007CB03E AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (53, 19, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB03E AS DateTime), NULL, CAST(0x0000A52F007CB03E AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (54, 20, 3, CAST(100.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB095 AS DateTime), NULL, CAST(0x0000A52F007CB095 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (55, 20, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB095 AS DateTime), NULL, CAST(0x0000A52F007CB095 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (56, 21, 3, CAST(100.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB2DE AS DateTime), NULL, CAST(0x0000A52F007CB2DE AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (57, 21, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB2DE AS DateTime), NULL, CAST(0x0000A52F007CB2DE AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (58, 22, 3, CAST(100.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB3D5 AS DateTime), NULL, CAST(0x0000A52F007CB3D5 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (59, 22, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB3D5 AS DateTime), NULL, CAST(0x0000A52F007CB3D5 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (60, 23, 3, CAST(100.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB4BD AS DateTime), NULL, CAST(0x0000A52F007CB4BD AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (61, 23, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A52F007CB4BD AS DateTime), NULL, CAST(0x0000A52F007CB4BD AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (62, 1018, 8, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A53400348475 AS DateTime), NULL, CAST(0x0000A53400348475 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (63, 1018, 11, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A53400348475 AS DateTime), NULL, CAST(0x0000A53400348475 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1062, 2018, 15, CAST(50.00 AS Decimal(10, 2)), CAST(450.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A55C00F67223 AS DateTime), NULL, CAST(0x0000A55C00F67223 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1063, 2018, 14, CAST(100.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A55C00F67223 AS DateTime), NULL, CAST(0x0000A55C00F67223 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1064, 2022, 3, CAST(0.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A55C010672CF AS DateTime), NULL, CAST(0x0000A55C010672CF AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1065, 2022, 11, CAST(50.00 AS Decimal(10, 2)), CAST(400.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A55C010672CF AS DateTime), NULL, CAST(0x0000A55C010672CF AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1066, 2023, 2, CAST(0.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A56000F9EA3D AS DateTime), NULL, CAST(0x0000A56000F9EA3D AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1067, 2025, 5, CAST(25.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A61A0125D38F AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1068, 2025, 7, CAST(6.00 AS Decimal(10, 2)), CAST(600.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(0x0000A61A0125E76C AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1069, 2026, 3, CAST(5.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(0x0000A61D01153625 AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1070, 2026, 2, CAST(5.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(0x0000A61D01153981 AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1071, 2027, 3, CAST(5.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(0x0000A61D011669C6 AS DateTime), NULL, NULL)
INSERT [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID], [DiagReqID], [DiagnosisID], [Discount], [Amount], [IsDelivered], [DispatchedOn], [IsSampleCollected], [SampleCollectedOn], [IsReportReady], [ReportReadyOn], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1072, 2027, 2, CAST(5.00 AS Decimal(10, 2)), CAST(300.00 AS Decimal(10, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(0x0000A61D011669C6 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblDiagRequisitionInfo] OFF
/****** Object:  Table [dbo].[tblFloor]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFloor](
	[FloorId] [int] IDENTITY(1,1) NOT NULL,
	[FloorType] [varchar](50) NOT NULL,
	[TowerID] [int] NOT NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblFloor] PRIMARY KEY CLUSTERED 
(
	[FloorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblFloor] ON
INSERT [dbo].[tblFloor] ([FloorId], [FloorType], [TowerID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Normal', 2, 1, 1, CAST(0x0000A05900000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblFloor] ([FloorId], [FloorType], [TowerID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Luxary', 1, 1, 1, CAST(0x0000975900000000 AS DateTime), 1, CAST(0x0000A6390102DCC2 AS DateTime))
INSERT [dbo].[tblFloor] ([FloorId], [FloorType], [TowerID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Luxary', 2, 1, 1, CAST(0x0000A6390100E035 AS DateTime), NULL, NULL)
INSERT [dbo].[tblFloor] ([FloorId], [FloorType], [TowerID], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'General', 1, 0, 1, CAST(0x0000A64F00C8A672 AS DateTime), 1, CAST(0x0000A64F00CA1AEC AS DateTime))
SET IDENTITY_INSERT [dbo].[tblFloor] OFF
/****** Object:  Table [dbo].[tblEmpPaySlip]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEmpPaySlip](
	[SlipID] [int] IDENTITY(1,1) NOT NULL,
	[EID] [int] NOT NULL,
	[PayID] [int] NOT NULL,
	[Month] [date] NOT NULL,
	[TotalDays] [tinyint] NOT NULL,
	[TotalAttendanceDays] [tinyint] NOT NULL,
	[TotalLeaveDays] [tinyint] NOT NULL,
	[Basic] [money] NOT NULL,
	[HRA] [money] NOT NULL,
	[OtherAllowances] [money] NOT NULL,
	[MedicalAllowances] [money] NOT NULL,
	[Conveyance] [money] NOT NULL,
	[LTA] [money] NOT NULL,
	[ProfTax] [money] NOT NULL,
	[TDS] [money] NOT NULL,
	[PF] [money] NOT NULL,
	[Loan] [money] NOT NULL,
	[FoodCoupons] [money] NOT NULL,
	[TelephoneExpenses] [money] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblEmpPaySlip] PRIMARY KEY CLUSTERED 
(
	[SlipID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDiagRequisitionInfoResults]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDiagRequisitionInfoResults](
	[DiagReqResultID] [int] IDENTITY(1,1) NOT NULL,
	[DiagReqInfoID] [int] NOT NULL,
	[ComponentID] [int] NOT NULL,
	[ComponentResValue] [varchar](1000) NOT NULL,
	[ReportResult] [nchar](10) NULL,
	[Comments] [varchar](250) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblPatientDiagnosisResults] PRIMARY KEY CLUSTERED 
(
	[DiagReqResultID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblDiagRequisitionInfoResults] ON
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 5, 1, N'20', NULL, NULL, 1, CAST(0x0000A5210137581E AS DateTime), 1, CAST(0x0000A5210137581E AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 5, 2, N'pale yellow', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 5, 3, N'clear', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 5, 4, N'acidic', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 5, 5, N'1.20', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 5, 6, N'track', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, 5, 7, N'nil', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 5, 8, N'--', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, 5, 9, N'--', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, 5, 10, N'1-2/hpf', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, 5, 11, N'1-2/hpf', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, 5, 12, N'5-7/hpf', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, 5, 13, N'nill', NULL, NULL, 1, CAST(0x0000A5210138C62A AS DateTime), 1, CAST(0x0000A5210138C62A AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, 5, 14, N'nil', NULL, NULL, 1, CAST(0x0000A5210138C636 AS DateTime), 1, CAST(0x0000A5210138C636 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, 5, 15, N'nil', NULL, NULL, 1, CAST(0x0000A5210138C636 AS DateTime), 1, CAST(0x0000A5210138C636 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, 8, 136, N'15.0', NULL, NULL, 1, CAST(0x0000A522012917BC AS DateTime), NULL, CAST(0x0000A522012917BC AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (19, 8, 137, N'4.8', NULL, NULL, 1, CAST(0x0000A522012917BF AS DateTime), NULL, CAST(0x0000A522012917BF AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, 8, 138, N'44', NULL, NULL, 1, CAST(0x0000A522012917C0 AS DateTime), NULL, CAST(0x0000A522012917C0 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (21, 8, 139, N'78', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (22, 8, 140, N'30', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (23, 8, 141, N'35', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (24, 8, 55, N'6000', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (25, 8, 143, N'40', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (26, 8, 145, N'40', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (27, 8, 146, N'3', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (28, 8, 147, N'5', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (29, 8, 148, N'0.3', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (30, 8, 149, N'400', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (31, 8, 150, N'4', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (32, 8, 151, N'3', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (33, 8, 153, N'45', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (34, 8, 154, N'6', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (35, 8, 155, N'6', NULL, NULL, 1, CAST(0x0000A522012917C7 AS DateTime), NULL, CAST(0x0000A522012917C7 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (36, 20, 136, N'15.0', NULL, NULL, 1, CAST(0x0000A523013F7502 AS DateTime), 1, CAST(0x0000A523013F7502 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (37, 20, 137, N'4.8', NULL, NULL, 1, CAST(0x0000A523013F7502 AS DateTime), 1, CAST(0x0000A523013F7502 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (38, 20, 138, N'44', NULL, NULL, 1, CAST(0x0000A523013F7502 AS DateTime), 1, CAST(0x0000A523013F7502 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (39, 20, 139, N'78', NULL, NULL, 1, CAST(0x0000A523013F7502 AS DateTime), 1, CAST(0x0000A523013F7502 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40, 20, 140, N'30', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (41, 20, 141, N'35', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (42, 20, 142, N'6000', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (43, 20, 143, N'40', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (44, 20, 145, N'40', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (45, 20, 146, N'3', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (46, 20, 147, N'5', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (47, 20, 148, N'0.3', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (48, 20, 149, N'400', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (49, 20, 150, N'4', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (50, 20, 151, N'3', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (51, 20, 153, N'45', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (52, 20, 154, N'6', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (53, 20, 155, N'6', NULL, NULL, 1, CAST(0x0000A523013F7503 AS DateTime), 1, CAST(0x0000A523013F7503 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (54, 48, 68, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (55, 48, 71, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (56, 48, 72, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (57, 48, 73, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (58, 48, 74, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (59, 48, 75, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (60, 48, 76, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (61, 48, 77, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (62, 48, 82, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (63, 48, 83, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (64, 48, 84, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (65, 48, 85, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (66, 48, 86, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC22 AS DateTime), NULL, CAST(0x0000A52600CEDC22 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (67, 48, 87, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC23 AS DateTime), NULL, CAST(0x0000A52600CEDC23 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (68, 48, 92, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC23 AS DateTime), NULL, CAST(0x0000A52600CEDC23 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (69, 48, 93, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC23 AS DateTime), NULL, CAST(0x0000A52600CEDC23 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (70, 48, 94, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC23 AS DateTime), NULL, CAST(0x0000A52600CEDC23 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (71, 48, 95, N'--', NULL, NULL, 1, CAST(0x0000A52600CEDC23 AS DateTime), NULL, CAST(0x0000A52600CEDC23 AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (73, 47, 151, N'152/60', NULL, NULL, 1, CAST(0x0000A620012AE501 AS DateTime), 1, CAST(0x0000A620012B428E AS DateTime))
INSERT [dbo].[tblDiagRequisitionInfoResults] ([DiagReqResultID], [DiagReqInfoID], [ComponentID], [ComponentResValue], [ReportResult], [Comments], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (75, 47, 151, N'152/60', NULL, NULL, 1, CAST(0x0000A647011DCE86 AS DateTime), 1, CAST(0x0000A647011E1B28 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblDiagRequisitionInfoResults] OFF
/****** Object:  Table [dbo].[tblBed]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBed](
	[BedId] [int] IDENTITY(1,1) NOT NULL,
	[BedNo] [int] NOT NULL,
	[BedType] [varchar](20) NOT NULL,
	[WardClass] [varchar](20) NULL,
	[BedBooKCat] [varchar](20) NOT NULL,
	[WardType] [varchar](20) NULL,
	[FloorId] [int] NULL,
	[BedName] [varchar](20) NOT NULL,
	[RoomId] [int] NOT NULL,
	[BedStatus] [varchar](20) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](1000) NULL,
 CONSTRAINT [PK_tblBed] PRIMARY KEY CLUSTERED 
(
	[BedId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_tblBed] UNIQUE NONCLUSTERED 
(
	[BedName] ASC,
	[RoomId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBed] ON
INSERT [dbo].[tblBed] ([BedId], [BedNo], [BedType], [WardClass], [BedBooKCat], [WardType], [FloorId], [BedName], [RoomId], [BedStatus], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (1, 1, N'room', N'LU', N'hour', N'NICU', NULL, N'Bed1', 2, N'Ready', 1, 0, CAST(0x0000A6150105D3C6 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblBed] ([BedId], [BedNo], [BedType], [WardClass], [BedBooKCat], [WardType], [FloorId], [BedName], [RoomId], [BedStatus], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (2, 3, N'Test', N'LUxary', N'Online', NULL, NULL, N'Bed', 2, NULL, 1, 0, CAST(0x0000A61501062CC3 AS DateTime), 1, CAST(0x0000A64F00D4F6C6 AS DateTime), NULL)
INSERT [dbo].[tblBed] ([BedId], [BedNo], [BedType], [WardClass], [BedBooKCat], [WardType], [FloorId], [BedName], [RoomId], [BedStatus], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (3, 1, N'room', N'LU', N'hour', N'ICU', NULL, N'Bed1', 5, N'Ready', 1, 0, CAST(0x0000A615010644D4 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblBed] ([BedId], [BedNo], [BedType], [WardClass], [BedBooKCat], [WardType], [FloorId], [BedName], [RoomId], [BedStatus], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (15, 2, N'Test', N'Sl', N'DoubleCot', NULL, NULL, N'Bed5', 2, NULL, 1, 1, CAST(0x0000A64F00D454FB AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblBed] OFF
/****** Object:  Table [dbo].[tblBloodBank]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBloodBank](
	[Bloodbankid] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Towerid] [int] NOT NULL,
	[Hospitalid] [int] NOT NULL,
	[Floorid] [int] NOT NULL,
	[InchargeName] [varchar](50) NULL,
	[Inchargeno] [varchar](20) NULL,
	[OperatingTimes] [varchar](50) NULL,
	[BBShortCode] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblBloodBank] PRIMARY KEY CLUSTERED 
(
	[Bloodbankid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBloodBank] ON
INSERT [dbo].[tblBloodBank] ([Bloodbankid], [Name], [Towerid], [Hospitalid], [Floorid], [InchargeName], [Inchargeno], [OperatingTimes], [BBShortCode], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'sree', 1, 4, 1, N'vasu', N'9095678902', NULL, NULL, 1, 1, CAST(0x0000A64A00000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblBloodBank] OFF
/****** Object:  View [dbo].[View_MedicineQuantity]    Script Date: 09/30/2016 15:00:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_MedicineQuantity]
AS
select o.PharmacyID,oi.PurchaseID as PurchaseID,oi.ProductID as ProductID,oi.BatchNo as BatchNo,
(isnull(oi.Quantity+oi.FreeQuantity,0)-
(select isnull(SUM(Quantity),0) from tblMedicineInvoiceInfo where PurchaseID=oi.PurchaseID and
 ProductID=oi.ProductID and BatchNo=oi.BatchNo)+
 (select isnull(SUM(Quantity),0) from tblMedicineSalesReturnInfo where PurchaseID=oi.PurchaseID and 
 ProductID=oi.ProductID and BatchNo=oi.BatchNo)-
 (select isnull(SUM(Quantity),0) from tblMedicineStockReturnInfo where PurchaseID=oi.PurchaseID and 
 ProductID=oi.ProductID and BatchNo=oi.BatchNo)) as Available_Stock from tblMedicinePurchaseInfo oi 
 inner join tblMedicinePurchase o on o.PurchaseID=oi.PurchaseID
 group by o.PharmacyID,oi.PurchaseID,oi.ProductID,oi.BatchNo,oi.Quantity,oi.FreeQuantity;
GO
/****** Object:  Table [dbo].[TblTreatmentCareCalander]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblTreatmentCareCalander](
	[trtCareCalenderid] [int] IDENTITY(1,1) NOT NULL,
	[TreatmentCareCompid] [int] NULL,
	[CareInstructionID] [int] NOT NULL,
	[VitalsignsID] [int] NOT NULL,
	[TrMedicineID] [int] NOT NULL,
	[TrDiagnosisID] [int] NOT NULL,
	[trtCareTime] [datetime] NULL,
	[Status] [varchar](50) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_TblTreatmentCareCalander] PRIMARY KEY CLUSTERED 
(
	[trtCareCalenderid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblTreatmentCareStatus]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTreatmentCareStatus](
	[TreatmentCareid] [int] NOT NULL,
	[TreatmentCareCompid] [int] NULL,
	[trtCareCalenderid] [int] NOT NULL,
	[PerformedOn] [datetime] NULL,
	[PerformedBy] [varchar](50) NULL,
	[Reactions] [varchar](50) NULL,
	[Comments] [varchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblAdmission]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAdmission](
	[AdmissionID] [int] IDENTITY(1,1) NOT NULL,
	[PatientID] [int] NULL,
	[FacilityBranch] [int] NULL,
	[AdmitDate] [datetime] NULL,
	[AdmitTime] [time](7) NULL,
	[AdmittingPhysician] [varchar](50) NULL,
	[AttendingPhysician] [varchar](50) NULL,
	[Department] [int] NULL,
	[PatientType] [varchar](50) NULL,
	[WardType] [varchar](50) NULL,
	[BedID] [int] NULL,
	[AdmitStatus] [varchar](50) NULL,
	[AdmitSource] [varchar](50) NULL,
	[Diagnosiscode] [int] NULL,
	[DiagnosisDescription] [varchar](1200) NULL,
	[ProcedureCode] [int] NULL,
	[Tryas] [varchar](50) NULL,
 CONSTRAINT [PK_tblOpAdmission] PRIMARY KEY CLUSTERED 
(
	[AdmissionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblAdmission] ON
INSERT [dbo].[tblAdmission] ([AdmissionID], [PatientID], [FacilityBranch], [AdmitDate], [AdmitTime], [AdmittingPhysician], [AttendingPhysician], [Department], [PatientType], [WardType], [BedID], [AdmitStatus], [AdmitSource], [Diagnosiscode], [DiagnosisDescription], [ProcedureCode], [Tryas]) VALUES (1, 5, 1, CAST(0x0000A05C00000000 AS DateTime), CAST(0x07003CB8192E0000 AS Time), N'name', N'1', 1, NULL, N'1', 1, N'1', N'2', 1, NULL, 2, NULL)
INSERT [dbo].[tblAdmission] ([AdmissionID], [PatientID], [FacilityBranch], [AdmitDate], [AdmitTime], [AdmittingPhysician], [AttendingPhysician], [Department], [PatientType], [WardType], [BedID], [AdmitStatus], [AdmitSource], [Diagnosiscode], [DiagnosisDescription], [ProcedureCode], [Tryas]) VALUES (2, 6, 2, CAST(0x0000A05700000000 AS DateTime), CAST(0x0700E0052D0B0000 AS Time), N'leg', N'leg', 1, NULL, N'2', 2, N' Inpatient - Long Term Acute Care ', N'Emergency Department ', 5, NULL, NULL, NULL)
INSERT [dbo].[tblAdmission] ([AdmissionID], [PatientID], [FacilityBranch], [AdmitDate], [AdmitTime], [AdmittingPhysician], [AttendingPhysician], [Department], [PatientType], [WardType], [BedID], [AdmitStatus], [AdmitSource], [Diagnosiscode], [DiagnosisDescription], [ProcedureCode], [Tryas]) VALUES (4, 5, 1, CAST(0x0000A05700000000 AS DateTime), CAST(0x0700E0052D0B0000 AS Time), N'leg', N'leg', 1, NULL, N'1', 3, N'inpatient', N'2', 1, NULL, 2, NULL)
INSERT [dbo].[tblAdmission] ([AdmissionID], [PatientID], [FacilityBranch], [AdmitDate], [AdmitTime], [AdmittingPhysician], [AttendingPhysician], [Department], [PatientType], [WardType], [BedID], [AdmitStatus], [AdmitSource], [Diagnosiscode], [DiagnosisDescription], [ProcedureCode], [Tryas]) VALUES (5, 8, 1, CAST(0x0000A61600000000 AS DateTime), CAST(0x07009CA6920C0000 AS Time), N'JUSTIN', N'LEG', 1, NULL, N'1', 1, N'INPATIENT', N'2', 1, N'2', 2, NULL)
INSERT [dbo].[tblAdmission] ([AdmissionID], [PatientID], [FacilityBranch], [AdmitDate], [AdmitTime], [AdmittingPhysician], [AttendingPhysician], [Department], [PatientType], [WardType], [BedID], [AdmitStatus], [AdmitSource], [Diagnosiscode], [DiagnosisDescription], [ProcedureCode], [Tryas]) VALUES (6, 7, 1, CAST(0x0000A60C00000000 AS DateTime), CAST(0x070048CA8E130000 AS Time), N'HAND', N'HAND', 1, NULL, N'1', 1, N'1', N'1', 1, N'1', 1, NULL)
SET IDENTITY_INSERT [dbo].[tblAdmission] OFF
/****** Object:  Table [dbo].[tblBBDonation]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBDonation](
	[Donationid] [int] IDENTITY(1,1) NOT NULL,
	[Donarid] [int] NOT NULL,
	[donationDate] [datetime] NOT NULL,
	[DateofExpiry] [datetime] NOT NULL,
	[Bloodbankid] [int] NOT NULL,
	[Patientid] [int] NOT NULL,
	[DonorComments] [varchar](200) NULL,
	[Status] [varchar](50) NULL,
	[CompName] [varchar](50) NULL,
	[BarCode] [varchar](50) NULL,
	[CompExpiry] [datetime] NULL,
	[compCreatedOn] [datetime] NULL,
	[compCreatedBy] [int] NULL,
	[CampId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_BBDonation] PRIMARY KEY CLUSTERED 
(
	[Donationid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBDonation] ON
INSERT [dbo].[tblBBDonation] ([Donationid], [Donarid], [donationDate], [DateofExpiry], [Bloodbankid], [Patientid], [DonorComments], [Status], [CompName], [BarCode], [CompExpiry], [compCreatedOn], [compCreatedBy], [CampId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, CAST(0x0000A6DA00000000 AS DateTime), CAST(0x0000A65C00000000 AS DateTime), 1, 5, NULL, N'Ready', NULL, NULL, CAST(0x0000B77400000000 AS DateTime), NULL, NULL, 1, 1, 1, CAST(0x0000A63D00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBDonation] ([Donationid], [Donarid], [donationDate], [DateofExpiry], [Bloodbankid], [Patientid], [DonorComments], [Status], [CompName], [BarCode], [CompExpiry], [compCreatedOn], [compCreatedBy], [CampId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 2, CAST(0x0000A6DA00000000 AS DateTime), CAST(0x0000A6E200000000 AS DateTime), 1, 6, NULL, N'Ready', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, CAST(0x0000A58600000000 AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBDonation] ([Donationid], [Donarid], [donationDate], [DateofExpiry], [Bloodbankid], [Patientid], [DonorComments], [Status], [CompName], [BarCode], [CompExpiry], [compCreatedOn], [compCreatedBy], [CampId], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 1, CAST(0x0000A64A00000000 AS DateTime), CAST(0x0000A6A500000000 AS DateTime), 1, 6, N'Not Good', N'Ready', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, CAST(0x0000A64B012AC234 AS DateTime), 2, CAST(0x0000A64B0136FF61 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblBBDonation] OFF
/****** Object:  Table [dbo].[tblBedAllocation]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBedAllocation](
	[BedAllocationId] [int] IDENTITY(1,1) NOT NULL,
	[PatientId] [int] NOT NULL,
	[PTName] [varchar](50) NOT NULL,
	[BedId] [int] NOT NULL,
	[BedStatus] [varchar](20) NULL,
	[AllocatedDate] [datetime] NOT NULL,
	[VacatedDate] [datetime] NULL,
	[TenDisDate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Comments] [varchar](1000) NULL,
 CONSTRAINT [PK_tblBedAllocation] PRIMARY KEY CLUSTERED 
(
	[BedAllocationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBedAllocation] ON
INSERT [dbo].[tblBedAllocation] ([BedAllocationId], [PatientId], [PTName], [BedId], [BedStatus], [AllocatedDate], [VacatedDate], [TenDisDate], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Comments]) VALUES (2, 6, N'sreenu', 1, NULL, CAST(0x0000A60400000000 AS DateTime), CAST(0x0000A62300000000 AS DateTime), CAST(0x0000A62300000000 AS DateTime), 1, 1, CAST(0x0000A07000000000 AS DateTime), NULL, CAST(0x0000A625004373B1 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tblBedAllocation] OFF
/****** Object:  Table [dbo].[tblBBSettings]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBSettings](
	[BBSettingID] [int] IDENTITY(1,1) NOT NULL,
	[BloodbankId] [int] NOT NULL,
	[DisplayName] [varchar](50) NULL,
	[logo] [image] NULL,
	[AddressID] [int] NOT NULL,
	[AuthDrName] [varchar](50) NULL,
	[BagsCapacity] [varchar](50) NULL,
	[NoOfRefrigrators] [int] NULL,
	[establishedOn] [datetime] NULL,
	[LicenceNum] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblBBSettings] PRIMARY KEY CLUSTERED 
(
	[BBSettingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBSettings] ON
INSERT [dbo].[tblBBSettings] ([BBSettingID], [BloodbankId], [DisplayName], [logo], [AddressID], [AuthDrName], [BagsCapacity], [NoOfRefrigrators], [establishedOn], [LicenceNum], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, 1, N'Image', NULL, 1, N'Sree', N'aaaa', 10, CAST(0x00008E0800000000 AS DateTime), 10030, 1, 1, CAST(0x0000A6560134EE1D AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBSettings] ([BBSettingID], [BloodbankId], [DisplayName], [logo], [AddressID], [AuthDrName], [BagsCapacity], [NoOfRefrigrators], [establishedOn], [LicenceNum], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, 1, N'SUN', NULL, 4, N'ABCG', N'mmmm', 10, CAST(0x00008CBA00000000 AS DateTime), 234567, 1, 1, CAST(0x0000A6560135F6E2 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblBBSettings] OFF
/****** Object:  Table [dbo].[tblBBScreeningBiology]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBScreeningBiology](
	[Screeningid] [int] IDENTITY(1,1) NOT NULL,
	[Donationid] [int] NOT NULL,
	[Glucose] [int] NOT NULL,
	[Urea] [int] NOT NULL,
	[ALT] [int] NOT NULL,
	[TestResult] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblBBScreeningBiology] PRIMARY KEY CLUSTERED 
(
	[Screeningid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBScreeningBiology] ON
INSERT [dbo].[tblBBScreeningBiology] ([Screeningid], [Donationid], [Glucose], [Urea], [ALT], [TestResult], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, 1, 80, 20, 40, N'wertyu', 1, 1, CAST(0x0000A64A012FE5AE AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBScreeningBiology] ([Screeningid], [Donationid], [Glucose], [Urea], [ALT], [TestResult], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, 6, 60, 54, 94, N'trfdsds', 1, 1, CAST(0x0000A64B00000000 AS DateTime), 1, CAST(0x0000A64B00F9ABF1 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblBBScreeningBiology] OFF
/****** Object:  Table [dbo].[tblBBQuestionnaire]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBQuestionnaire](
	[QuetID] [int] IDENTITY(1,1) NOT NULL,
	[Donationid] [int] NOT NULL,
	[Question] [varchar](500) NULL,
	[Answer] [varchar](500) NULL,
	[Remarks] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[Createdon] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[Modifiedon] [datetime] NULL,
 CONSTRAINT [PK_tblBBQuestionnaire] PRIMARY KEY CLUSTERED 
(
	[QuetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBQuestionnaire] ON
INSERT [dbo].[tblBBQuestionnaire] ([QuetID], [Donationid], [Question], [Answer], [Remarks], [IsActive], [CreatedBy], [Createdon], [ModifiedBy], [Modifiedon]) VALUES (4, 1, N'No', N'yes', N'yes', 1, 1, CAST(0x0000A64A0120393B AS DateTime), 1, CAST(0x0000A64B00D162DF AS DateTime))
INSERT [dbo].[tblBBQuestionnaire] ([QuetID], [Donationid], [Question], [Answer], [Remarks], [IsActive], [CreatedBy], [Createdon], [ModifiedBy], [Modifiedon]) VALUES (5, 1, N'Are You in good Health ', N'YES', N'aaaa', 1, 0, CAST(0x0000A64E011CCE3A AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBQuestionnaire] ([QuetID], [Donationid], [Question], [Answer], [Remarks], [IsActive], [CreatedBy], [Createdon], [ModifiedBy], [Modifiedon]) VALUES (6, 1, N'Are You in good Health ', N'YES', N'aaaa', 1, 0, CAST(0x0000A64E0120B0D6 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblBBQuestionnaire] OFF
/****** Object:  Table [dbo].[tblBBPhysicalExamination]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBPhysicalExamination](
	[PhyExamID] [int] IDENTITY(1,1) NOT NULL,
	[DonationId] [int] NOT NULL,
	[Heamoglobin] [int] NULL,
	[Temperature] [int] NULL,
	[Weight] [int] NULL,
	[Pulse] [int] NULL,
	[Systolic] [int] NULL,
	[Diastolic] [int] NULL,
	[RhType] [varchar](50) NOT NULL,
	[BGbyDonoar] [varchar](50) NULL,
	[BGafterScreening] [varchar](50) NULL,
	[BagType] [varchar](50) NOT NULL,
	[BagVolume] [int] NOT NULL,
	[Refrigeratorid] [int] NOT NULL,
	[Remarks] [varchar](200) NULL,
	[DeferralType] [varchar](50) NOT NULL,
	[DeferralRemarks] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblBBPhysicalExamination] PRIMARY KEY CLUSTERED 
(
	[PhyExamID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBPhysicalExamination] ON
INSERT [dbo].[tblBBPhysicalExamination] ([PhyExamID], [DonationId], [Heamoglobin], [Temperature], [Weight], [Pulse], [Systolic], [Diastolic], [RhType], [BGbyDonoar], [BGafterScreening], [BagType], [BagVolume], [Refrigeratorid], [Remarks], [DeferralType], [DeferralRemarks], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 220, 384, NULL, NULL, NULL, NULL, N'serum', NULL, NULL, N'Blood', 20, 1, NULL, N'kill', NULL, 1, 1, CAST(0x0000A64A01315FD4 AS DateTime), 1, CAST(0x0000A652000100D6 AS DateTime))
INSERT [dbo].[tblBBPhysicalExamination] ([PhyExamID], [DonationId], [Heamoglobin], [Temperature], [Weight], [Pulse], [Systolic], [Diastolic], [RhType], [BGbyDonoar], [BGafterScreening], [BagType], [BagVolume], [Refrigeratorid], [Remarks], [DeferralType], [DeferralRemarks], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 1, 25, 36, NULL, NULL, NULL, NULL, N'serum', NULL, NULL, N'Blood', 60, 1, NULL, N'killd', NULL, 1, 1, CAST(0x0000A651018A85C3 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblBBPhysicalExamination] OFF
/****** Object:  Table [dbo].[tblBBMandatoryTests]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBMandatoryTests](
	[MandatoryTestID] [int] IDENTITY(1,1) NOT NULL,
	[Donationid] [int] NOT NULL,
	[HIV1] [varchar](50) NOT NULL,
	[HIV2] [varchar](50) NOT NULL,
	[HBSAG] [varchar](50) NOT NULL,
	[HCV] [varchar](50) NOT NULL,
	[VDRL] [varchar](50) NOT NULL,
	[MP] [varchar](50) NOT NULL,
	[TestResult] [varchar](50) NULL,
	[Remarks] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblBBMandatoryTests] PRIMARY KEY CLUSTERED 
(
	[MandatoryTestID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBMandatoryTests] ON
INSERT [dbo].[tblBBMandatoryTests] ([MandatoryTestID], [Donationid], [HIV1], [HIV2], [HBSAG], [HCV], [VDRL], [MP], [TestResult], [Remarks], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, 1, N'Non Reactive', N'Reactive', N'Positive', N'Reactive', N'Reactive', N'Found', N'sdsgdf', N'rerwadgdf', 1, 1, CAST(0x0000A64A011EA99C AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBMandatoryTests] ([MandatoryTestID], [Donationid], [HIV1], [HIV2], [HBSAG], [HCV], [VDRL], [MP], [TestResult], [Remarks], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, 1, N' Reactive', N'Reactive', N'Negative', N'Non Reactive', N'Reactive', N'Not Found', NULL, N'ertredfgdh', 1, 1, CAST(0x0000A64A0132C3DF AS DateTime), 2, CAST(0x0000A64B00D18DC9 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblBBMandatoryTests] OFF
/****** Object:  Table [dbo].[tblBBIssue]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBIssue](
	[IssueID] [int] IDENTITY(1,1) NOT NULL,
	[RequestId] [int] NOT NULL,
	[Donationid] [int] NOT NULL,
	[Donorid] [int] NOT NULL,
	[NoofUnits] [varchar](50) NULL,
	[isCrossMatch] [bit] NULL,
	[IssuedOn] [datetime] NULL,
	[IssuedTo] [varchar](50) NULL,
	[BarCode] [varchar](50) NULL,
	[IssueStatus] [varchar](50) NULL,
	[IssuedBy] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblBBIssue] PRIMARY KEY CLUSTERED 
(
	[IssueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBIssue] ON
INSERT [dbo].[tblBBIssue] ([IssueID], [RequestId], [Donationid], [Donorid], [NoofUnits], [isCrossMatch], [IssuedOn], [IssuedTo], [BarCode], [IssueStatus], [IssuedBy], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 1, 1, 1, N'40', 1, CAST(0x0000A64F00000000 AS DateTime), N'raju', N'567345', N'Issued', 2, 1, 1, CAST(0x0000A64F00000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblBBIssue] OFF
/****** Object:  Table [dbo].[tblBBHaemotologyTest]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBHaemotologyTest](
	[HaemotologyID] [int] IDENTITY(1,1) NOT NULL,
	[Donationid] [int] NOT NULL,
	[Heamoglobin] [int] NOT NULL,
	[PlateletCount] [int] NOT NULL,
	[WBCCount] [int] NOT NULL,
	[MP] [int] NULL,
	[G6PD] [int] NULL,
	[Remarks] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_tblBBHaemotologyTest] PRIMARY KEY CLUSTERED 
(
	[HaemotologyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBHaemotologyTest] ON
INSERT [dbo].[tblBBHaemotologyTest] ([HaemotologyID], [Donationid], [Heamoglobin], [PlateletCount], [WBCCount], [MP], [G6PD], [Remarks], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, 1, 12, 15000, 5000, 6767, 8943, N'ertredfgdh', 1, 1, CAST(0x0000A64A01277B55 AS DateTime), NULL, NULL)
INSERT [dbo].[tblBBHaemotologyTest] ([HaemotologyID], [Donationid], [Heamoglobin], [PlateletCount], [WBCCount], [MP], [G6PD], [Remarks], [IsActive], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, 1, 20, 19000, 5000, 1368, 8887, N'ertredfgdh', 1, 1, CAST(0x0000A64A0133E63A AS DateTime), 1, CAST(0x0000A64B00CFAF97 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblBBHaemotologyTest] OFF
/****** Object:  Table [dbo].[tblBBGroupingSerum]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBGroupingSerum](
	[BBGroupingSerumid] [int] IDENTITY(1,1) NOT NULL,
	[Donationid] [int] NULL,
	[A] [varchar](50) NULL,
	[B] [varchar](50) NULL,
	[O] [varchar](50) NULL,
	[SerumGroup] [varchar](50) NULL,
	[IrregulatorAB] [varchar](50) NULL,
	[Assignedto] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[Remarks] [varchar](50) NULL,
 CONSTRAINT [PK_tblBBGroupingSerum] PRIMARY KEY CLUSTERED 
(
	[BBGroupingSerumid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBGroupingSerum] ON
INSERT [dbo].[tblBBGroupingSerum] ([BBGroupingSerumid], [Donationid], [A], [B], [O], [SerumGroup], [IrregulatorAB], [Assignedto], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Remarks]) VALUES (1, NULL, NULL, NULL, NULL, NULL, N'positive', NULL, 1, 1, CAST(0x0000A64A0149316C AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblBBGroupingSerum] ([BBGroupingSerumid], [Donationid], [A], [B], [O], [SerumGroup], [IrregulatorAB], [Assignedto], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Remarks]) VALUES (2, 1, N'+', N'-', N'-', N'B', N'+', N'Dhananjay', 1, 0, CAST(0x0000A64B00D13EA9 AS DateTime), 2, CAST(0x0000A64B013883B9 AS DateTime), NULL)
INSERT [dbo].[tblBBGroupingSerum] ([BBGroupingSerumid], [Donationid], [A], [B], [O], [SerumGroup], [IrregulatorAB], [Assignedto], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Remarks]) VALUES (3, 1, N'+', N'-', N'-', N'B', N'+', NULL, 1, 0, CAST(0x0000A64B00FB4D48 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblBBGroupingSerum] ([BBGroupingSerumid], [Donationid], [A], [B], [O], [SerumGroup], [IrregulatorAB], [Assignedto], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Remarks]) VALUES (4, 1, N'+', N'-', N'-', N'B', N'+', N'Ramya', 1, 0, CAST(0x0000A64B010274C1 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[tblBBGroupingSerum] ([BBGroupingSerumid], [Donationid], [A], [B], [O], [SerumGroup], [IrregulatorAB], [Assignedto], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Remarks]) VALUES (5, 5, N'+', N'-', N'-', N'B', N'+', N'Sree', 1, 0, CAST(0x0000A64E00CC32A7 AS DateTime), 5, CAST(0x0000A64E00D090C7 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tblBBGroupingSerum] OFF
/****** Object:  Table [dbo].[tblBBGroupingCell]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBGroupingCell](
	[BBGroupingcellid] [int] IDENTITY(1,1) NOT NULL,
	[Donationid] [int] NOT NULL,
	[AntiA] [varchar](50) NULL,
	[AntiB] [varchar](50) NULL,
	[AntiAB] [varchar](50) NULL,
	[AntiD] [varchar](50) NULL,
	[AntiH] [varchar](50) NULL,
	[AntiA1] [varchar](50) NULL,
	[BloodGroup] [nvarchar](50) NULL,
	[RHType] [varchar](50) NULL,
	[IrregulatorAB] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[Createdon] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[Assignedto] [varchar](50) NULL,
	[Remarks] [varchar](50) NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblBBGroupingCell] PRIMARY KEY CLUSTERED 
(
	[BBGroupingcellid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBGroupingCell] ON
INSERT [dbo].[tblBBGroupingCell] ([BBGroupingcellid], [Donationid], [AntiA], [AntiB], [AntiAB], [AntiD], [AntiH], [AntiA1], [BloodGroup], [RHType], [IrregulatorAB], [IsActive], [Createdon], [CreatedBy], [Assignedto], [Remarks], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, N'-', N'+', N'+', N'+', N'_', N'_', N'B+', N'Positive', N'+', 1, CAST(0x0000A64A013FCE6F AS DateTime), 0, NULL, NULL, NULL, CAST(0x0000A64B01301679 AS DateTime))
INSERT [dbo].[tblBBGroupingCell] ([BBGroupingcellid], [Donationid], [AntiA], [AntiB], [AntiAB], [AntiD], [AntiH], [AntiA1], [BloodGroup], [RHType], [IrregulatorAB], [IsActive], [Createdon], [CreatedBy], [Assignedto], [Remarks], [ModifiedBy], [ModifiedDate]) VALUES (2, 6, N'-', N'+', N'+', N'+', N'_', N'_', N'B+', N'Positive', N'+', 1, CAST(0x0000A64B01287977 AS DateTime), 0, N'Ramaa', NULL, 2, CAST(0x0000A64B01356D7E AS DateTime))
INSERT [dbo].[tblBBGroupingCell] ([BBGroupingcellid], [Donationid], [AntiA], [AntiB], [AntiAB], [AntiD], [AntiH], [AntiA1], [BloodGroup], [RHType], [IrregulatorAB], [IsActive], [Createdon], [CreatedBy], [Assignedto], [Remarks], [ModifiedBy], [ModifiedDate]) VALUES (3, 5, N'-', N'+', N'+', N'+', N'_', N'_', N'B+', N'Positive', N'+', 1, CAST(0x0000A64E00C16456 AS DateTime), 0, N'Ramya', NULL, 3, CAST(0x0000A64E00C5F3E1 AS DateTime))
SET IDENTITY_INSERT [dbo].[tblBBGroupingCell] OFF
/****** Object:  Table [dbo].[tblBBDiscard]    Script Date: 09/30/2016 15:00:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBBDiscard](
	[Discardid] [int] IDENTITY(1,1) NOT NULL,
	[Donationid] [int] NOT NULL,
	[DisDate] [datetime] NOT NULL,
	[DisComponent] [varchar](50) NULL,
	[DisReason] [varchar](50) NULL,
	[DisBy] [int] NULL,
	[DisSummary] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblBBDiscard] PRIMARY KEY CLUSTERED 
(
	[Discardid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblBBDiscard] ON
INSERT [dbo].[tblBBDiscard] ([Discardid], [Donationid], [DisDate], [DisComponent], [DisReason], [DisBy], [DisSummary], [IsActive], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, CAST(0x0000A66B00000000 AS DateTime), N'FrozenPlasma', NULL, 1, N'qwejkl', 1, CAST(0x0000A64C00C2CACA AS DateTime), 1, 2, CAST(0x0000A64C00CA2D88 AS DateTime))
INSERT [dbo].[tblBBDiscard] ([Discardid], [Donationid], [DisDate], [DisComponent], [DisReason], [DisBy], [DisSummary], [IsActive], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate]) VALUES (4, 1, CAST(0x0000A64F00000000 AS DateTime), N'fdsfsa', NULL, 1, NULL, 1, CAST(0x0000A64F00F0427D AS DateTime), 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[tblBBDiscard] OFF
/****** Object:  Default [DF_tblPatientAppointment_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAppointment] ADD  CONSTRAINT [DF_tblPatientAppointment_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF__tblBed__Modified__68487DD7]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBed] ADD  CONSTRAINT [DF__tblBed__Modified__68487DD7]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF__tblBedAll__Modif__74444068]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBedAllocation] ADD  CONSTRAINT [DF__tblBedAll__Modif__74444068]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblComponentStandardRange_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblComponentStandardRanges] ADD  CONSTRAINT [DF_tblComponentStandardRange_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblComponentValues_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblComponentValues] ADD  CONSTRAINT [DF_tblComponentValues_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblCustomer_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblCustomer] ADD  CONSTRAINT [DF_tblCustomer_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblDiagnosis_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagnosis] ADD  CONSTRAINT [DF_tblDiagnosis_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblDiagnosisComponent_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagnosisComponent] ADD  CONSTRAINT [DF_tblDiagnosisComponent_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblDiagRequisition_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagRequisition] ADD  CONSTRAINT [DF_tblDiagRequisition_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblDiagRequisitionInfo_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagRequisitionInfo] ADD  CONSTRAINT [DF_tblDiagRequisitionInfo_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblPatientDiagnosisResults_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagRequisitionInfoResults] ADD  CONSTRAINT [DF_tblPatientDiagnosisResults_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblDoctorSchedule_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDoctorSchedule] ADD  CONSTRAINT [DF_tblDoctorSchedule_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblEmpBankDetails_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpBankDetails] ADD  CONSTRAINT [DF_tblEmpBankDetails_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_EmpEducation_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpEducation] ADD  CONSTRAINT [DF_EmpEducation_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_tblEmployee_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmployee] ADD  CONSTRAINT [DF_tblEmployee_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_EmpPaySetting_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpPaySetting] ADD  CONSTRAINT [DF_EmpPaySetting_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_EmpPaySlip_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpPaySlip] ADD  CONSTRAINT [DF_EmpPaySlip_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_EmpPreviousExperience_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpPreviousExperience] ADD  CONSTRAINT [DF_EmpPreviousExperience_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_EmpSalary_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpSalarySettings] ADD  CONSTRAINT [DF_EmpSalary_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_Equipment_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEquipment] ADD  CONSTRAINT [DF_Equipment_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF__tblHospit__Modif__0BC6C43E]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblHospital] ADD  CONSTRAINT [DF__tblHospit__Modif__0BC6C43E]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblHospitalBranchDetails_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblHospitalBranchDetails] ADD  CONSTRAINT [DF_tblHospitalBranchDetails_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblHospitalSettings_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblHospitalSettings] ADD  CONSTRAINT [DF_tblHospitalSettings_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblDiagnosisDetails_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblLabDetails] ADD  CONSTRAINT [DF_tblDiagnosisDetails_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblLabSettings_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblLabSettings] ADD  CONSTRAINT [DF_tblLabSettings_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_LeavesCredit_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblLeavesCredit] ADD  CONSTRAINT [DF_LeavesCredit_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF__tblLocati__Modif__3B75D760]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblLocation] ADD  CONSTRAINT [DF__tblLocati__Modif__3B75D760]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF__tblLookup__Modif__76969D2E]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblLookup] ADD  CONSTRAINT [DF__tblLookup__Modif__76969D2E]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF__tblLookup__Modif__778AC167]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblLookupCategory] ADD  CONSTRAINT [DF__tblLookup__Modif__778AC167]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_TblManufactures_IsDeleted]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblManufacturers] ADD  CONSTRAINT [DF_TblManufactures_IsDeleted]  DEFAULT ('False') FOR [IsDeleted]
GO
/****** Object:  Default [DF_tblCompanies_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblManufacturers] ADD  CONSTRAINT [DF_tblCompanies_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblMedicineInvoice_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineInvoice] ADD  CONSTRAINT [DF_tblMedicineInvoice_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblMedicineInvoiceInfo_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineInvoiceInfo] ADD  CONSTRAINT [DF_tblMedicineInvoiceInfo_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblMedicineInvoicePayMode_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineInvoicePayMode] ADD  CONSTRAINT [DF_tblMedicineInvoicePayMode_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblPurchase_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicinePurchase] ADD  CONSTRAINT [DF_tblPurchase_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblOrderInfo_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicinePurchaseInfo] ADD  CONSTRAINT [DF_tblOrderInfo_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblMedicineSalesReturn_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineSalesReturn] ADD  CONSTRAINT [DF_tblMedicineSalesReturn_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF__tblMedici__Modif__6FE99F9F]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineSalesReturnInfo] ADD  CONSTRAINT [DF__tblMedici__Modif__6FE99F9F]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblMedicineStockReturn_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineStockReturn] ADD  CONSTRAINT [DF_tblMedicineStockReturn_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblMedicineStockReturnInfo_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineStockReturnInfo] ADD  CONSTRAINT [DF_tblMedicineStockReturnInfo_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblMedicineSuggested_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineSuggested] ADD  CONSTRAINT [DF_tblMedicineSuggested_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblPatientContactInfo_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPatientContactInfo] ADD  CONSTRAINT [DF_tblPatientContactInfo_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF__tblPatien__Modif__14B10FFA]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPatientDisease] ADD  CONSTRAINT [DF__tblPatien__Modif__14B10FFA]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblPatientInsurance_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPatientInsurance] ADD  CONSTRAINT [DF_tblPatientInsurance_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblPharmacyDetails_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPharmacyDetails] ADD  CONSTRAINT [DF_tblPharmacyDetails_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblPharmacySettings_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPharmacySettings] ADD  CONSTRAINT [DF_tblPharmacySettings_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_Customers_Deleted]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblProducts] ADD  CONSTRAINT [DF_Customers_Deleted]  DEFAULT ('False') FOR [IsDeleted]
GO
/****** Object:  Default [DF_tblMedicines_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblProducts] ADD  CONSTRAINT [DF_tblMedicines_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblSpecializationFieldLookUp_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblSpecializationFieldLookUp] ADD  CONSTRAINT [DF_tblSpecializationFieldLookUp_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblSpecializationFieldValues_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblSpecializationFieldValues] ADD  CONSTRAINT [DF_tblSpecializationFieldValues_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblSupplier_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblSupplier] ADD  CONSTRAINT [DF_tblSupplier_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblTransactionDetails_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTransactionInfo] ADD  CONSTRAINT [DF_tblTransactionDetails_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_Transactions_ModifiedOn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTransactions] ADD  CONSTRAINT [DF_Transactions_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF__tblTreatm__Modif__1332DBDC]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTreatment] ADD  CONSTRAINT [DF__tblTreatm__Modif__1332DBDC]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblDiagnosisSuggestedDetails_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTreatmentDiagnosis] ADD  CONSTRAINT [DF_tblDiagnosisSuggestedDetails_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF_tblMedicineSuggestedDetails_ModifiedDate]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTreatmentMedicine] ADD  CONSTRAINT [DF_tblMedicineSuggestedDetails_ModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  Default [DF__tblRooms__Modifi__1975C517]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblWard] ADD  CONSTRAINT [DF__tblRooms__Modifi__1975C517]  DEFAULT (getdate()) FOR [ModifiedDate]
GO
/****** Object:  ForeignKey [FK_tblAdmission_tblDepartments]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAdmission]  WITH CHECK ADD  CONSTRAINT [FK_tblAdmission_tblDepartments] FOREIGN KEY([Department])
REFERENCES [dbo].[tblDepartments] ([DeptID])
GO
ALTER TABLE [dbo].[tblAdmission] CHECK CONSTRAINT [FK_tblAdmission_tblDepartments]
GO
/****** Object:  ForeignKey [FK_tblIpAdmission_tblBed]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAdmission]  WITH CHECK ADD  CONSTRAINT [FK_tblIpAdmission_tblBed] FOREIGN KEY([BedID])
REFERENCES [dbo].[tblBed] ([BedId])
GO
ALTER TABLE [dbo].[tblAdmission] CHECK CONSTRAINT [FK_tblIpAdmission_tblBed]
GO
/****** Object:  ForeignKey [FK_tblOpAdmission_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAdmission]  WITH CHECK ADD  CONSTRAINT [FK_tblOpAdmission_tblPatient] FOREIGN KEY([PatientID])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblAdmission] CHECK CONSTRAINT [FK_tblOpAdmission_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblAppointment_tblDepartments]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAppointment]  WITH CHECK ADD  CONSTRAINT [FK_tblAppointment_tblDepartments] FOREIGN KEY([DeptID])
REFERENCES [dbo].[tblDepartments] ([DeptID])
GO
ALTER TABLE [dbo].[tblAppointment] CHECK CONSTRAINT [FK_tblAppointment_tblDepartments]
GO
/****** Object:  ForeignKey [FK_tblAppointment_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAppointment]  WITH CHECK ADD  CONSTRAINT [FK_tblAppointment_tblEmployee] FOREIGN KEY([DoctorID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblAppointment] CHECK CONSTRAINT [FK_tblAppointment_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblAppointment_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAppointment]  WITH CHECK ADD  CONSTRAINT [FK_tblAppointment_tblPatient] FOREIGN KEY([PatientID])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblAppointment] CHECK CONSTRAINT [FK_tblAppointment_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblAppointment_tblTransactions]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAppointment]  WITH CHECK ADD  CONSTRAINT [FK_tblAppointment_tblTransactions] FOREIGN KEY([TransID])
REFERENCES [dbo].[tblTransactions] ([TransID])
GO
ALTER TABLE [dbo].[tblAppointment] CHECK CONSTRAINT [FK_tblAppointment_tblTransactions]
GO
/****** Object:  ForeignKey [FK_tblAssessment_tblAssessmentSpecialityHistory]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessment]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessment_tblAssessmentSpecialityHistory] FOREIGN KEY([SpecialityHistoryID])
REFERENCES [dbo].[tblAssessmentSpecialityHistory] ([AsseSpeciHisId])
GO
ALTER TABLE [dbo].[tblAssessment] CHECK CONSTRAINT [FK_tblAssessment_tblAssessmentSpecialityHistory]
GO
/****** Object:  ForeignKey [FK_tblAssessment_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessment]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessment_tblPatient] FOREIGN KEY([PatientID])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblAssessment] CHECK CONSTRAINT [FK_tblAssessment_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblAssessmentAllergies_tblAssessment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessmentAllergies]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessmentAllergies_tblAssessment] FOREIGN KEY([AssessmentID])
REFERENCES [dbo].[tblAssessment] ([AssessmentID])
GO
ALTER TABLE [dbo].[tblAssessmentAllergies] CHECK CONSTRAINT [FK_tblAssessmentAllergies_tblAssessment]
GO
/****** Object:  ForeignKey [FK_tblAssessmentAllergies_tblFood]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessmentAllergies]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessmentAllergies_tblFood] FOREIGN KEY([Foodcode])
REFERENCES [dbo].[tblAsseFood] ([FoodCode])
GO
ALTER TABLE [dbo].[tblAssessmentAllergies] CHECK CONSTRAINT [FK_tblAssessmentAllergies_tblFood]
GO
/****** Object:  ForeignKey [FK_tblAssessmentFamily_tblAssessment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessmentFamily]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessmentFamily_tblAssessment] FOREIGN KEY([AssessmentID])
REFERENCES [dbo].[tblAssessment] ([AssessmentID])
GO
ALTER TABLE [dbo].[tblAssessmentFamily] CHECK CONSTRAINT [FK_tblAssessmentFamily_tblAssessment]
GO
/****** Object:  ForeignKey [FK_tblAssessmentGenMedical_tblAssessment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessmentGenMedical]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessmentGenMedical_tblAssessment] FOREIGN KEY([AssessmentID])
REFERENCES [dbo].[tblAssessment] ([AssessmentID])
GO
ALTER TABLE [dbo].[tblAssessmentGenMedical] CHECK CONSTRAINT [FK_tblAssessmentGenMedical_tblAssessment]
GO
/****** Object:  ForeignKey [FK_tblAssessmentGenMedical_tblDisorder]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessmentGenMedical]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessmentGenMedical_tblDisorder] FOREIGN KEY([DisorderCode])
REFERENCES [dbo].[tblAsseDisorder] ([DisorderCode])
GO
ALTER TABLE [dbo].[tblAssessmentGenMedical] CHECK CONSTRAINT [FK_tblAssessmentGenMedical_tblDisorder]
GO
/****** Object:  ForeignKey [FK_tblAssessmentMedication_tblAssessment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessmentMedication]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessmentMedication_tblAssessment] FOREIGN KEY([AssessmentID])
REFERENCES [dbo].[tblAssessment] ([AssessmentID])
GO
ALTER TABLE [dbo].[tblAssessmentMedication] CHECK CONSTRAINT [FK_tblAssessmentMedication_tblAssessment]
GO
/****** Object:  ForeignKey [FK_tblAssessmentMedication_tblMedications]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessmentMedication]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessmentMedication_tblMedications] FOREIGN KEY([Medicode])
REFERENCES [dbo].[tblAsseMedications] ([MediCode])
GO
ALTER TABLE [dbo].[tblAssessmentMedication] CHECK CONSTRAINT [FK_tblAssessmentMedication_tblMedications]
GO
/****** Object:  ForeignKey [FK_tblAssessmentOthers_tblAssessment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessmentOthers]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessmentOthers_tblAssessment] FOREIGN KEY([AssessmentID])
REFERENCES [dbo].[tblAssessment] ([AssessmentID])
GO
ALTER TABLE [dbo].[tblAssessmentOthers] CHECK CONSTRAINT [FK_tblAssessmentOthers_tblAssessment]
GO
/****** Object:  ForeignKey [FK_tblAssessmentSocial_tblAssessment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessmentSocial]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessmentSocial_tblAssessment] FOREIGN KEY([AssessmentID])
REFERENCES [dbo].[tblAssessment] ([AssessmentID])
GO
ALTER TABLE [dbo].[tblAssessmentSocial] CHECK CONSTRAINT [FK_tblAssessmentSocial_tblAssessment]
GO
/****** Object:  ForeignKey [FK_tblAssessmentSpecialityHistory_tblAssessment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessmentSpecialityHistory]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessmentSpecialityHistory_tblAssessment] FOREIGN KEY([AssessmentID])
REFERENCES [dbo].[tblAssessment] ([AssessmentID])
GO
ALTER TABLE [dbo].[tblAssessmentSpecialityHistory] CHECK CONSTRAINT [FK_tblAssessmentSpecialityHistory_tblAssessment]
GO
/****** Object:  ForeignKey [FK_tblAssessmentSurgical_tblAssessment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblAssessmentSurgical]  WITH CHECK ADD  CONSTRAINT [FK_tblAssessmentSurgical_tblAssessment] FOREIGN KEY([AssessmentID])
REFERENCES [dbo].[tblAssessment] ([AssessmentID])
GO
ALTER TABLE [dbo].[tblAssessmentSurgical] CHECK CONSTRAINT [FK_tblAssessmentSurgical_tblAssessment]
GO
/****** Object:  ForeignKey [FK_tblBBDiscard_tblBBDonation]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBDiscard]  WITH CHECK ADD  CONSTRAINT [FK_tblBBDiscard_tblBBDonation] FOREIGN KEY([Donationid])
REFERENCES [dbo].[tblBBDonation] ([Donationid])
GO
ALTER TABLE [dbo].[tblBBDiscard] CHECK CONSTRAINT [FK_tblBBDiscard_tblBBDonation]
GO
/****** Object:  ForeignKey [FK_tblBBDonation_tblBBCampRegistration]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBDonation]  WITH CHECK ADD  CONSTRAINT [FK_tblBBDonation_tblBBCampRegistration] FOREIGN KEY([CampId])
REFERENCES [dbo].[tblBBCampRegistration] ([CampID])
GO
ALTER TABLE [dbo].[tblBBDonation] CHECK CONSTRAINT [FK_tblBBDonation_tblBBCampRegistration]
GO
/****** Object:  ForeignKey [FK_tblBBDonation_tblBBDonor]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBDonation]  WITH CHECK ADD  CONSTRAINT [FK_tblBBDonation_tblBBDonor] FOREIGN KEY([Donarid])
REFERENCES [dbo].[tblBBDonor] ([DonorId])
GO
ALTER TABLE [dbo].[tblBBDonation] CHECK CONSTRAINT [FK_tblBBDonation_tblBBDonor]
GO
/****** Object:  ForeignKey [FK_tblBBDonation_tblBBDonor1]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBDonation]  WITH CHECK ADD  CONSTRAINT [FK_tblBBDonation_tblBBDonor1] FOREIGN KEY([Donarid])
REFERENCES [dbo].[tblBBDonor] ([DonorId])
GO
ALTER TABLE [dbo].[tblBBDonation] CHECK CONSTRAINT [FK_tblBBDonation_tblBBDonor1]
GO
/****** Object:  ForeignKey [FK_tblBBDonation_tblBloodBank]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBDonation]  WITH CHECK ADD  CONSTRAINT [FK_tblBBDonation_tblBloodBank] FOREIGN KEY([Bloodbankid])
REFERENCES [dbo].[tblBloodBank] ([Bloodbankid])
GO
ALTER TABLE [dbo].[tblBBDonation] CHECK CONSTRAINT [FK_tblBBDonation_tblBloodBank]
GO
/****** Object:  ForeignKey [FK_tblBBDonation_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBDonation]  WITH CHECK ADD  CONSTRAINT [FK_tblBBDonation_tblPatient] FOREIGN KEY([Patientid])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblBBDonation] CHECK CONSTRAINT [FK_tblBBDonation_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblbbDonor_tblAddress]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBDonor]  WITH CHECK ADD  CONSTRAINT [FK_tblbbDonor_tblAddress] FOREIGN KEY([AddressID])
REFERENCES [dbo].[tblAddress] ([AddressID])
GO
ALTER TABLE [dbo].[tblBBDonor] CHECK CONSTRAINT [FK_tblbbDonor_tblAddress]
GO
/****** Object:  ForeignKey [FK_tblBBGroupingCell_tblBBDonation]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBGroupingCell]  WITH CHECK ADD  CONSTRAINT [FK_tblBBGroupingCell_tblBBDonation] FOREIGN KEY([Donationid])
REFERENCES [dbo].[tblBBDonation] ([Donationid])
GO
ALTER TABLE [dbo].[tblBBGroupingCell] CHECK CONSTRAINT [FK_tblBBGroupingCell_tblBBDonation]
GO
/****** Object:  ForeignKey [FK_tblBBGroupingSerum_tblBBDonation]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBGroupingSerum]  WITH CHECK ADD  CONSTRAINT [FK_tblBBGroupingSerum_tblBBDonation] FOREIGN KEY([Donationid])
REFERENCES [dbo].[tblBBDonation] ([Donationid])
GO
ALTER TABLE [dbo].[tblBBGroupingSerum] CHECK CONSTRAINT [FK_tblBBGroupingSerum_tblBBDonation]
GO
/****** Object:  ForeignKey [FK_tblBBHaemotologyTest_tblBBDonation]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBHaemotologyTest]  WITH CHECK ADD  CONSTRAINT [FK_tblBBHaemotologyTest_tblBBDonation] FOREIGN KEY([Donationid])
REFERENCES [dbo].[tblBBDonation] ([Donationid])
GO
ALTER TABLE [dbo].[tblBBHaemotologyTest] CHECK CONSTRAINT [FK_tblBBHaemotologyTest_tblBBDonation]
GO
/****** Object:  ForeignKey [FK_tblBBIssue_tblBBDonation]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBIssue]  WITH CHECK ADD  CONSTRAINT [FK_tblBBIssue_tblBBDonation] FOREIGN KEY([Donationid])
REFERENCES [dbo].[tblBBDonation] ([Donationid])
GO
ALTER TABLE [dbo].[tblBBIssue] CHECK CONSTRAINT [FK_tblBBIssue_tblBBDonation]
GO
/****** Object:  ForeignKey [FK_tblBBIssue_tblBBDonor]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBIssue]  WITH CHECK ADD  CONSTRAINT [FK_tblBBIssue_tblBBDonor] FOREIGN KEY([Donorid])
REFERENCES [dbo].[tblBBDonor] ([DonorId])
GO
ALTER TABLE [dbo].[tblBBIssue] CHECK CONSTRAINT [FK_tblBBIssue_tblBBDonor]
GO
/****** Object:  ForeignKey [FK_tblBBIssue_tblBBTranfusionRequest]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBIssue]  WITH CHECK ADD  CONSTRAINT [FK_tblBBIssue_tblBBTranfusionRequest] FOREIGN KEY([RequestId])
REFERENCES [dbo].[tblBBTransfusionRequest] ([RequestID])
GO
ALTER TABLE [dbo].[tblBBIssue] CHECK CONSTRAINT [FK_tblBBIssue_tblBBTranfusionRequest]
GO
/****** Object:  ForeignKey [FK_tblBBMandatoryTests_tblBBDonation]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBMandatoryTests]  WITH CHECK ADD  CONSTRAINT [FK_tblBBMandatoryTests_tblBBDonation] FOREIGN KEY([Donationid])
REFERENCES [dbo].[tblBBDonation] ([Donationid])
GO
ALTER TABLE [dbo].[tblBBMandatoryTests] CHECK CONSTRAINT [FK_tblBBMandatoryTests_tblBBDonation]
GO
/****** Object:  ForeignKey [FK_tblBBPhysicalExamination_tblBBDonation]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBPhysicalExamination]  WITH CHECK ADD  CONSTRAINT [FK_tblBBPhysicalExamination_tblBBDonation] FOREIGN KEY([DonationId])
REFERENCES [dbo].[tblBBDonation] ([Donationid])
GO
ALTER TABLE [dbo].[tblBBPhysicalExamination] CHECK CONSTRAINT [FK_tblBBPhysicalExamination_tblBBDonation]
GO
/****** Object:  ForeignKey [FK_tblBBPhysicalExamination_tblBBRefrigerators]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBPhysicalExamination]  WITH CHECK ADD  CONSTRAINT [FK_tblBBPhysicalExamination_tblBBRefrigerators] FOREIGN KEY([Refrigeratorid])
REFERENCES [dbo].[tblBBRefrigerators] ([Refrigeratorid])
GO
ALTER TABLE [dbo].[tblBBPhysicalExamination] CHECK CONSTRAINT [FK_tblBBPhysicalExamination_tblBBRefrigerators]
GO
/****** Object:  ForeignKey [FK_tblBBQuestionnaire_tblBBDonation]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBQuestionnaire]  WITH CHECK ADD  CONSTRAINT [FK_tblBBQuestionnaire_tblBBDonation] FOREIGN KEY([Donationid])
REFERENCES [dbo].[tblBBDonation] ([Donationid])
GO
ALTER TABLE [dbo].[tblBBQuestionnaire] CHECK CONSTRAINT [FK_tblBBQuestionnaire_tblBBDonation]
GO
/****** Object:  ForeignKey [FK_tblBBScreeningBiology_tblBBDonation]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBScreeningBiology]  WITH CHECK ADD  CONSTRAINT [FK_tblBBScreeningBiology_tblBBDonation] FOREIGN KEY([Donationid])
REFERENCES [dbo].[tblBBDonation] ([Donationid])
GO
ALTER TABLE [dbo].[tblBBScreeningBiology] CHECK CONSTRAINT [FK_tblBBScreeningBiology_tblBBDonation]
GO
/****** Object:  ForeignKey [FK_tblBBSettings_tblAddress]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBSettings]  WITH CHECK ADD  CONSTRAINT [FK_tblBBSettings_tblAddress] FOREIGN KEY([AddressID])
REFERENCES [dbo].[tblAddress] ([AddressID])
GO
ALTER TABLE [dbo].[tblBBSettings] CHECK CONSTRAINT [FK_tblBBSettings_tblAddress]
GO
/****** Object:  ForeignKey [FK_tblBBSettings_tblBloodBank]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBSettings]  WITH CHECK ADD  CONSTRAINT [FK_tblBBSettings_tblBloodBank] FOREIGN KEY([BloodbankId])
REFERENCES [dbo].[tblBloodBank] ([Bloodbankid])
GO
ALTER TABLE [dbo].[tblBBSettings] CHECK CONSTRAINT [FK_tblBBSettings_tblBloodBank]
GO
/****** Object:  ForeignKey [FK_tblBBTranfusionRequest_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBBTransfusionRequest]  WITH CHECK ADD  CONSTRAINT [FK_tblBBTranfusionRequest_tblPatient] FOREIGN KEY([Patientid])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblBBTransfusionRequest] CHECK CONSTRAINT [FK_tblBBTranfusionRequest_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblBed_tblRooms]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBed]  WITH CHECK ADD  CONSTRAINT [FK_tblBed_tblRooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[tblRooms] ([RoomId])
GO
ALTER TABLE [dbo].[tblBed] CHECK CONSTRAINT [FK_tblBed_tblRooms]
GO
/****** Object:  ForeignKey [FK_tblBedAllocation_tblBed]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBedAllocation]  WITH CHECK ADD  CONSTRAINT [FK_tblBedAllocation_tblBed] FOREIGN KEY([BedId])
REFERENCES [dbo].[tblBed] ([BedId])
GO
ALTER TABLE [dbo].[tblBedAllocation] CHECK CONSTRAINT [FK_tblBedAllocation_tblBed]
GO
/****** Object:  ForeignKey [FK_tblBedAllocation_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBedAllocation]  WITH CHECK ADD  CONSTRAINT [FK_tblBedAllocation_tblPatient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblBedAllocation] CHECK CONSTRAINT [FK_tblBedAllocation_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblBloodBank_tblFloor]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBloodBank]  WITH CHECK ADD  CONSTRAINT [FK_tblBloodBank_tblFloor] FOREIGN KEY([Floorid])
REFERENCES [dbo].[tblFloor] ([FloorId])
GO
ALTER TABLE [dbo].[tblBloodBank] CHECK CONSTRAINT [FK_tblBloodBank_tblFloor]
GO
/****** Object:  ForeignKey [FK_tblBloodBank_tblHospital]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBloodBank]  WITH CHECK ADD  CONSTRAINT [FK_tblBloodBank_tblHospital] FOREIGN KEY([Hospitalid])
REFERENCES [dbo].[tblHospital] ([HospitalId])
GO
ALTER TABLE [dbo].[tblBloodBank] CHECK CONSTRAINT [FK_tblBloodBank_tblHospital]
GO
/****** Object:  ForeignKey [FK_tblBloodBank_tblHospitalTowers]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblBloodBank]  WITH CHECK ADD  CONSTRAINT [FK_tblBloodBank_tblHospitalTowers] FOREIGN KEY([Towerid])
REFERENCES [dbo].[tblHospitalTowers] ([TowerId])
GO
ALTER TABLE [dbo].[tblBloodBank] CHECK CONSTRAINT [FK_tblBloodBank_tblHospitalTowers]
GO
/****** Object:  ForeignKey [FK_tblCallRegister_tblDepartments1]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblCallRegister]  WITH CHECK ADD  CONSTRAINT [FK_tblCallRegister_tblDepartments1] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[tblDepartments] ([DeptID])
GO
ALTER TABLE [dbo].[tblCallRegister] CHECK CONSTRAINT [FK_tblCallRegister_tblDepartments1]
GO
/****** Object:  ForeignKey [FK_tblCallRegister_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblCallRegister]  WITH CHECK ADD  CONSTRAINT [FK_tblCallRegister_tblPatient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblCallRegister] CHECK CONSTRAINT [FK_tblCallRegister_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblChemicalUsed_tblChemicalUsed]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblChemicalUsed]  WITH CHECK ADD  CONSTRAINT [FK_tblChemicalUsed_tblChemicalUsed] FOREIGN KEY([ChemicalID])
REFERENCES [dbo].[tblChemicalPurchase] ([ChemicalID])
GO
ALTER TABLE [dbo].[tblChemicalUsed] CHECK CONSTRAINT [FK_tblChemicalUsed_tblChemicalUsed]
GO
/****** Object:  ForeignKey [FK_tblComponentStandardRanges_tblDiagnosisComponent]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblComponentStandardRanges]  WITH CHECK ADD  CONSTRAINT [FK_tblComponentStandardRanges_tblDiagnosisComponent] FOREIGN KEY([ComponentID])
REFERENCES [dbo].[tblDiagnosisComponent] ([ComponentID])
GO
ALTER TABLE [dbo].[tblComponentStandardRanges] CHECK CONSTRAINT [FK_tblComponentStandardRanges_tblDiagnosisComponent]
GO
/****** Object:  ForeignKey [FK_tblComponentValues_tblDiagnosisComponent]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblComponentValues]  WITH CHECK ADD  CONSTRAINT [FK_tblComponentValues_tblDiagnosisComponent] FOREIGN KEY([ComponentID])
REFERENCES [dbo].[tblDiagnosisComponent] ([ComponentID])
GO
ALTER TABLE [dbo].[tblComponentValues] CHECK CONSTRAINT [FK_tblComponentValues_tblDiagnosisComponent]
GO
/****** Object:  ForeignKey [FK_tblCustomer_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblCustomer]  WITH CHECK ADD  CONSTRAINT [FK_tblCustomer_tblPatient] FOREIGN KEY([PatientID])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblCustomer] CHECK CONSTRAINT [FK_tblCustomer_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblDeptFieldResults_tblSpecializationField]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDeptFieldResults]  WITH CHECK ADD  CONSTRAINT [FK_tblDeptFieldResults_tblSpecializationField] FOREIGN KEY([SpFieldID])
REFERENCES [dbo].[tblSpecializationField] ([SpFieldID])
GO
ALTER TABLE [dbo].[tblDeptFieldResults] CHECK CONSTRAINT [FK_tblDeptFieldResults_tblSpecializationField]
GO
/****** Object:  ForeignKey [FK_tblDeptFieldResults_tblTreatment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDeptFieldResults]  WITH CHECK ADD  CONSTRAINT [FK_tblDeptFieldResults_tblTreatment] FOREIGN KEY([TreatmentID])
REFERENCES [dbo].[tblTreatment] ([TreatmentId])
GO
ALTER TABLE [dbo].[tblDeptFieldResults] CHECK CONSTRAINT [FK_tblDeptFieldResults_tblTreatment]
GO
/****** Object:  ForeignKey [FK_tblDiagnosisComponent_tblDiagnosis]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagnosisComponent]  WITH CHECK ADD  CONSTRAINT [FK_tblDiagnosisComponent_tblDiagnosis] FOREIGN KEY([DiagnosisID])
REFERENCES [dbo].[tblDiagnosis] ([DiagnosisID])
GO
ALTER TABLE [dbo].[tblDiagnosisComponent] CHECK CONSTRAINT [FK_tblDiagnosisComponent_tblDiagnosis]
GO
/****** Object:  ForeignKey [FK_tblDiagRequisition_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagRequisition]  WITH CHECK ADD  CONSTRAINT [FK_tblDiagRequisition_tblPatient] FOREIGN KEY([PatientID])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblDiagRequisition] CHECK CONSTRAINT [FK_tblDiagRequisition_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblDiagRequisitionInfo_tblDiagnosis]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagRequisitionInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblDiagRequisitionInfo_tblDiagnosis] FOREIGN KEY([DiagnosisID])
REFERENCES [dbo].[tblDiagnosis] ([DiagnosisID])
GO
ALTER TABLE [dbo].[tblDiagRequisitionInfo] CHECK CONSTRAINT [FK_tblDiagRequisitionInfo_tblDiagnosis]
GO
/****** Object:  ForeignKey [FK_tblDiagRequisitionInfo_tblDiagRequisition]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagRequisitionInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblDiagRequisitionInfo_tblDiagRequisition] FOREIGN KEY([DiagReqID])
REFERENCES [dbo].[tblDiagRequisition] ([DiagReqID])
GO
ALTER TABLE [dbo].[tblDiagRequisitionInfo] CHECK CONSTRAINT [FK_tblDiagRequisitionInfo_tblDiagRequisition]
GO
/****** Object:  ForeignKey [FK_tblDiagRequisitionInfoResults_tblDiagRequisitionInfo]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagRequisitionInfoResults]  WITH CHECK ADD  CONSTRAINT [FK_tblDiagRequisitionInfoResults_tblDiagRequisitionInfo] FOREIGN KEY([DiagReqInfoID])
REFERENCES [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID])
GO
ALTER TABLE [dbo].[tblDiagRequisitionInfoResults] CHECK CONSTRAINT [FK_tblDiagRequisitionInfoResults_tblDiagRequisitionInfo]
GO
/****** Object:  ForeignKey [FK_tblPatientDiagnosisResults_tblDiagnosisComponent]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagRequisitionInfoResults]  WITH CHECK ADD  CONSTRAINT [FK_tblPatientDiagnosisResults_tblDiagnosisComponent] FOREIGN KEY([ComponentID])
REFERENCES [dbo].[tblDiagnosisComponent] ([ComponentID])
GO
ALTER TABLE [dbo].[tblDiagRequisitionInfoResults] CHECK CONSTRAINT [FK_tblPatientDiagnosisResults_tblDiagnosisComponent]
GO
/****** Object:  ForeignKey [FK_tblPatientDiagnosisResults_tblPatientDiagnosis]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDiagRequisitionInfoResults]  WITH CHECK ADD  CONSTRAINT [FK_tblPatientDiagnosisResults_tblPatientDiagnosis] FOREIGN KEY([DiagReqInfoID])
REFERENCES [dbo].[tblDiagRequisitionInfo] ([DiagReqInfoID])
GO
ALTER TABLE [dbo].[tblDiagRequisitionInfoResults] CHECK CONSTRAINT [FK_tblPatientDiagnosisResults_tblPatientDiagnosis]
GO
/****** Object:  ForeignKey [FK_tblDoctorSchedule_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDoctorSchedule]  WITH CHECK ADD  CONSTRAINT [FK_tblDoctorSchedule_tblEmployee] FOREIGN KEY([DoctorID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblDoctorSchedule] CHECK CONSTRAINT [FK_tblDoctorSchedule_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblDrSettings_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDrSettings]  WITH CHECK ADD  CONSTRAINT [FK_tblDrSettings_tblEmployee] FOREIGN KEY([DoctorID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblDrSettings] CHECK CONSTRAINT [FK_tblDrSettings_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblDrSettings_tblSpecialization]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblDrSettings]  WITH CHECK ADD  CONSTRAINT [FK_tblDrSettings_tblSpecialization] FOREIGN KEY([SpecialityID])
REFERENCES [dbo].[tblSpeciality] ([SpID])
GO
ALTER TABLE [dbo].[tblDrSettings] CHECK CONSTRAINT [FK_tblDrSettings_tblSpecialization]
GO
/****** Object:  ForeignKey [FK_tblEmpBankDetails_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpBankDetails]  WITH CHECK ADD  CONSTRAINT [FK_tblEmpBankDetails_tblEmployee] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblEmpBankDetails] CHECK CONSTRAINT [FK_tblEmpBankDetails_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblEmpBankDetails_tblLocation]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpBankDetails]  WITH CHECK ADD  CONSTRAINT [FK_tblEmpBankDetails_tblLocation] FOREIGN KEY([City])
REFERENCES [dbo].[tblLocation] ([LID])
GO
ALTER TABLE [dbo].[tblEmpBankDetails] CHECK CONSTRAINT [FK_tblEmpBankDetails_tblLocation]
GO
/****** Object:  ForeignKey [FK_tblEmpEducation_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpEducation]  WITH CHECK ADD  CONSTRAINT [FK_tblEmpEducation_tblEmployee] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblEmpEducation] CHECK CONSTRAINT [FK_tblEmpEducation_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblEmpFamily_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpFamily]  WITH CHECK ADD  CONSTRAINT [FK_tblEmpFamily_tblEmployee] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblEmpFamily] CHECK CONSTRAINT [FK_tblEmpFamily_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblEmployee_tblAddress]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmployee]  WITH CHECK ADD  CONSTRAINT [FK_tblEmployee_tblAddress] FOREIGN KEY([ProfAddressID])
REFERENCES [dbo].[tblAddress] ([AddressID])
GO
ALTER TABLE [dbo].[tblEmployee] CHECK CONSTRAINT [FK_tblEmployee_tblAddress]
GO
/****** Object:  ForeignKey [FK_tblEmployee_tblAddress1]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmployee]  WITH CHECK ADD  CONSTRAINT [FK_tblEmployee_tblAddress1] FOREIGN KEY([PersAddressID])
REFERENCES [dbo].[tblAddress] ([AddressID])
GO
ALTER TABLE [dbo].[tblEmployee] CHECK CONSTRAINT [FK_tblEmployee_tblAddress1]
GO
/****** Object:  ForeignKey [FK_tblEmployeeAccessbleBranchs_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmployeeAccessbleBranchs]  WITH CHECK ADD  CONSTRAINT [FK_tblEmployeeAccessbleBranchs_tblEmployee] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblEmployeeAccessbleBranchs] CHECK CONSTRAINT [FK_tblEmployeeAccessbleBranchs_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblEmployeeAccessbleBranchs_tblHospitalBranchDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmployeeAccessbleBranchs]  WITH CHECK ADD  CONSTRAINT [FK_tblEmployeeAccessbleBranchs_tblHospitalBranchDetails] FOREIGN KEY([HospitalBranchID])
REFERENCES [dbo].[tblHospitalBranchDetails] ([HospitalBranchID])
GO
ALTER TABLE [dbo].[tblEmployeeAccessbleBranchs] CHECK CONSTRAINT [FK_tblEmployeeAccessbleBranchs_tblHospitalBranchDetails]
GO
/****** Object:  ForeignKey [FK_tblEmployeeBranchLog_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmployeeBranchLog]  WITH CHECK ADD  CONSTRAINT [FK_tblEmployeeBranchLog_tblEmployee] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblEmployeeBranchLog] CHECK CONSTRAINT [FK_tblEmployeeBranchLog_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblEmployeeBranchLog_tblHospitalBranchDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmployeeBranchLog]  WITH CHECK ADD  CONSTRAINT [FK_tblEmployeeBranchLog_tblHospitalBranchDetails] FOREIGN KEY([HospitalBranchID])
REFERENCES [dbo].[tblHospitalBranchDetails] ([HospitalBranchID])
GO
ALTER TABLE [dbo].[tblEmployeeBranchLog] CHECK CONSTRAINT [FK_tblEmployeeBranchLog_tblHospitalBranchDetails]
GO
/****** Object:  ForeignKey [FK_tblEmpPaySetting_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpPaySetting]  WITH CHECK ADD  CONSTRAINT [FK_tblEmpPaySetting_tblEmployee] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblEmpPaySetting] CHECK CONSTRAINT [FK_tblEmpPaySetting_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblEmpPaySlip_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpPaySlip]  WITH CHECK ADD  CONSTRAINT [FK_tblEmpPaySlip_tblEmployee] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblEmpPaySlip] CHECK CONSTRAINT [FK_tblEmpPaySlip_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblEmpPaySlip_tblEmpPaySetting]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpPaySlip]  WITH CHECK ADD  CONSTRAINT [FK_tblEmpPaySlip_tblEmpPaySetting] FOREIGN KEY([PayID])
REFERENCES [dbo].[tblEmpPaySetting] ([PayID])
GO
ALTER TABLE [dbo].[tblEmpPaySlip] CHECK CONSTRAINT [FK_tblEmpPaySlip_tblEmpPaySetting]
GO
/****** Object:  ForeignKey [FK_tblEmpPersonalInfo_tblEmpPersonalInfo]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpPersonalInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblEmpPersonalInfo_tblEmpPersonalInfo] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblEmpPersonalInfo] CHECK CONSTRAINT [FK_tblEmpPersonalInfo_tblEmpPersonalInfo]
GO
/****** Object:  ForeignKey [FK_tblEmpPreviousExperience_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpPreviousExperience]  WITH CHECK ADD  CONSTRAINT [FK_tblEmpPreviousExperience_tblEmployee] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblEmpPreviousExperience] CHECK CONSTRAINT [FK_tblEmpPreviousExperience_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblProffesionalInfo_tblProffesionalInfo]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpProffesionalInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblProffesionalInfo_tblProffesionalInfo] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblEmpProffesionalInfo] CHECK CONSTRAINT [FK_tblProffesionalInfo_tblProffesionalInfo]
GO
/****** Object:  ForeignKey [FK_tblEmpSalarySettings_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblEmpSalarySettings]  WITH CHECK ADD  CONSTRAINT [FK_tblEmpSalarySettings_tblEmployee] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblEmpSalarySettings] CHECK CONSTRAINT [FK_tblEmpSalarySettings_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblFloor_tblHospitalTowers]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblFloor]  WITH CHECK ADD  CONSTRAINT [FK_tblFloor_tblHospitalTowers] FOREIGN KEY([TowerID])
REFERENCES [dbo].[tblHospitalTowers] ([TowerId])
GO
ALTER TABLE [dbo].[tblFloor] CHECK CONSTRAINT [FK_tblFloor_tblHospitalTowers]
GO
/****** Object:  ForeignKey [FK_tblHospital_tblAddress]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblHospital]  WITH CHECK ADD  CONSTRAINT [FK_tblHospital_tblAddress] FOREIGN KEY([AddressID])
REFERENCES [dbo].[tblAddress] ([AddressID])
GO
ALTER TABLE [dbo].[tblHospital] CHECK CONSTRAINT [FK_tblHospital_tblAddress]
GO
/****** Object:  ForeignKey [FK_tblHospitalDtl_tblHospitalDtl]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblHospital]  WITH CHECK ADD  CONSTRAINT [FK_tblHospitalDtl_tblHospitalDtl] FOREIGN KEY([ParentHospitalId])
REFERENCES [dbo].[tblHospital] ([HospitalId])
GO
ALTER TABLE [dbo].[tblHospital] CHECK CONSTRAINT [FK_tblHospitalDtl_tblHospitalDtl]
GO
/****** Object:  ForeignKey [FK_tblHospitalTowers_tblHospital]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblHospitalTowers]  WITH CHECK ADD  CONSTRAINT [FK_tblHospitalTowers_tblHospital] FOREIGN KEY([Hospitalid])
REFERENCES [dbo].[tblHospital] ([HospitalId])
GO
ALTER TABLE [dbo].[tblHospitalTowers] CHECK CONSTRAINT [FK_tblHospitalTowers_tblHospital]
GO
/****** Object:  ForeignKey [FK_tblDiagnosisDetails_tblHospitalBranchDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblLabDetails]  WITH CHECK ADD  CONSTRAINT [FK_tblDiagnosisDetails_tblHospitalBranchDetails] FOREIGN KEY([HospitalBranchID])
REFERENCES [dbo].[tblHospitalBranchDetails] ([HospitalBranchID])
GO
ALTER TABLE [dbo].[tblLabDetails] CHECK CONSTRAINT [FK_tblDiagnosisDetails_tblHospitalBranchDetails]
GO
/****** Object:  ForeignKey [FK_tblLabSettings_tblLabDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblLabSettings]  WITH CHECK ADD  CONSTRAINT [FK_tblLabSettings_tblLabDetails] FOREIGN KEY([LabID])
REFERENCES [dbo].[tblLabDetails] ([LabID])
GO
ALTER TABLE [dbo].[tblLabSettings] CHECK CONSTRAINT [FK_tblLabSettings_tblLabDetails]
GO
/****** Object:  ForeignKey [FK_tblLeave_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblLeave]  WITH CHECK ADD  CONSTRAINT [FK_tblLeave_tblEmployee] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblLeave] CHECK CONSTRAINT [FK_tblLeave_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblLookup_tblLookupCategory]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblLookup]  WITH CHECK ADD  CONSTRAINT [FK_tblLookup_tblLookupCategory] FOREIGN KEY([LookupCategoryId])
REFERENCES [dbo].[tblLookupCategory] ([LookupCategoryId])
GO
ALTER TABLE [dbo].[tblLookup] CHECK CONSTRAINT [FK_tblLookup_tblLookupCategory]
GO
/****** Object:  ForeignKey [FK_tblLookup_tblLookupCategory1]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblLookup]  WITH CHECK ADD  CONSTRAINT [FK_tblLookup_tblLookupCategory1] FOREIGN KEY([LookupCategoryId])
REFERENCES [dbo].[tblLookupCategory] ([LookupCategoryId])
GO
ALTER TABLE [dbo].[tblLookup] CHECK CONSTRAINT [FK_tblLookup_tblLookupCategory1]
GO
/****** Object:  ForeignKey [FK_tblMedicineInvoice_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineInvoice]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineInvoice_tblPatient] FOREIGN KEY([PatientID])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblMedicineInvoice] CHECK CONSTRAINT [FK_tblMedicineInvoice_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblMedicineInvoice_tblPharmacyDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineInvoice]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineInvoice_tblPharmacyDetails] FOREIGN KEY([PharmacyID])
REFERENCES [dbo].[tblPharmacyDetails] ([PharmacyID])
GO
ALTER TABLE [dbo].[tblMedicineInvoice] CHECK CONSTRAINT [FK_tblMedicineInvoice_tblPharmacyDetails]
GO
/****** Object:  ForeignKey [FK_tblMedicineInvoiceInfo_tblMedicineInvoice]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineInvoiceInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineInvoiceInfo_tblMedicineInvoice] FOREIGN KEY([InvoiceID])
REFERENCES [dbo].[tblMedicineInvoice] ([InvoiceID])
GO
ALTER TABLE [dbo].[tblMedicineInvoiceInfo] CHECK CONSTRAINT [FK_tblMedicineInvoiceInfo_tblMedicineInvoice]
GO
/****** Object:  ForeignKey [FK_tblMedicineInvoiceInfo_tblMedicinePurchase]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineInvoiceInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineInvoiceInfo_tblMedicinePurchase] FOREIGN KEY([PurchaseID])
REFERENCES [dbo].[tblMedicinePurchase] ([PurchaseID])
GO
ALTER TABLE [dbo].[tblMedicineInvoiceInfo] CHECK CONSTRAINT [FK_tblMedicineInvoiceInfo_tblMedicinePurchase]
GO
/****** Object:  ForeignKey [FK_tblMedicineInvoicePayMode_tblMedicineInvoice]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineInvoicePayMode]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineInvoicePayMode_tblMedicineInvoice] FOREIGN KEY([InvoiceID])
REFERENCES [dbo].[tblMedicineInvoice] ([InvoiceID])
GO
ALTER TABLE [dbo].[tblMedicineInvoicePayMode] CHECK CONSTRAINT [FK_tblMedicineInvoicePayMode_tblMedicineInvoice]
GO
/****** Object:  ForeignKey [FK_tblMedicinePurchase_tblPharmacyDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicinePurchase]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicinePurchase_tblPharmacyDetails] FOREIGN KEY([PharmacyID])
REFERENCES [dbo].[tblPharmacyDetails] ([PharmacyID])
GO
ALTER TABLE [dbo].[tblMedicinePurchase] CHECK CONSTRAINT [FK_tblMedicinePurchase_tblPharmacyDetails]
GO
/****** Object:  ForeignKey [FK_tblMedicinePurchase_tblSupplier]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicinePurchase]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicinePurchase_tblSupplier] FOREIGN KEY([SupplierID])
REFERENCES [dbo].[tblSupplier] ([SupplierID])
GO
ALTER TABLE [dbo].[tblMedicinePurchase] CHECK CONSTRAINT [FK_tblMedicinePurchase_tblSupplier]
GO
/****** Object:  ForeignKey [FK_tblMedicinePurchaseInfo_tblMedicinePurchase]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicinePurchaseInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicinePurchaseInfo_tblMedicinePurchase] FOREIGN KEY([PurchaseID])
REFERENCES [dbo].[tblMedicinePurchase] ([PurchaseID])
GO
ALTER TABLE [dbo].[tblMedicinePurchaseInfo] CHECK CONSTRAINT [FK_tblMedicinePurchaseInfo_tblMedicinePurchase]
GO
/****** Object:  ForeignKey [FK_tblMedicineSalesReturn_tblPharmacyDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineSalesReturn]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineSalesReturn_tblPharmacyDetails] FOREIGN KEY([PharmacyID])
REFERENCES [dbo].[tblPharmacyDetails] ([PharmacyID])
GO
ALTER TABLE [dbo].[tblMedicineSalesReturn] CHECK CONSTRAINT [FK_tblMedicineSalesReturn_tblPharmacyDetails]
GO
/****** Object:  ForeignKey [FK_tblMedicineSalesReturnInfo_tblMedicineInvoice]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineSalesReturnInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineSalesReturnInfo_tblMedicineInvoice] FOREIGN KEY([InvoiceID])
REFERENCES [dbo].[tblMedicineInvoice] ([InvoiceID])
GO
ALTER TABLE [dbo].[tblMedicineSalesReturnInfo] CHECK CONSTRAINT [FK_tblMedicineSalesReturnInfo_tblMedicineInvoice]
GO
/****** Object:  ForeignKey [FK_tblMedicineSalesReturnInfo_tblMedicinePurchase]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineSalesReturnInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineSalesReturnInfo_tblMedicinePurchase] FOREIGN KEY([PurchaseID])
REFERENCES [dbo].[tblMedicinePurchase] ([PurchaseID])
GO
ALTER TABLE [dbo].[tblMedicineSalesReturnInfo] CHECK CONSTRAINT [FK_tblMedicineSalesReturnInfo_tblMedicinePurchase]
GO
/****** Object:  ForeignKey [FK_tblMedicineSalesReturnInfo_tblMedicineSalesReturn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineSalesReturnInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineSalesReturnInfo_tblMedicineSalesReturn] FOREIGN KEY([SalesReturnID])
REFERENCES [dbo].[tblMedicineSalesReturn] ([SalesReturnID])
GO
ALTER TABLE [dbo].[tblMedicineSalesReturnInfo] CHECK CONSTRAINT [FK_tblMedicineSalesReturnInfo_tblMedicineSalesReturn]
GO
/****** Object:  ForeignKey [FK_tblMedicineStockReturn_tblPharmacyDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineStockReturn]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineStockReturn_tblPharmacyDetails] FOREIGN KEY([PharmacyID])
REFERENCES [dbo].[tblPharmacyDetails] ([PharmacyID])
GO
ALTER TABLE [dbo].[tblMedicineStockReturn] CHECK CONSTRAINT [FK_tblMedicineStockReturn_tblPharmacyDetails]
GO
/****** Object:  ForeignKey [FK_tblMedicineStockReturnInfo_tblMedicinePurchase]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineStockReturnInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineStockReturnInfo_tblMedicinePurchase] FOREIGN KEY([PurchaseID])
REFERENCES [dbo].[tblMedicinePurchase] ([PurchaseID])
GO
ALTER TABLE [dbo].[tblMedicineStockReturnInfo] CHECK CONSTRAINT [FK_tblMedicineStockReturnInfo_tblMedicinePurchase]
GO
/****** Object:  ForeignKey [FK_tblMedicineStockReturnInfo_tblMedicineStockReturn]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineStockReturnInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineStockReturnInfo_tblMedicineStockReturn] FOREIGN KEY([StockReturnID])
REFERENCES [dbo].[tblMedicineStockReturn] ([StockReturnID])
GO
ALTER TABLE [dbo].[tblMedicineStockReturnInfo] CHECK CONSTRAINT [FK_tblMedicineStockReturnInfo_tblMedicineStockReturn]
GO
/****** Object:  ForeignKey [FK_tblMedicineSuggested_tblTreatment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblMedicineSuggested]  WITH CHECK ADD  CONSTRAINT [FK_tblMedicineSuggested_tblTreatment] FOREIGN KEY([TreatmentID])
REFERENCES [dbo].[tblTreatment] ([TreatmentId])
GO
ALTER TABLE [dbo].[tblMedicineSuggested] CHECK CONSTRAINT [FK_tblMedicineSuggested_tblTreatment]
GO
/****** Object:  ForeignKey [FK_tblOpLabRequest_tblAppointment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblOpLabRequest]  WITH CHECK ADD  CONSTRAINT [FK_tblOpLabRequest_tblAppointment] FOREIGN KEY([AppointmentID])
REFERENCES [dbo].[tblAppointment] ([AppointmentID])
GO
ALTER TABLE [dbo].[tblOpLabRequest] CHECK CONSTRAINT [FK_tblOpLabRequest_tblAppointment]
GO
/****** Object:  ForeignKey [FK_tblOpLabRequest_tblLabTest]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblOpLabRequest]  WITH CHECK ADD  CONSTRAINT [FK_tblOpLabRequest_tblLabTest] FOREIGN KEY([LabTestID])
REFERENCES [dbo].[tblLabTest] ([LabTestID])
GO
ALTER TABLE [dbo].[tblOpLabRequest] CHECK CONSTRAINT [FK_tblOpLabRequest_tblLabTest]
GO
/****** Object:  ForeignKey [FK_tblOprAttndDoctors_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblOprAttndDoctors]  WITH CHECK ADD  CONSTRAINT [FK_tblOprAttndDoctors_tblEmployee] FOREIGN KEY([DoctorID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblOprAttndDoctors] CHECK CONSTRAINT [FK_tblOprAttndDoctors_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblOprAttndDoctors_tblOprAttndDoctors]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblOprAttndDoctors]  WITH CHECK ADD  CONSTRAINT [FK_tblOprAttndDoctors_tblOprAttndDoctors] FOREIGN KEY([OperationID])
REFERENCES [dbo].[tblPtOperations] ([OperationID])
GO
ALTER TABLE [dbo].[tblOprAttndDoctors] CHECK CONSTRAINT [FK_tblOprAttndDoctors_tblOprAttndDoctors]
GO
/****** Object:  ForeignKey [FK_tblOprAttndNurses_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblOprAttndNurses]  WITH CHECK ADD  CONSTRAINT [FK_tblOprAttndNurses_tblEmployee] FOREIGN KEY([NurseID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblOprAttndNurses] CHECK CONSTRAINT [FK_tblOprAttndNurses_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblOprAttndNurses_tblPtOperations]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblOprAttndNurses]  WITH CHECK ADD  CONSTRAINT [FK_tblOprAttndNurses_tblPtOperations] FOREIGN KEY([OperationID])
REFERENCES [dbo].[tblPtOperations] ([OperationID])
GO
ALTER TABLE [dbo].[tblOprAttndNurses] CHECK CONSTRAINT [FK_tblOprAttndNurses_tblPtOperations]
GO
/****** Object:  ForeignKey [FK_tblPatient_tblAddress]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPatient]  WITH CHECK ADD  CONSTRAINT [FK_tblPatient_tblAddress] FOREIGN KEY([AddressID])
REFERENCES [dbo].[tblAddress] ([AddressID])
GO
ALTER TABLE [dbo].[tblPatient] CHECK CONSTRAINT [FK_tblPatient_tblAddress]
GO
/****** Object:  ForeignKey [FK_tblPatientContactInfo_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPatientContactInfo]  WITH CHECK ADD  CONSTRAINT [FK_tblPatientContactInfo_tblPatient] FOREIGN KEY([PatientID])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblPatientContactInfo] CHECK CONSTRAINT [FK_tblPatientContactInfo_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblPatientDisease_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPatientDisease]  WITH CHECK ADD  CONSTRAINT [FK_tblPatientDisease_tblPatient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblPatientDisease] CHECK CONSTRAINT [FK_tblPatientDisease_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblPatientInsurance_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPatientInsurance]  WITH CHECK ADD  CONSTRAINT [FK_tblPatientInsurance_tblPatient] FOREIGN KEY([PatientID])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblPatientInsurance] CHECK CONSTRAINT [FK_tblPatientInsurance_tblPatient]
GO
/****** Object:  ForeignKey [FK_tblPatientSpecializationFieldResults_tblSpecializationField]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPatientSpecializationFieldResults]  WITH CHECK ADD  CONSTRAINT [FK_tblPatientSpecializationFieldResults_tblSpecializationField] FOREIGN KEY([SpFieldID])
REFERENCES [dbo].[tblSpecializationField] ([SpFieldID])
GO
ALTER TABLE [dbo].[tblPatientSpecializationFieldResults] CHECK CONSTRAINT [FK_tblPatientSpecializationFieldResults_tblSpecializationField]
GO
/****** Object:  ForeignKey [FK_tblPatientSpecializationFieldResults_tblTreatment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPatientSpecializationFieldResults]  WITH CHECK ADD  CONSTRAINT [FK_tblPatientSpecializationFieldResults_tblTreatment] FOREIGN KEY([TreatmentID])
REFERENCES [dbo].[tblTreatment] ([TreatmentId])
GO
ALTER TABLE [dbo].[tblPatientSpecializationFieldResults] CHECK CONSTRAINT [FK_tblPatientSpecializationFieldResults_tblTreatment]
GO
/****** Object:  ForeignKey [FK_tblPharmacyDetails_tblHospitalBranchDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPharmacyDetails]  WITH CHECK ADD  CONSTRAINT [FK_tblPharmacyDetails_tblHospitalBranchDetails] FOREIGN KEY([HospitalBranchID])
REFERENCES [dbo].[tblHospitalBranchDetails] ([HospitalBranchID])
GO
ALTER TABLE [dbo].[tblPharmacyDetails] CHECK CONSTRAINT [FK_tblPharmacyDetails_tblHospitalBranchDetails]
GO
/****** Object:  ForeignKey [FK_tblPharmacySettings_tblPharmacyDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPharmacySettings]  WITH CHECK ADD  CONSTRAINT [FK_tblPharmacySettings_tblPharmacyDetails] FOREIGN KEY([PharmacyID])
REFERENCES [dbo].[tblPharmacyDetails] ([PharmacyID])
GO
ALTER TABLE [dbo].[tblPharmacySettings] CHECK CONSTRAINT [FK_tblPharmacySettings_tblPharmacyDetails]
GO
/****** Object:  ForeignKey [FK_tblPrescriptions_tblAppointment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPrescriptions]  WITH CHECK ADD  CONSTRAINT [FK_tblPrescriptions_tblAppointment] FOREIGN KEY([AppointmentID])
REFERENCES [dbo].[tblAppointment] ([AppointmentID])
GO
ALTER TABLE [dbo].[tblPrescriptions] CHECK CONSTRAINT [FK_tblPrescriptions_tblAppointment]
GO
/****** Object:  ForeignKey [FK_tblPrescriptions_tblProducts]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblPrescriptions]  WITH CHECK ADD  CONSTRAINT [FK_tblPrescriptions_tblProducts] FOREIGN KEY([ProductID])
REFERENCES [dbo].[tblProducts] ([ProductID])
GO
ALTER TABLE [dbo].[tblPrescriptions] CHECK CONSTRAINT [FK_tblPrescriptions_tblProducts]
GO
/****** Object:  ForeignKey [FK_tblProduct_tblManufactures]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblProducts]  WITH CHECK ADD  CONSTRAINT [FK_tblProduct_tblManufactures] FOREIGN KEY([MfgID])
REFERENCES [dbo].[tblManufacturers] ([MfgID])
GO
ALTER TABLE [dbo].[tblProducts] CHECK CONSTRAINT [FK_tblProduct_tblManufactures]
GO
/****** Object:  ForeignKey [FK_tblRoleMenu_tblMenu]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblRoleMenu]  WITH CHECK ADD  CONSTRAINT [FK_tblRoleMenu_tblMenu] FOREIGN KEY([MenuID])
REFERENCES [dbo].[tblMenu] ([MenuId])
GO
ALTER TABLE [dbo].[tblRoleMenu] CHECK CONSTRAINT [FK_tblRoleMenu_tblMenu]
GO
/****** Object:  ForeignKey [FK_tblRoleMenu_tblRoles]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblRoleMenu]  WITH CHECK ADD  CONSTRAINT [FK_tblRoleMenu_tblRoles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[tblRoles] ([RoleID])
GO
ALTER TABLE [dbo].[tblRoleMenu] CHECK CONSTRAINT [FK_tblRoleMenu_tblRoles]
GO
/****** Object:  ForeignKey [FK_tblRooms_tblHospital]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblRooms]  WITH CHECK ADD  CONSTRAINT [FK_tblRooms_tblHospital] FOREIGN KEY([HospitalBranchId])
REFERENCES [dbo].[tblHospitalBranchDetails] ([HospitalBranchID])
GO
ALTER TABLE [dbo].[tblRooms] CHECK CONSTRAINT [FK_tblRooms_tblHospital]
GO
/****** Object:  ForeignKey [FK_tblRooms_tblHospitalTowers]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblRooms]  WITH CHECK ADD  CONSTRAINT [FK_tblRooms_tblHospitalTowers] FOREIGN KEY([TowerId])
REFERENCES [dbo].[tblHospitalTowers] ([TowerId])
GO
ALTER TABLE [dbo].[tblRooms] CHECK CONSTRAINT [FK_tblRooms_tblHospitalTowers]
GO
/****** Object:  ForeignKey [FK_tblSpeciality_tblDrSettings]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblSpeciality]  WITH CHECK ADD  CONSTRAINT [FK_tblSpeciality_tblDrSettings] FOREIGN KEY([DrSettingsID])
REFERENCES [dbo].[tblDrSettings] ([DrSettingsID])
GO
ALTER TABLE [dbo].[tblSpeciality] CHECK CONSTRAINT [FK_tblSpeciality_tblDrSettings]
GO
/****** Object:  ForeignKey [FK_tblSpeclizationField_tblSpecalization]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblSpecializationField]  WITH CHECK ADD  CONSTRAINT [FK_tblSpeclizationField_tblSpecalization] FOREIGN KEY([SpID])
REFERENCES [dbo].[tblSpeciality] ([SpID])
GO
ALTER TABLE [dbo].[tblSpecializationField] CHECK CONSTRAINT [FK_tblSpeclizationField_tblSpecalization]
GO
/****** Object:  ForeignKey [FK_tblSpeclizationFieldLinkUp_tblSpecalization]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblSpecializationFieldLookUp]  WITH CHECK ADD  CONSTRAINT [FK_tblSpeclizationFieldLinkUp_tblSpecalization] FOREIGN KEY([SpID])
REFERENCES [dbo].[tblSpeciality] ([SpID])
GO
ALTER TABLE [dbo].[tblSpecializationFieldLookUp] CHECK CONSTRAINT [FK_tblSpeclizationFieldLinkUp_tblSpecalization]
GO
/****** Object:  ForeignKey [FK_tblSpeclizationFieldLinkUp_tblSpeclizationField]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblSpecializationFieldLookUp]  WITH CHECK ADD  CONSTRAINT [FK_tblSpeclizationFieldLinkUp_tblSpeclizationField] FOREIGN KEY([SpFieldID])
REFERENCES [dbo].[tblSpecializationField] ([SpFieldID])
GO
ALTER TABLE [dbo].[tblSpecializationFieldLookUp] CHECK CONSTRAINT [FK_tblSpeclizationFieldLinkUp_tblSpeclizationField]
GO
/****** Object:  ForeignKey [FK_tblSpeclizationFieldValues_tblSpeclizationField]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblSpecializationFieldValues]  WITH CHECK ADD  CONSTRAINT [FK_tblSpeclizationFieldValues_tblSpeclizationField] FOREIGN KEY([SpFieldID])
REFERENCES [dbo].[tblSpecializationField] ([SpFieldID])
GO
ALTER TABLE [dbo].[tblSpecializationFieldValues] CHECK CONSTRAINT [FK_tblSpeclizationFieldValues_tblSpeclizationField]
GO
/****** Object:  ForeignKey [FK_tblStore_tblHospitalBranchDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblStore]  WITH CHECK ADD  CONSTRAINT [FK_tblStore_tblHospitalBranchDetails] FOREIGN KEY([BranchID])
REFERENCES [dbo].[tblHospitalBranchDetails] ([HospitalBranchID])
GO
ALTER TABLE [dbo].[tblStore] CHECK CONSTRAINT [FK_tblStore_tblHospitalBranchDetails]
GO
/****** Object:  ForeignKey [FK_tblSupplier_tblAddress]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblSupplier]  WITH CHECK ADD  CONSTRAINT [FK_tblSupplier_tblAddress] FOREIGN KEY([AddressID])
REFERENCES [dbo].[tblAddress] ([AddressID])
GO
ALTER TABLE [dbo].[tblSupplier] CHECK CONSTRAINT [FK_tblSupplier_tblAddress]
GO
/****** Object:  ForeignKey [FK_TransactionDetails_Transactions]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTransactionInfo]  WITH CHECK ADD  CONSTRAINT [FK_TransactionDetails_Transactions] FOREIGN KEY([TransInfoID])
REFERENCES [dbo].[tblTransactions] ([TransID])
GO
ALTER TABLE [dbo].[tblTransactionInfo] CHECK CONSTRAINT [FK_TransactionDetails_Transactions]
GO
/****** Object:  ForeignKey [FK_tblTreatment_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTreatment]  WITH CHECK ADD  CONSTRAINT [FK_tblTreatment_tblEmployee] FOREIGN KEY([DoctorId])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblTreatment] CHECK CONSTRAINT [FK_tblTreatment_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblTreatment_tblPatient]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTreatment]  WITH CHECK ADD  CONSTRAINT [FK_tblTreatment_tblPatient] FOREIGN KEY([PatientId])
REFERENCES [dbo].[tblPatient] ([PatientId])
GO
ALTER TABLE [dbo].[tblTreatment] CHECK CONSTRAINT [FK_tblTreatment_tblPatient]
GO
/****** Object:  ForeignKey [FK_TblTreatmentCareCalander_tblCareInstruction]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[TblTreatmentCareCalander]  WITH CHECK ADD  CONSTRAINT [FK_TblTreatmentCareCalander_tblCareInstruction] FOREIGN KEY([CareInstructionID])
REFERENCES [dbo].[tblCareInstruction] ([CareInstructionID])
GO
ALTER TABLE [dbo].[TblTreatmentCareCalander] CHECK CONSTRAINT [FK_TblTreatmentCareCalander_tblCareInstruction]
GO
/****** Object:  ForeignKey [FK_TblTreatmentCareCalander_TblTreatmentCareCalander]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[TblTreatmentCareCalander]  WITH CHECK ADD  CONSTRAINT [FK_TblTreatmentCareCalander_TblTreatmentCareCalander] FOREIGN KEY([trtCareCalenderid])
REFERENCES [dbo].[TblTreatmentCareCalander] ([trtCareCalenderid])
GO
ALTER TABLE [dbo].[TblTreatmentCareCalander] CHECK CONSTRAINT [FK_TblTreatmentCareCalander_TblTreatmentCareCalander]
GO
/****** Object:  ForeignKey [FK_TblTreatmentCareCalander_tblTreatmentDiagnosis]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[TblTreatmentCareCalander]  WITH CHECK ADD  CONSTRAINT [FK_TblTreatmentCareCalander_tblTreatmentDiagnosis] FOREIGN KEY([TrDiagnosisID])
REFERENCES [dbo].[tblTreatmentDiagnosis] ([TrDiagnosisID])
GO
ALTER TABLE [dbo].[TblTreatmentCareCalander] CHECK CONSTRAINT [FK_TblTreatmentCareCalander_tblTreatmentDiagnosis]
GO
/****** Object:  ForeignKey [FK_TblTreatmentCareCalander_tblTreatmentMedicine]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[TblTreatmentCareCalander]  WITH CHECK ADD  CONSTRAINT [FK_TblTreatmentCareCalander_tblTreatmentMedicine] FOREIGN KEY([TrMedicineID])
REFERENCES [dbo].[tblTreatmentMedicine] ([TrMedicineID])
GO
ALTER TABLE [dbo].[TblTreatmentCareCalander] CHECK CONSTRAINT [FK_TblTreatmentCareCalander_tblTreatmentMedicine]
GO
/****** Object:  ForeignKey [FK_TblTreatmentCareCalander_tblTreatmentVitalSigns]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[TblTreatmentCareCalander]  WITH CHECK ADD  CONSTRAINT [FK_TblTreatmentCareCalander_tblTreatmentVitalSigns] FOREIGN KEY([VitalsignsID])
REFERENCES [dbo].[tblTreatmentVitalSigns] ([VitalsignsID])
GO
ALTER TABLE [dbo].[TblTreatmentCareCalander] CHECK CONSTRAINT [FK_TblTreatmentCareCalander_tblTreatmentVitalSigns]
GO
/****** Object:  ForeignKey [FK_tblTreatmentCareStatus_TblTreatmentCareCalander]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTreatmentCareStatus]  WITH CHECK ADD  CONSTRAINT [FK_tblTreatmentCareStatus_TblTreatmentCareCalander] FOREIGN KEY([trtCareCalenderid])
REFERENCES [dbo].[TblTreatmentCareCalander] ([trtCareCalenderid])
GO
ALTER TABLE [dbo].[tblTreatmentCareStatus] CHECK CONSTRAINT [FK_tblTreatmentCareStatus_TblTreatmentCareCalander]
GO
/****** Object:  ForeignKey [FK_tblTreatmentDiagnosis_tblTreatment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTreatmentDiagnosis]  WITH CHECK ADD  CONSTRAINT [FK_tblTreatmentDiagnosis_tblTreatment] FOREIGN KEY([TreatmentID])
REFERENCES [dbo].[tblTreatment] ([TreatmentId])
GO
ALTER TABLE [dbo].[tblTreatmentDiagnosis] CHECK CONSTRAINT [FK_tblTreatmentDiagnosis_tblTreatment]
GO
/****** Object:  ForeignKey [FK_tblTreatmentMedicine_tblTreatment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTreatmentMedicine]  WITH CHECK ADD  CONSTRAINT [FK_tblTreatmentMedicine_tblTreatment] FOREIGN KEY([TreatmentID])
REFERENCES [dbo].[tblTreatment] ([TreatmentId])
GO
ALTER TABLE [dbo].[tblTreatmentMedicine] CHECK CONSTRAINT [FK_tblTreatmentMedicine_tblTreatment]
GO
/****** Object:  ForeignKey [FK_tblTreatmentVitalSigns_tblTreatment]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblTreatmentVitalSigns]  WITH CHECK ADD  CONSTRAINT [FK_tblTreatmentVitalSigns_tblTreatment] FOREIGN KEY([TreatmentID])
REFERENCES [dbo].[tblTreatment] ([TreatmentId])
GO
ALTER TABLE [dbo].[tblTreatmentVitalSigns] CHECK CONSTRAINT [FK_tblTreatmentVitalSigns_tblTreatment]
GO
/****** Object:  ForeignKey [FK_tblUser_tblEmployee]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblUser]  WITH CHECK ADD  CONSTRAINT [FK_tblUser_tblEmployee] FOREIGN KEY([EID])
REFERENCES [dbo].[tblEmployee] ([EID])
GO
ALTER TABLE [dbo].[tblUser] CHECK CONSTRAINT [FK_tblUser_tblEmployee]
GO
/****** Object:  ForeignKey [FK_tblUser_tblRoles]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblUser]  WITH CHECK ADD  CONSTRAINT [FK_tblUser_tblRoles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[tblRoles] ([RoleID])
GO
ALTER TABLE [dbo].[tblUser] CHECK CONSTRAINT [FK_tblUser_tblRoles]
GO
/****** Object:  ForeignKey [FK_tblRooms_tblHospitalBranchDetails]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblWard]  WITH CHECK ADD  CONSTRAINT [FK_tblRooms_tblHospitalBranchDetails] FOREIGN KEY([HospitalBranchId])
REFERENCES [dbo].[tblHospital] ([HospitalId])
GO
ALTER TABLE [dbo].[tblWard] CHECK CONSTRAINT [FK_tblRooms_tblHospitalBranchDetails]
GO
/****** Object:  ForeignKey [FK_tblRooms_tblLookup]    Script Date: 09/30/2016 15:00:10 ******/
ALTER TABLE [dbo].[tblWard]  WITH CHECK ADD  CONSTRAINT [FK_tblRooms_tblLookup] FOREIGN KEY([RoomTypeId])
REFERENCES [dbo].[tblLookup] ([LookupId])
GO
ALTER TABLE [dbo].[tblWard] CHECK CONSTRAINT [FK_tblRooms_tblLookup]
GO
