﻿using System;
using HMISAPI.Infrastructure.Repository;
using System.Collections.Generic;
namespace HMISAPI.Domain.Interfaces
{
    public interface IPharmacy
    {
        #region Supplier Master
        List<SupplierRepository> GetSuppliers();
        List<SupplierRepository> GetSuppliersById(int id);
        SupplierRepository PostSupplier(SupplierRepository PobjSupplier);
        bool PutSupplier(int id, SupplierRepository PobjSupplier);
        bool DeleteSupplier(int id);
        List<SupplierRepository> GetSupplierBySearch(string PstrSearchTerm);
        #endregion

        #region Manufacturer Master
        List<ManufacturerRepository> GetManufacturers();
        ManufacturerRepository GetManufacturesById(int id);
        List<ManufacturerRepository> GetManufacturers(bool isActive = true);
        ManufacturerRepository PostManufacturer(ManufacturerRepository PobjManufacturer);
        bool PutManufacturer(int mfgId, ManufacturerRepository PobjManufacturer);
        bool DeleteManufacturer(int mfgId);
        List<ManufacturerRepository> GetManufacturerBySearch(string PstrSearchTerm);
        List<ManufacturerRepository> GetManufacturerByName(string PstrSearchTerm);
        #endregion


        #region MedicinePurchase
        MedicinePurchaseRepository PostMedicinePurchase(MedicinePurchaseRepository PobjMedicinePurchase);
        #endregion

        #region MedicineType
        List<MedicineTypeRepository> GetMedicineType();
        List<MedicineTypeRepository> GetMedicineTypeById(int id);
        MedicineTypeRepository PostMedicineType(MedicineTypeRepository PobjMedicineType);
        bool PutMedicineType(int FormId, MedicineTypeRepository PobjMedicineType);
        bool DeleteMedicineType(int LookupId);
        List<MedicineTypeRepository> GetMedicineTypeBySearch(string name);
       // List<MedicineTypeRepository> GetMedicineTypeByName(MedicineTypeRepository PobjMedicineGroup);
        #endregion

        #region MedicineGroup
        List<MedicineGroupRepository> GetMedicineGroup();
        List<MedicineGroupRepository> GetMedicineGroupById(int id);
        MedicineGroupRepository PostMedicineGroup(MedicineGroupRepository PobjProductGroup);
        bool PutMedicineGroup(int LookupId, MedicineGroupRepository PobjProductGroup);
        bool DeleteMedicineGroup(int LookupId);
        List<MedicineGroupRepository> GetMedicineGroupBySearch(string PstrSearchTerm);

        #endregion

        #region Product
        List<ProductsRepository> GetProduct();
        ProductsRepository PostProduct(ProductsRepository PobjProductDetails);
        bool PutProduct(int id, ProductsRepository PobjProduct);
        bool deleteProduct(int id);
        List<ProductsRepository> GetProductBySearch(string type, string PstrSearchTerm);
        List<ProductsRepository> GetProductByName(string PstrSearchTerm);
        #endregion

        #region SalesReturn
        List<MedicineInvoiceRepository> GetSalesReturn(int id);
        salesReturnRepository PostSalesReturn(salesReturnRepository pobjMedicineRepository);
        //List<MedicineBatchRepository> GetBatchNoByProductId(MedicineBatchRepository PIintProductId);
        #endregion

        #region LabReports
        List<LabTestReportRepository> GetLabTestReport();
        LabTestReportRepository PostLabTest(LabTestReportRepository PobjLabTest);

       List<LabOpRequestReportRepository> GetLabOpRequestReport();
       LabOpRequestReportRepository PostLabOpRequest(LabOpRequestReportRepository PobjLabOpRequest);
        #endregion

       #region Prescriptions
       List<DrugPrescriptionRepository> GetDrugPrescriptions();

       DrugPrescriptionRepository GetDrugPrescriptions(int id);
       DrugPrescriptionRepository PostDrugPrescription(DrugPrescriptionRepository PobjDrugPrescription);
       bool PutDrugPrescription(int TreatmentID, DrugPrescriptionRepository PobjDrugPrescription);

       List<LabPrescriptionsRepository> GetLabPrescriptions();

       LabPrescriptionsRepository GetLabPrescriptions(int id);
       LabPrescriptionsRepository PostLabPrescription(LabPrescriptionsRepository PobjLabPrescription);
       bool PutLabPrescription(int TreatmentID, LabPrescriptionsRepository PobjLabPrescription);

       #endregion


       #region StockReturn
       List<StockReturnMedicineDetailsRepository> GetStockReturnSuppliersByMedicineId(StockReturnMedicineDetailsRepository pstrSearch);
        List<StockReturnMedicineDetailsRepository> GetStockReturnBatchesBySupplier(StockReturnMedicineDetailsRepository pstrSearch);
        List<StockReturnMedicineDetailsRepository> GetStockReturnMedicineDetailsByBatchNo(StockReturnMedicineDetailsRepository pstrSearch);
        StockReturnMedicineDetailsRepository PostStockReturn(StockReturnMedicineDetailsRepository PobjStockReturnMedicines);
        #endregion

        #region MedicineInvoice        
        MedicineInvoiceRepository PostMedicineInvoice(MedicineInvoiceRepository PobjMedicineInvoice);
        List<MedicineBatchRepository> GetBatchNoByProductId(int id);
        #endregion

        #region Customer
        /// </summary>
        /// <param name="PobjCustomer"></param>
        /// <returns></returns>
        //CustomerRepository PostCustomer(CustomerRepository PobjCustomer);
        //List<CustomerRepository> GetCustomerBySearch(string SearchColumnName, string SearchTerm);
        //List<CustomerRepository> GetCustomerByName(CustomerRepository PobjCustomer);
        //List<CustomerRepository> GetCustomerById(int id);
        #endregion

        #region Alerts
        //List<ExpiredRepository> GetAllExpired();
        //List<ExpiredRepository> GetExpired(int days);
        //List<EndOfStockRepository> EndOfStock(int number);
        #endregion

        #region Reports

        List<DailySalesReportRepository> GetDailySalesReport();
        List<SalesReportRepository> GetSalesReportByMonth(DateTime PdtMonthSalesReport);
        List<SalesReportRepository> GetSalesReportByDate(DateTime PdtFromDate, DateTime PdtToDate);
        List<SalesReturnReportRepository> GetSalesReturnReportByMonth(DateTime PdtMonthSalesReturnReport);
        List<SalesReturnReportRepository> GetSalesReturnReportByDate(DateTime PdtFromDate, DateTime PdtToDate);
        List<StockReturnReportRepository> GetStockReturnReportByMonth(DateTime PdtMonthStockReturnReport);
        List<StockReturnReportRepository> GetStockReturnReportByDate(DateTime PdtFromDate, DateTime PdtToDate);
        List<PatientInvoiceReportRepository> GetPatientInvoiceReportByDate(DateTime PdtFromDate, DateTime PdtToDate);
        List<PatientInvoiceReportRepository> GetPatientInvoiceReportByPatientName(string PstrPatientName);
        List<PatientSalesReportRepository> GetPatientSalesReportByDate(DateTime PdtFromDate, DateTime PdtToDate);
        List<PatientSalesReportRepository> GetPatientSalesReportByMedicineName(string strMedicineName);
        List<PatientSalesReportRepository> GetPatientSalesReportByPatientName(string strPatientName);
        List<CollectionReportRepository> GetCollectionReportByMonth(DateTime PdtMonthCollectionReport);
        List<CollectionReportRepository> GetCollectionReportByDate(DateTime PdtFromDate, DateTime PdtToDate);
        List<ConsultationReportRepository> GetConsultationReportByMonth(DateTime PdtMonthCollectionReport);
        List<ConsultationReportRepository> GetConsultationReportByDate(DateTime PdtFromDate, DateTime PdtToDate);

        List<MedicineSalesandStockReportRepository> GetMedicineSalesandStockReportByName(string PstrMedicineName);
        List<MedicineStockReportRepository> GetMedicineStockReport(string PstrMedicineName, int? PintQuantityGreater, int? PintQuantityLessthan);

        #endregion


        
    }
}
