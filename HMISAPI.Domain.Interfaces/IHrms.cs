﻿using System;
using HMISAPI.Infrastructure.Repository;
using System.Collections.Generic;

namespace HMISAPI.Domain.Interfaces
{
    public interface IHrms
    {
        #region EmpFamily
        EmpFamilyRepository PostEmpFamily(EmpFamilyRepository PobjEmpFamily);
        List<EmpFamilyRepository> GetEmpFamily();
        EmpFamilyRepository GetEmpFamily(int id);
        bool PutEmpFamily(int id, EmpFamilyRepository pobjEmpFamily);
        bool DeleteEmpFamily(int id);
        #endregion
        #region EmpProfessional
        EmpProffesionalInfoRepository PostEmpProfess(EmpProffesionalInfoRepository PobjEmpPro);
        List<EmpProffesionalInfoRepository> GetEmpProfess(int? id);
        bool PutEmpProfess(int id, EmpProffesionalInfoRepository PobjEmpPro);
        bool DeleteEmpProfess(int id);
        #endregion
        #region EmpPersonal
        EmployeePersonalInfoRepository PostEmpPersonalInfo(EmployeePersonalInfoRepository EmployeePersonalInfo);
        List<EmployeePersonalInfoRepository> GetEmpPersonalInfo();
        EmployeePersonalInfoRepository GetEmpPersonalInfo(int id);
        bool DeleteEmpPersonalInfo(int id);
        bool PutEmpPersonalInfo(int id, EmployeePersonalInfoRepository EmployeePersonalInfo);
        #endregion

        List<HrmsDashboardRepository> GetHrmsDashBoard();
        List<HrmsDashboardRepository> GetEmployees();
        HrmsDashboardRepository GetEmployees(int id);
        HrmsDashboardRepository PostEmployee(HrmsDashboardRepository PobjManufacturer);
        bool PutEmployee(int EID, HrmsDashboardRepository PobjPutEmployee);
        bool DeleteEmployee(int eid);
        CtcRepository PostCTC(CtcRepository PobjSupplier);
    }
}
