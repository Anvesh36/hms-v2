﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMIS.Domain.Interfaces
{
    public interface IFrontOffice
    {
        CallRegisterRepository PostCallRegister(CallRegisterRepository CallRegister);
        //List<CallRegisterRepository> GetCallRegister(CallRegisterRepository CallRegister);
        bool PutCallRegister(int id, CallRegisterRepository CallRegister);

        #region CallRegister
        List<CallRegisterRepository> GetCallRegInfo();
        CallRegisterRepository GetCallRegInfo(int id);
        bool DeleteCallRegInfo(int id);
        #endregion
    }
}
