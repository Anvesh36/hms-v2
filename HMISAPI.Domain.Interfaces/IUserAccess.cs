﻿using System;
using HMISAPI.Infrastructure.Repository;
using System.Collections.Generic;

namespace HMISAPI.Domain.Interfaces
{
    public interface IUserAccess
    {
        #region Roles
        List<RolesRepository> GetRoles();
        RolesRepository GetRoles(int id);
        RolesRepository PostRole(RolesRepository PobjPostRole);
        bool PutRole(int EID, RolesRepository PobjPutRole);
        bool DeleteRole(int RoleID);

        #endregion

        #region UserInfo
        List<UserInfoRepository> GetUsers();
        UserInfoRepository GetUsers(int id);
        UserInfoRepository PostUser(UserInfoRepository PobjPostRole);
        bool PutUser(int EID, UserInfoRepository PobjPutRole);
        bool DeleteUser(int RoleID);
        #endregion

        #region Menu
        MenuRepository PostMenu(MenuRepository PobjPostMenu);
        List<MenuRepository> GetMenu();
        MenuRepository GetMenu(int id);
        bool PutMenu(int EID, MenuRepository PobjPutMenu);
        bool DeleteMenu(int RoleID);

        #endregion

        #region RoleMenu
        RoleMenuRepository PostRoleMenu(RoleMenuRepository PobjPostRoleMenu);
        List<RoleMenuRepository> GetRoleMenu();
        RoleMenuRepository GetRoleMenu(int id);
        bool PutRoleMenu(int roleid, RoleMenuRepository PobjPutRoleMenu);
        bool DeleteRoleMenu(int roleid);
        #endregion

    }
}
