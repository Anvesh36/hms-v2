﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMIS.Domain.Interfaces
{
    public interface IDiagnosis
    {
        List<GetPatientDetailsRepository> GetPatientDetailsByPatientId(GetPatientDetailsRepository PstrSearchTerm);
        List<GetAmountByDiagnosisRepository> GetAmountByDiagnosis(GetAmountByDiagnosisRepository PstrSearchTerm);
    }
}
