﻿using System;
using HMISAPI.Infrastructure.Repository;
using System.Collections.Generic;

namespace HMISAPI.Domain.Interfaces
{
    public interface IBloodBank
    {

        #region PhysicalExam
        BBDonorRepository PostDonor(BBDonorRepository PobjBBDonor);
        List<BBDonorRepository> GetDonor(); 
        BBDonorRepository GetDonor(int id);
        bool PutDonor(int id, BBDonorRepository PobjBBDonor);
        //BBQuestionnaireRepository PostQuestionnaire(BBQuestionnaireRepository PobjBBque);
        //List<BBQuestionnaireRepository> GettQuestionnaire(int? id);
        //bool PutQuestionnaire(int id,BBQuestionnaireRepository PobjBBque);
        #endregion

        #region PhysicalExam
        BBPhysicalExaminationRepository PostPhyExam(BBPhysicalExaminationRepository Pobjphyex);
        bool PutPhyExam(int id, BBPhysicalExaminationRepository Pobjphyex);
        List<BBPhysicalExaminationRepository> GetPhyExam();
        BBPhysicalExaminationRepository GetPhyExam(int id);
        #endregion

        #region Refrigerator
        List<BBRefrigeratorRepository> GetRefrigerator(int? id);
        BBRefrigeratorRepository PostRefrigerator(BBRefrigeratorRepository Pobjref);
        bool PutRefrigerator(int id, BBRefrigeratorRepository Pobjref);

        #endregion

        #region CellGrDetail
        BBGroupingCellRepository PostCellGrDetail(BBGroupingCellRepository PobjgrCell);
        BBGroupingCellRepository GetCellGroup(int id);
        List<BBGroupingCellRepository> GetCellGroup();
        bool PutCellGrDetail(int id, BBGroupingCellRepository PobjgrCell);

        #endregion

        #region BBserumGroup
        BBGroupingSerumRepository PostBBSerumGroup(BBGroupingSerumRepository PobjgrSerum);
        List<BBGroupingSerumRepository> GetBBSerumGroup();
        BBGroupingSerumRepository GetBBSerumGroup(int id);
        bool PutSerumGrDetail(int id, BBGroupingSerumRepository PobjgrSerum);

        #endregion

        #region Questionnaire
        BBQuestionnaireRepository PostQuestionnaire(BBQuestionnaireRepository PobjQue);
        List<BBQuestionnaireRepository> GetBBQuestionnaire();
        BBQuestionnaireRepository GetBBQuestionnaire(int id);
        bool PutQuestionnaire(int id, BBQuestionnaireRepository PobjQue);
        #endregion

        #region TransfusionReq

        BBTransfusionRequestRepository PostTransfusionReq(BBTransfusionRequestRepository PobjTreq);
        BBTransfusionRequestRepository GetTransfusionReq(int id);
        List<BBTransfusionRequestRepository> GetTransfusionReq();
        bool PutTransfusionReq(int id, BBTransfusionRequestRepository PobjTreq);
       
        
        bool DeleteBBTransfusionReq(int id);
        #endregion

        #region BBsettings
        BBSettingRepository PostBBSetting(BBSettingRepository PobjBBSet);
        
        BBSettingRepository GetBBSetting(int id);
        List<BBSettingRepository> GetBBSetting();
        bool PutBBSetting(int id, BBSettingRepository PobjBBSet);
        bool DeleteBBSetting(int id);

        #endregion

        #region MandatoryTest
        BBMandatoryTestrRepository PostBBMandatoryTest(BBMandatoryTestrRepository MandatoryTest);
        List<BBMandatoryTestrRepository> GetBBMandatoryTest();
        bool PutBBMandatoryTest(int id, BBMandatoryTestrRepository MandatoryTest);
        #endregion

        #region HaemotologyTest
        BBHaemotologyTestRepository PostBBHaemotologyTest(BBHaemotologyTestRepository HaemotologyTest);
        List<BBHaemotologyTestRepository> GetBBHaemotologyTest();
        bool PutBBHaemotologyTest(int id, BBHaemotologyTestRepository HaemotologyTest);
        #endregion

        #region  BBScreeningBiology
        BBScreeningBiologyRepository PostBBScreeningBiology(BBScreeningBiologyRepository BBScreeningBiology);
        List<BBScreeningBiologyRepository> GetBBScreeningBiology();
        bool PutBBScreeningBiology(int id, BBScreeningBiologyRepository BBScreeningBiology);
        #endregion

        #region  BBDiscard
        List<BBDiscardRepository> GetBBDiscard();

        BBDiscardRepository GetBBDiscard(int id);
        BBDiscardRepository PostBBDiscard(BBDiscardRepository BBDiscard);
        bool PutBBDiscard(int id, BBDiscardRepository BBDiscard);
        bool DeleteBBDiscard(int id);
        #endregion

        #region BloodBank
        List<BloodBankRepository> GetBloodBank();
        BloodBankRepository GetBloodBank(int id);
        BloodBankRepository PostBloodBank(BloodBankRepository PobjBB);
        bool PutBloodBank(int id, BloodBankRepository PobjBB);
        bool DeleteBloodBank(int id);
        #endregion

        #region BBDonation
        BBDonationRepository PostBBDonation(BBDonationRepository BBDonation);
        List<BBDonationRepository> GetBBDonation();
        BBDonationRepository GetBBDonation(int id);
        bool PutBBDonation(int Donationid, BBDonationRepository PobjBBDonation);
        bool DeleteBBDonation(int Donationid);
        #endregion


        #region BBCampRegistration
        BBCampRegistrationRepository PostCampRegistration(BBCampRegistrationRepository CampRegis);
        List<BBCampRegistrationRepository> GetCampRegistration();
        BBCampRegistrationRepository GetCampRegistration(int id);
        bool PutCampRegistration(int CampID, BBCampRegistrationRepository PobjCampRegistr);
        bool DeleteCampRegistration(int Donationid);
        #endregion

        #region  BBIssue
        List<BBIssueRepository> GetBBIssue();

        BBIssueRepository GetBBIssue(int id);
        BBIssueRepository PostBBIssue(BBIssueRepository BBDiscard);
        bool PutBBIssue(int id, BBIssueRepository BBDiscard);
        bool DeleteBBIssue(int id);
        #endregion

        List<BBTransfusionRequestRepository> GetBloodMatching(string strbloodgroup, bool isCrossMatch, DateTime RequiredOn, string component);
        BBTransfusionRequestRepository PostTransReqConfirm(BBTransfusionRequestRepository PobjTreqConfirm);
    }
}
