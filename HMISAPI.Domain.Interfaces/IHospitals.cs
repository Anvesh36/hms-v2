﻿using System;
using HMISAPI.Infrastructure.Repository;
using System.Collections.Generic;

namespace HMIS.Domain.Interfaces
{
    public interface IHospitals
    {
        # region Floors
        List<FloorRepository> GetFloors(int blockid);

        FloorRepository AddFloor(FloorRepository PobjFloor);

        bool UpdateFloor(int id,FloorRepository PobjFloor);

        bool DeleteFloor(int Id);
        #endregion

        #region Towers

        List<HospitalTowerRepository> GetTowers(int id);

        HospitalTowerRepository AddTowers(HospitalTowerRepository PobjTower);

        bool UpdateTowers(int id,HospitalTowerRepository model);

        bool DeleteTowers(int Id);
        #endregion

        #region Room
        RoomRepository AddRoom(RoomRepository PobjRoom);
        List<RoomRepository> GetRooms(int blockid);
        bool UpdateRoom(int id, RoomRepository PobjRoom);
        bool DeleteRoom(int Id);
        #endregion

        #region Bed
        List<BedRepository> GetBeds(int roomid);
        BedRepository AddBed(BedRepository PobjBed);
        bool UpdateBed(int id,BedRepository PobjBed);
        bool DeleteBed(int Id);
        #endregion

        #region Hospital
        List<HospitalRepository> GetHospital();
        HospitalRepository GetHospital(int id);
        HospitalRepository PostHospital(HospitalRepository PobjHospital);
        bool PutHospital(int id, HospitalRepository PobjHospital);
        bool DeleteHospital(int Id);
        #endregion

    }
}
