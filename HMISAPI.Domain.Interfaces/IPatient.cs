﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMISAPI.Domain.Interfaces
{
    public interface IPatient
    {
        Patientinfo RegisterPatient(Patientinfo PobjPatientDetails);
        List<Patientinfo> GetPatientList();
        bool PatientUpdate(int id, Patientinfo pobjpatienupdate);
        bool PatientDelete(int id);
        List<Patientinfo> GetOpReview();
        List<Patientinfo> GetPatientDetailsByID(int id);
        List<Patientinfo> GetPatients(string type, string name);
        List<AppointmentRepository> GetAppointMentsBYType(string type);

        // LookUpCatergory GetLookUpByCategory(int CategoryID);
        //List<LookupandCategory> GetAllCategoris();
        //LookUpDetails GetLookups();

        List<Patientinfo> GetPatientVisits(int id);
        Patientinfo PostAppointments(Patientinfo PobjAppointments);
     
        List<AppointmentRepository> GetAppointments();
        bool DeleteAppointment(int id);
        bool PutAppointment(int id, AppointmentRepository PobjAppointments);

        AppointmentRepository GenerateTokens(string aptType, DateTime aptdate, int aptid);
        DoctorScheduleRepository PostDoctorSchedule(DoctorScheduleRepository PobjDoctorSchedule);
 
        List<EmployeesRepository> GetDoctorVisits(int? id, DateTime? date);
        TreatmentRepository PosttreatmentVitalsigns(TreatmentRepository PobjTreatment);

        //  List<Patientinfo> GetPatients(string type, string name);
        Patientinfo PostOpReview(Patientinfo PobOpReview);
        List<TreatmentRepository> GetOpLstReview(DateTime treatdate);//DateTime treatdate
        List<Patientinfo> GetBillingDetails(int id);
        List<EmployeesRepository> GetDoctorsList();
        EmployeesRepository PostDoctor(EmployeesRepository Pobjemployees);
        bool PutDoctor(int id, EmployeesRepository Pobjemployees);
        List<IpAdmissionRepository> GetIpPatients(IpAdmissionRepository PobjIpAdmission);

        List<AppointmentRepository> GetAppointMentsByDoctorID(int DoctorID);

        #region DrSettings
        List<DrsettingsRepository> GetDrSettings();

        DrsettingsRepository PostDrSettings(DrsettingsRepository PobjDrsettings);

        DrsettingsRepository GetDrSettings(int id);

        bool PutDrSettings(int id, DrsettingsRepository PobjDrsettings);

        bool DeleteDrSettings(int id);
        #endregion

        List<AppointmentRepository> GetDoctorAppointments(int id, DateTime date);
        List<PatientDetailsRepository> GetDoctorPatients(int doctorId, string PatientType);

        List<AppointmentRepository> GetTokenSummary(DateTime date, int? id);

    }
}
