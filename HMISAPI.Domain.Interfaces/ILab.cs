﻿using System;
using HMISAPI.Infrastructure.Repository;
using System.Collections.Generic;

namespace HMISAPI.Domain.Interfaces
{
    public interface ILab 
    {
        #region Lab 
        List<DiagnosisRepository> GetInvestigationsBySearch(string Pstrterm);
        DiagnosisRepository PostInvestigations(DiagnosisRepository PobjDiagRep);
        List<DiagnosisRequisitionRepository> GetPatientsByID(int id);
        DiagnosisRequisitionRepository PostDiagnosisRequisition(DiagnosisRequisitionRepository pobjdiagrequisition);
        DiagnosisRequisitionRepository PostOpRequest(DiagnosisRequisitionRepository pobjdiagrequisition);
        List<OpCollectionsReport> GetDailyOpCollectionReport(DateTime PdtMonthOpCollectionReport);
        List<OpCollectionsReport> OpCollectionReportByDate(DateTime dtFromDate, DateTime dtToDate);
        List<OpCollectionsReport> OpCollectionReportByMonth(DateTime Date);
        List<DiagnosisRequisitionRepository> GetDailyLabCollection(DateTime date);
        List<DiagnosisRequisitionRepository> GetLabCollectionByDate(DateTime Frmdate,DateTime Todate);
        List<DiagnosisRequisitionRepository> GetMonthlyLabCollection(DateTime dtyear);
        List<DiagnosisRepository> GetTemplates(string pstrsearch);

        DiagnosisComponentsRepository PostTemplate(DiagnosisComponentsRepository PobjCompValue);    
        
        DiagnosisRepository PostDiagnosis(DiagnosisRepository PobjDiagRep);
        bool PutDiagnosis(int Id, DiagnosisRepository PobjDiagRep);
        DiagRequisitionInfoResultRepository PostDiagResults(DiagRequisitionInfoResultRepository PobjDiagReqResult);
        bool PutDiagResults(int id, DiagRequisitionInfoResultRepository PobjDiagReqResult);
        TransactionRepository PostVocherPayment(TransactionRepository pobjtransaction);

        List<TransactionRepository> GetVocherPayment();
        List<GetDiagnosisReportRepository> GetDiagnosisReport();

       

        bool PutVocherPayment(int TransID, TransactionRepository pobjtransaction);
        List<RequestSummaryRepository> GetRequestSummary();
        DiagnosisRepository PostDiagnosisComponents(DiagnosisRepository PobjDiagnosisComponenet);



       
        #endregion
    }
}
