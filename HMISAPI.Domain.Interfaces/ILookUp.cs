﻿using System;
using Repository = HMISAPI.Infrastructure.Repository;
using System.Collections.Generic;

namespace HMISAPI.Domain.Interfaces
{
    public interface ILookUp
    {
        List<Repository.LookUp> GetLookUps(string name, bool isActive = true);
        Repository.PatientLookUps GetPatientLookUps();

        Repository.LookUp CreateLookUp(Repository.LookUp poLookUp, string categoryName);
        bool UpdateLookUp(int id, Repository.LookUp poLookUp);
        bool DeleteLookUp(int id);
    }

}
