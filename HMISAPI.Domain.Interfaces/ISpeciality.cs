﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Infrastructure.Repository;

namespace HMIS.Domain.Interfaces
{
    public interface ISpeciality
    {
        SpecialityRepository PostSpeciality(SpecialityRepository PobjSpe);
        List<SpecialityRepository> GetSpeciality();
        SpecialityRepository GetSpeciality(int id);
        bool PutSpecialityDetail(int id, SpecialityRepository PobjSpe);
        bool DeleteSpecialityDetail(int id);
    }

}
