﻿using System;
using HMISAPI.Infrastructure.Repository;
using System.Collections.Generic;

namespace HMISAPI.Domain.Interfaces
{
    public interface ICentralStores
    {
        #region Equipment
        EquipmentRepository GetEquipment(int id);
        List<EquipmentRepository> GetEquipment();
        EquipmentRepository PostEquipment(EquipmentRepository pobjEquip);
        bool PutEquipment(int id,EquipmentRepository PobjEquip);
        bool DeleteEquipment(int id);
        #endregion
        
        #region store
        StoreRepository GetStore(int id);
        List<StoreRepository> GetStore();
        StoreRepository PostStore(StoreRepository PobjStore);
        bool PutStore(int id, StoreRepository PobjStore);
        bool DeleteStore(int id);

        #endregion
        #region ChemicalUsed
        ChemicalUsedRepository GetChemicalUsed(int id);
        List<ChemicalUsedRepository> GetChemicalUsed();
        ChemicalUsedRepository PostChemicalUsed(ChemicalUsedRepository PobjChemUsed);
        bool PutChemicalUsed(int id, ChemicalUsedRepository PobjChemUsed);
        bool DeleteChemicalUsed(int id);

        #endregion

        #region ChemicalPurchase
        ChemicalPurchaseRepository GetChemicalPurchase(int id);
        List<ChemicalPurchaseRepository> GetChemicalPurchase();
        ChemicalPurchaseRepository PostChemicalPurchase(ChemicalPurchaseRepository pobjChemPur);
        bool PutChemicalPurchase(int id, ChemicalPurchaseRepository pobjChemPur);
        bool DeleteChemicalPurchase(int id);

        #endregion

    }
}
