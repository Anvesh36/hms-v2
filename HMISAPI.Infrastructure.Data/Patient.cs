﻿using HMISAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Domain.Entities;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;
using System.Data.Linq.SqlClient;
using System.Data.Entity.Validation;


namespace HMISAPI.Infrastructure.Data
{
    public class Patient : IPatient
    {
        HospitalTenantContext _db = new HospitalTenantContext();
        public Patientinfo RegisterPatient(Patientinfo PobjPatientDetails)
        {
            HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();

            try
            {
                tblPatient LobjtblPatient = new tblPatient();
                tblAddress LobjtblAddress = new tblAddress();
                tblPatientInsurance LobjtblPatientInsurance = new tblPatientInsurance();
                ObjectCopier.CopyObjectData(PobjPatientDetails.Address, LobjtblAddress);
                ObjectCopier.CopyObjectData(PobjPatientDetails, LobjtblPatient);
                LobjtblAddress.IsActive = true;
                LobjtblAddress.CreatedBy = 1;
                LobjtblAddress.CreatedDate = DateTime.Now;

                LobjtblPatient.IsActive = true;
                LobjtblPatient.CreatedBy = 1;
                LobjtblPatient.CreatedDate = DateTime.Now;

                //LobjtblPatient.tblPatientInsurances = LobjtblPatientInsurance;
                List<tblPatientInsurance> lsttblPatientins = new List<tblPatientInsurance>();
                foreach (PatientInsuranceRepository LobjPatins in PobjPatientDetails.lstPatientInsurance)
                {
                    tblPatientInsurance lobjpatientInsurance = new tblPatientInsurance();
                    lobjpatientInsurance.EffictiveFrom = LobjPatins.EffictiveFrom;
                    lobjpatientInsurance.EffictiveTo = LobjPatins.EffictiveTo;
                    lobjpatientInsurance.CompanyName = LobjPatins.CompanyName;
                    lobjpatientInsurance.InsuranceName = LobjPatins.InsuranceName;
                    lobjpatientInsurance.IsActive = 1;
                    lobjpatientInsurance.CreatedBy = LobjPatins.CreatedBy;
                    lobjpatientInsurance.CreatedDate = DateTime.Now;
                    lsttblPatientins.Add(lobjpatientInsurance);
                }
                LobjtblPatient.tblPatientInsurances = lsttblPatientins;

                //LobjtblPatient.tblAddress = LobjtblAddress;

                LobjHMISTenantContext.tblAddresses.Add(LobjtblAddress);
                LobjHMISTenantContext.tblPatients.Add(LobjtblPatient);

                //LobjHMISTenantContext.tblPatientInsurances.Add(LobjtblPatientInsurance);
                int res = LobjHMISTenantContext.SaveChanges();
                int addressid = LobjtblAddress.AddressID;
                LobjtblPatient.AddressID = addressid;
                int res1 = LobjHMISTenantContext.SaveChanges();
                PobjPatientDetails.Address.AddressID = addressid;
                ObjectCopier.CopyObjectData(LobjtblAddress, PobjPatientDetails);
                ObjectCopier.CopyObjectData(LobjtblPatient, PobjPatientDetails);
                ObjectCopier.CopyObjectData(LobjtblPatientInsurance, PobjPatientDetails);
                return PobjPatientDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool PatientUpdate(int id, Patientinfo pobjPatientUpdate)
        {
            try
            {
                tblPatient LobjtblPatintDemographic = new tblPatient();
                using (var LobjHMISTenantContext = new HospitalTenantContext())
                {
                    LobjtblPatintDemographic = LobjHMISTenantContext.tblPatients.Where(P => P.PatientId == id).FirstOrDefault<tblPatient>();
                    if (LobjtblPatintDemographic != null)
                    {
                        LobjtblPatintDemographic.LastName = pobjPatientUpdate.LastName;
                        LobjtblPatintDemographic.ModifiedBy = pobjPatientUpdate.ModifiedBy;
                    }

                    //LobjtblPatintDemographic = LobjHMISTenantContext.tblPatients.Find(pobjPatientUpdate.PatientID);
                    //LobjHMISTenantContext.tblPatients.Remove(LobjtblPatintDemographic);

                    var data = LobjHMISTenantContext.SaveChanges();
                    if (data == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<Patientinfo> GetPatientList()
        {
            HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
            List<Patientinfo> LlstPatints = new List<Patientinfo>();
            var suprs = from sup in LobjHMISTenantContext.tblPatients
                        join addres in LobjHMISTenantContext.tblAddresses
                        on sup.AddressID equals addres.AddressID
                        join insurnc in LobjHMISTenantContext.tblPatientInsurances
                        on sup.PatientId equals insurnc.PatientID

                        select new Patientinfo
                        {
                            PatientID = sup.PatientId,
                            HospitalBranchId = sup.HospitalBranchId,
                            Title = sup.Title,
                            FirstName = sup.FirstName,
                            LastName = sup.LastName,
                            CallName = sup.CallName,
                            DOB = sup.DOB,
                            Age = sup.Age,
                            GenderName = sup.Gender,
                            ContactNumber = sup.ContactNumber,
                            GuardianName = sup.GuardianName,
                            GuardianContactNumber = sup.GuardianContactNumber,
                            PatientCode = sup.PatientCode,
                            PatientType = sup.PatientType,
                            AdharNo = sup.AadhaarNo,
                            BloodGroup = sup.BloodGroup,
                            MaritalStatus = sup.MaritalStatus,
                            Occupation = sup.Occupation,
                            Address = new AddressRepository
                            {
                                AddressLine1 = addres.AddressLine1,
                                AddressLine2 = addres.AddressLine2,
                                City = addres.City
                            },
                            PatientInsurance = new PatientInsuranceRepository
                            {
                                EffictiveFrom = insurnc.EffictiveFrom,
                                EffictiveTo = insurnc.EffictiveTo,
                                InsuranceName = insurnc.InsuranceName,
                                CompanyName = insurnc.CompanyName
                            },
                            IsActive = sup.IsActive,
                            CreatedBy = sup.CreatedBy,
                            CreatedDate = sup.CreatedDate,
                        };
            LlstPatints = suprs.ToList<Patientinfo>();
            return LlstPatints;
        }
        public bool PatientDelete(int id)
        {
            try
            {
                tblPatient LobjtblPatient = new tblPatient();
                HospitalTenantContext _db = new HospitalTenantContext();
                LobjtblPatient = _db.tblPatients.Where(m => m.PatientId == id).FirstOrDefault<tblPatient>();
                if (LobjtblPatient != null)
                {
                    LobjtblPatient.IsActive = false;
                    LobjtblPatient.ModifiedDate = DateTime.Now;
                    //LobjtblPatient.ModifiedBy = 2;
                }
                _db.tblPatients.Remove(LobjtblPatient);
                int res = _db.SaveChanges();
                if (res == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                //tblPatient LobjtblPatintDemographic = new tblPatient();
                //using (var LobjHMISTenantContext = new HospitalTenantContext())
                //{
                //    LobjtblPatintDemographic = LobjHMISTenantContext.tblPatients.Find(id);
                //    LobjHMISTenantContext.tblPatients.Remove(LobjtblPatintDemographic);
                //    var data = LobjHMISTenantContext.SaveChanges();
                //    if (data == 1)
                //    {
                //        return true;
                //    }
                //    else
                //    {
                //        return false;
                //    }
                //}

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Patientinfo> GetPatientDetailsByID(int id)
        {
            HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
            List<Patientinfo> LlstPatints = new List<Patientinfo>();
            var suprs = from sup in LobjHMISTenantContext.tblPatients
                        where sup.PatientId == id

                        select new Patientinfo
                        {
                            PatientID = sup.PatientId,
                            HospitalBranchId = sup.HospitalBranchId,
                            Title = sup.Title,
                            FirstName = sup.FirstName,
                            LastName = sup.LastName,
                            CallName = sup.CallName,
                            DOB = sup.DOB,
                            Age = sup.Age,
                            GenderName = sup.Gender,
                            ContactNumber = sup.ContactNumber,
                            GuardianName = sup.GuardianName,
                            GuardianContactNumber = sup.GuardianContactNumber,
                            PatientCode = sup.PatientCode,
                            PatientType = sup.PatientType,
                            AdharNo = sup.AadhaarNo,
                            BloodGroup = sup.BloodGroup,
                            MaritalStatus = sup.MaritalStatus,
                            Occupation = sup.Occupation,
                            IsActive = sup.IsActive,
                            CreatedBy = sup.CreatedBy,
                            CreatedDate = sup.CreatedDate,
                        };
            LlstPatints = suprs.ToList<Patientinfo>();
            return LlstPatints;
        }
        public List<Patientinfo> GetPatients(string type, string PstrSearchTerm)
        {

            HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
            List<Patientinfo> LobjlstProduct = new List<Patientinfo>();
            // tblProduct LobtblProduct = new tblProduct();
            if (type == "FirstName")
            {


                var suprs = from sup in GobjHospitalTenantContext.tblPatients

                            // where SqlMethods.Like(sup.FirstName, "PstrSearchTerm")
                            where sup.FirstName.StartsWith(PstrSearchTerm)

                            select new Patientinfo
                            {

                                PatientID = sup.PatientId,
                                FirstName = sup.FirstName,
                                LastName = sup.LastName,
                                Email = sup.Email,
                                ContactNumber = sup.ContactNumber

                            };
                LobjlstProduct = suprs.ToList<Patientinfo>();
            }
            else if (type == "MobileNumber")
            {
                var suprs = from sup in GobjHospitalTenantContext.tblPatients

                            //where SqlMethods.Like(sup.ContactNumber, "%/PstrSearchTerm/%")

                            where sup.ContactNumber.StartsWith(PstrSearchTerm)

                            select new Patientinfo
                            {
                                PatientID = sup.PatientId,
                                FirstName = sup.FirstName,
                                LastName = sup.LastName,
                                Email = sup.Email,
                                ContactNumber = sup.ContactNumber

                            };

                LobjlstProduct = suprs.ToList<Patientinfo>();
            }

            else if (type == "PatientId")
            {
                var suprs = from sup in GobjHospitalTenantContext.tblPatients

                            //where SqlMethods.Like(sup.ContactNumber, "%/PstrSearchTerm/%")

                            where sup.PatientId.ToString().Contains(PstrSearchTerm)

                            select new Patientinfo
                            {
                                PatientID = sup.PatientId,
                                FirstName = sup.FirstName,
                                LastName = sup.LastName,
                                Email = sup.Email,
                                ContactNumber = sup.ContactNumber

                            };

                LobjlstProduct = suprs.ToList<Patientinfo>();
            }

            return LobjlstProduct;
        }

        //public LookUpCatergory GetLookUpByCategory(int intCategoryID)
        //{
        //    HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
        //    LookUpCatergory _Loblookupcatgory = new LookUpCatergory();
        //    LobjHMISTenantContext.tblLookupCategories.Find(intCategoryID);
        //    return _Loblookupcatgory;

        //}      

        //public List<Patientinfo> GetPatients(string type, string PstrSearchTerm)
        //{

        //    HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
        //    List<Patientinfo> LobjlstProduct = new List<Patientinfo>();
        //    // tblProduct LobtblProduct = new tblProduct();
        //    if (type == "FirstName")
        //    {
        //        var suprs = from sup in GobjHospitalTenantContext.tblPatients

        //                    // where SqlMethods.Like(sup.FirstName, "PstrSearchTerm")
        //                    where sup.FirstName.StartsWith(PstrSearchTerm)

        //                    select new Patientinfo
        //                    {

        //                        PatientID = sup.PatientId,
        //                        FirstName = sup.FirstName,
        //                        LastName = sup.LastName,
        //                        Email = sup.Email,
        //                        ContactNumber = sup.ContactNumber

        //                    };
        //        LobjlstProduct = suprs.ToList<Patientinfo>();
        //    }
        //    else if (type == "MobileNumber")
        //    {
        //        var suprs = from sup in GobjHospitalTenantContext.tblPatients

        //                    //where SqlMethods.Like(sup.ContactNumber, "%/PstrSearchTerm/%")

        //                    where sup.ContactNumber.StartsWith(PstrSearchTerm)

        //                    select new Patientinfo
        //                    {
        //                        PatientID = sup.PatientId,
        //                        FirstName = sup.FirstName,
        //                        LastName = sup.LastName,
        //                        Email = sup.Email,
        //                        ContactNumber = sup.ContactNumber

        //                    };

        //        LobjlstProduct = suprs.ToList<Patientinfo>();
        //    }

        //    else if (type == "PatientId")
        //    {
        //        var suprs = from sup in GobjHospitalTenantContext.tblPatients

        //                    //where SqlMethods.Like(sup.ContactNumber, "%/PstrSearchTerm/%")

        //                    where sup.PatientId.ToString().Contains(PstrSearchTerm)

        //                    select new Patientinfo
        //                    {
        //                        PatientID = sup.PatientId,
        //                        FirstName = sup.FirstName,
        //                        LastName = sup.LastName,
        //                        Email = sup.Email,
        //                        ContactNumber = sup.ContactNumber

        //                    };

        //        LobjlstProduct = suprs.ToList<Patientinfo>();
        //    }

        //    return LobjlstProduct;
        //}
        public List<Patientinfo> GetPatientVisits(int id)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<Patientinfo> LobjtblPatient = new List<Patientinfo>();
            var suprs = from p in _db.tblPatients
                        join
                        pd in _db.tblPatientDiseases on p.PatientId equals pd.PatientId
                        join

                        ap in _db.tblAppointments on p.PatientId equals ap.PatientID

                        where p.PatientId == id

                        select new Patientinfo
                        {

                            PatientID = p.PatientId,
                            FirstName = p.FirstName,
                            Age = p.Age,
                            Syndrome = pd.DiseaseName,
                            AppointmentID = ap.AppointmentID
                        };
            LobjtblPatient = suprs.ToList<Patientinfo>();

            return LobjtblPatient;
        }
        public  AppointmentRepository GetAppointmentsByid(int id)
        {
            AppointmentRepository lstApp = new AppointmentRepository();
            lstApp = (from a in _db.tblAppointments
                      where a.IsActive == true && a.AppointmentID == id
                      select new AppointmentRepository
                      {
                          AppointmentID = a.AppointmentID,
                          AppointmentMode = a.AppointmentMode,
                          AppointmentStatus = a.AppointmentStatus,
                          DoctorID = a.DoctorID,
                          PayStatus=a.PayStatus,
                          //Discount = a.Discount,
                          //EndDateTime = a.EndDateTime,
                          PayMode = a.PayMode,
                          //StartDateTime = a.StartDateTime,
                          TokenNo = a.TokenNo
                      }).FirstOrDefault<AppointmentRepository>();

            return lstApp;
        }
        public Patientinfo PostAppointments(Patientinfo PobjPatient)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            tblPatient LobjPatient = new tblPatient();
            tblAppointment LobjAppointment = new tblAppointment();
            ObjectCopier.CopyObjectData(PobjPatient, LobjPatient);
            ObjectCopier.CopyObjectData(PobjPatient.Apprep, LobjAppointment);
            LobjPatient.IsActive = true;
            LobjPatient.CreatedDate = DateTime.Now;
            LobjAppointment.IsActive = true;
            LobjAppointment.CreatedDate = DateTime.Now;
            _db.tblPatients.Add(LobjPatient);
            _db.tblAppointments.Add(LobjAppointment);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjPatient, PobjPatient);
            ObjectCopier.CopyObjectData(LobjAppointment, PobjPatient.Apprep);
            return PobjPatient;
        }

        public AppointmentRepository GetAppointmentsbyName(string apmode)
        {
            // appmode,appid,date==""
            AppointmentRepository lstApp = new AppointmentRepository();
            var Online = (from p in _db.tblAppointments
                          where p.PayStatus == "paid" && p.AppointmentMode == apmode
                          select p.AppointmentID).Max();
            lstApp = (from a in _db.tblAppointments
                      where a.IsActive == true && a.AppointmentID == Online
                      select new AppointmentRepository
                      {
                          AppointmentID = a.AppointmentID,
                          AppointmentMode = a.AppointmentMode,
                          AppointmentStatus = a.AppointmentStatus,
                          DoctorID = a.DoctorID,
                          Discount = a.Discount,
                          EndDateTime = a.EndDateTime,
                          PayMode = a.PayMode,
                          StartDateTime = a.StartDateTime,
                          TokenNo = a.TokenNo
                      }).FirstOrDefault<AppointmentRepository>();

            return lstApp;
        }


        public AppointmentRepository GenerateTokens(string aptType, DateTime aptdate, int aptid)
        {
            string aptType1 = aptType.ToUpper();
            tblAppointment LobjAppointment = new tblAppointment();
            AppointmentRepository lstApp = new AppointmentRepository();
            AppointmentRepository apprep = GetAppointmentsByid(aptid);

            if (apprep.PayStatus == "paid")
            {
                var Online = (from p in _db.tblAppointments
                              where p.PayStatus == "Paid" && p.AppointmentMode == aptType
                                  && p.StartDateTime.Year == aptdate.Year && p.StartDateTime.Day == aptdate.Day && p.StartDateTime.Month == aptdate.Month
                              orderby p.TokenNo descending
                              select p.TokenNo).Take(1).FirstOrDefault();
                //string str = Convert.ToString(Online);
                LobjAppointment = _db.tblAppointments.Where(A => A.AppointmentID == aptid).FirstOrDefault<tblAppointment>();
                if (LobjAppointment != null)
                {
                    if (apprep.TokenNo == null)
                    {
                        if (Online != null)
                        {
                            if (Online.StartsWith("w") || Online.StartsWith("p") || Online.StartsWith("o") || Online.StartsWith("P") || Online.StartsWith("W") || Online.StartsWith("O"))
                            {
                                string str1 = Online.Substring(1, 3);
                                int i = Convert.ToInt32(str1);
                                i = i + 1;
                                string s = i.ToString().PadLeft(3, '0');
                                if (aptType1 == "WALKIN")
                                {
                                    LobjAppointment.TokenNo = ("W" + Convert.ToString(s));
                                }
                                else if (aptType1 == "ONLINE")
                                {
                                    LobjAppointment.TokenNo = ("O" + Convert.ToString(s));
                                }
                                else if (aptType1 == "PREBOOK")
                                {
                                    LobjAppointment.TokenNo = ("P" + Convert.ToString(s));
                                }
                                else
                                {
                                    LobjAppointment.TokenNo = null;
                                }
                            }
                        }

                        else if (Online == null)
                        {
                            if (aptType1 == "ONLINE")
                            {
                                LobjAppointment.TokenNo = "O001";
                            }
                            else if (aptType1 == "WALKIN")
                            {
                                LobjAppointment.TokenNo = "W001";
                            }
                            else if (aptType1 == "PREBOOK")
                            {
                                LobjAppointment.TokenNo = "P001";
                            }
                        }
                    }
                }
            }
            else
            {
                LobjAppointment.TokenNo = "NULL";
            }

            LobjAppointment.ModifiedDate = DateTime.Now;
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjAppointment, lstApp);
            return lstApp;
        }

        public List<AppointmentRepository> GetAppointments()
        {
            List<AppointmentRepository> Lstappointment = new List<AppointmentRepository>();
            HospitalTenantContext _db = new HospitalTenantContext();

            var res = from a in _db.tblAppointments
                      where a.IsActive == true
                      select new AppointmentRepository
                      {
                          AppointmentID = a.AppointmentID,
                          AppointmentMode = a.AppointmentMode,
                          AppointmentStatus = a.AppointmentStatus,
                          BranchID = a.BranchID,

                          ConsultFee = a.ConsultFee,

                          Comments = a.Comments,
                          DoctorID = a.DoctorID,
                          Discount = a.Discount,
                          EndDateTime = a.EndDateTime,

                          IsWaiveOff = a.IsWaiveOff,
                          PayMode = a.PayMode,
                          ReviewID = a.ReviewID,
                          StartDateTime = a.StartDateTime,
                          TokenNo = a.TokenNo

                      };
            Lstappointment = res.ToList<AppointmentRepository>();
            return Lstappointment;
        }
        public bool DeleteAppointment(int id)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            tblAppointment Lobjtblappointment = new tblAppointment();
            Lobjtblappointment = _db.tblAppointments.Where(a => a.AppointmentID == id).FirstOrDefault<tblAppointment>();
            if (Lobjtblappointment != null)
            {
                Lobjtblappointment.IsActive = false;
                Lobjtblappointment.ModifiedDate = DateTime.Now;
            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool PutAppointment(int id, AppointmentRepository PobjAppointments)
        {
            try
            {
                tblAppointment LobjAppointment = new tblAppointment();
                using (var LobjHMISTenantContext = new HospitalTenantContext())
                {
                    LobjAppointment = LobjHMISTenantContext.tblAppointments.Where(A => A.AppointmentID == id).FirstOrDefault<tblAppointment>();
                    if (LobjAppointment != null)
                    {
                        LobjAppointment.AppointmentStatus = PobjAppointments.AppointmentStatus;
                        LobjAppointment.ModifiedBy = PobjAppointments.ModifiedBy;
                        LobjAppointment.ConsultFee = PobjAppointments.ConsultFee;
                        LobjAppointment.ModifiedDate = DateTime.Now;
                        LobjAppointment.IsWaiveOff = PobjAppointments.IsWaiveOff;
                        LobjAppointment.PayMode = PobjAppointments.PayMode;
                        LobjAppointment.PayStatus = PobjAppointments.PayStatus;
                        LobjAppointment.BranchID = PobjAppointments.BranchID;
                        LobjAppointment.DoctorID = PobjAppointments.DoctorID;
                        LobjAppointment.AppointmentType = PobjAppointments.AppointmentType;
                        LobjAppointment.AppointmentStatus = PobjAppointments.AppointmentStatus;
                        LobjAppointment.TokenNo = PobjAppointments.TokenNo;
                        LobjAppointment.StartDateTime = PobjAppointments.StartDateTime;
                        LobjAppointment.EndDateTime = PobjAppointments.EndDateTime;
                        LobjAppointment.Comments = PobjAppointments.Comments;

                    }
                    var data = LobjHMISTenantContext.SaveChanges();
                    if (data == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public DoctorScheduleRepository PostDoctorSchedule(DoctorScheduleRepository PobjDoctorSchedule)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            tblDoctorSchedule LobjDoctorSchedule = new tblDoctorSchedule();
            ObjectCopier.CopyObjectData(PobjDoctorSchedule, LobjDoctorSchedule);
            LobjDoctorSchedule.IsActive = true;
            // LobjDoctorSchedule.CreatedBy = 1;
            LobjDoctorSchedule.CreatedDate = DateTime.Now;
            _db.tblDoctorSchedules.Add(LobjDoctorSchedule);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjDoctorSchedule, PobjDoctorSchedule);
            return PobjDoctorSchedule;
        }

        public List<EmployeesRepository> GetDoctorVisits(int? id, DateTime? date)
        {
            //string str = date.ToString("yyyy-MM-dd");
            //DateTime datee = Convert.ToDateTime(str);


            HospitalTenantContext _db = new HospitalTenantContext();
            List<EmployeesRepository> LobjEmployee = new List<EmployeesRepository>();
            var res = from sup in _db.tblEmployees
                      join
                      a in _db.tblAppointments on sup.EID equals a.DoctorID
                      join
                      d in _db.tblDepartments on sup.DepartmentId equals d.DeptID


                      select new EmployeesRepository
                      {
                          EID = sup.EID,
                          FirstName = sup.FirstName,
                          DepartmentName = d.DeptName,
                          AppointmentID = a.AppointmentID,
                          StartAppDate = a.StartDateTime

                      };

            if (id != null)
            {
                res = res.Where(p => p.EID == id);
            }
            if (date != null)
            {
                var datee = Convert.ToDateTime(date).Date;
                res = res.Where(a => a.StartAppDate.Year == datee.Year && a.StartAppDate.Month == datee.Month && a.StartAppDate.Day == datee.Day);
            }
            LobjEmployee = res.ToList<EmployeesRepository>();
            return LobjEmployee;
        }
        public List<Patientinfo> GetOpReview()
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<Patientinfo> LobjtblPatient = new List<Patientinfo>();
            var suprs = from sup in _db.tblPatients
                        join
                        pd in _db.tblPatientDiseases on sup.PatientId equals pd.PatientId
                        join
                        ap in _db.tblAppointments on sup.PatientId equals ap.PatientID
                        join em in _db.tblEmployees on ap.DoctorID equals em.EID
                        where sup.IsActive == true
                        select new Patientinfo
                        {
                            PatientID = sup.PatientId,
                            Age = sup.Age,
                            ContactNumber = sup.ContactNumber,

                            AppointmentTime = ap.StartDateTime,
                            PaymentType = ap.PayMode,
                            DoctorName = em.FirstName + " " + em.LastName


                        };
            LobjtblPatient = suprs.ToList<Patientinfo>();
            return LobjtblPatient;
        }
        public TreatmentRepository PosttreatmentVitalsigns(TreatmentRepository PobjTreatment)
        {
            tblTreatment LobjTreatment = new tblTreatment();
            HospitalTenantContext _db = new HospitalTenantContext();
            //LobjTreatment = _db.tblTreatments.Find(PobjTreatment.TreatmentId);
            List<tblTreatmentVitalSign> lstVitalsignsRep = new List<tblTreatmentVitalSign>();
            foreach (TreatmentVitalSignRepository Lobjvitalsignrep in PobjTreatment.lstVitalsignsRep)
            {
                tblTreatmentVitalSign lobjvitalsign = new tblTreatmentVitalSign();
                lobjvitalsign.TreatmentID = Lobjvitalsignrep.TreatmentID;
                lobjvitalsign.Value1 = Lobjvitalsignrep.Value1;
                lobjvitalsign.Value2 = Lobjvitalsignrep.Value2;
                lobjvitalsign.VitalSignType = Lobjvitalsignrep.VitalSignType;
                lobjvitalsign.CreatedBy = Lobjvitalsignrep.CreatedBy;
                lobjvitalsign.CreatedDate = DateTime.Now;
                lobjvitalsign.Note = Lobjvitalsignrep.Note;
                lstVitalsignsRep.Add(lobjvitalsign);
            }
            LobjTreatment.tblTreatmentVitalSigns = lstVitalsignsRep;
            //string str = "";
            //try
            //{
            int res = _db.SaveChanges();
            //}
            //catch (DbEntityValidationException ex)
            //{

            //    foreach (var entityValidationErrors in ex.EntityValidationErrors)
            //    {
            //        foreach (var validationError in entityValidationErrors.ValidationErrors)
            //        {
            //            str += "Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage;
            //        }
            //    }
            //}
            ObjectCopier.CopyObjectData(LobjTreatment, PobjTreatment);
            return PobjTreatment;
        }

        public List<Patientinfo> GetPatientProfile()
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<Patientinfo> LlstPatientProfile = new List<Patientinfo>();
            //var suprs = from sup in _db.tblPatients
            //            join ad in _db.tblAddresses
            //            on sup.AddressID equals ad.AddressID
            var suprs = from sup in _db.tblPatients
                        join ad in _db.tblAddresses on sup.AddressID equals ad.AddressID
                        join insur in _db.tblPatientInsurances on sup.PatientId equals insur.PatientID

                        // where sup.IsActive == true
                        select new Patientinfo
                        {
                            PatientID = sup.PatientId,
                            HospitalBranchId = sup.HospitalBranchId,
                            Title = sup.Title,
                            FirstName = sup.FirstName,
                            LastName = sup.LastName,
                            CallName = sup.CallName,
                            DOB = sup.DOB,
                            Age = sup.Age,
                            GenderName = sup.Gender,
                            ContactNumber = sup.ContactNumber,
                            GuardianName = sup.GuardianName,
                            GuardianContactNumber = sup.GuardianContactNumber,
                            Email = sup.Email,



                            Address = new AddressRepository
                            {
                                AddressLine1 = ad.AddressLine1,
                                AddressLine2 = ad.AddressLine2,
                                City = ad.City
                            },
                            PatientInsurance = new PatientInsuranceRepository
                            {
                                PatientInsuranceID = insur.PatientInsuranceID,
                                PatientID = insur.PatientID,
                                InsuranceName = insur.InsuranceName,
                                CompanyName = insur.CompanyName,
                                EffictiveFrom = insur.EffictiveFrom,
                                EffictiveTo = insur.EffictiveTo,
                                Limit = insur.Limit,
                                BankID = insur.BankID,
                                IFSCCode = insur.IFSCCode,
                                AcctHolderName = insur.AcctHolderName,
                                AcctNumber = insur.AcctNumber,
                                CreatedBy = insur.CreatedBy,

                            }

                        };
            LlstPatientProfile = suprs.ToList<Patientinfo>();
            return LlstPatientProfile;

        }

        public List<AppointmentRepository> GetAppointMentsBYType(string type)
        {
            HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
            List<AppointmentRepository> LlstPatints = new List<AppointmentRepository>();
            var suprs = from sup in LobjHMISTenantContext.tblAppointments
                        where sup.AppointmentType == type

                        select new AppointmentRepository
                        {
                            AppointmentID = sup.AppointmentID,
                            PatientID = sup.PatientID,
                            DoctorID = sup.DoctorID,
                            TokenNo = sup.TokenNo,
                            AppointmentMode = sup.AppointmentMode,
                            AppointmentStatus = sup.AppointmentStatus,
                            AppointmentType = sup.AppointmentType,
                            StartDateTime = sup.StartDateTime,
                            EndDateTime = sup.EndDateTime,
                            ConsultFee = sup.ConsultFee,
                            PayStatus = sup.PayStatus,
                            IsActive = sup.IsActive,
                            CreatedBy = sup.CreatedBy,
                            CreatedDate = sup.CreatedDate,
                        };
            LlstPatints = suprs.ToList<AppointmentRepository>();
            return LlstPatints;


        }

        public Patientinfo PostOpReview(Patientinfo PobOpReview)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            tblPatient objPatient = new tblPatient();
            objPatient = _db.tblPatients.Find(PobOpReview.PatientID);
            List<tblTreatment> LstTreatment = new List<tblTreatment>();
            foreach (TreatmentRepository LobTreatment in PobOpReview.lsttreatmentrep)
            {
                tblTreatment lobjDiagComponent = _db.tblTreatments.Create();
                lobjDiagComponent.PatientId = LobTreatment.PatientId;
                lobjDiagComponent.DoctorId = LobTreatment.DoctorId;
                lobjDiagComponent.TreatmentDate = LobTreatment.TreatmentDate;
                lobjDiagComponent.DiscussionSummary = LobTreatment.DiscussionSummary;
                lobjDiagComponent.Instruction = LobTreatment.Instruction;
                lobjDiagComponent.IsDiagnosisSuggested = LobTreatment.IsDiagnosisSuggested;
                lobjDiagComponent.IsMedicineSuggested = LobTreatment.IsMedicineSuggested;
                lobjDiagComponent.IsActive = true;
                lobjDiagComponent.CreatedBy = LobTreatment.CreatedBy;
                lobjDiagComponent.CreatedDate = DateTime.Now;

                foreach (TreatmentDiagnosisRepository LobtreatDiagnosis in LobTreatment.lstDiagnosisRep)
                {
                    tblTreatmentDiagnosi LobjtblTreatmentDiagnosi = _db.tblTreatmentDiagnosis.Create(); ;
                    LobjtblTreatmentDiagnosi.DiagnosisName = LobtreatDiagnosis.DiagnosisName;
                    LobjtblTreatmentDiagnosi.CreatedBy = LobtreatDiagnosis.CreatedBy;
                    LobjtblTreatmentDiagnosi.CreatedDate = DateTime.Now;
                    lobjDiagComponent.tblTreatmentDiagnosis.Add(LobjtblTreatmentDiagnosi);
                }

                foreach (TreatmentVitalSignRepository LobtreatmentVital in LobTreatment.lstVitalsignsRep)
                {
                    tblTreatmentVitalSign LobjtblTreatmentVitalsign = _db.tblTreatmentVitalSigns.Create(); ;
                    LobjtblTreatmentVitalsign.VitalSignType = LobtreatmentVital.VitalSignType;
                    LobjtblTreatmentVitalsign.Value1 = LobtreatmentVital.Value1;
                    LobjtblTreatmentVitalsign.Value2 = LobtreatmentVital.Value2;
                    LobjtblTreatmentVitalsign.Note = LobtreatmentVital.Note;
                    LobjtblTreatmentVitalsign.CreatedBy = LobtreatmentVital.CreatedBy;
                    LobjtblTreatmentVitalsign.CreatedDate = DateTime.Now;
                    lobjDiagComponent.tblTreatmentVitalSigns.Add(LobjtblTreatmentVitalsign);
                }

                foreach (TreatmentMendicianRepository LobtreatMedician in LobTreatment.lstMedicianRep)
                {
                    tblTreatmentMedicine LobjTreatmentMedicine = _db.tblTreatmentMedicines.Create(); ;
                    LobjTreatmentMedicine.MedicineName = LobtreatMedician.MedicineName;
                    //LobjTreatmentMedicine.Dose = LobtreatMedician.Dose;
                    LobjTreatmentMedicine.Quantity = LobtreatMedician.Quantity;
                    LobjTreatmentMedicine.IsActive = true;
                    LobjTreatmentMedicine.CreatedBy = LobtreatMedician.CreatedBy;
                    LobjTreatmentMedicine.CreatedDate = DateTime.Now;
                    lobjDiagComponent.tblTreatmentMedicines.Add(LobjTreatmentMedicine);
                }
                objPatient.tblTreatments.Add(lobjDiagComponent);


            }
            string str = "";
            try
            {

                int res = _db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {
                        str += "Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage;
                    }
                }
            }
            return PobOpReview;

        }

        public List<Patientinfo> GetBillingDetails(int id)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<Patientinfo> LobjtblPatient = new List<Patientinfo>();
            var suprs = from p in _db.tblPatients
                        join b in _db.tblBedAllocations
                        on p.PatientId equals b.PatientId
                        join t in _db.tblTransactions
                        on p.PatientId equals t.PatientID
                        where p.PatientId == id && p.IsActive == true
                        select new Patientinfo
                        {
                            PatientID = p.PatientId,
                            transaction = new TransactionRepository
                            {
                                TotalAmount = t.TotalAmount,

                            },
                            bedallocation = new BedAllocationRepository
                            {
                                AllocatedDate = b.AllocatedDate,
                                VacatedDate = b.VacatedDate,
                            }
                        };
            LobjtblPatient = suprs.ToList<Patientinfo>();
            return LobjtblPatient;
        }

        public List<EmployeesRepository> GetDoctorsList()
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<EmployeesRepository> lsttblEmployee = new List<EmployeesRepository>();

            var DoctorsList = from emp in _db.tblEmployees
                              join dep in _db.tblDepartments on emp.DepartmentId equals dep.DeptID
                              join branch in _db.tblHospitalBranchDetails on emp.WorkLocation equals branch.HospitalBranchID

                              where emp.IsActive == true && emp.EmployeeType == "Doctor"

                              select new EmployeesRepository
                                {
                                    FirstName = emp.FirstName,
                                    LastName = emp.LastName,
                                    DOB = emp.DOB,
                                    Age = emp.Age,
                                    Gender = emp.Gender,
                                    DOJ = emp.DOJ,
                                    EmploymentType = emp.EmploymentType,
                                    Qualification = emp.Qualification,
                                    Experience = emp.Experience,
                                    DesignationName = emp.DesignationName,
                                    DepartmentName = dep.DeptName,
                                    Location = branch.BranchName,
                                    Salary = emp.Salary,
                                    Email = emp.Email,
                                    MobileNo = emp.MobileNo,
                                    EmployeeType = emp.EmployeeType
                                };
            lsttblEmployee = DoctorsList.ToList<EmployeesRepository>();
            return lsttblEmployee;
        }

        public EmployeesRepository PostDoctor(EmployeesRepository Pobjemployees)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            tblAddress lobjtbladdress = new tblAddress();
            tblEmployee lobjtblemployee = new tblEmployee();

            ObjectCopier.CopyObjectData(Pobjemployees.AddressRep, lobjtbladdress);
            ObjectCopier.CopyObjectData(Pobjemployees, lobjtblemployee);

            lobjtbladdress.IsActive = true;
            lobjtbladdress.CreatedDate = DateTime.Now;
            lobjtblemployee.IsActive = true;
            lobjtblemployee.CreatedDate = DateTime.Now;

            _db.tblAddresses.Add(lobjtbladdress);
            _db.tblEmployees.Add(lobjtblemployee);
            int res = _db.SaveChanges();

            ObjectCopier.CopyObjectData(lobjtbladdress, Pobjemployees.AddressRep);
            ObjectCopier.CopyObjectData(lobjtblemployee, Pobjemployees);

            return Pobjemployees;
        }

        public bool PutDoctor(int id, EmployeesRepository Pobjemployees)
        {
            try
            {
                tblEmployee LobjEmployee = new tblEmployee();
                using (var LobjHMISTenantContext = new HospitalTenantContext())
                {
                    LobjEmployee = LobjHMISTenantContext.tblEmployees.Where(e => e.EID == id).FirstOrDefault<tblEmployee>();
                    if (LobjEmployee != null)
                    {
                        LobjEmployee.Title = Pobjemployees.Title;
                        LobjEmployee.FirstName = Pobjemployees.FirstName;
                        LobjEmployee.LastName = Pobjemployees.LastName;
                        LobjEmployee.DisplayName = Pobjemployees.DisplayName;
                        LobjEmployee.DOB = Pobjemployees.DOB;
                        LobjEmployee.Age = Pobjemployees.Age;
                        LobjEmployee.DOJ = Pobjemployees.DOJ;
                        LobjEmployee.EmploymentType = Pobjemployees.EmploymentType;
                        LobjEmployee.EmployeeId = Pobjemployees.EmployeeId;
                        LobjEmployee.EmployeeType = Pobjemployees.EmployeeType;
                        LobjEmployee.Qualification = Pobjemployees.Qualification;
                        LobjEmployee.Experience = Pobjemployees.Experience;
                        LobjEmployee.DesignationName = Pobjemployees.DesignationName;
                        LobjEmployee.DepartmentId = Pobjemployees.DepartmentId;
                        LobjEmployee.WorkLocation = Pobjemployees.WorkLocation;
                        LobjEmployee.HomeBranch = Pobjemployees.HomeBranch;
                        LobjEmployee.ManagerId = Pobjemployees.ManagerId;
                        LobjEmployee.Salary = Pobjemployees.Salary;
                        LobjEmployee.Achievements = Pobjemployees.Achievements;
                        LobjEmployee.MobileNo = Pobjemployees.MobileNo;
                        LobjEmployee.LandLine = Pobjemployees.LandLine;
                        LobjEmployee.Email = Pobjemployees.Email;
                        LobjEmployee.EmerContactName = Pobjemployees.EmerContactName;
                        LobjEmployee.EmerContactNo = Pobjemployees.EmerContactNo;
                        LobjEmployee.EmrRelation = Pobjemployees.EmrRelation;
                        LobjEmployee.PersAddressID = Pobjemployees.PersAddressID;
                        LobjEmployee.ProfAddressID = Pobjemployees.ProfAddressID;
                        LobjEmployee.AadhaarNo = Pobjemployees.AadhaarNo;
                        LobjEmployee.PanNo = Pobjemployees.PanNo;
                        LobjEmployee.Comments = Pobjemployees.Comments;
                        LobjEmployee.ModifiedBy = Pobjemployees.ModifiedBy;
                        LobjEmployee.ModifiedDate = DateTime.Now;

                        //LobjEmployee.tblAddress.AddressLine1 = Pobjemployees.AddressRep.AddressLine1;
                        //LobjEmployee.tblAddress.AddressLine2 = Pobjemployees.AddressRep.AddressLine2;
                        //LobjEmployee.tblAddress.City = Pobjemployees.AddressRep.City;
                        //LobjEmployee.tblAddress.State = Pobjemployees.AddressRep.State;
                        //LobjEmployee.tblAddress.Country = Pobjemployees.AddressRep.Country;
                        //LobjEmployee.tblAddress.Country = Pobjemployees.AddressRep.Country;
                        //LobjEmployee.tblAddress.ZipCode = Pobjemployees.AddressRep.ZipCode;
                        //LobjEmployee.tblAddress.ModifiedBy = Pobjemployees.AddressRep.ModifiedBy;
                        //LobjEmployee.tblAddress.ModifiedDate = DateTime.Now;
                    }
                    var data = LobjHMISTenantContext.SaveChanges();
                    if (data >= 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<TreatmentRepository> GetOpLstReview(DateTime treatdate)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<TreatmentRepository> LlstPatientProfile = new List<TreatmentRepository>();
            var suprs = from Treat in _db.tblTreatments
                        join tDiag in _db.tblTreatmentDiagnosis on Treat.TreatmentId equals tDiag.TreatmentID
                        join tmedicine in _db.tblTreatmentMedicines on Treat.TreatmentId equals tmedicine.TreatmentID
                        join vital in _db.tblTreatmentVitalSigns on Treat.TreatmentId equals vital.TreatmentID
                        where Treat.TreatmentDate == treatdate

                        select new TreatmentRepository
                        {
                            TreatmentId = Treat.TreatmentId,
                            PatientId = Treat.PatientId,
                            DiscussionSummary = Treat.DiscussionSummary,
                            Instruction = Treat.Instruction,
                            IsDiagnosisSuggested = Treat.IsDiagnosisSuggested,
                            NextVisit = Treat.NextVisit,



                            DiagnosisRep = new TreatmentDiagnosisRepository
                            {
                                TreatmentID = tDiag.TreatmentID,
                                DiagnosisName = tDiag.DiagnosisName,
                                Comments = tDiag.Comments,
                                IsActive = tDiag.IsActive,
                                CreatedBy = tDiag.CreatedBy,
                                CreatedDate = tDiag.CreatedDate,
                            },

                            VitalsignsRep = new TreatmentVitalSignRepository
                            {
                                TreatmentID = vital.TreatmentID,
                                // VitalSignType = vital.VitalSignType,
                                Value1 = vital.Value1,
                                Value2 = vital.Value2,
                                Note = vital.Note,
                                CreatedBy = vital.CreatedBy,
                                CreatedDate = vital.CreatedDate
                            },

                            MedicianRep = new TreatmentMendicianRepository
                            {
                                TreatmentID = tmedicine.TreatmentID,
                                MedicineName = tmedicine.MedicineName,
                                IsActive = tmedicine.IsActive,
                                Quantity = tmedicine.Quantity,
                                Dose = tmedicine.Dose,
                                CreatedBy = tmedicine.CreatedBy,
                                CreatedDate = tmedicine.CreatedDate
                            },


                        };
            LlstPatientProfile = suprs.ToList<TreatmentRepository>();
            return LlstPatientProfile;

        }
        public List<IpAdmissionRepository> GetIpPatients(IpAdmissionRepository PobjIpAdmission)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<IpAdmissionRepository> LobjPatient = new List<IpAdmissionRepository>();

            var res = from p in _db.tblPatients
                      join ip in _db.tblAdmissions
                      on p.PatientId equals ip.PatientID
                      join addr in _db.tblAddresses on p.AddressID equals addr.AddressID
                      where p.PatientType == "ip"
                      select new IpAdmissionRepository
                      {
                          ipAdmissionID = ip.AdmissionID,
                          PatientID = ip.PatientID,
                          AdmitDate = ip.AdmitDate,
                          AdmitTime = ip.AdmitTime,
                          Patient = new Patientinfo
                        {
                            PatientID = p.PatientId,
                            FirstName = p.FirstName,
                            Title = p.Title,
                            Age = p.Age,
                            ContactNumber = p.ContactNumber,
                            Address = new AddressRepository
                            {
                                AddressID = addr.AddressID,
                                AddressLine1 = addr.AddressLine1,
                                AddressLine2 = addr.AddressLine2,
                                City = addr.City,
                                Country = addr.Country,
                                State = addr.State,

                            }

                        }
                      };
            LobjPatient = res.ToList<IpAdmissionRepository>();
            return LobjPatient;
        }

        //GetAppointMents By DoctorID

        public List<AppointmentRepository> GetAppointMentsByDoctorID(int DoctorID)
        {
            HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
            List<AppointmentRepository> LlstPatints = new List<AppointmentRepository>();
            var suprs = from sup in LobjHMISTenantContext.tblAppointments
                        where sup.DoctorID == DoctorID

                        select new AppointmentRepository
                        {
                            AppointmentID = sup.AppointmentID,
                            PatientID = sup.PatientID,
                            DoctorID = sup.DoctorID,
                            TokenNo = sup.TokenNo,
                            AppointmentMode = sup.AppointmentMode,
                            AppointmentStatus = sup.AppointmentStatus,
                            AppointmentType = sup.AppointmentType,
                            StartDateTime = sup.StartDateTime,
                            EndDateTime = sup.EndDateTime,
                            ConsultFee = sup.ConsultFee,
                            PayStatus = sup.PayStatus,

                        };
            LlstPatints = suprs.ToList<AppointmentRepository>();
            return LlstPatints;


        }

        #region DrSettings
        public List<DrsettingsRepository> GetDrSettings()
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<DrsettingsRepository> LobjtblDrsettings = new List<DrsettingsRepository>();
            var Drsettings = from ds in _db.tblDrSettings
                             join speciality in _db.tblSpecialities on ds.SpecialityID equals speciality.SpID
                             join doctor in _db.tblEmployees on ds.DoctorID equals doctor.EID

                             where ds.IsActive == true

                             select new DrsettingsRepository
                             {
                                 DrSettingsID = ds.DrSettingsID,
                                 DoctorName = doctor.FirstName + " " + doctor.LastName,
                                 Age = doctor.Age,
                                 Gender = doctor.Gender,
                                 SpecializationName = speciality.SpName,
                                 OPFee = ds.OPFee,
                                 IPFee = ds.IPFee,
                                 Qualification = ds.Qualification,
                                 IsHead = ds.IsHead,
                                 Membership = ds.Membership,
                                 Awards = ds.Awards,
                                 CertificationDate = ds.CertificationDate,
                                 CertificationExpires = ds.CertificationExpires,
                                 OpSlotTime = ds.OpSlotTime,
                                 AvgSlotTime = ds.AvgSlotTime,
                                 IsActive = ds.IsActive
                             };
            LobjtblDrsettings = Drsettings.ToList<DrsettingsRepository>();

            return LobjtblDrsettings;
        }
        public DrsettingsRepository GetDrSettings(int id)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            DrsettingsRepository LobjtblDrsettings = new DrsettingsRepository();
            LobjtblDrsettings = (from ds in _db.tblDrSettings
                                 join speciality in _db.tblSpecialities on ds.SpecialityID equals speciality.SpID
                                 join doctor in _db.tblEmployees on ds.DoctorID equals doctor.EID

                                 where ds.IsActive == true && ds.DrSettingsID == id

                                 select new DrsettingsRepository
                                 {
                                     DrSettingsID = ds.DrSettingsID,
                                     DoctorName = doctor.FirstName + " " + doctor.LastName,
                                     Age = doctor.Age,
                                     Gender = doctor.Gender,
                                     SpecializationName = speciality.SpName,
                                     OPFee = ds.OPFee,
                                     IPFee = ds.IPFee,
                                     Qualification = ds.Qualification,
                                     IsHead = ds.IsHead,
                                     Membership = ds.Membership,
                                     Awards = ds.Awards,
                                     CertificationDate = ds.CertificationDate,
                                     CertificationExpires = ds.CertificationExpires,
                                     OpSlotTime = ds.OpSlotTime,
                                     AvgSlotTime = ds.AvgSlotTime,
                                     IsActive = ds.IsActive
                                 }).FirstOrDefault();
            return LobjtblDrsettings;
        }

        public DrsettingsRepository PostDrSettings(DrsettingsRepository PobjDrsettings)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            tblDrSetting LobjDrsettings = new tblDrSetting();
            ObjectCopier.CopyObjectData(PobjDrsettings, LobjDrsettings);
            LobjDrsettings.IsActive = true;
            LobjDrsettings.CreatedDate = DateTime.Now;
            _db.tblDrSettings.Add(LobjDrsettings);

            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjDrsettings, PobjDrsettings);
            return PobjDrsettings;
        }
        public bool PutDrSettings(int id, DrsettingsRepository PobjDrSettings)
        {
            try
            {
                tblDrSetting LobjDrSettings = new tblDrSetting();
                using (var LobjHMISTenantContext = new HospitalTenantContext())
                {
                    LobjDrSettings = LobjHMISTenantContext.tblDrSettings.Where(d => d.DrSettingsID == id).FirstOrDefault();
                    if (LobjDrSettings != null)
                    {
                        LobjDrSettings.OPFee = PobjDrSettings.OPFee;
                        LobjDrSettings.IPFee = PobjDrSettings.IPFee;
                        LobjDrSettings.Qualification = PobjDrSettings.Qualification;
                        LobjDrSettings.IsHead = PobjDrSettings.IsHead;
                        LobjDrSettings.Membership = PobjDrSettings.Membership;
                        LobjDrSettings.Awards = PobjDrSettings.Awards;
                        LobjDrSettings.CertificationDate = PobjDrSettings.CertificationDate;
                        LobjDrSettings.CertificationExpires = PobjDrSettings.CertificationExpires;
                        LobjDrSettings.OpSlotTime = PobjDrSettings.OpSlotTime;
                        LobjDrSettings.AvgSlotTime = PobjDrSettings.AvgSlotTime;
                        LobjDrSettings.ModifiedBy = PobjDrSettings.ModifiedBy;
                        LobjDrSettings.ModifiedDate = DateTime.Now;
                    }
                    var data = LobjHMISTenantContext.SaveChanges();
                    if (data == 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool DeleteDrSettings(int id)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            tblDrSetting LobjtblDrSettings = new tblDrSetting();
            LobjtblDrSettings = _db.tblDrSettings.Where(d => d.DrSettingsID == id).FirstOrDefault();
            if (LobjtblDrSettings != null)
            {
                LobjtblDrSettings.IsActive = false;
                LobjtblDrSettings.ModifiedDate = DateTime.Now;
            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        public List<AppointmentRepository> GetDoctorAppointments(int id, DateTime date)
        {
            HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
            List<AppointmentRepository> Llstappointments = new List<AppointmentRepository>();
            var appointments = from ap in LobjHMISTenantContext.tblAppointments
                               join e in LobjHMISTenantContext.tblEmployees on ap.DoctorID equals e.EID
                               join p in LobjHMISTenantContext.tblPatients on ap.PatientID equals p.PatientId
                               join branch in LobjHMISTenantContext.tblHospitalBranchDetails on ap.BranchID equals branch.HospitalBranchID
                               join dept in LobjHMISTenantContext.tblDepartments on ap.DeptID equals dept.DeptID

                               where e.EID == id && ap.StartDateTime == date && ap.IsActive == true
                               select new AppointmentRepository
                               {
                                   AppointmentID = ap.AppointmentID,
                                   PatientName = p.FirstName + " " + p.LastName,
                                   DoctorName = e.FirstName + " " + e.LastName,
                                   Branch = branch.BranchName,
                                   Department = dept.DeptName,
                                   TokenNo = ap.TokenNo,
                                   AppointmentMode = ap.AppointmentMode,
                                   AppointmentStatus = ap.AppointmentStatus,
                                   AppointmentType = ap.AppointmentType,
                                   StartDateTime = ap.StartDateTime,
                                   EndDateTime = ap.EndDateTime,
                                   ConsultFee = ap.ConsultFee,
                                   Discount = ap.Discount,
                                   PayStatus = ap.PayStatus,
                                   PayMode = ap.PayMode,
                                   IsWaiveOff = ap.IsWaiveOff,
                                   ReviewID = ap.ReviewID,
                                   Comments = ap.Comments,
                                   IsActive = ap.IsActive,

                               };
            Llstappointments = appointments.ToList<AppointmentRepository>();
            return Llstappointments;
        }

        public List<PatientDetailsRepository> GetDoctorPatients(int doctorId, string PatientType)
        {
            List<PatientDetailsRepository> LobjtblPatientDetails = new List<PatientDetailsRepository>();
            if (PatientType == "OP")
            {
                HospitalTenantContext _db = new HospitalTenantContext();

                var PatientDetails = from appointment in _db.tblAppointments
                                     join patient in _db.tblPatients on appointment.PatientID equals patient.PatientId
                                     join doctor in _db.tblEmployees on appointment.DoctorID equals doctor.EID
                                     join dept in _db.tblDepartments on appointment.DeptID equals dept.DeptID
                                     where patient.IsActive == true && doctor.EID == doctorId && patient.PatientType == "OP"
                                     select new PatientDetailsRepository
                                     {
                                         PatientID = patient.PatientId,
                                         PatientName = patient.FirstName + " " + patient.LastName,
                                         Age = patient.Age,
                                         Gender = patient.Gender,
                                         ContactNumber = patient.ContactNumber,
                                         Email = patient.Email,
                                         DoctorName = doctor.FirstName + " " + doctor.LastName,
                                         Department = dept.DeptName,
                                         patientType = patient.PatientType
                                     };
                LobjtblPatientDetails = PatientDetails.ToList<PatientDetailsRepository>();

            }
            else if (PatientType == "IP")
            {
                HospitalTenantContext _db = new HospitalTenantContext();
                var PatientDetails = from appointment in _db.tblAppointments
                                     join doctor in _db.tblEmployees on appointment.DoctorID equals doctor.EID
                                     join patient in _db.tblPatients on appointment.PatientID equals patient.PatientId
                                     join admission in _db.tblAdmissions on patient.PatientId equals admission.PatientID
                                     join dept in _db.tblDepartments on admission.Department equals dept.DeptID
                                     where patient.IsActive == true && doctor.EID == doctorId && patient.PatientType == "IP"
                                     select new PatientDetailsRepository
                                     {
                                         PatientID = patient.PatientId,
                                         PatientName = patient.FirstName + " " + patient.LastName,
                                         Age = patient.Age,
                                         Gender = patient.Gender,
                                         ContactNumber = patient.ContactNumber,
                                         Email = patient.Email,
                                         DoctorName = doctor.FirstName + " " + doctor.LastName,
                                         Department = dept.DeptName,
                                         patientType = patient.PatientType,
                                         AdmintDate = admission.AdmitDate,
                                         AdmitTime = admission.AdmitTime

                                     };
                LobjtblPatientDetails = PatientDetails.ToList<PatientDetailsRepository>();
            }
            return LobjtblPatientDetails;

        }

        public List<AppointmentRepository> GetTokenSummary(DateTime date, int? id)
        {

            List<AppointmentRepository> Llstappointments = new List<AppointmentRepository>();
            var appointments = from app in _db.tblAppointments
                               where app.StartDateTime.Day == date.Day && app.StartDateTime.Month == date.Month &&
                               app.StartDateTime.Year == date.Year && app.IsActive == true
                               select new AppointmentRepository
                               {
                                   AppointmentID = app.AppointmentID,
                                   TokenNo = app.TokenNo,
                                   StartDateTime = app.StartDateTime,
                                   EndDateTime = app.EndDateTime,
                                   DoctorID = app.DoctorID,
                               };
            if (id != null)
            {
                appointments = appointments.Where(x => x.DoctorID == id);
            }
            Llstappointments = appointments.ToList<AppointmentRepository>();
            return Llstappointments;
        }
    }
}

