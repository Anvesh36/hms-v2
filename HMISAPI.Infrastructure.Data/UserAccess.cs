﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Domain.Entities;
using System.Data.Linq.SqlClient;
using System.Net;

namespace HMISAPI.Infrastructure.Data
{
    //public class Hrms : HMISAPI.Domain.Interfaces.IHrms
   public class UserAccess:HMISAPI.Domain.Interfaces.IUserAccess
    {
       HospitalTenantContext _db = new HospitalTenantContext();
       #region Role
       //Get Roles
       public List<RolesRepository> GetRoles()
       {
           //HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
           List<RolesRepository> LlstRols = new List<RolesRepository>();
           var Roles = from role in _db.tblRoles where role.IsActive==true


                       select new RolesRepository
                       {
                          RoleID=role.RoleID,
                          RoleTitle=role.RoleTitle,
                          Description=role.Description,
                          IsActive=role.IsActive,

                       };
           LlstRols = Roles.ToList<RolesRepository>();
           return LlstRols;

       }

       //Get RoleBy id
       public RolesRepository GetRoles(int id)
       {
       
           //HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
           RolesRepository LlstRols = new RolesRepository();
           var Roles = from role in _db.tblRoles where role.RoleID==id
                       select new RolesRepository
                       {
                           RoleID = role.RoleID,
                           RoleTitle = role.RoleTitle,
                           Description = role.Description,
                           IsActive = role.IsActive,

                       };
           


          
           return LlstRols;


          // return LlstRols;

       }


      

       //Post Roles
       public RolesRepository PostRole(RolesRepository PobjPostRole)
       {
           tblRole LobjtblRole = new tblRole();
           ObjectCopier.CopyObjectData(PobjPostRole, LobjtblRole);
           LobjtblRole.CreatedDate = DateTime.Now;
           LobjtblRole.IsActive = true;
           LobjtblRole.CreatedBy = PobjPostRole.CreatedBy;

           _db.tblRoles.Add(LobjtblRole);
           int res = _db.SaveChanges();
           ObjectCopier.CopyObjectData(LobjtblRole, PobjPostRole);
           return PobjPostRole;
       }

       //put Employee
       public bool PutRole(int RoleID, RolesRepository PobjPutRole)
       {

           tblRole LobjtblRole = new tblRole();
           LobjtblRole = _db.tblRoles.Where(m => m.RoleID == RoleID).FirstOrDefault<tblRole>();
           if (LobjtblRole != null)
           {
               LobjtblRole.RoleTitle = PobjPutRole.RoleTitle;
               LobjtblRole.Description = PobjPutRole.Description;
               LobjtblRole.ModifiedDate = DateTime.Now;
               LobjtblRole.ModifiedBy = PobjPutRole.ModifiedBy;
           }
           int res = _db.SaveChanges();
           if (res == 1)
           {
               return true;
           }
           else
           {
               return false;
           }
       }

       //Delete role
       public bool DeleteRole(int roleid)
       {

           tblRole LobjtblEmployee = new tblRole();
           LobjtblEmployee = _db.tblRoles.Where(m => m.RoleID == roleid).FirstOrDefault<tblRole>();
           if (LobjtblEmployee != null)
           {
               LobjtblEmployee.IsActive = false;
           }
           int res = _db.SaveChanges();
           if (res == 1)
           {
               return true;
           }
           else
           {
               return false;
           }
       }

       #endregion

       #region UserInfo
       //Get Roles
       public List<UserInfoRepository> GetUsers()
       {
           //HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
           List<UserInfoRepository> LlstUsers = new List<UserInfoRepository>();
           var Users = from user in _db.tblUsers  where user.IsActive==true

                       select new UserInfoRepository
                       {
                           UserID = user.UserID,
                           Name = user.Name,
                           EID = user.EID,
                           SecurityAnswer1 = user.SecurityAnswer1,
                           SecurityQuestion1 = user.SecurityQuestion1,
                           RoleID = user.RoleID,
                           IsActive = user.IsActive,

                       };
           LlstUsers = Users.ToList<UserInfoRepository>();
           return LlstUsers;

       }
       //Get By Id

       public UserInfoRepository GetUsers(int id)
       {
           //HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
           UserInfoRepository LlstUsers = new UserInfoRepository();
           var Users = from user in _db.tblUsers
                       where user.UserID==id

                       select new UserInfoRepository
                       {
                           UserID = user.UserID,
                           Name = user.Name,
                           EID = user.EID,
                           SecurityAnswer1 = user.SecurityAnswer1,
                           SecurityQuestion1 = user.SecurityQuestion1,
                           RoleID = user.RoleID,
                           IsActive = user.IsActive,

                       };
          // LlstUsers = Users.ToList<UserInfoRepository>();
           return LlstUsers;

       }

       //Post Roles
       public UserInfoRepository PostUser(UserInfoRepository PobjPostRole)
       {
           tblUser LobjtblUser = new tblUser();
           ObjectCopier.CopyObjectData(PobjPostRole, LobjtblUser);
           LobjtblUser.CreatedOn = DateTime.Now;
           LobjtblUser.IsActive = true;
           LobjtblUser.CreatedBy = PobjPostRole.CreatedBy;

           _db.tblUsers.Add(LobjtblUser);
           int res = _db.SaveChanges();
           ObjectCopier.CopyObjectData(LobjtblUser, PobjPostRole);
           return PobjPostRole;
       }

       //put Employee
       public bool PutUser(int RoleID, UserInfoRepository PobjPutRole)
       {

           tblUser LobjtblUser = new tblUser();
           LobjtblUser = _db.tblUsers.Where(m => m.UserID == RoleID).FirstOrDefault<tblUser>();
           if (LobjtblUser != null)
           {
               LobjtblUser.Name = PobjPutRole.Name;
               LobjtblUser.Password = PobjPutRole.Password;
               LobjtblUser.CreatedOn = DateTime.Now;
               LobjtblUser.ModifiedBy = PobjPutRole.ModifiedBy;
           }
           int res = _db.SaveChanges();
           if (res == 1)
           {
               return true;
           }
           else
           {
               return false;
           }
       }

       //Delete role
       public bool DeleteUser(int roleid)
       {

           tblUser LobjtblUser = new tblUser();
           LobjtblUser = _db.tblUsers.Where(m => m.UserID == roleid).FirstOrDefault<tblUser>();
           if (LobjtblUser != null)
           {
               LobjtblUser.IsActive = false;
           }
           int res = _db.SaveChanges();
           if (res == 1)
           {
               return true;
           }
           else
           {
               return false;
           }
       }

       #endregion

        #region Menu

       //Get Menus
       public List<MenuRepository> GetMenu()
       {
           //HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
           List<MenuRepository> LlstMenus = new List<MenuRepository>();
           var menus = from user in _db.tblMenus

                       select new MenuRepository
                       { 
                           MenuId=user.MenuId,
                           ParentID=user.ParentID,
                           MenuName=user.MenuName,
                           NavigateURL=user.NavigateURL,
                           MenuLogo=user.MenuLogo,
                           MenuOrder=user.MenuOrder,
                           IsActive = user.IsActive,

                       };
           LlstMenus = menus.ToList<MenuRepository>();
           return LlstMenus;

       }

       //Get Menus By Id
       public MenuRepository GetMenu(int id)
       {
           //HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
           MenuRepository LlstMenus = new MenuRepository();
           var menus = from user in _db.tblMenus where user.MenuId==id

                       select new MenuRepository
                       {
                           MenuId = user.MenuId,
                           ParentID = user.ParentID,
                           MenuName = user.MenuName,
                           NavigateURL = user.NavigateURL,
                           MenuLogo = user.MenuLogo,
                           MenuOrder = user.MenuOrder,
                           IsActive = user.IsActive,

                       };
           //LlstMenus = menus.ToList<MenuRepository>();
           return LlstMenus;

       }
       //Post Menus
       public MenuRepository PostMenu(MenuRepository PobjPostMenu)
       {
           tblMenu LobjtblMenu = new tblMenu();
           ObjectCopier.CopyObjectData(PobjPostMenu, LobjtblMenu);
           LobjtblMenu.CreatedDate = DateTime.Now;
           LobjtblMenu.IsActive = true;
           LobjtblMenu.CreatedBy = PobjPostMenu.CreatedBy;

           _db.tblMenus.Add(LobjtblMenu);
           int res = _db.SaveChanges();
           ObjectCopier.CopyObjectData(LobjtblMenu, PobjPostMenu);
           return PobjPostMenu;
       }

       //put Menus
       public bool PutMenu(int MenuId, MenuRepository PobjPutMenu)
       {

           tblMenu LobjtblMenu = new tblMenu();
           LobjtblMenu = _db.tblMenus.Where(m => m.MenuId == MenuId).FirstOrDefault<tblMenu>();
           if (LobjtblMenu != null)
           {
               LobjtblMenu.ParentID = PobjPutMenu.ParentID;
               LobjtblMenu.MenuName = PobjPutMenu.MenuName;
               LobjtblMenu.NavigateURL = PobjPutMenu.NavigateURL;
               LobjtblMenu.MenuLogo = PobjPutMenu.MenuLogo;
               LobjtblMenu.MenuOrder = PobjPutMenu.MenuOrder;
               LobjtblMenu.ModifiedBy = PobjPutMenu.ModifiedBy;
               LobjtblMenu.ModifiedDate = DateTime.Now;
           }
           int res = _db.SaveChanges();
           if (res == 1)
           {
               return true;
           }
           else
           {
               return false;
           }
       }

       //Delete Menus
       public bool DeleteMenu(int menuid)
       {

           tblMenu LobjtblMenu = new tblMenu();
           LobjtblMenu = _db.tblMenus.Where(m => m.MenuId == menuid).FirstOrDefault<tblMenu>();
           if (LobjtblMenu != null)
           {
               LobjtblMenu.IsActive = false;
           }
           int res = _db.SaveChanges();
           if (res == 1)
           {
               return true;
           }
           else
           {
               return false;
           }
       }

        #endregion

        #region RoleMenu
       public List<RoleMenuRepository> GetRoleMenu()
       {
           //HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
           List<RoleMenuRepository> LlstRoleMenus = new List<RoleMenuRepository>();
           var menus = from user in _db.tblRoleMenus 


                       select new RoleMenuRepository
                       {
                           RoleMenuID=user.RoleMenuID,
                           RoleID = user.RoleID,
                           MenuID = user.MenuID
                       };
           LlstRoleMenus = menus.ToList<RoleMenuRepository>();
           return LlstRoleMenus;

       }
       public RoleMenuRepository GetRoleMenu(int id)
       {
           //HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
           RoleMenuRepository LlstRoleMenus = new RoleMenuRepository();
           var menus = from user in _db.tblRoleMenus where user.RoleMenuID==id


                       select new RoleMenuRepository
                       {
                           RoleMenuID = user.RoleMenuID,
                           RoleID = user.RoleID,
                           MenuID = user.MenuID
                       };
          // LlstRoleMenus = menus.ToList<RoleMenuRepository>();
           return LlstRoleMenus;

       }
       //Post Menus
       public RoleMenuRepository PostRoleMenu(RoleMenuRepository PobjPostRoleMenu)
       {
           tblRoleMenu LobjtblRoleMenu = new tblRoleMenu();
           ObjectCopier.CopyObjectData(PobjPostRoleMenu, LobjtblRoleMenu);
           LobjtblRoleMenu.CreatedDate = DateTime.Now;
           LobjtblRoleMenu.IsActive = 1;
           //LobjtblRoleMenu.IsActive = true;
           LobjtblRoleMenu.CreatedBy = PobjPostRoleMenu.CreatedBy;

           _db.tblRoleMenus.Add(LobjtblRoleMenu);
           int res = _db.SaveChanges();
           ObjectCopier.CopyObjectData(LobjtblRoleMenu, PobjPostRoleMenu);
           return PobjPostRoleMenu;
       }

       //put Menus
       public bool PutRoleMenu(int MenuId, RoleMenuRepository PobjPutMenu)
       {

           tblRoleMenu LobjtblMenu = new tblRoleMenu();
           LobjtblMenu = _db.tblRoleMenus.Where(m => m.RoleMenuID == MenuId).FirstOrDefault<tblRoleMenu>();
           if (LobjtblMenu != null)
           {
           
               LobjtblMenu.RoleID = PobjPutMenu.RoleID;
               LobjtblMenu.MenuID = PobjPutMenu.MenuID;
               LobjtblMenu.ModifiedBy = PobjPutMenu.ModifiedBy;
               LobjtblMenu.ModifiedDate = DateTime.Now;
           }
           int res = _db.SaveChanges();
           if (res == 1)
           {
               return true;
           }
           else
           {
               return false;
           }
       }

       //Delete Menus
       public bool DeleteRoleMenu(int Rolemenuid)
       {

           tblRoleMenu LobjtblMenu = new tblRoleMenu();
           LobjtblMenu = _db.tblRoleMenus.Where(m => m.RoleMenuID == Rolemenuid).FirstOrDefault<tblRoleMenu>();
           if (LobjtblMenu != null)
           {
             LobjtblMenu.IsActive = 0;
           }
           int res = _db.SaveChanges();
           if (res == 1)
           {
               return true;
           }
           else
           {
               return false;
           }
       }

        #endregion


    }
}
