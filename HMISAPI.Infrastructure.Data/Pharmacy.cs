﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Domain.Entities;
using System.Data.Linq.SqlClient;

namespace HMISAPI.Infrastructure.Data
{
    public class Pharmacy : HMISAPI.Domain.Interfaces.IPharmacy
    {
        HospitalTenantContext _db = new HospitalTenantContext();

        Utilities GobjUtilities = new Utilities();

        #region Supplier
        public List<SupplierRepository> GetSuppliers()
        {

            List<SupplierRepository> Llstsupliers = new List<SupplierRepository>();
            var suprs = from sup in _db.tblSuppliers
                        join ad in _db.tblAddresses
                        on sup.AddressID equals ad.AddressID
                        where sup.IsActive == true
                        select new SupplierRepository
                        {
                            SupplierID = sup.SupplierID,
                            SupplierName = sup.SupplierName,
                            Address = new AddressRepository
                            {
                                AddressLine1 = ad.AddressLine1,
                                AddressLine2 = ad.AddressLine2,
                                City = ad.City
                            },
                            ContactPerson = sup.ContactPerson,
                            ContactNo = sup.ContactNo,
                            TinNo = sup.TinNo,
                            Comments = sup.Comments
                        };
            Llstsupliers = suprs.ToList<SupplierRepository>();
            return Llstsupliers;
        }

        public List<SupplierRepository> GetSuppliersById(int id)
        {

            List<SupplierRepository> Llstsupliers = new List<SupplierRepository>();
            var suprs = from sup in _db.tblSuppliers
                        join ad in _db.tblAddresses
                        on sup.AddressID equals ad.AddressID
                        where sup.IsActive == true && sup.SupplierID == id
                        select new SupplierRepository
                        {
                            SupplierID = sup.SupplierID,
                            SupplierName = sup.SupplierName,
                            Address = new AddressRepository
                            {
                                AddressLine1 = ad.AddressLine1,
                                AddressLine2 = ad.AddressLine2,
                                City = ad.City
                            },
                            ContactPerson = sup.ContactPerson,
                            ContactNo = sup.ContactNo,
                            TinNo = sup.TinNo,
                            Comments = sup.Comments
                        };
            Llstsupliers = suprs.ToList<SupplierRepository>();
            return Llstsupliers;
        }

        public List<SupplierRepository> GetSupplierBySearch(string PstrSearchTerm)
        {
            List<SupplierRepository> Llstsupliers = new List<SupplierRepository>();
            var suprs = from sup in _db.tblSuppliers
                        join ad in _db.tblAddresses
                        on sup.AddressID equals ad.AddressID
                        where sup.IsActive == true && sup.SupplierName.StartsWith(PstrSearchTerm)
                        //we can use SqlMethods.Like(sup.SupplierName, PstrSearchTerm + "%") in place of sup.SupplierName.StartsWith(PstrSearchTerm), only when we are not working with entities.
                        select new SupplierRepository
                        {
                            SupplierID = sup.SupplierID,
                            SupplierName = sup.SupplierName,
                            Address = new AddressRepository
                            {
                                AddressLine1 = ad.AddressLine1,
                                AddressLine2 = ad.AddressLine2,
                                City = ad.City
                            },
                            ContactPerson = sup.ContactPerson,
                            ContactNo = sup.ContactNo,
                            TinNo = sup.TinNo,
                            Comments = sup.Comments
                        };

            Llstsupliers = suprs.ToList<SupplierRepository>();
            return Llstsupliers;
        }

        public List<SupplierRepository> GetSupplierByName(string PstrSearchTerm)
        {

            List<SupplierRepository> Llstsupliers = new List<SupplierRepository>();
            var suprs = from sup in _db.tblSuppliers
                        join ad in _db.tblAddresses
                         on sup.AddressID equals ad.AddressID

                        where sup.IsActive == true && sup.SupplierName.Equals(PstrSearchTerm)
                        select new SupplierRepository
                        {
                            SupplierID = sup.SupplierID,
                            SupplierName = sup.SupplierName,

                            Address = new AddressRepository
                            {
                                AddressLine1 = ad.AddressLine1,
                                AddressLine2 = ad.AddressLine2,
                                City = ad.City
                            },

                            ContactPerson = sup.ContactPerson,
                            ContactNo = sup.ContactNo,
                            TinNo = sup.TinNo,
                            Comments = sup.Comments
                        };

            Llstsupliers = suprs.ToList<SupplierRepository>();
            return Llstsupliers;
        }

        public SupplierRepository PostSupplier(SupplierRepository PobjSupplier)
        {
            tblSupplier LobjSupplier = new tblSupplier();
            tblAddress Lobjaddress = new tblAddress();

            ObjectCopier.CopyObjectData(PobjSupplier.Address, Lobjaddress);
            ObjectCopier.CopyObjectData(PobjSupplier, LobjSupplier);
            LobjSupplier.IsActive = true;
            // LobjSupplier.CreatedBy = 1;
            LobjSupplier.CreatedDate = DateTime.Now;

            // LobjStaff.CreatedDate = DateTime.Now;

            Lobjaddress.IsActive = true;
            // Lobjaddress.CreatedBy = 1;
            Lobjaddress.CreatedDate = DateTime.Now;
            LobjSupplier.tblAddress = Lobjaddress;

            _db.tblAddresses.Add(Lobjaddress);
            _db.tblSuppliers.Add(LobjSupplier);


            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(Lobjaddress, PobjSupplier.Address);
            ObjectCopier.CopyObjectData(LobjSupplier, PobjSupplier);
            return PobjSupplier;



        }

        public bool PutSupplier(int id, SupplierRepository PobjSupplier)
        {

            tblSupplier LobjtblSupplier = new tblSupplier();

            LobjtblSupplier = _db.tblSuppliers.Where(s => s.SupplierID == id).FirstOrDefault<tblSupplier>();
            if (LobjtblSupplier != null)
            {
                LobjtblSupplier.SupplierName = PobjSupplier.SupplierName;
                LobjtblSupplier.ContactPerson = PobjSupplier.ContactPerson;
                LobjtblSupplier.ContactNo = PobjSupplier.ContactNo;
                LobjtblSupplier.tblAddress.AddressLine1 = PobjSupplier.Address.AddressLine1;
                LobjtblSupplier.tblAddress.AddressLine2 = PobjSupplier.Address.AddressLine2;
                LobjtblSupplier.tblAddress.City = PobjSupplier.Address.City;
                LobjtblSupplier.tblAddress.State = PobjSupplier.Address.State;
                LobjtblSupplier.tblAddress.Country = PobjSupplier.Address.Country;
                LobjtblSupplier.tblAddress.IsActive = true; ;
                LobjtblSupplier.tblAddress.ModifiedBy = PobjSupplier.Address.ModifiedBy;
                LobjtblSupplier.tblAddress.ModifiedDate = DateTime.Now; ;
                LobjtblSupplier.TinNo = PobjSupplier.TinNo;
                LobjtblSupplier.ModifiedDate = DateTime.Now;
                LobjtblSupplier.ModifiedBy = PobjSupplier.ModifiedBy;
            }
            int res = _db.SaveChanges();

            if (res >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteSupplier(int id)
        {

            tblSupplier LobjtblSupplier = new tblSupplier();
            LobjtblSupplier = _db.tblSuppliers.Where(s => s.SupplierID == id).FirstOrDefault<tblSupplier>();
            if (LobjtblSupplier != null)
            {
                LobjtblSupplier.IsActive = false;
                LobjtblSupplier.ModifiedDate = DateTime.Now;
                LobjtblSupplier.ModifiedBy = 2;


            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Manufacture

        public List<ManufacturerRepository> GetManufacturers()
        {

            List<ManufacturerRepository> LlstMfgs = new List<ManufacturerRepository>();
            var mfgs = from mfg in _db.tblManufacturers
                       where mfg.IsActive == true
                       select new ManufacturerRepository
                       {
                           MfgID = mfg.MfgID,
                           MfgName = mfg.MfgName,
                           AgentContactNumber = mfg.AgentContactNumber,
                           AgentName = mfg.AgentName,
                           IsActive = mfg.IsActive,
                           CreatedBy = mfg.CreatedBy,
                           CreatedDate = mfg.CreatedDate
                       };
            LlstMfgs = mfgs.ToList<ManufacturerRepository>();
            return LlstMfgs;
        }
        public List<ManufacturerRepository> GetManufacturers(bool isActive = true)
        {
            List<ManufacturerRepository> objMfgs = new List<ManufacturerRepository>();
            using (HospitalTenantContext context = new HospitalTenantContext())
            {
                List<tblManufacturer> objTblMfgs = new List<tblManufacturer>();
                if (isActive)
                {
                    objTblMfgs = context.tblManufacturers.Where(c => c.IsActive == true).OrderBy(c => c.MfgName).ToList<tblManufacturer>();
                }
                else
                {
                    objTblMfgs = context.tblManufacturers.OrderBy(c => c.MfgName).ToList<tblManufacturer>();
                }
                foreach (tblManufacturer objTblMfg in objTblMfgs)
                {
                    objMfgs.Add(new ManufacturerRepository()
                    {
                        MfgID = objTblMfg.MfgID,
                        MfgName = objTblMfg.MfgName,
                        AgentContactNumber = objTblMfg.AgentContactNumber,
                        AgentName = objTblMfg.AgentName,
                        // CityName = objTblMfg.City,
                        IsActive = objTblMfg.IsActive,
                        CreatedBy = objTblMfg.CreatedBy,
                        CreatedDate = objTblMfg.CreatedDate
                    });
                }
            }
            return objMfgs;
        }

        public ManufacturerRepository GetManufacturesById(int id)
        {

            ManufacturerRepository objMfg = new ManufacturerRepository();
            objMfg = (from mfg in _db.tblManufacturers
                      where mfg.IsActive == true && mfg.MfgID == id
                      select new ManufacturerRepository
                      {
                          MfgID = mfg.MfgID,
                          MfgName = mfg.MfgName,
                          AgentContactNumber = mfg.AgentContactNumber,
                          AgentName = mfg.AgentName,
                          IsActive = mfg.IsActive,
                          //CityName = mfg.City,
                          CreatedBy = mfg.CreatedBy,
                          CreatedDate = mfg.CreatedDate
                      }).FirstOrDefault<ManufacturerRepository>();
            return objMfg;
        }
        public List<ManufacturerRepository> GetManufacturerBySearch(string PstrSearchTerm)
        {

            List<ManufacturerRepository> LlstMfgs = new List<ManufacturerRepository>();
            var mfgs = from mfg in _db.tblManufacturers
                       where mfg.IsActive == true && mfg.MfgName.StartsWith(PstrSearchTerm)
                       select new ManufacturerRepository
                       {
                           MfgID = mfg.MfgID,
                           MfgName = mfg.MfgName,
                           AgentContactNumber = mfg.AgentContactNumber,
                           AgentName = mfg.AgentName,
                           IsActive = mfg.IsActive
                       };
            LlstMfgs = mfgs.ToList<ManufacturerRepository>();
            return LlstMfgs;
        }
        public List<ManufacturerRepository> GetManufacturerByName(string PstrSearchTerm)
        {

            List<ManufacturerRepository> LlstMfgs = new List<ManufacturerRepository>();
            var mfgs = from mfg in _db.tblManufacturers
                       where mfg.IsActive == true && mfg.MfgName.Equals(PstrSearchTerm)
                       select new ManufacturerRepository
                       {
                           MfgID = mfg.MfgID,
                           MfgName = mfg.MfgName,
                           AgentContactNumber = mfg.AgentContactNumber,
                           AgentName = mfg.AgentName,
                           IsActive = mfg.IsActive
                       };
            LlstMfgs = mfgs.ToList<ManufacturerRepository>();
            return LlstMfgs;
        }
        public ManufacturerRepository PostManufacturer(ManufacturerRepository PobjManufacturer)
        {
            tblManufacturer LobjtblManufacturer = new tblManufacturer();
            ObjectCopier.CopyObjectData(PobjManufacturer, LobjtblManufacturer);
            LobjtblManufacturer.CreatedDate = DateTime.Now;
            LobjtblManufacturer.IsActive = true;
            LobjtblManufacturer.CreatedBy = PobjManufacturer.CreatedBy;
            LobjtblManufacturer.CreatedBy = 1;
            LobjtblManufacturer.IsActive = true;
            _db.tblManufacturers.Add(LobjtblManufacturer);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblManufacturer, PobjManufacturer);
            return PobjManufacturer;
        }

        public bool PutManufacturer(int mfgId, ManufacturerRepository PobjManufacturer)
        {

            tblManufacturer LobjtblManufacturer = new tblManufacturer();
            LobjtblManufacturer = _db.tblManufacturers.Where(m => m.MfgID == mfgId).FirstOrDefault<tblManufacturer>();
            if (LobjtblManufacturer != null)
            {
                LobjtblManufacturer.MfgName = PobjManufacturer.MfgName;
                LobjtblManufacturer.AgentName = PobjManufacturer.AgentName;
                LobjtblManufacturer.AgentContactNumber = PobjManufacturer.AgentContactNumber;
                LobjtblManufacturer.ModifiedDate = DateTime.Now;
                LobjtblManufacturer.ModifiedBy = PobjManufacturer.ModifiedBy;
            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteManufacturer(int mfgId)
        {

            tblManufacturer LobjtblManufacturer = new tblManufacturer();
            LobjtblManufacturer = _db.tblManufacturers.Where(m => m.MfgID == mfgId).FirstOrDefault<tblManufacturer>();
            _db.tblManufacturers.Remove(LobjtblManufacturer);
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region MedicinePurchase
        public MedicinePurchaseRepository PostMedicinePurchase(MedicinePurchaseRepository PobjMedicinePurchase)
        {

            tblMedicinePurchase LobjtblMedicinePurchase = new tblMedicinePurchase();
            if (PobjMedicinePurchase.SupplierID == 0 & !string.IsNullOrWhiteSpace(PobjMedicinePurchase.SupplierName))
            {
                List<SupplierRepository> Llstsupliers = GetSupplierByName(PobjMedicinePurchase.SupplierName);
                //If supplier Name does not exists in database, we will insert new supplier and get the supplierID of newly added one.
                if (Llstsupliers.Count == 0)
                {
                    SupplierRepository LobjSupplier = new SupplierRepository();
                    LobjSupplier.SupplierName = PobjMedicinePurchase.SupplierName;
                    LobjSupplier.IsActive = true;
                    PostSupplier(LobjSupplier);
                    Llstsupliers = GetSupplierByName(PobjMedicinePurchase.SupplierName);
                }
                else
                {
                    PobjMedicinePurchase.SupplierID = Llstsupliers[0].SupplierID;
                }
                LobjtblMedicinePurchase.SupplierID = PobjMedicinePurchase.SupplierID;
                LobjtblMedicinePurchase.PurchaseDate = PobjMedicinePurchase.PurchaseDate;
                LobjtblMedicinePurchase.BillNo = PobjMedicinePurchase.BillNo;
                LobjtblMedicinePurchase.PharmacyID = 1;
            }
            else
            {
                List<tblMedicinePurchaseInfo> LobjlsttblMedicinePurchaseInfo = new List<tblMedicinePurchaseInfo>();
                foreach (MedicinePurchaseInfoRepository objMedicinePurInfo in PobjMedicinePurchase.lstMedicinePurchaseInfo)
                {
                    tblMedicinePurchaseInfo LobjtblMedicinePurchaseInfo = new tblMedicinePurchaseInfo();
                    ProductsRepository LobjProduct = objMedicinePurInfo.Product;
                    LobjtblMedicinePurchaseInfo.ProductID = LobjProduct.ProductID;
                    LobjtblMedicinePurchaseInfo.BatchNo = objMedicinePurInfo.BatchNo;
                    LobjtblMedicinePurchaseInfo.ManufactureDate = objMedicinePurInfo.MfgDate;
                    LobjtblMedicinePurchaseInfo.ExpiryDate = objMedicinePurInfo.ExpDate;
                    LobjtblMedicinePurchaseInfo.Quantity = objMedicinePurInfo.Quantity;
                    LobjtblMedicinePurchaseInfo.FreeQuantity = objMedicinePurInfo.FreeQuantity;
                    LobjtblMedicinePurchaseInfo.PackQuantity = objMedicinePurInfo.PackQuantity;
                    LobjtblMedicinePurchaseInfo.PackPurchaseCost = objMedicinePurInfo.PurchaseCostPerUnit * objMedicinePurInfo.PackQuantity;
                    LobjtblMedicinePurchaseInfo.PurchaseCostPerUnit = objMedicinePurInfo.PurchaseCostPerUnit;
                    LobjtblMedicinePurchaseInfo.SellingCostPerUnit = objMedicinePurInfo.SellingCostPerUnit;
                    LobjtblMedicinePurchaseInfo.VatPercentage = objMedicinePurInfo.VatPercentage;
                    LobjtblMedicinePurchaseInfo.Discount = objMedicinePurInfo.Discount;
                    LobjtblMedicinePurchaseInfo.CreatedBy = 1;
                    LobjtblMedicinePurchaseInfo.CreatedDate = DateTime.Now;
                    LobjlsttblMedicinePurchaseInfo.Add(LobjtblMedicinePurchaseInfo);
                }
                LobjtblMedicinePurchase.tblMedicinePurchaseInfoes = LobjlsttblMedicinePurchaseInfo;
                LobjtblMedicinePurchase.CreatedBy = 1;
                LobjtblMedicinePurchase.CreatedDate = DateTime.Now;
                _db.tblMedicinePurchases.Add(LobjtblMedicinePurchase);

                GobjUtilities.GetResultInText(_db.SaveChanges());
            }
            return PobjMedicinePurchase;
        }

        #endregion

        #region MedicineType
        public List<MedicineTypeRepository> GetMedicineType()
        {
            List<MedicineTypeRepository> LObjlstProductForm = new List<MedicineTypeRepository>();

            var varProductForm = from p in _db.tblProducts
                                 join m in _db.tblManufacturers on p.MfgID equals m.MfgID
                                 where p.FormCategory == "ProductForm" && p.IsActive == true
                                 select new MedicineTypeRepository
                                 {
                                     ProductID = p.ProductID,
                                     ProductName = p.ProductName,
                                     FormName = p.FormName,
                                     FormCategory = p.FormCategory,
                                     GroupName = p.GroupName,
                                     ManufactureName = m.MfgName

                                 };
            LObjlstProductForm = varProductForm.ToList<MedicineTypeRepository>();
            return LObjlstProductForm;
        }
        public List<MedicineTypeRepository> GetMedicineTypeById(int id)
        {
            List<MedicineTypeRepository> LObjlstProductForm = new List<MedicineTypeRepository>();

            var varProductForm = from p in _db.tblProducts
                                 join m in _db.tblManufacturers on p.MfgID equals m.MfgID
                                 where p.FormCategory == "ProductForm" && p.IsActive == true && p.ProductID == id


                                 select new MedicineTypeRepository
                                 {
                                     ProductID = p.ProductID,
                                     ProductName = p.ProductName,
                                     FormName = p.FormName,
                                     FormCategory = p.FormCategory,
                                     GroupName = p.GroupName,
                                     ManufactureName = m.MfgName
                                 };
            LObjlstProductForm = varProductForm.ToList<MedicineTypeRepository>();
            return LObjlstProductForm;
        }
        public MedicineTypeRepository PostMedicineType(MedicineTypeRepository PobjMedicineType)
        {

            tblProduct LobjtblProduct = new tblProduct();
            ObjectCopier.CopyObjectData(PobjMedicineType, LobjtblProduct);
            LobjtblProduct.CreatedDate = DateTime.Now;
            LobjtblProduct.IsActive = true;
            _db.tblProducts.Add(LobjtblProduct);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblProduct, PobjMedicineType);
            return PobjMedicineType;
        }
        public bool PutMedicineType(int ProductID, MedicineTypeRepository PobjMedicineType)
        {
            tblProduct Lobjtblproduct = new tblProduct();
            Lobjtblproduct = _db.tblProducts.Where(p => p.ProductID == ProductID).FirstOrDefault<tblProduct>();
            if (Lobjtblproduct != null)
            {
                Lobjtblproduct.ProductName = PobjMedicineType.ProductName;
                Lobjtblproduct.ModifiedDate = DateTime.Now;
            }
            int res = _db.SaveChanges();
            if (res >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteMedicineType(int ProductID)
        {
            tblProduct LobjtblProduct = new tblProduct();
            LobjtblProduct = _db.tblProducts.Where(p => p.ProductID == ProductID).FirstOrDefault<tblProduct>();
            if (LobjtblProduct != null)
            {
                LobjtblProduct.IsActive = false;
            }
            int res = _db.SaveChanges();
            if (res >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<MedicineTypeRepository> GetMedicineTypeBySearch(string PstrSearchTerm)
        {
            List<MedicineTypeRepository> LlstMedicineType = new List<MedicineTypeRepository>();
            var varMedicineType = from p in _db.tblProducts
                                  join m in _db.tblManufacturers on p.MfgID equals m.MfgID
                                  where p.FormCategory == "ProductForm" && p.IsActive == true && p.ProductName.StartsWith(PstrSearchTerm)
                                  select new MedicineTypeRepository
                                  {
                                      ProductID = p.ProductID,
                                      ProductName = p.ProductName,
                                      FormName = p.FormName,
                                      GroupName = p.GroupName,
                                      FormCategory = p.FormCategory,
                                      ManufactureName = m.MfgName
                                  };
            LlstMedicineType = varMedicineType.ToList<MedicineTypeRepository>();
            return LlstMedicineType;
        }

        #endregion

        #region MedicineGroup
        public List<MedicineGroupRepository> GetMedicineGroup()
        {

            List<MedicineGroupRepository> LobjlstMedicineGroup = new List<MedicineGroupRepository>();

            var varMedicineGroup = from p in _db.tblProducts
                                   join m in _db.tblManufacturers on p.MfgID equals m.MfgID
                                   where p.FormCategory == "ProductGroup" && p.IsActive == true
                                   select new MedicineGroupRepository
                    {
                        ProductID = p.ProductID,
                        ProductName = p.ProductName,
                        FormName = p.FormName,
                        GroupName = p.GroupName,
                        FormCategory = p.FormCategory,
                        ManufactureName = m.MfgName
                    };
            LobjlstMedicineGroup = varMedicineGroup.ToList<MedicineGroupRepository>();
            return LobjlstMedicineGroup;
        }
        public List<MedicineGroupRepository> GetMedicineGroupById(int id)
        {

            List<MedicineGroupRepository> LobjlstMedicineGroup = new List<MedicineGroupRepository>();

            var varMedicineGroup = from p in _db.tblProducts
                                   join m in _db.tblManufacturers on p.MfgID equals m.MfgID
                                   where p.FormCategory == "ProductGroup" && p.IsActive == true && p.ProductID == id

                                   select new MedicineGroupRepository
                                   {
                                       ProductID = p.ProductID,
                                       ProductName = p.ProductName,
                                       FormName = p.FormName,
                                       GroupName = p.GroupName,
                                       FormCategory = p.FormCategory,
                                       ManufactureName = m.MfgName

                                   };
            LobjlstMedicineGroup = varMedicineGroup.ToList<MedicineGroupRepository>();
            return LobjlstMedicineGroup;
        }
        public MedicineGroupRepository PostMedicineGroup(MedicineGroupRepository PobjMedicineGroup)
        {
            tblProduct LobjtblProduct = new tblProduct();
            ObjectCopier.CopyObjectData(PobjMedicineGroup, LobjtblProduct);
            LobjtblProduct.CreatedDate = DateTime.Now;
            LobjtblProduct.IsActive = true;
            _db.tblProducts.Add(LobjtblProduct);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblProduct, PobjMedicineGroup);
            return PobjMedicineGroup;
        }
        public bool PutMedicineGroup(int ProductId, MedicineGroupRepository PobjMedicineGroup)
        {
            tblProduct LobjtblProduct = new tblProduct();
            LobjtblProduct = _db.tblProducts.Where(p => p.ProductID == ProductId).FirstOrDefault<tblProduct>();
            if (LobjtblProduct != null)
            {
                LobjtblProduct.ProductName = PobjMedicineGroup.ProductName;
                LobjtblProduct.ModifiedDate = DateTime.Now;
            }
            int res = _db.SaveChanges();
            if (res >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteMedicineGroup(int ProductId)
        {
            tblProduct LobjtblProduct = new tblProduct();
            LobjtblProduct = _db.tblProducts.Where(p => p.ProductID == ProductId).FirstOrDefault<tblProduct>();
            if (LobjtblProduct != null)
            {
                LobjtblProduct.IsActive = false;
            }
            int res = _db.SaveChanges();
            if (res >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<MedicineGroupRepository> GetMedicineGroupBySearch(string PstrSearchTerm)
        {
            List<MedicineGroupRepository> LobjlstMedicineGroup = new List<MedicineGroupRepository>();
            var varMedicineGroup = from p in _db.tblProducts
                                   join m in _db.tblManufacturers on p.MfgID equals m.MfgID
                                   where p.FormCategory == "ProductGroup" && p.IsActive == true && p.ProductName.StartsWith(PstrSearchTerm)
                                   select new MedicineGroupRepository
                                   {
                                       ProductID = p.ProductID,
                                       ProductName = p.ProductName,
                                       FormName = p.FormName,
                                       GroupName = p.GroupName,
                                       FormCategory = p.FormCategory,
                                       ManufactureName = m.MfgName
                                   };
            LobjlstMedicineGroup = varMedicineGroup.ToList<MedicineGroupRepository>();
            return LobjlstMedicineGroup;
        }
        #endregion

        #region Product
        public List<ProductsRepository> GetProduct()
        {
            HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
            List<ProductsRepository> LobjlstProduct = new List<ProductsRepository>();
            var suprs = from sup in GobjHospitalTenantContext.tblProducts
                        join
                            m in GobjHospitalTenantContext.tblManufacturers on sup.MfgID equals m.MfgID
                        
                        where sup.IsActive == true
                        select new ProductsRepository
                        {
                            ProductID = sup.ProductID,
                            ProductName = sup.ProductName,
                            //GroupID = sup.GroupID,
                            MfgID = sup.MfgID,
                            LicenceNo = sup.LicenceNo,
                            QuantityPerUnit = sup.QuantityPerUnit,
                            RackLocation = sup.RackLocation,
                            Comments = sup.Comments,
                            //Schedule = sup.Schedule,
                            // FormID = (Int32)sup.FormID,
                            IsActive = sup.IsActive,
                            MfgName = m.MfgName,
                            AgentName = m.AgentName,
                            AgentContactNumber = m.AgentContactNumber,
                            FormName = sup.FormName,
                            
                        };

            LobjlstProduct = suprs.ToList<ProductsRepository>();
            return LobjlstProduct;
        }

        public ProductsRepository PostProduct(ProductsRepository PobjProductDetails)
        {

            
            int res = 0;
            if (!_db.tblProducts.Any(p => p.ProductName == PobjProductDetails.ProductName))
            {
                tblProduct LobjtblProduct = new tblProduct();
                ObjectCopier.CopyObjectData(PobjProductDetails, LobjtblProduct);
                //List<MedicineTypeRepository> LobjMedicineType = GetMedicineTypeByName(PobjProductDetails.FormName);
                //if (LobjMedicineType.Count == 0)
                //{
                //    MedicineTypeRepository lobjlookup = new MedicineTypeRepository();
                //    lobjlookup.LookupName = PobjProductDetails.FormName;
                //    lobjlookup.LookupCategoryId = 19;
                //    lobjlookup.IsActive = true;
                //    PostMedicineType(lobjlookup);
                //    LobjMedicineType = GetMedicineTypeByName(PobjProductDetails.FormName);
                //}
                // LobjtblProduct.FormID = LobjMedicineType[0].LookupId;
                //List<ManufacturerRepository> LobjlstManufacturerRepository = GetManufacturerByName(PobjProductDetails.MfgName);
                //if (LobjlstManufacturerRepository.Count == 0)
                //{
                //    ManufacturerRepository lobjManufacturerRepository = new ManufacturerRepository();
                //    lobjManufacturerRepository.MfgName = PobjProductDetails.MfgName;

                //    PostManufacturer(lobjManufacturerRepository);
                //    LobjlstManufacturerRepository = GetManufacturerByName(PobjProductDetails.MfgName);
                //}
                //LobjtblProduct.MfgID = LobjlstManufacturerRepository[0].MfgID;
                LobjtblProduct.CreatedDate = DateTime.Now;

                LobjtblProduct.IsActive = true;
                _db.tblProducts.Add(LobjtblProduct);
                res = _db.SaveChanges();

                ObjectCopier.CopyObjectData(LobjtblProduct, PobjProductDetails);

            }
            return PobjProductDetails;
        }
        public bool PutProduct(int id, ProductsRepository PobjProduct)
        {
            try
            {
                //int ProductID = PobjProduct.ProductID;
                HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
                tblProduct LobtblProduct = new tblProduct();

                LobtblProduct = GobjHospitalTenantContext.tblProducts.Where(P => P.ProductID == id).FirstOrDefault<tblProduct>();
                if (LobtblProduct != null)
                {
                    LobtblProduct.ProductName = PobjProduct.ProductName;
                    LobtblProduct.FormName = PobjProduct.FormName;
                    LobtblProduct.FormCategory = PobjProduct.FormCategory;
                    LobtblProduct.GroupName = PobjProduct.GroupName;

                    LobtblProduct.ModifiedDate = DateTime.Now;
                }
                int data = GobjHospitalTenantContext.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool deleteProduct(int id)
        {
            try
            {

                HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
                tblProduct LobtblProduct = new tblProduct();

                LobtblProduct = GobjHospitalTenantContext.tblProducts.Where(P => P.ProductID == id).FirstOrDefault<tblProduct>();
                if (LobtblProduct != null)
                {
                    LobtblProduct.IsActive = false;
                }
                int data = GobjHospitalTenantContext.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductsRepository> GetProductBySearch(string type, string PstrSearchTerm)
        {
            //string str = PstrSearchTerm.ProductName.ToString();
            HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
            List<ProductsRepository> LobjlstProduct = new List<ProductsRepository>();
            // tblProduct LobtblProduct = new tblProduct();
            if (type == "MedicineName")
            {
                var suprs = from sup in GobjHospitalTenantContext.tblProducts
                            join
                                m in GobjHospitalTenantContext.tblManufacturers on sup.MfgID equals m.MfgID
                            where sup.IsActive == true && sup.ProductName.StartsWith(PstrSearchTerm)

                            select new ProductsRepository
                            {
                                ProductID = sup.ProductID,
                                ProductName = sup.ProductName,
                                // GroupID = sup.GroupID,
                                //MfgID = sup.MfgID,
                                LicenceNo = sup.LicenceNo,
                                QuantityPerUnit = sup.QuantityPerUnit,
                                RackLocation = sup.RackLocation,
                                Comments = sup.Comments,
                                //Schedule = sup.Schedule,
                                // FormID = (Int32)sup.FormID,
                                MfgName = m.MfgName,
                                AgentName = m.AgentName,
                                AgentContactNumber = m.AgentContactNumber,
                                FormName = sup.FormName,
                                GroupName = sup.GroupName,
                                FormCategory = sup.FormCategory,
                                
                                //CreatedBy = sup.CreatedBy,
                                // CreatedDate = sup.CreatedDate,
                                // ModifiedBy = sup.ModifiedBy,
                                // ModifiedDate = (DateTime)sup.ModifiedDate,

                            };
                LobjlstProduct = suprs.ToList<ProductsRepository>();
            }
            else if (type == "CompanyName")
            {
                var suprs = from sup in GobjHospitalTenantContext.tblProducts
                            join
                                m in GobjHospitalTenantContext.tblManufacturers on sup.MfgID equals m.MfgID                           
                            where sup.IsActive == true && m.MfgName.StartsWith(PstrSearchTerm)

                            select new ProductsRepository
                            {
                                ProductID = sup.ProductID,
                                ProductName = sup.ProductName,
                                // GroupID = sup.GroupID,
                                //MfgID = sup.MfgID,
                                LicenceNo = sup.LicenceNo,
                                QuantityPerUnit = sup.QuantityPerUnit,
                                RackLocation = sup.RackLocation,
                                Comments = sup.Comments,
                                //Schedule = sup.Schedule,
                                // FormID = (Int32)sup.FormID,
                                MfgName = m.MfgName,
                                AgentName = m.AgentName,
                                AgentContactNumber = m.AgentContactNumber,
                                FormName = sup.FormName,
                                GroupName = sup.GroupName,
                                FormCategory = sup.FormCategory,


                            };

                LobjlstProduct = suprs.ToList<ProductsRepository>();
            }

            return LobjlstProduct;
        }

        public List<ProductsRepository> GetProductByName(string PstrSearchTerm)
        {
            //string str = PstrSearchTerm.ProductName.ToString();
            HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
            List<ProductsRepository> LobjlstProduct = new List<ProductsRepository>();
            // tblProduct LobtblProduct = new tblProduct();

            var suprs = from sup in GobjHospitalTenantContext.tblProducts
                        join
                            m in GobjHospitalTenantContext.tblManufacturers on sup.MfgID equals m.MfgID
                        where
                        sup.IsActive == true && sup.ProductName.StartsWith(PstrSearchTerm)
                        select new ProductsRepository
                        {
                            ProductID = sup.ProductID,
                            ProductName = sup.ProductName,
                            // GroupID = sup.GroupID,
                            //MfgID = sup.MfgID,
                            LicenceNo = sup.LicenceNo,
                            QuantityPerUnit = sup.QuantityPerUnit,
                            RackLocation = sup.RackLocation,
                            Comments = sup.Comments,
                            //Schedule = sup.Schedule,
                            // FormID = (Int32)sup.FormID,
                            MfgName = m.MfgName,
                            AgentName = m.AgentName,
                            AgentContactNumber = m.AgentContactNumber,
                            FormName = sup.FormName,
                            FormCategory = sup.FormCategory,
                            GroupName = sup.GroupName,
                            
                            //CreatedBy = sup.CreatedBy,
                            // CreatedDate = sup.CreatedDate,
                            // ModifiedBy = sup.ModifiedBy,
                            // ModifiedDate = (DateTime)sup.ModifiedDate,

                        };


            LobjlstProduct = suprs.ToList<ProductsRepository>();
            return LobjlstProduct;
        }
        #endregion

        #region LabReports
        public List<LabTestReportRepository> GetLabTestReport()
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<LabTestReportRepository> LObjlstLabTestReport = new List<LabTestReportRepository>();
            var LabTestReports = from lt in LobjHospitalTenantContext.tblLabTests

                                 select new LabTestReportRepository
                                 {
                                     LabTestID = lt.LabTestID,
                                     Invs_Name = lt.Invs_Name,
                                     Invs_Code = lt.Invs_Code,
                                     Price = lt.Price,
                                     Category = lt.Category,
                                     Description = lt.Description,
                                     ReferAmount = lt.ReferAmount

                                 };
            LObjlstLabTestReport = LabTestReports.ToList<LabTestReportRepository>();
            return LObjlstLabTestReport;
        }
        public LabTestReportRepository PostLabTest(LabTestReportRepository PobjLabTest)
        {
            tblLabTest LobjtbllabTest = new tblLabTest();

            ObjectCopier.CopyObjectData(PobjLabTest, LobjtbllabTest);
            LobjtbllabTest.CreatedDate = DateTime.Now;
            LobjtbllabTest.CreatedBy = 1;
            _db.tblLabTests.Add(LobjtbllabTest);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtbllabTest, PobjLabTest);

            return PobjLabTest;
        }

        public List<LabOpRequestReportRepository> GetLabOpRequestReport()
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<LabOpRequestReportRepository> LObjlstLabRequestReport = new List<LabOpRequestReportRepository>();
            var LabRequestReports = from lReq in LobjHospitalTenantContext.tblOpLabRequests
                                    join a in LobjHospitalTenantContext.tblAppointments on lReq.AppointmentID equals a.AppointmentID
                                    join e in LobjHospitalTenantContext.tblEmployees on a.DoctorID equals e.EID
                                    select new LabOpRequestReportRepository
                                  {
                                      LabTestID = lReq.LabTestID,
                                      OpLabRequestID = lReq.OpLabRequestID,
                                      DoctorName = e.FirstName + " " + e.LastName,
                                      StartDate = a.StartDateTime,
                                      TestName = lReq.TestName,
                                      Status = lReq.Status,

                                  };
            LObjlstLabRequestReport = LabRequestReports.ToList<LabOpRequestReportRepository>();
            return LObjlstLabRequestReport;
        }
        public LabOpRequestReportRepository PostLabOpRequest(LabOpRequestReportRepository PobjLabOpRequest)
        {
            tblOpLabRequest LobjtblOpLabRequest = new tblOpLabRequest();

            ObjectCopier.CopyObjectData(PobjLabOpRequest, LobjtblOpLabRequest);
            LobjtblOpLabRequest.CreatedDate = DateTime.Now;
            LobjtblOpLabRequest.CreatedBy = 1;
            _db.tblOpLabRequests.Add(LobjtblOpLabRequest);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblOpLabRequest, PobjLabOpRequest);

            return PobjLabOpRequest;
        }


        #endregion

        #region Drugprescriptions

        public List<DrugPrescriptionRepository> GetDrugPrescriptions()
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<DrugPrescriptionRepository> lobjlstDrugPrescriptions = new List<DrugPrescriptionRepository>();
            var DrugPrescription = from tm in LobjHospitalTenantContext.tblTreatmentMedicines
                                   join t in LobjHospitalTenantContext.tblTreatments on tm.TreatmentID equals t.TreatmentId
                                   join p in LobjHospitalTenantContext.tblPatients on t.PatientId equals p.PatientId
                                   join d in LobjHospitalTenantContext.tblEmployees on t.DoctorId equals d.EID

                                   where tm.IsActive == true

                                   select new DrugPrescriptionRepository
                                   {
                                       TrMedicineID = tm.TrMedicineID,
                                       TreatmentID = tm.TreatmentID,
                                       MedicineID = tm.MedicineID,
                                       MedicineName = tm.MedicineName,
                                       Dose = tm.Dose,
                                       Quantity = tm.Quantity,
                                       NoofDays = tm.NoofDays,
                                       Directions = tm.Directions,
                                       PatientName = p.FirstName + " " + p.LastName,
                                       Gender = p.Gender,
                                       ContactNumber = p.ContactNumber,
                                       DoctorName = d.FirstName + " " + d.LastName


                                   };
            lobjlstDrugPrescriptions = DrugPrescription.ToList<DrugPrescriptionRepository>();
            return lobjlstDrugPrescriptions;

        }
        public DrugPrescriptionRepository GetDrugPrescriptions(int id)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            DrugPrescriptionRepository lobjlstDrugPrescriptions = new DrugPrescriptionRepository();
            lobjlstDrugPrescriptions = (from tm in LobjHospitalTenantContext.tblTreatmentMedicines
                                        join t in LobjHospitalTenantContext.tblTreatments on tm.TreatmentID equals t.TreatmentId
                                        join p in LobjHospitalTenantContext.tblPatients on t.PatientId equals p.PatientId
                                        join d in LobjHospitalTenantContext.tblEmployees on t.DoctorId equals d.EID

                                        where tm.IsActive == true && tm.TrMedicineID == id

                                        select new DrugPrescriptionRepository
                                        {
                                            TrMedicineID = tm.TrMedicineID,
                                            TreatmentID = tm.TreatmentID,
                                            MedicineID = tm.MedicineID,
                                            MedicineName = tm.MedicineName,
                                            Dose = tm.Dose,
                                            Quantity = tm.Quantity,
                                            NoofDays = tm.NoofDays,
                                            Directions = tm.Directions,
                                            PatientName = p.FirstName + " " + p.LastName,
                                            Gender = p.Gender,
                                            ContactNumber = p.ContactNumber,
                                            DoctorName = d.FirstName + " " + d.LastName


                                        }).FirstOrDefault();
            return lobjlstDrugPrescriptions;

        }
        public DrugPrescriptionRepository PostDrugPrescription(DrugPrescriptionRepository PobjDrugPrescription)
        {
            tblTreatment Lobjtbltreatment = new tblTreatment();
            tblTreatmentMedicine lobjtbltreatmentmedicine = new tblTreatmentMedicine();

            ObjectCopier.CopyObjectData(PobjDrugPrescription.TreatmentDetails, Lobjtbltreatment);
            ObjectCopier.CopyObjectData(PobjDrugPrescription, lobjtbltreatmentmedicine);

            Lobjtbltreatment.CreatedDate = DateTime.Now;
            lobjtbltreatmentmedicine.CreatedDate = DateTime.Now;

            lobjtbltreatmentmedicine.TreatmentID = Lobjtbltreatment.TreatmentId;

            _db.tblTreatments.Add(Lobjtbltreatment);
            _db.tblTreatmentMedicines.Add(lobjtbltreatmentmedicine);

            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(Lobjtbltreatment, PobjDrugPrescription.TreatmentDetails);
            ObjectCopier.CopyObjectData(lobjtbltreatmentmedicine, PobjDrugPrescription);

            return PobjDrugPrescription;
        }

        public bool PutDrugPrescription(int TreatmentID, DrugPrescriptionRepository PobjDrugPrescription)
        {
            tblTreatmentMedicine lobjtbltreatmentmedicine = new tblTreatmentMedicine();
            lobjtbltreatmentmedicine = _db.tblTreatmentMedicines.Where(t => t.TreatmentID == TreatmentID).FirstOrDefault<tblTreatmentMedicine>();
            if (lobjtbltreatmentmedicine != null)
            {
                lobjtbltreatmentmedicine.MedicineID = PobjDrugPrescription.MedicineID;
                lobjtbltreatmentmedicine.MedicineName = PobjDrugPrescription.MedicineName;
                lobjtbltreatmentmedicine.Dose = PobjDrugPrescription.Dose;
                lobjtbltreatmentmedicine.Quantity = PobjDrugPrescription.Quantity;
                lobjtbltreatmentmedicine.NoofDays = PobjDrugPrescription.NoofDays;
                lobjtbltreatmentmedicine.Directions = PobjDrugPrescription.Directions;
                lobjtbltreatmentmedicine.Advice = PobjDrugPrescription.Advice;
                lobjtbltreatmentmedicine.Comments = PobjDrugPrescription.Comments;
                lobjtbltreatmentmedicine.ModifiedBy = PobjDrugPrescription.ModifiedBy;
                lobjtbltreatmentmedicine.ModifiedDate = DateTime.Now;
            }
            int res = _db.SaveChanges();

            if (res >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
       #endregion

        #region LabPrescription

        public List<LabPrescriptionsRepository> GetLabPrescriptions()
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<LabPrescriptionsRepository> lobjlstLabPrescriptions = new List<LabPrescriptionsRepository>();
            var LabPrescription = from td in LobjHospitalTenantContext.tblTreatmentDiagnosis
                                  join t in LobjHospitalTenantContext.tblTreatments on td.TreatmentID equals t.TreatmentId
                                  join p in LobjHospitalTenantContext.tblPatients on t.PatientId equals p.PatientId
                                  join d in LobjHospitalTenantContext.tblEmployees on t.DoctorId equals d.EID

                                  where td.IsActive == true

                                  select new LabPrescriptionsRepository
                                  {
                                      TrDiagnosisID = td.TrDiagnosisID,
                                      TreatmentID = td.TreatmentID,
                                      DiagnosisID = td.DiagnosisID,
                                      DiagnosisName = td.DiagnosisName,
                                      InMandatory = td.InMandatory,
                                      Priority = td.Priority,
                                      Comments = td.Comments,
                                      PatientName = p.FirstName + " " + p.LastName,
                                      Gender = p.Gender,
                                      ContactNumber = p.ContactNumber,
                                      DoctorName = d.FirstName + " " + d.LastName

                                  };
            lobjlstLabPrescriptions = LabPrescription.ToList<LabPrescriptionsRepository>();
            return lobjlstLabPrescriptions;

        }
        public LabPrescriptionsRepository GetLabPrescriptions(int id)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            LabPrescriptionsRepository lobjlstLabPrescriptions = new LabPrescriptionsRepository();
            lobjlstLabPrescriptions = (from td in LobjHospitalTenantContext.tblTreatmentDiagnosis
                                       join t in LobjHospitalTenantContext.tblTreatments on td.TreatmentID equals t.TreatmentId
                                       join p in LobjHospitalTenantContext.tblPatients on t.PatientId equals p.PatientId
                                       join d in LobjHospitalTenantContext.tblEmployees on t.DoctorId equals d.EID

                                       where td.IsActive == true && td.TrDiagnosisID == id

                                       select new LabPrescriptionsRepository
                                       {
                                           TrDiagnosisID = td.TrDiagnosisID,
                                           TreatmentID = td.TreatmentID,
                                           DiagnosisID = td.DiagnosisID,
                                           DiagnosisName = td.DiagnosisName,
                                           InMandatory = td.InMandatory,
                                           Priority = td.Priority,
                                           Comments = td.Comments,
                                           PatientName = p.FirstName + " " + p.LastName,
                                           Gender = p.Gender,
                                           ContactNumber = p.ContactNumber,
                                           DoctorName = d.FirstName + " " + d.LastName

                                       }).FirstOrDefault();

            return lobjlstLabPrescriptions;

        }

        public LabPrescriptionsRepository PostLabPrescription(LabPrescriptionsRepository PobjLabPrescription)
        {
            tblTreatment Lobjtbltreatment = new tblTreatment();
            tblTreatmentDiagnosi lobjtbltreatmentdiagnosis = new tblTreatmentDiagnosi();

            ObjectCopier.CopyObjectData(PobjLabPrescription.TreatmentDetails, Lobjtbltreatment);
            ObjectCopier.CopyObjectData(PobjLabPrescription, lobjtbltreatmentdiagnosis);

            Lobjtbltreatment.CreatedDate = DateTime.Now;
            lobjtbltreatmentdiagnosis.CreatedDate = DateTime.Now;

            lobjtbltreatmentdiagnosis.TreatmentID = Lobjtbltreatment.TreatmentId;

            _db.tblTreatments.Add(Lobjtbltreatment);
            _db.tblTreatmentDiagnosis.Add(lobjtbltreatmentdiagnosis);

            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(Lobjtbltreatment, PobjLabPrescription.TreatmentDetails);
            ObjectCopier.CopyObjectData(lobjtbltreatmentdiagnosis, PobjLabPrescription);

            return PobjLabPrescription;
        }

        public bool PutLabPrescription(int TreatmentID, LabPrescriptionsRepository PobjLabPrescription)
        {
            tblTreatmentDiagnosi lobjtbltreatmentdiagnosis = new tblTreatmentDiagnosi();
            lobjtbltreatmentdiagnosis = _db.tblTreatmentDiagnosis.Where(t => t.TreatmentID == TreatmentID).FirstOrDefault<tblTreatmentDiagnosi>();
            if (lobjtbltreatmentdiagnosis != null)
            {
                lobjtbltreatmentdiagnosis.DiagnosisID = PobjLabPrescription.DiagnosisID;
                lobjtbltreatmentdiagnosis.DiagnosisName = PobjLabPrescription.DiagnosisName;
                lobjtbltreatmentdiagnosis.InMandatory = PobjLabPrescription.InMandatory;
                lobjtbltreatmentdiagnosis.Priority = PobjLabPrescription.Priority;
                lobjtbltreatmentdiagnosis.Comments = PobjLabPrescription.Comments;
                lobjtbltreatmentdiagnosis.ModifiedDate = DateTime.Now;
            }
            int res = _db.SaveChanges();

            if (res >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region SalesReturn
        public List<MedicineInvoiceRepository> GetSalesReturn(int id)
        {
            HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
            List<MedicineInvoiceRepository> LobjlstMedicineInvoice = new List<MedicineInvoiceRepository>();
            var result = from mi in GobjHospitalTenantContext.tblMedicineInvoices
                         join mr in GobjHospitalTenantContext.tblMedicineInvoiceInfoes on mi.InvoiceID equals mr.InvoiceID
                         join p in GobjHospitalTenantContext.tblProducts on mr.ProductID equals p.ProductID
                         join c in GobjHospitalTenantContext.tblPatients on mi.PatientID equals c.PatientId
                         join mp in GobjHospitalTenantContext.tblMedicinePurchaseInfoes on new { a = mr.PurchaseID, b = mr.ProductID, c = mr.BatchNo }
                         equals new { a = mp.PurchaseID, b = mp.ProductID, c = mp.BatchNo }
                         where mi.InvoiceID == id
                         select new MedicineInvoiceRepository
                         {
                             InvoiceID = mi.InvoiceID,
                             PharmacyID = mi.PharmacyID,
                             InvoiceDate = mi.InvoiceDate,
                             Patient = new Patientinfo
                             {
                                 Age = c.Age,
                                 FirstName = c.FirstName,
                                 ContactNumber = c.ContactNumber,
                                 PatientType = c.PatientType,
                                 PatientID = c.PatientId,
                                 GenderName = c.Gender,
                             },
                             lstMedicineInvoiceInfo = new List<MedicineInvoiceInfoRepository>{
                                new MedicineInvoiceInfoRepository(){
                                    ProductID = p.ProductID,
                                    Quantity = mr.Quantity,
                                    BatchNo = mr.BatchNo,
                                    PurchaseID=mr.PurchaseID,
                                    Product=new ProductsRepository{
                                        ProductName = p.ProductName,
                                        ProductID = p.ProductID
                                    },
                                    MedicinePurchaseInfo=new MedicinePurchaseInfoRepository{
                                        PurchaseCostPerUnit = mp.SellingCostPerUnit,
                                        ExpDate = mp.ExpiryDate,
                                    }
                                }
                            }
                         };
            LobjlstMedicineInvoice = result.ToList<MedicineInvoiceRepository>();
            return LobjlstMedicineInvoice;
        }

        public salesReturnRepository PostSalesReturn(salesReturnRepository pobjMedicineRepository)
        {
            HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
            int res = 0;
            tblMedicineSalesReturn LobjtblMedicineSalesReturn = new tblMedicineSalesReturn();
            tblMedicineSalesReturnInfo LobjMedicineSalesReturnInfo = new tblMedicineSalesReturnInfo();

            List<MedicineInvoiceRepository> LobjMedicineInvoice = new List<MedicineInvoiceRepository>();

            ObjectCopier.CopyObjectData(pobjMedicineRepository, LobjtblMedicineSalesReturn);

            LobjtblMedicineSalesReturn.ReturnDate = DateTime.Now;
            //LobjtblMedicineSalesReturn.CreatedBy = 1;
            LobjtblMedicineSalesReturn.CreatedDate = DateTime.Now;

            GobjHospitalTenantContext.tblMedicineSalesReturns.Add(LobjtblMedicineSalesReturn);
            // GobjHospitalTenantContext.SaveChanges();
            List<tblMedicineSalesReturnInfo> lstmedicinesalesreturninfo = new List<tblMedicineSalesReturnInfo>();
            foreach (salesReturninfoRepository LobjSalesReturninfo in pobjMedicineRepository.lstSalesReturnInfo)
            {
                //LobjMedicineSalesReturnInfo.SalesReturnID = LobjMedicineSalesReturn.SalesReturnID;
                tblMedicineSalesReturnInfo LobjtblMedicineSalesReturnInfo = new tblMedicineSalesReturnInfo();
                LobjtblMedicineSalesReturnInfo.Amount = LobjSalesReturninfo.Amount;
                LobjtblMedicineSalesReturnInfo.InvoiceID = LobjSalesReturninfo.InvoiceID;
                LobjtblMedicineSalesReturnInfo.ProductID = LobjSalesReturninfo.ProductID;
                LobjtblMedicineSalesReturnInfo.BatchNo = LobjSalesReturninfo.BatchNo;
                LobjtblMedicineSalesReturnInfo.PurchaseID = LobjSalesReturninfo.PurchaseID;
                LobjtblMedicineSalesReturnInfo.Quantity = LobjSalesReturninfo.Quantity;
                LobjtblMedicineSalesReturnInfo.IsActive = 1;
                 LobjtblMedicineSalesReturnInfo.CreatedBy = 1;
                LobjtblMedicineSalesReturnInfo.CreatedDate = DateTime.Now;
                lstmedicinesalesreturninfo.Add(LobjtblMedicineSalesReturnInfo);
                // GobjHospitalTenantContext.tblMedicineSalesReturnInfoes.Add(LobjtblMedicineSalesReturnInfo);
            }
            LobjtblMedicineSalesReturn.tblMedicineSalesReturnInfoes = lstmedicinesalesreturninfo;
            GobjHospitalTenantContext.tblMedicineSalesReturns.Add(LobjtblMedicineSalesReturn);
            res = GobjHospitalTenantContext.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblMedicineSalesReturn, pobjMedicineRepository);

            return pobjMedicineRepository;
        }
        #endregion

        #region StockReturn

        public List<StockReturnMedicineDetailsRepository> GetStockReturnSuppliersByMedicineId(StockReturnMedicineDetailsRepository PstrSearch)
        {
            string pstrsearch = PstrSearch.ProductName;
            HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
            List<StockReturnMedicineDetailsRepository> LobjLstMedicineDetails = new List<StockReturnMedicineDetailsRepository>();
            var res = from p in GobjHospitalTenantContext.tblProducts
                      join m in GobjHospitalTenantContext.tblManufacturers on p.MfgID equals m.MfgID
                      join mpi in GobjHospitalTenantContext.tblMedicinePurchaseInfoes on p.ProductID equals mpi.ProductID
                      join mp in GobjHospitalTenantContext.tblMedicinePurchases on mpi.PurchaseID equals mp.PurchaseID
                      join s in GobjHospitalTenantContext.tblSuppliers on mp.SupplierID equals s.SupplierID

                      where p.ProductName == pstrsearch && p.IsActive == true
                      select new StockReturnMedicineDetailsRepository
                      {
                          ProductName = p.ProductName,
                          MfgName = m.MfgName,
                          FormName = p.FormName,
                          SupplierName = s.SupplierName,
                          SupplierID = s.SupplierID

                      };

            LobjLstMedicineDetails = res.ToList<StockReturnMedicineDetailsRepository>();
            return LobjLstMedicineDetails;


        }
        public List<StockReturnMedicineDetailsRepository> GetStockReturnBatchesBySupplier(StockReturnMedicineDetailsRepository Pstrsearch)
        {
            HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
            List<StockReturnMedicineDetailsRepository> LobjLstMedicineDetails = new List<StockReturnMedicineDetailsRepository>();
            var res = from mp in GobjHospitalTenantContext.tblMedicinePurchases
                      join mpi in GobjHospitalTenantContext.tblMedicinePurchaseInfoes on mp.PurchaseID equals mpi.PurchaseID
                      join s in GobjHospitalTenantContext.tblSuppliers on mp.SupplierID equals s.SupplierID
                      join p in GobjHospitalTenantContext.tblProducts on mpi.ProductID equals p.ProductID
                      where p.ProductID.Equals(Pstrsearch.ProductID) && mpi.PurchaseID.Equals(Pstrsearch.PurchaseID)
                      select new StockReturnMedicineDetailsRepository
                      {
                          SupplierName = s.SupplierName,
                          PurchaseDate = mp.PurchaseDate,
                          BatchNo = mpi.BatchNo
                      };
            LobjLstMedicineDetails = res.ToList<StockReturnMedicineDetailsRepository>();
            return LobjLstMedicineDetails;
        }
        public List<StockReturnMedicineDetailsRepository> GetStockReturnMedicineDetailsByBatchNo(StockReturnMedicineDetailsRepository Pstrsearch)
        {
            HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
            List<StockReturnMedicineDetailsRepository> LobjLstMedicineDetails = new List<StockReturnMedicineDetailsRepository>();
            var res = from mp in GobjHospitalTenantContext.tblMedicinePurchases
                      join mpi in GobjHospitalTenantContext.tblMedicinePurchaseInfoes on mp.PurchaseID equals mpi.PurchaseID
                      join p in GobjHospitalTenantContext.tblProducts on mpi.ProductID equals p.ProductID
                      join vw in GobjHospitalTenantContext.View_MedicineQuantity on new { a = mpi.PurchaseID, b = mpi.ProductID, c = mpi.BatchNo }
                           equals new { a = vw.PurchaseID, b = vw.ProductID, c = vw.BatchNo }
                      where mpi.ProductID.Equals(Pstrsearch.ProductID) && mpi.PurchaseID.Equals(Pstrsearch.PurchaseID) && mpi.BatchNo.Equals(Pstrsearch.BatchNo)
                      select new StockReturnMedicineDetailsRepository
                      {
                          BatchNo = mpi.BatchNo,
                          ExpiryDate = mpi.ExpiryDate,
                          PurchasedQuantity = mpi.Quantity + mpi.FreeQuantity,
                          AvailableStock = vw.Available_Stock,
                          PurchaseCostPerUnit = mpi.PurchaseCostPerUnit,
                          QuantityToBeReturned = Pstrsearch.QuantityToBeReturned,
                          Amount = (mpi.PurchaseCostPerUnit * Pstrsearch.QuantityToBeReturned)

                      };
            LobjLstMedicineDetails = res.ToList<StockReturnMedicineDetailsRepository>();
            return LobjLstMedicineDetails;
        }
        public StockReturnMedicineDetailsRepository PostStockReturn(StockReturnMedicineDetailsRepository PobjStockReturnMedicines)
        {
            HospitalTenantContext GobjHospitalTenantContext = new HospitalTenantContext();
            int res = 0;
            tblMedicineStockReturn LobjMedicineStockReturn = new tblMedicineStockReturn();
            tblMedicineStockReturnInfo LobjMedicieneStockReturnInfo = new tblMedicineStockReturnInfo();
            //List<StockReturnMedicineDetailsRepository> LobjLstMedicineDetails = new List<StockReturnMedicineDetailsRepository>();
            // Pharmacy objPharmacy = new Pharmacy();


            LobjMedicineStockReturn.CreatedBy = PobjStockReturnMedicines.CreatedBy;
            LobjMedicineStockReturn.CreatedDate = DateTime.Now;
            LobjMedicineStockReturn.IsActive = 1;
            LobjMedicineStockReturn.ReturnDate = DateTime.Now;
            LobjMedicineStockReturn.PharmacyID = PobjStockReturnMedicines.PharmacyID;
            LobjMedicineStockReturn.TotalAmount = Convert.ToDecimal(PobjStockReturnMedicines.TotalAmount);
            GobjHospitalTenantContext.tblMedicineStockReturns.Add(LobjMedicineStockReturn);
            List<tblMedicineStockReturnInfo> lstStockReturnInfo = new List<tblMedicineStockReturnInfo>();
            foreach (MedicineStockReturnInfoRepository lobjStockReturninfo in PobjStockReturnMedicines.lstStockRetrunInfo)
            {
                tblMedicineStockReturnInfo lobjtblMedicineStockReturnInfo = new tblMedicineStockReturnInfo();
                lobjtblMedicineStockReturnInfo.PurchaseID = lobjStockReturninfo.PurchaseID;

                lobjtblMedicineStockReturnInfo.ProductID = lobjStockReturninfo.ProductID;
                lobjtblMedicineStockReturnInfo.BatchNo = lobjStockReturninfo.BatchNo;
                lobjtblMedicineStockReturnInfo.Quantity = lobjStockReturninfo.Quantity;
                lobjtblMedicineStockReturnInfo.Amount = Convert.ToString(lobjStockReturninfo.Amount);
                lobjtblMedicineStockReturnInfo.CreatedBy = lobjStockReturninfo.CreatedBy;
                lobjtblMedicineStockReturnInfo.CreatedDate = DateTime.Now;
                lobjtblMedicineStockReturnInfo.IsActive = 1;
                lstStockReturnInfo.Add(lobjtblMedicineStockReturnInfo);
                //GobjHospitalTenantContext.tblMedicineStockReturnInfoes.Add(LobjMedicieneStockReturnInfo);
            }
            LobjMedicineStockReturn.tblMedicineStockReturnInfoes = lstStockReturnInfo;
            GobjHospitalTenantContext.tblMedicineStockReturns.Add(LobjMedicineStockReturn);
            res = GobjHospitalTenantContext.SaveChanges();
            ObjectCopier.CopyObjectData(LobjMedicieneStockReturnInfo, PobjStockReturnMedicines);
            return PobjStockReturnMedicines;

        }

        #endregion

        #region MedicineInvoice
        public List<MedicineBatchRepository> GetBatchNoByProductId(int id)
        {
            //int ProductId = PintProductId.ProductId;

            List<MedicineBatchRepository> LlBatchNumbers = new List<MedicineBatchRepository>();

            var LobBatchNo = (from products in _db.tblProducts
                              join PurchageInfo in _db.tblMedicinePurchaseInfoes on products.ProductID equals PurchageInfo.ProductID
                              join vwMedicine in _db.View_MedicineQuantity on PurchageInfo.ProductID equals vwMedicine.ProductID
                              where PurchageInfo.ProductID == id
                              select new MedicineBatchRepository
                              {
                                  BatchNo = PurchageInfo.BatchNo,
                                  AvailableQuantity = vwMedicine.Available_Stock,
                                  UnitPrice = PurchageInfo.SellingCostPerUnit,
                                  ExpDate = PurchageInfo.ExpiryDate,
                                  ProductId = PurchageInfo.ProductID
                              });
            LlBatchNumbers = LobBatchNo.ToList<MedicineBatchRepository>();
            return LlBatchNumbers;
        }
        public MedicineInvoiceRepository PostMedicineInvoice(MedicineInvoiceRepository PobjMedicineInvoice)
        {
            tblMedicineInvoice LobjMedicineInvoice = new tblMedicineInvoice();
            tblMedicineInvoiceInfo lobjMedicineinfo = new tblMedicineInvoiceInfo();
            tblPatient LobjPatient = new tblPatient();
            List<Patientinfo> LobjlstPatient = new List<Patientinfo>();

            if (_db.tblPatients.Any(p => (p.FirstName == PobjMedicineInvoice.Patient.FirstName && p.LastName == PobjMedicineInvoice.Patient.LastName && p.ContactNumber == PobjMedicineInvoice.Patient.ContactNumber && p.PatientType == "OP") || (p.PatientId == PobjMedicineInvoice.Patient.PatientID && p.PatientType == "IP" && (p.PatientId != 0 || p.PatientId != null))))
            {
                LobjlstPatient = GetPatientByName(PobjMedicineInvoice.Patient);
            }
            else
            {
                PostPatient(PobjMedicineInvoice.Patient);
                LobjlstPatient = GetPatientByName(PobjMedicineInvoice.Patient);
            }
            LobjPatient.PatientId = LobjlstPatient[0].PatientID;


            PobjMedicineInvoice.Patient.PatientID = LobjPatient.PatientId;

            //LobjMedicineInvoice.tblPatient = LobjPatient;

            ObjectCopier.CopyObjectData(PobjMedicineInvoice.Patient, LobjPatient);
            ObjectCopier.CopyObjectData(PobjMedicineInvoice, LobjMedicineInvoice);

            //ObjectCopier.CopyObjectData(PobjMedicineInvoice.lstMedicineiIvoiceInfo, lobjMedicineinfo);
            //LobjMedicineInvoice.ReferredDoctor = PobjMedicineInvoice.ReferredDoctor;
            //LobjMedicineInvoice.PharmacyID = PobjMedicineInvoice.PharmacyID;
            //LobjMedicineInvoice.InvoiceDate = PobjMedicineInvoice.InvoiceDate;
            //LobjMedicineInvoice.TotalAmount = PobjMedicineInvoice.TotalAmount;

            LobjMedicineInvoice.InvoiceDate = DateTime.Now;
            LobjMedicineInvoice.PatientID = LobjPatient.PatientId;


            List<tblMedicineInvoiceInfo> LobjLstMedicineinvoiceinfo = new List<tblMedicineInvoiceInfo>();

            foreach (MedicineInvoiceInfoRepository LobjmedInvoiceInfo in PobjMedicineInvoice.lstMedicineInvoiceInfo)
            {
                tblMedicineInvoiceInfo LobjtblMedicineInvoiceInfo = new tblMedicineInvoiceInfo();

                LobjtblMedicineInvoiceInfo.ProductID = LobjmedInvoiceInfo.ProductID;
                LobjtblMedicineInvoiceInfo.PurchaseID = LobjmedInvoiceInfo.PurchaseID;
                LobjtblMedicineInvoiceInfo.Quantity = LobjmedInvoiceInfo.Quantity;
                LobjtblMedicineInvoiceInfo.BatchNo = LobjmedInvoiceInfo.BatchNo;
                LobjtblMedicineInvoiceInfo.SubTotal = LobjmedInvoiceInfo.SubTotal;
                LobjtblMedicineInvoiceInfo.Usage = LobjmedInvoiceInfo.Usage;
                LobjtblMedicineInvoiceInfo.Discount = LobjmedInvoiceInfo.Discount;
                LobjLstMedicineinvoiceinfo.Add(LobjtblMedicineInvoiceInfo);
                LobjtblMedicineInvoiceInfo.CreatedDate = DateTime.Now;
            }
            LobjMedicineInvoice.CreatedDate = DateTime.Now;
            //ObjectCopier.CopyObjectData(LobjLstMedicineinvoiceinfo, lobjMedicineinfo);

            LobjMedicineInvoice.tblMedicineInvoiceInfoes = LobjLstMedicineinvoiceinfo;

            _db.tblMedicineInvoices.Add(LobjMedicineInvoice);

            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjPatient, PobjMedicineInvoice.Patient);
            ObjectCopier.CopyObjectData(lobjMedicineinfo, PobjMedicineInvoice.lstMedicineInvoiceInfo);
            ObjectCopier.CopyObjectData(LobjMedicineInvoice, PobjMedicineInvoice);

            PobjMedicineInvoice.Patient.CreatedBy = LobjMedicineInvoice.CreatedBy;
            PobjMedicineInvoice.Patient.CreatedDate = LobjMedicineInvoice.CreatedDate;
            return PobjMedicineInvoice;
        }
        #endregion
        public Patientinfo PostPatient(Patientinfo PobjPatient)
        {
            tblPatient LobjPatient = new tblPatient();
            ObjectCopier.CopyObjectData(PobjPatient, LobjPatient);
            LobjPatient.HospitalBranchId = 1;
            LobjPatient.IsActive = true;
            LobjPatient.CreatedDate = DateTime.Now;
            LobjPatient.CreatedBy = 1;
            _db.tblPatients.Add(LobjPatient);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjPatient, PobjPatient);
            return PobjPatient;
        }
        public List<Patientinfo> GetPatientByName(Patientinfo PobjPatient)
        {
            List<Patientinfo> Patientinfo = new List<Patientinfo>();
            var custmrs = from p in _db.tblPatients
                          //join lp in _db.tblLookups on p.LkGender equals lp.LookupId
                          where ((p.FirstName == PobjPatient.FirstName && p.LastName == PobjPatient.LastName && p.ContactNumber == PobjPatient.ContactNumber) || p.PatientId == PobjPatient.PatientID)
                          select new Patientinfo
                          {
                              PatientID = p.PatientId,
                              Title = p.Title,
                              FirstName = p.FirstName,
                              LastName = p.LastName,
                              DOB = p.DOB,
                              Age = p.Age,
                              GenderName = p.Gender,
                              ContactNumber = p.ContactNumber,
                              Email = p.Email,
                              PatientType = p.PatientType
                          };
            Patientinfo = custmrs.ToList<Patientinfo>();
            return Patientinfo;
        }
        

        #region Customer
        //public CustomerRepository PostCustomer(CustomerRepository PobjCustomer)
        //{
        //    tblCustomer LobjCustomer = new tblCustomer();
        //    ObjectCopier.CopyObjectData(PobjCustomer, LobjCustomer);
        //    LobjCustomer.IsActive = true;
        //    LobjCustomer.CreatedDate = DateTime.Now;
        //    LobjCustomer.CreatedBy = 1;
        //    _db.tblCustomers.Add(LobjCustomer);
        //    int res = _db.SaveChanges();
        //    ObjectCopier.CopyObjectData(LobjCustomer, PobjCustomer);
        //    return PobjCustomer;
        //}
        //public List<CustomerRepository> GetCustomerBySearch(string PstrSearchColumnName, string PstrSearchTerm)
        //{
        //    List<CustomerRepository> Llstcustmrs = new List<CustomerRepository>();
        //    var custmrs = from cus in _db.tblCustomers
        //                  join lp in _db.tblLookups on cus.LkGender equals lp.LookupId into Customers
        //                  from lp in Customers.DefaultIfEmpty()
        //                  select new CustomerRepository
        //                  {
        //                      CustomerID = cus.CustomerID,
        //                      PatientID = cus.PatientID,
        //                      FirstName = cus.FirstName,
        //                      LastName = cus.LastName,
        //                      MobileNumber = cus.MobileNumber,
        //                      Age = cus.Age,
        //                      GenderName = cus.LkGender == null ? "No Gender" : lp.LookupName,
        //                      IsPatient = cus.IsPatient
        //                  };
        //    var res = custmrs;
        //    if (PstrSearchColumnName == "FirstName")
        //    {
        //        res = custmrs.Where(cus => cus.FirstName == PstrSearchTerm);
        //    }
        //    else if (PstrSearchColumnName == "LastNmae")
        //    {
        //        res = custmrs.Where(cus => cus.LastName == PstrSearchTerm);
        //    }
        //    else if (PstrSearchColumnName == "MobileNumber")
        //    {
        //        res = custmrs.Where(cus => cus.MobileNumber == PstrSearchTerm);
        //    }
        //    else if (PstrSearchColumnName == "Patient_Id")
        //    {
        //        res = custmrs.Where(cus => cus.PatientID == Convert.ToInt32(PstrSearchTerm));
        //    }
        //    Llstcustmrs = res.ToList<CustomerRepository>();
        //    return Llstcustmrs;
        //}
        //public List<CustomerRepository> GetCustomerByName(CustomerRepository PobjCustomer)
        //{

        //    List<CustomerRepository> Llstcustmrs = new List<CustomerRepository>();
        //    var custmrs = from cus in _db.tblCustomers
        //                  join lp in _db.tblLookups on cus.LkGender equals lp.LookupId into Customers
        //                  from lp in Customers.DefaultIfEmpty()
        //                  where ((cus.FirstName == PobjCustomer.FirstName && cus.LastName == PobjCustomer.LastName && cus.MobileNumber == PobjCustomer.MobileNumber) || cus.PatientID == PobjCustomer.PatientID)
        //                  select new CustomerRepository
        //                  {
        //                      CustomerID = cus.CustomerID,
        //                      PatientID = cus.PatientID,
        //                      FirstName = cus.FirstName,
        //                      LastName = cus.LastName,
        //                      MobileNumber = cus.MobileNumber,
        //                      Age = cus.Age,
        //                      GenderName = cus.LkGender == null ? "No Gender" : lp.LookupName,
        //                      IsPatient = cus.IsPatient

        //                  };
        //    Llstcustmrs = custmrs.ToList<CustomerRepository>();

        //    return Llstcustmrs;
        //}

        //public List<CustomerRepository> GetCustomerById(int id)
        //{
        //    List<CustomerRepository> Llstcustmrs = new List<CustomerRepository>();
        //    var custmrs = from cus in _db.tblCustomers
        //                  join lp in _db.tblLookups on cus.LkGender equals lp.LookupId into Customers
        //                  from lp in Customers.DefaultIfEmpty()
        //                  where cus.CustomerID == id
        //                  select new CustomerRepository
        //                  {
        //                      CustomerID = cus.CustomerID,
        //                      PatientID = cus.PatientID,
        //                      FirstName = cus.FirstName,
        //                      LastName = cus.LastName,
        //                      MobileNumber = cus.MobileNumber,
        //                      Age = cus.Age,
        //                      GenderName = cus.LkGender == null ? "No Gender" : lp.LookupName,
        //                      IsPatient = cus.IsPatient

        //                  };
        //    Llstcustmrs = custmrs.ToList<CustomerRepository>();

        //    return Llstcustmrs;
        //}
        #endregion

        #region Alerts
        //public List<ExpiredRepository> GetAllExpired()
        //{

        //    List<ExpiredRepository> Llstsupliers = new List<ExpiredRepository>();
        //    var suprs = from mi in _db.tblMedicinePurchaseInfoes
        //                join vm in _db.View_MedicineQuantity on new { ColA = mi.PurchaseID, ColB = mi.ProductID, mi.BatchNo } equals new { ColA = vm.PurchaseID, ColB = vm.ProductID, vm.BatchNo }
        //                join p in _db.tblProducts on mi.ProductID equals p.ProductID
        //                join Lp in _db.tblLookups on p.MfgID equals Lp.LookupId
        //                join mf in _db.tblManufacturers on p.MfgID equals mf.MfgID
        //                where
        //                vm.Available_Stock > 0 && mi.ExpiryDate >= DateTime.Now && DateTime.Now >= mi.ExpiryDate

        //                select new ExpiredRepository
        //                {
        //                    ExpiryDate = mi.ExpiryDate,
        //                    MfgName = mf.MfgName,
        //                    AgentName = mf.AgentName,
        //                    AgentContactNumber = mf.AgentContactNumber,
        //                    Name = Lp.LookupName,
        //                    RackLocation = p.RackLocation,
        //                    DaysToExpiry = mi.ExpiryDate.Day - DateTime.Now.Day
        //                };

        //    Llstsupliers = suprs.ToList<ExpiredRepository>();
        //    return Llstsupliers;
        //}

        //public List<ExpiredRepository> GetExpired(int days)
        //{

        //    List<ExpiredRepository> Llstsupliers = new List<ExpiredRepository>();
        //    var suprs = from mi in _db.tblMedicinePurchaseInfoes
        //                join vm in _db.View_MedicineQuantity on new { ColA = mi.PurchaseID, ColB = mi.ProductID, mi.BatchNo } equals new { ColA = vm.PurchaseID, ColB = vm.ProductID, vm.BatchNo }
        //                join p in _db.tblProducts on mi.ProductID equals p.ProductID
        //                join Lp in _db.tblLookups on p.MfgID equals Lp.LookupId
        //                join mf in _db.tblManufacturers on p.MfgID equals mf.MfgID
        //                where (mi.ExpiryDate.Day) < days
        //                select new ExpiredRepository
        //                {
        //                    ExpiryDate = mi.ExpiryDate,
        //                    MfgName = mf.MfgName,
        //                    AgentName = mf.AgentName,
        //                    AgentContactNumber = mf.AgentContactNumber,
        //                    Name = Lp.LookupName,
        //                    RackLocation = p.RackLocation,
        //                    DaysToExpiry = mi.ExpiryDate.Day - DateTime.Now.Day
        //                };

        //    Llstsupliers = suprs.ToList<ExpiredRepository>();
        //    return Llstsupliers;
        //}

        //public List<EndOfStockRepository> EndOfStock(int number)
        //{
        //    //var total = from T1 in context.T1
        //    //join T2 in context.T2 on T1.T2ID equals T2.T2ID
        //    //join T3 in context.T3 on T2.T3ID equals T3.T3ID
        //    //group T3 by new { T1.Column1, T1.Column2 } into g
        //    //select new {
        //    //    Column1 = T1.Column1,
        //    //    Column2 = T2.Column2,
        //    //    Amount = g.Sum(t3 => t3.Column1)
        //    //};


        //    List<EndOfStockRepository> Llstsupliers = new List<EndOfStockRepository>();
        //    var suprs = from mi in _db.tblMedicinePurchaseInfoes
        //                join vm in _db.View_MedicineQuantity on new { ColA = mi.PurchaseID, ColB = mi.ProductID, mi.BatchNo } equals new { ColA = vm.PurchaseID, ColB = vm.ProductID, vm.BatchNo }
        //                join p in _db.tblProducts on mi.ProductID equals p.ProductID
        //                join Lp in _db.tblLookups on p.FormID equals Lp.LookupId
        //                join mf in _db.tblManufacturers on p.MfgID equals mf.MfgID
        //                group vm.Available_Stock by new { p.ProductName, p.RackLocation, mf.AgentName, mf.AgentContactNumber } into grp
        //                where grp.Sum() < number
        //                select new EndOfStockRepository
        //                {
        //                    Quantity = grp.Sum() == null ? 0 : (int)grp.Sum(),
        //                    ProductName = grp.Key.ProductName,
        //                    AgentName = grp.Key.AgentName,
        //                };

        //    Llstsupliers = suprs.ToList<EndOfStockRepository>();
        //    return Llstsupliers;
        //}

        #endregion

        #region Reports

        public List<CollectionReportRepository> GetCollectionReportByMonth(DateTime PdtMonthCollectionReport)
        {

            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<CollectionReportRepository> LObjlstCollectionReport = new List<CollectionReportRepository>();
            var MonthCollectionReports = from mi in LobjHospitalTenantContext.tblMedicineInvoices
                                         join p in LobjHospitalTenantContext.tblPatients on mi.PatientID equals p.PatientId
                                         join mii in LobjHospitalTenantContext.tblMedicineInvoiceInfoes on mi.InvoiceID equals mii.InvoiceID
                                         join mp in LobjHospitalTenantContext.tblMedicinePurchases on mii.PurchaseID equals mp.PurchaseID
                                         join t in LobjHospitalTenantContext.tblTransactions on p.PatientId equals t.PatientID

                                         join ui in LobjHospitalTenantContext.tblUserInfoes on mii.CreatedBy equals ui.UID

                                         where mp.PurchaseDate.Month == PdtMonthCollectionReport.Month && mp.PurchaseDate.Year == PdtMonthCollectionReport.Year

                                         select new CollectionReportRepository
                                         {
                                             InvoiceId = mi.InvoiceID,
                                             PatientId = p.PatientId,
                                             PatientName = p.FirstName + " " + p.LastName,
                                             Amount = mi.TotalAmount,
                                             TransType = t.TransType,
                                             PaymentType = t.PaymentMode,
                                             UserName = ui.LoginID
                                         };
            LObjlstCollectionReport = MonthCollectionReports.ToList<CollectionReportRepository>();
            return LObjlstCollectionReport;
        }
        public List<CollectionReportRepository> GetCollectionReportByDate(DateTime PdtFromDate, DateTime PdtToDate)
        {

            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<CollectionReportRepository> LObjlstCollectionReport = new List<CollectionReportRepository>();
            var DateCollectionReports = from mi in LobjHospitalTenantContext.tblMedicineInvoices

                                        join p in LobjHospitalTenantContext.tblPatients on mi.PatientID equals p.PatientId
                                        join mii in LobjHospitalTenantContext.tblMedicineInvoiceInfoes on mi.InvoiceID equals mii.InvoiceID
                                        join mp in LobjHospitalTenantContext.tblMedicinePurchases on mii.PurchaseID equals mp.PurchaseID
                                        join t in LobjHospitalTenantContext.tblTransactions on p.PatientId equals t.PatientID

                                        join ui in LobjHospitalTenantContext.tblUserInfoes on mii.CreatedBy equals ui.UID

                                        where mp.PurchaseDate >= PdtFromDate && mp.PurchaseDate <= PdtToDate
                                        select new CollectionReportRepository
                                        {
                                            InvoiceId = mi.InvoiceID,
                                            PatientId = p.PatientId,
                                            PatientName = p.FirstName + " " + p.LastName,
                                            Amount = mi.TotalAmount,
                                            TransType = t.TransType,
                                            PaymentType = t.PaymentMode,
                                            UserName = ui.LoginID
                                        };
            LObjlstCollectionReport = DateCollectionReports.ToList<CollectionReportRepository>();
            return LObjlstCollectionReport;
        }

        public List<DailySalesReportRepository> GetDailySalesReport()
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<DailySalesReportRepository> LObjlstDailySalesReport = new List<DailySalesReportRepository>();
            var DailySalesReports = from p in LobjHospitalTenantContext.tblProducts
                                    join m in LobjHospitalTenantContext.tblManufacturers on p.MfgID equals m.MfgID

                                    join mpi in LobjHospitalTenantContext.tblMedicinePurchaseInfoes on p.ProductID equals mpi.ProductID
                                    join mii in LobjHospitalTenantContext.tblMedicineInvoiceInfoes on p.ProductID equals mii.ProductID
                                    join ui in LobjHospitalTenantContext.tblUserInfoes on mii.CreatedBy equals ui.UID
                                    select new DailySalesReportRepository
                                    {
                                        ProductName = p.ProductName,
                                        MfgName = m.MfgName,
                                        Type = p.FormName,
                                        BatchNo = mpi.BatchNo,
                                        Quantity = mpi.Quantity,
                                        PurchageAmount = mpi.PurchaseCostPerUnit * mpi.Quantity,
                                        SalesAmount = (mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value),
                                        Profit = ((mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value)) - (mpi.PurchaseCostPerUnit * mpi.Quantity),
                                        User = ui.LoginID
                                    };
            LObjlstDailySalesReport = DailySalesReports.ToList<DailySalesReportRepository>();
            return LObjlstDailySalesReport;
        }
        public List<SalesReportRepository> GetSalesReportByMonth(DateTime PdtMonthSalesReport)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<SalesReportRepository> LObjlstSalesReport = new List<SalesReportRepository>();
            var MonthSalesReports = from p in LobjHospitalTenantContext.tblProducts
                                    join m in LobjHospitalTenantContext.tblManufacturers on p.MfgID equals m.MfgID

                                    join mpi in LobjHospitalTenantContext.tblMedicinePurchaseInfoes on p.ProductID equals mpi.ProductID
                                    join mii in LobjHospitalTenantContext.tblMedicineInvoiceInfoes on p.ProductID equals mii.ProductID
                                    join ui in LobjHospitalTenantContext.tblUserInfoes on mii.CreatedBy equals ui.UID
                                    join mi in LobjHospitalTenantContext.tblMedicineInvoices on mii.InvoiceID equals mi.InvoiceID
                                    where mi.InvoiceDate.Month == PdtMonthSalesReport.Month && mi.InvoiceDate.Year == PdtMonthSalesReport.Year
                                    select new SalesReportRepository
                                    {
                                        ProductName = p.ProductName,
                                        MfgName = m.MfgName,
                                        Type = p.FormName,
                                        BatchNo = mpi.BatchNo,
                                        Quantity = mpi.Quantity,
                                        PurchageAmount = mpi.PurchaseCostPerUnit * mpi.Quantity,
                                        SalesAmount = (mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value),
                                        Profit = ((mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value)) - (mpi.PurchaseCostPerUnit * mpi.Quantity),
                                        User = ui.LoginID,
                                        InvoiceDate = mi.InvoiceDate
                                    };
            LObjlstSalesReport = MonthSalesReports.ToList<SalesReportRepository>();
            return LObjlstSalesReport;

        }
        public List<SalesReportRepository> GetSalesReportByDate(DateTime PdtFromDate, DateTime PdtToDate)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<SalesReportRepository> LObjlstSalesReport = new List<SalesReportRepository>();
            var DateSalesReports = from p in LobjHospitalTenantContext.tblProducts
                                   join m in LobjHospitalTenantContext.tblManufacturers on p.MfgID equals m.MfgID

                                   join mpi in LobjHospitalTenantContext.tblMedicinePurchaseInfoes on p.ProductID equals mpi.ProductID
                                   join mii in LobjHospitalTenantContext.tblMedicineInvoiceInfoes on p.ProductID equals mii.ProductID
                                   join ui in LobjHospitalTenantContext.tblUserInfoes on mii.CreatedBy equals ui.UID
                                   join mi in LobjHospitalTenantContext.tblMedicineInvoices on mii.InvoiceID equals mi.InvoiceID
                                   where mi.InvoiceDate >= PdtFromDate && mi.InvoiceDate <= PdtToDate
                                   select new SalesReportRepository
                                   {
                                       ProductName = p.ProductName,
                                       MfgName = m.MfgName,
                                       Type = p.FormName,
                                       BatchNo = mpi.BatchNo,
                                       Quantity = mpi.Quantity,
                                       PurchageAmount = mpi.PurchaseCostPerUnit * mpi.Quantity,
                                       SalesAmount = (mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value),
                                       Profit = ((mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value)) - (mpi.PurchaseCostPerUnit * mpi.Quantity),
                                       User = ui.LoginID,
                                       InvoiceDate = mi.InvoiceDate
                                   };
            LObjlstSalesReport = DateSalesReports.ToList<SalesReportRepository>();
            return LObjlstSalesReport;
        }
        public List<SalesReturnReportRepository> GetSalesReturnReportByMonth(DateTime PdtMonthSalesReturnReport)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<SalesReturnReportRepository> LObjlstSalesReturnReport = new List<SalesReturnReportRepository>();
            var MonthSalesReturnReports = from msri in LobjHospitalTenantContext.tblMedicineSalesReturnInfoes
                                          join p in LobjHospitalTenantContext.tblProducts on msri.ProductID equals p.ProductID
                                          join m in LobjHospitalTenantContext.tblManufacturers on p.MfgID equals m.MfgID

                                          join msr in LobjHospitalTenantContext.tblMedicineSalesReturns on msri.SalesReturnID equals msr.SalesReturnID
                                          join pur in LobjHospitalTenantContext.tblMedicinePurchases on msri.PurchaseID equals pur.PurchaseID
                                          join s in LobjHospitalTenantContext.tblSuppliers on pur.SupplierID equals s.SupplierID

                                          where msr.ReturnDate.Month == PdtMonthSalesReturnReport.Month && msr.ReturnDate.Year == PdtMonthSalesReturnReport.Year
                                          select new SalesReturnReportRepository
                                          {
                                              ProductName = p.ProductName,
                                              MfgName = m.MfgName,
                                              Type = p.FormName,
                                              BatchNo = msri.BatchNo,
                                              OrderCode = s.SupplierID,
                                              Quantity = msri.Quantity,
                                              ReturnDate = msr.ReturnDate
                                          };
            LObjlstSalesReturnReport = MonthSalesReturnReports.ToList<SalesReturnReportRepository>();
            return LObjlstSalesReturnReport;

        }

        public List<SalesReturnReportRepository> GetSalesReturnReportByDate(DateTime PdtFromDate, DateTime PdtToDate)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<SalesReturnReportRepository> LObjlstSalesReturnReport = new List<SalesReturnReportRepository>();
            var DateSalesReturnReports = from msri in LobjHospitalTenantContext.tblMedicineSalesReturnInfoes
                                         join p in LobjHospitalTenantContext.tblProducts on msri.ProductID equals p.ProductID
                                         join m in LobjHospitalTenantContext.tblManufacturers on p.MfgID equals m.MfgID

                                         join msr in LobjHospitalTenantContext.tblMedicineSalesReturns on msri.SalesReturnID equals msr.SalesReturnID
                                         join pur in LobjHospitalTenantContext.tblMedicinePurchases on msri.PurchaseID equals pur.PurchaseID
                                         join s in LobjHospitalTenantContext.tblSuppliers on pur.SupplierID equals s.SupplierID

                                         where msr.ReturnDate >= PdtFromDate && msr.ReturnDate <= PdtToDate
                                         select new SalesReturnReportRepository
                                         {
                                             ProductName = p.ProductName,
                                             MfgName = m.MfgName,
                                             Type = p.FormName,
                                             BatchNo = msri.BatchNo,
                                             OrderCode = s.SupplierID,
                                             Quantity = msri.Quantity,
                                             ReturnDate = msr.ReturnDate
                                         };
            LObjlstSalesReturnReport = DateSalesReturnReports.ToList<SalesReturnReportRepository>();
            return LObjlstSalesReturnReport;
        }

        public List<StockReturnReportRepository> GetStockReturnReportByMonth(DateTime PdtMonthStockReturnReport)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<StockReturnReportRepository> LObjlstStockReturnReports = new List<StockReturnReportRepository>();
            var MonthStockReturnReports = from msri in LobjHospitalTenantContext.tblMedicineStockReturnInfoes
                                          join p in LobjHospitalTenantContext.tblProducts on msri.ProductID equals p.ProductID
                                          join m in LobjHospitalTenantContext.tblManufacturers on p.MfgID equals m.MfgID

                                          join mp in LobjHospitalTenantContext.tblMedicinePurchases on msri.PurchaseID equals mp.PurchaseID
                                          join s in LobjHospitalTenantContext.tblSuppliers on mp.SupplierID equals s.SupplierID
                                          join mpi in LobjHospitalTenantContext.tblMedicinePurchaseInfoes on p.ProductID equals mpi.ProductID
                                          join msr in LobjHospitalTenantContext.tblMedicineStockReturns on msri.StockReturnID equals msr.StockReturnID
                                          join vw in LobjHospitalTenantContext.View_MedicineQuantity on
                                          new { a = mpi.PurchaseID, b = mpi.ProductID, c = mpi.BatchNo }
                                          equals new { a = vw.PurchaseID, b = vw.ProductID, c = vw.BatchNo }

                                          where msr.ReturnDate.Month == PdtMonthStockReturnReport.Month && msr.ReturnDate.Year == PdtMonthStockReturnReport.Year

                                          select new StockReturnReportRepository
                                          {
                                              ProductName = p.ProductName,
                                              MfgName = m.MfgName,
                                              AgentName = m.AgentName,
                                              AgentContactNumber = m.AgentContactNumber,
                                              Type = p.FormName,
                                              OrderCode = s.SupplierID,
                                              PurchaseDate = mp.PurchaseDate,
                                              ExpiryDate = mpi.ExpiryDate,
                                              PurchaseQuantity = mpi.Quantity + mpi.FreeQuantity,
                                              ReturnQuantity = msri.Quantity,
                                              AvailableQuantity = vw.Available_Stock,
                                              ReturnDate = msr.ReturnDate,
                                              Amount = msri.Amount
                                          };
            LObjlstStockReturnReports = MonthStockReturnReports.ToList<StockReturnReportRepository>();
            return LObjlstStockReturnReports;
        }
        public List<StockReturnReportRepository> GetStockReturnReportByDate(DateTime PdtFromDate, DateTime PdtToDate)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<StockReturnReportRepository> LObjlstStockReturnReports = new List<StockReturnReportRepository>();
            var DateStockReturnReports = from msri in LobjHospitalTenantContext.tblMedicineStockReturnInfoes
                                         join p in LobjHospitalTenantContext.tblProducts on msri.ProductID equals p.ProductID
                                         join m in LobjHospitalTenantContext.tblManufacturers on p.MfgID equals m.MfgID

                                         join mp in LobjHospitalTenantContext.tblMedicinePurchases on msri.PurchaseID equals mp.PurchaseID
                                         join s in LobjHospitalTenantContext.tblSuppliers on mp.SupplierID equals s.SupplierID
                                         join mpi in LobjHospitalTenantContext.tblMedicinePurchaseInfoes on p.ProductID equals mpi.ProductID
                                         join msr in LobjHospitalTenantContext.tblMedicineStockReturns on msri.StockReturnID equals msr.StockReturnID
                                         join vw in LobjHospitalTenantContext.View_MedicineQuantity on
                                         new { a = mpi.PurchaseID, b = mpi.ProductID, c = mpi.BatchNo }
                                         equals new { a = vw.PurchaseID, b = vw.ProductID, c = vw.BatchNo }

                                         where msr.ReturnDate >= PdtFromDate && msr.ReturnDate <= PdtToDate

                                         select new StockReturnReportRepository
                                         {
                                             ProductName = p.ProductName,
                                             MfgName = m.MfgName,
                                             AgentName = m.AgentName,
                                             AgentContactNumber = m.AgentContactNumber,
                                             Type = p.FormName,
                                             OrderCode = s.SupplierID,
                                             PurchaseDate = mp.PurchaseDate,
                                             ExpiryDate = mpi.ExpiryDate,
                                             PurchaseQuantity = mpi.Quantity + mpi.FreeQuantity,
                                             ReturnQuantity = msri.Quantity,
                                             AvailableQuantity = vw.Available_Stock,
                                             ReturnDate = msr.ReturnDate,
                                             Amount = msri.Amount
                                         };
            LObjlstStockReturnReports = DateStockReturnReports.ToList<StockReturnReportRepository>();
            return LObjlstStockReturnReports;
        }

        public List<PatientInvoiceReportRepository> GetPatientInvoiceReportByDate(DateTime PdtFromDate, DateTime PdtToDate)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<PatientInvoiceReportRepository> LObjlstPatientInvoiceReports = new List<PatientInvoiceReportRepository>();
            var DatePatientInvoiceReports = from mi in LobjHospitalTenantContext.tblMedicineInvoices

                                            join p in LobjHospitalTenantContext.tblPatients on mi.PatientID equals p.PatientId
                                            join mii in LobjHospitalTenantContext.tblMedicineInvoiceInfoes on mi.InvoiceID equals mii.InvoiceID
                                            join mp in LobjHospitalTenantContext.tblMedicinePurchases on mii.PurchaseID equals mp.PurchaseID

                                            where mp.PurchaseDate >= PdtFromDate && mp.PurchaseDate <= PdtToDate

                                            select new PatientInvoiceReportRepository
                                            {
                                                InvoiceId = mi.InvoiceID,
                                                PatientName = p.FirstName + " " + p.LastName,
                                                Age = p.Age,
                                                MobileNumber = p.ContactNumber,
                                                PurchaseDate = mp.PurchaseDate,
                                                TotalAmount = mi.TotalAmount

                                            };
            LObjlstPatientInvoiceReports = DatePatientInvoiceReports.ToList<PatientInvoiceReportRepository>();
            return LObjlstPatientInvoiceReports;
        }
        public List<PatientInvoiceReportRepository> GetPatientInvoiceReportByPatientName(string PstrPatientName)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<PatientInvoiceReportRepository> LObjlstPatientInvoiceReports = new List<PatientInvoiceReportRepository>();
            var NamePatientInvoiceReports = from mi in LobjHospitalTenantContext.tblMedicineInvoices

                                            join p in LobjHospitalTenantContext.tblPatients on mi.PatientID equals p.PatientId
                                            join mii in LobjHospitalTenantContext.tblMedicineInvoiceInfoes on mi.InvoiceID equals mii.InvoiceID
                                            join mp in LobjHospitalTenantContext.tblMedicinePurchases on mii.PurchaseID equals mp.PurchaseID

                                            where p.FirstName.StartsWith(PstrPatientName)

                                            select new PatientInvoiceReportRepository
                                            {
                                                InvoiceId = mi.InvoiceID,
                                                PatientName = p.FirstName + " " + p.LastName,
                                                Age = p.Age,
                                                MobileNumber = p.ContactNumber,
                                                PurchaseDate = mp.PurchaseDate,
                                                TotalAmount = mi.TotalAmount

                                            };
            LObjlstPatientInvoiceReports = NamePatientInvoiceReports.ToList<PatientInvoiceReportRepository>();
            return LObjlstPatientInvoiceReports;
        }
        public List<ConsultationReportRepository> GetConsultationReportByMonth(DateTime PdtMonthConsultationReport)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<ConsultationReportRepository> LObjlstConsultationReport = new List<ConsultationReportRepository>();
            var MonthConsultationReports = from mi in LobjHospitalTenantContext.tblMedicineInvoices
                                           join p in LobjHospitalTenantContext.tblPatients on mi.PatientID equals p.PatientId

                                           where mi.InvoiceDate.Month == PdtMonthConsultationReport.Month && mi.InvoiceDate.Year == PdtMonthConsultationReport.Year

                                           select new ConsultationReportRepository
                                           {
                                               InvoiceId = mi.InvoiceID,
                                               FirstName = p.FirstName,
                                               LastName = p.LastName,
                                               Age = p.Age,
                                               MobileNumber = p.ContactNumber,
                                               gender = p.Gender,
                                               ConsultationDate = mi.InvoiceDate,
                                               Consultation = mi.TotalAmount

                                           };
            LObjlstConsultationReport = MonthConsultationReports.ToList<ConsultationReportRepository>();
            return LObjlstConsultationReport;
        }
        public List<ConsultationReportRepository> GetConsultationReportByDate(DateTime PdtFromDate, DateTime PdtToDate)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<ConsultationReportRepository> LObjlstConsultationReport = new List<ConsultationReportRepository>();
            var DateConsultationReports = from mi in LobjHospitalTenantContext.tblMedicineInvoices

                                          join p in LobjHospitalTenantContext.tblPatients on mi.PatientID equals p.PatientId

                                          where mi.InvoiceDate >= PdtFromDate && mi.InvoiceDate <= PdtToDate


                                          select new ConsultationReportRepository
                                          {
                                              InvoiceId = mi.InvoiceID,
                                              FirstName = p.FirstName,
                                              LastName = p.LastName,
                                              Age = p.Age,
                                              MobileNumber = p.ContactNumber,
                                              gender = p.Gender,
                                              ConsultationDate = mi.InvoiceDate,
                                              Consultation = mi.TotalAmount

                                          };
            LObjlstConsultationReport = DateConsultationReports.ToList<ConsultationReportRepository>();
            return LObjlstConsultationReport;
        }
        public List<PatientSalesReportRepository> GetPatientSalesReportByDate(DateTime PdtFromDate, DateTime PdtToDate)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<PatientSalesReportRepository> LObjlstPatientSalesReport = new List<PatientSalesReportRepository>();
            var DatePatientSalesReports = from mi in LobjHospitalTenantContext.tblMedicineInvoices

                                          join p in LobjHospitalTenantContext.tblPatients on mi.PatientID equals p.PatientId
                                          join mii in LobjHospitalTenantContext.tblMedicineInvoiceInfoes on mi.InvoiceID equals mii.InvoiceID
                                          join mp in LobjHospitalTenantContext.tblMedicinePurchases on mii.PurchaseID equals mp.PurchaseID
                                          join pro in LobjHospitalTenantContext.tblProducts on mii.ProductID equals pro.ProductID
                                          join m in LobjHospitalTenantContext.tblManufacturers on pro.MfgID equals m.MfgID

                                          join mpi in LobjHospitalTenantContext.tblMedicinePurchaseInfoes on pro.ProductID equals mpi.ProductID
                                          join msri in LobjHospitalTenantContext.tblMedicineStockReturnInfoes on pro.ProductID equals msri.ProductID

                                          where mp.PurchaseDate >= PdtFromDate && mp.PurchaseDate <= PdtToDate
                                          select new PatientSalesReportRepository
                                          {
                                              InvoiceId = mi.InvoiceID,
                                              PatientName = p.FirstName + " " + p.LastName,
                                              PurchaseDate = mp.PurchaseDate,
                                              ProductName = pro.ProductName,
                                              MfgName = m.MfgName,
                                              Type = pro.FormName,
                                              BatchNo = msri.BatchNo,
                                              Quantity = mpi.Quantity,
                                              PurchaseAmount = mpi.PurchaseCostPerUnit * mpi.Quantity,
                                              SalesAmount = (mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value),
                                              Profit = ((mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value)) - (mpi.PurchaseCostPerUnit * mpi.Quantity)

                                          };
            LObjlstPatientSalesReport = DatePatientSalesReports.ToList<PatientSalesReportRepository>();
            return LObjlstPatientSalesReport;
        }

        public List<PatientSalesReportRepository> GetPatientSalesReportByMedicineName(string strMedicineName)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<PatientSalesReportRepository> LObjlstPatientSalesReport = new List<PatientSalesReportRepository>();
            var MedicineNamePatientSalesReports = from mi in LobjHospitalTenantContext.tblMedicineInvoices

                                                  join p in LobjHospitalTenantContext.tblPatients on mi.PatientID equals p.PatientId
                                                  join mii in LobjHospitalTenantContext.tblMedicineInvoiceInfoes on mi.InvoiceID equals mii.InvoiceID
                                                  join mp in LobjHospitalTenantContext.tblMedicinePurchases on mii.PurchaseID equals mp.PurchaseID
                                                  join pro in LobjHospitalTenantContext.tblProducts on mii.ProductID equals pro.ProductID
                                                  join m in LobjHospitalTenantContext.tblManufacturers on pro.MfgID equals m.MfgID

                                                  join mpi in LobjHospitalTenantContext.tblMedicinePurchaseInfoes on pro.ProductID equals mpi.ProductID
                                                  join msri in LobjHospitalTenantContext.tblMedicineStockReturnInfoes on pro.ProductID equals msri.ProductID

                                                  where pro.ProductName.StartsWith(strMedicineName)
                                                  select new PatientSalesReportRepository
                                                  {
                                                      InvoiceId = mi.InvoiceID,
                                                      PatientName = p.FirstName + " " + p.LastName,
                                                      PurchaseDate = mp.PurchaseDate,
                                                      ProductName = pro.ProductName,
                                                      MfgName = m.MfgName,
                                                      Type = pro.FormName,
                                                      BatchNo = msri.BatchNo,
                                                      Quantity = mpi.Quantity,
                                                      PurchaseAmount = mpi.PurchaseCostPerUnit * mpi.Quantity,
                                                      SalesAmount = (mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value),
                                                      Profit = ((mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value)) - (mpi.PurchaseCostPerUnit * mpi.Quantity)
                                                  };
            LObjlstPatientSalesReport = MedicineNamePatientSalesReports.ToList<PatientSalesReportRepository>();
            return LObjlstPatientSalesReport;
        }
        public List<PatientSalesReportRepository> GetPatientSalesReportByPatientName(string strPatientName)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<PatientSalesReportRepository> LObjlstPatientSalesReport = new List<PatientSalesReportRepository>();
            var PatientNamePatientSalesReports = from mi in LobjHospitalTenantContext.tblMedicineInvoices

                                                 join p in LobjHospitalTenantContext.tblPatients on mi.PatientID equals p.PatientId
                                                 join mii in LobjHospitalTenantContext.tblMedicineInvoiceInfoes on mi.InvoiceID equals mii.InvoiceID
                                                 join mp in LobjHospitalTenantContext.tblMedicinePurchases on mii.PurchaseID equals mp.PurchaseID
                                                 join pro in LobjHospitalTenantContext.tblProducts on mii.ProductID equals pro.ProductID
                                                 join m in LobjHospitalTenantContext.tblManufacturers on pro.MfgID equals m.MfgID

                                                 join mpi in LobjHospitalTenantContext.tblMedicinePurchaseInfoes on pro.ProductID equals mpi.ProductID
                                                 join msri in LobjHospitalTenantContext.tblMedicineStockReturnInfoes on pro.ProductID equals msri.ProductID

                                                 where (p.FirstName + " " + p.LastName).StartsWith(strPatientName)
                                                 select new PatientSalesReportRepository
                                                 {
                                                     InvoiceId = mi.InvoiceID,
                                                     PatientName = p.FirstName + " " + p.LastName,
                                                     PurchaseDate = mp.PurchaseDate,
                                                     ProductName = pro.ProductName,
                                                     MfgName = m.MfgName,
                                                     Type = pro.FormName,
                                                     BatchNo = msri.BatchNo,
                                                     Quantity = mpi.Quantity,
                                                     PurchaseAmount = mpi.PurchaseCostPerUnit * mpi.Quantity,
                                                     SalesAmount = (mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value),
                                                     Profit = ((mpi.SellingCostPerUnit * mpi.Quantity) - (mii.Discount.Value == null ? 0 : mii.Discount.Value)) - (mpi.PurchaseCostPerUnit * mpi.Quantity)
                                                 };
            LObjlstPatientSalesReport = PatientNamePatientSalesReports.ToList<PatientSalesReportRepository>();
            return LObjlstPatientSalesReport;
        }
        public List<MedicineSalesandStockReportRepository> GetMedicineSalesandStockReportByName(string PstrMedicineName)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<MedicineSalesandStockReportRepository> LObjlstMedicineSalesandStockReport = new List<MedicineSalesandStockReportRepository>();
            var MedicineSalesandStockReport = from p in LobjHospitalTenantContext.tblProducts
                                              join m in LobjHospitalTenantContext.tblManufacturers on p.MfgID equals m.MfgID

                                              join msri in LobjHospitalTenantContext.tblMedicineStockReturnInfoes on p.ProductID equals msri.ProductID
                                              join msr in LobjHospitalTenantContext.tblMedicineStockReturns on msri.StockReturnID equals msr.StockReturnID
                                              join mpi in LobjHospitalTenantContext.tblMedicinePurchaseInfoes on p.ProductID equals mpi.ProductID
                                              join mii in LobjHospitalTenantContext.tblMedicineInvoiceInfoes on p.ProductID equals mii.ProductID
                                              join sri in LobjHospitalTenantContext.tblMedicineSalesReturnInfoes on p.ProductID equals sri.ProductID
                                              join stri in LobjHospitalTenantContext.tblMedicineStockReturnInfoes on p.ProductID equals stri.ProductID

                                              where p.ProductName == PstrMedicineName
                                              select new MedicineSalesandStockReportRepository
                                              {
                                                  MedicineName = p.ProductName,
                                                  CompanyName = m.MfgName,
                                                  Type = p.FormName,
                                                  Date = msr.ReturnDate,
                                                  Received_Quantity = mpi.Quantity,
                                                  Selled_Quantity = mii.Quantity,
                                                  SalesReturn_Quantity = sri.Quantity,
                                                  StockReturn_Quantity = stri.Quantity,
                                              };
            LObjlstMedicineSalesandStockReport = MedicineSalesandStockReport.ToList<MedicineSalesandStockReportRepository>();
            return LObjlstMedicineSalesandStockReport;
        }
        public List<MedicineStockReportRepository> GetMedicineStockReport(string PstrMedicineName, int? PintQuantityGreater, int? PintQuantityLessthan)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();

            List<MedicineStockReportRepository> LObjlstMedicineStockReport = new List<MedicineStockReportRepository>();
            var MedicineStockReports = from p in LobjHospitalTenantContext.tblProducts
                                       join m in LobjHospitalTenantContext.tblManufacturers on p.MfgID equals m.MfgID

                                       join msri in LobjHospitalTenantContext.tblMedicineStockReturnInfoes on p.ProductID equals msri.ProductID
                                       join msr in LobjHospitalTenantContext.tblMedicineStockReturns on msri.StockReturnID equals msr.StockReturnID
                                       select new MedicineStockReportRepository
                                       {
                                           MedicineName = p.ProductName,
                                           CompanyName = m.MfgName,
                                           Type = p.FormName,
                                           AvailableQuantity = msri.Quantity,
                                           StockAmount = msr.TotalAmount
                                       };
            var result = "";

            if (!(PstrMedicineName.Equals("null") || string.IsNullOrEmpty(PstrMedicineName)))
            {
                result += MedicineStockReports.Where(p => p.MedicineName.StartsWith(PstrMedicineName));
            }
            if (PintQuantityGreater != null)
            {
                result += MedicineStockReports.Where(msri => msri.AvailableQuantity >= PintQuantityGreater);
            }
            if (PintQuantityLessthan != null)
            {
                result += MedicineStockReports.Where(msr => msr.StockAmount <= PintQuantityLessthan);
            }
            LObjlstMedicineStockReport = MedicineStockReports.ToList<MedicineStockReportRepository>();
            return LObjlstMedicineStockReport;
        }
    }
        #endregion
}
