﻿using HMIS.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Domain.Entities;


namespace HMISAPI.Infrastructure.Data
{
    public class Hospitals : IHospitals
    {
        HospitalTenantContext _db = new HospitalTenantContext();


        #region HospitalTower

        public List<HospitalTowerRepository> GetTowers(int hospitalid)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<HospitalTowerRepository> LobjHospitalTower = new List<HospitalTowerRepository>();
            var query = from hosptower in _db.tblHospitalTowers
                        join hosp in _db.tblHospitals on hosptower.Hospitalid equals hosp.HospitalId
                        where hosptower.Hospitalid == hospitalid && hosptower.isActive == true
                        select new HospitalTowerRepository
                        {
                            Hospitalid = hosptower.Hospitalid,
                            TowerId = hosptower.TowerId,
                            Type = hosptower.Type,
                            Name = hosptower.Name,
                            Comments = hosptower.Comments
                        };
            LobjHospitalTower = query.ToList<HospitalTowerRepository>();
            return LobjHospitalTower;
        }
        public HospitalTowerRepository AddTowers(HospitalTowerRepository PobjTower)
        {
            tblHospitalTower LobjtblTower = new tblHospitalTower();
            ObjectCopier.CopyObjectData(PobjTower, LobjtblTower);
            LobjtblTower.CreatedDate = DateTime.Now;
            LobjtblTower.isActive = true;
            _db.tblHospitalTowers.Add(LobjtblTower);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblTower, PobjTower);
            return PobjTower;

        }
        public bool UpdateTowers(int id, HospitalTowerRepository PobjTower)
        {
            try
            {
                tblHospitalTower LobjTower = new tblHospitalTower();
                LobjTower = _db.tblHospitalTowers.FirstOrDefault(x => x.TowerId == id);
                if (LobjTower != null)
                {
                    LobjTower.Name = PobjTower.Name;
                    LobjTower.Type = PobjTower.Type;
                    LobjTower.Hospitalid = PobjTower.Hospitalid;
                    LobjTower.ModifiedDate = DateTime.Now;
                    LobjTower.ModifiedBy = PobjTower.ModifiedBy;
                    LobjTower.Comments = PobjTower.Comments;

                }
                var query = _db.SaveChanges();
                if (query == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool DeleteTowers(int Id)
        {
            try
            {
                tblHospitalTower LobjTower = new tblHospitalTower();
                LobjTower = _db.tblHospitalTowers.FirstOrDefault(x => x.TowerId == Id);
                if (LobjTower != null)
                {
                    LobjTower.isActive = false;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Floor

        public List<FloorRepository> GetFloors(int Towerid)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<FloorRepository> LobjFloor = new List<FloorRepository>();
            var query = from Floor in _db.tblFloors
                        join hosptowe in _db.tblHospitalTowers on Floor.TowerID equals hosptowe.TowerId
                        where Floor.TowerID == Towerid && Floor.IsActive == true
                        select new FloorRepository
                        {
                            TowerID = Floor.TowerID,
                            FloorType = Floor.FloorType,
                            
                        };
            LobjFloor = query.ToList<FloorRepository>();
            return LobjFloor;
        }

        public FloorRepository AddFloor(FloorRepository PobjFloor)
        {
            tblFloor LobjtblFloor = new tblFloor();
            ObjectCopier.CopyObjectData(PobjFloor, LobjtblFloor);
            LobjtblFloor.CreatedDate = DateTime.Now;
            LobjtblFloor.IsActive = true;
            _db.tblFloors.Add(LobjtblFloor);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblFloor, PobjFloor);
            return PobjFloor;
        }

        public bool UpdateFloor(int id, FloorRepository PobjFloor)
        {
            try
            {
                tblFloor LobjFloor = new tblFloor();
                LobjFloor = _db.tblFloors.FirstOrDefault(x => x.FloorId == id);
                if (LobjFloor != null)
                {

                    LobjFloor.FloorType = PobjFloor.FloorType;
                    LobjFloor.TowerID = PobjFloor.TowerID;
                    LobjFloor.ModifiedDate = DateTime.Now;
                    LobjFloor.ModifiedBy = PobjFloor.ModifiedBy;


                }
                var query = _db.SaveChanges();
                if (query == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteFloor(int Id)
        {
            try
            {
                tblFloor Lobjtblfloor = new tblFloor();
                Lobjtblfloor = _db.tblFloors.FirstOrDefault(x => x.FloorId == Id);
                if (Lobjtblfloor != null)
                {
                    Lobjtblfloor.IsActive = false;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Room

        public List<RoomRepository> GetRooms(int Towerid)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<RoomRepository> LobjRoom = new List<RoomRepository>();
            var query = from Room in _db.tblRooms
                        join HospTower in _db.tblHospitalTowers on Room.TowerId equals HospTower.TowerId
                        where Room.TowerId == Towerid && Room.IsActive == true
                        select new RoomRepository
                        {
                            TowerId = Room.TowerId,
                            RoomTypeId = Room.RoomTypeId,
                            RoomName = Room.RoomName,
                            Cost = Room.Cost,
                            HospitalBranchId = Room.HospitalBranchId,
                            Comments = Room.Comments,
                            RoomId=Room.RoomId
                            

                        };
            LobjRoom = query.ToList<RoomRepository>();
            return LobjRoom;
        }

        public RoomRepository AddRoom(RoomRepository PobjRoom)
        {
            tblRoom Lobjtblroom = new tblRoom();
            ObjectCopier.CopyObjectData(PobjRoom, Lobjtblroom);
            Lobjtblroom.CreatedDate = DateTime.Now;
            Lobjtblroom.IsActive = true;
            _db.tblRooms.Add(Lobjtblroom);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(Lobjtblroom, PobjRoom);
            return PobjRoom;
        }

        public bool UpdateRoom(int id, RoomRepository PobjRoom)
        {
            try
            {
                tblRoom Lobjtblroom = new tblRoom();
                Lobjtblroom = _db.tblRooms.FirstOrDefault(x => x.RoomId == id);
                if (Lobjtblroom != null)
                {

                    Lobjtblroom.RoomTypeId = PobjRoom.RoomTypeId;
                    Lobjtblroom.TowerId = PobjRoom.TowerId;
                    Lobjtblroom.RoomName = PobjRoom.RoomName;
                    Lobjtblroom.Cost = PobjRoom.Cost;
                    Lobjtblroom.HospitalBranchId = PobjRoom.HospitalBranchId;
                    Lobjtblroom.Comments = PobjRoom.Comments;
                    Lobjtblroom.ModifiedDate = DateTime.Now;
                    Lobjtblroom.ModifiedBy = PobjRoom.ModifiedBy;


                }
                var query = _db.SaveChanges();
                if (query == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteRoom(int Id)
        {
            try
            {
                tblRoom Lobjtblroom = new tblRoom();
                Lobjtblroom = _db.tblRooms.FirstOrDefault(x => x.RoomId == Id);
                if (Lobjtblroom != null)
                {
                    Lobjtblroom.IsActive = false;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Bed

        public List<BedRepository> GetBeds(int roomid)
        {
            List<BedRepository> LobjBed = new List<BedRepository>();
            var query = from Bed in _db.tblBeds
                        join Room in _db.tblRooms on Bed.RoomId equals Room.RoomId
                        where Bed.RoomId == roomid && Bed.IsActive == true
                        select new BedRepository
                        {
                            BedId = Bed.BedId,
                            BedNo = Bed.BedNo,
                            BedType = Bed.BedType,
                            WardClass = Bed.WardClass,
                            BedBooKCat = Bed.BedBooKCat,
                            WardType = Bed.WardType,
                            FloorId = Bed.FloorId,
                            BedName = Bed.BedName,
                            RoomId = Bed.RoomId,
                            BedStatus = Bed.BedStatus,
                            Comments = Bed.Comments

                        };
            LobjBed = query.ToList<BedRepository>();
            return LobjBed;
        }

        public BedRepository AddBed(BedRepository PobjBed)
        {
            tblBed LobjtblBed = new tblBed();
            ObjectCopier.CopyObjectData(PobjBed, LobjtblBed);
            LobjtblBed.CreatedDate = DateTime.Now;
            LobjtblBed.IsActive = true;
            _db.tblBeds.Add(LobjtblBed);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblBed, PobjBed);
            return PobjBed;
        }

        public bool UpdateBed(int id, BedRepository PobjBed)
        {
            try
            {
                tblBed LobjBed = new tblBed();
                LobjBed = _db.tblBeds.FirstOrDefault(x => x.BedId == id);
                if (LobjBed != null)
                {

                    LobjBed.BedNo = PobjBed.BedNo;
                    LobjBed.BedType = PobjBed.BedType;
                    LobjBed.WardClass = PobjBed.WardClass;
                    LobjBed.BedBooKCat = PobjBed.BedBooKCat;
                    LobjBed.WardType = PobjBed.WardType;
                    LobjBed.FloorId = PobjBed.FloorId;
                    LobjBed.BedName = PobjBed.BedName;
                    LobjBed.RoomId = PobjBed.RoomId;
                    LobjBed.BedStatus = PobjBed.BedStatus;
                    LobjBed.IsActive = PobjBed.IsActive;
                    LobjBed.ModifiedBy = PobjBed.CreatedBy;
                    LobjBed.ModifiedDate = DateTime.Now;
                    LobjBed.Comments = PobjBed.Comments;


                }
                var query = _db.SaveChanges();
                if (query == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteBed(int Id)
        {
            try
            {
                tblBed LobjtblBed = new tblBed();
                LobjtblBed = _db.tblBeds.FirstOrDefault(x => x.BedId == Id);
                if (LobjtblBed != null)
                {
                    LobjtblBed.IsActive = false;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Hospital

        public List<HospitalRepository> GetHospital()
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            List<HospitalRepository> LobjHospital = new List<HospitalRepository>();
            var query = from hospital in _db.tblHospitals
                        join address in _db.tblAddresses on hospital.AddressID equals address.AddressID
                        where hospital.IsActive == true
                        select new HospitalRepository
                        {
                            HospitalId = hospital.HospitalId,
                            HospitalName = hospital.HospitalName,
                            ParentHospitalId = hospital.ParentHospitalId,
                            HospitalCode = hospital.HospitalCode,
                            Address = new AddressRepository
                            {
                                AddressID = address.AddressID,
                                AddressLine1 = address.AddressLine1,
                                AddressLine2 = address.AddressLine2,
                                City = address.City,
                                State = address.State,
                                Country = address.Country,
                                ZipCode = address.ZipCode,
                            },
                            PrimaryContactNo = hospital.PrimaryContactNo,
                            SecodaryContactNo = hospital.SecodaryContactNo,
                            OtherContactNo = hospital.OtherContactNo,
                            PriContactPersonName = hospital.PriContactPersonName,
                            PriMobileNumber = hospital.PriMobileNumber,
                            RegistrationNo = hospital.RegistrationNo,
                            TIN = hospital.TIN,
                            VAT = hospital.VAT,
                            PAN = hospital.PAN
                        };
            LobjHospital = query.ToList<HospitalRepository>();
            return LobjHospital;
        }
        public HospitalRepository GetHospital(int id)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            HospitalRepository LobjHospital = new HospitalRepository();
            LobjHospital = (from hospital in _db.tblHospitals
                            join address in _db.tblAddresses on hospital.AddressID equals address.AddressID
                            where hospital.IsActive == true && hospital.HospitalId == id
                            select new HospitalRepository
                            {
                                HospitalId = hospital.HospitalId,
                                HospitalName = hospital.HospitalName,
                                ParentHospitalId = hospital.ParentHospitalId,
                                HospitalCode = hospital.HospitalCode,
                                Address = new AddressRepository
                                {
                                    AddressID = address.AddressID,
                                    AddressLine1 = address.AddressLine1,
                                    AddressLine2 = address.AddressLine2,
                                    City = address.City,
                                    State = address.State,
                                    Country = address.Country,
                                    ZipCode = address.ZipCode,
                                },
                                PrimaryContactNo = hospital.PrimaryContactNo,
                                SecodaryContactNo = hospital.SecodaryContactNo,
                                OtherContactNo = hospital.OtherContactNo,
                                PriContactPersonName = hospital.PriContactPersonName,
                                PriMobileNumber = hospital.PriMobileNumber,
                                RegistrationNo = hospital.RegistrationNo,
                                TIN = hospital.TIN,
                                VAT = hospital.VAT,
                                PAN = hospital.PAN
                            }).FirstOrDefault();
            return LobjHospital;
        }
        public HospitalRepository PostHospital(HospitalRepository PobjHospital)
        {
            tblHospital LobjHospital = new tblHospital();
            tblAddress LobjAddress = new tblAddress();
            ObjectCopier.CopyObjectData(PobjHospital.Address, LobjAddress);
            ObjectCopier.CopyObjectData(PobjHospital, LobjHospital);
            LobjHospital.CreatedDate = DateTime.Now;
            LobjHospital.IsActive = true;
            LobjAddress.CreatedDate = DateTime.Now;
            LobjAddress.IsActive = true;

            _db.tblHospitals.Add(LobjHospital);
            _db.tblAddresses.Add(LobjAddress);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjAddress, PobjHospital.Address);
            ObjectCopier.CopyObjectData(LobjHospital, PobjHospital);
            return PobjHospital;
        }
        public bool PutHospital(int id, HospitalRepository PobjHospital)
        {
            try
            {
                tblHospital LobjtblHospital = new tblHospital();
                LobjtblHospital = _db.tblHospitals.FirstOrDefault(x => x.HospitalId == id);
                if (LobjtblHospital != null)
                {
                    LobjtblHospital.HospitalName = PobjHospital.HospitalName;
                    LobjtblHospital.HospitalCode = PobjHospital.HospitalCode;
                    LobjtblHospital.PrimaryContactNo = PobjHospital.PrimaryContactNo;
                    LobjtblHospital.SecodaryContactNo = PobjHospital.SecodaryContactNo;
                    LobjtblHospital.OtherContactNo = PobjHospital.OtherContactNo;
                    LobjtblHospital.PriContactPersonName = PobjHospital.PriContactPersonName;
                    LobjtblHospital.PriMobileNumber = PobjHospital.PriMobileNumber;
                    LobjtblHospital.RegistrationNo = PobjHospital.RegistrationNo;
                    LobjtblHospital.TIN = PobjHospital.TIN;
                    LobjtblHospital.VAT = PobjHospital.VAT;
                    LobjtblHospital.PAN = PobjHospital.PAN;
                    LobjtblHospital.ModifiedBy = PobjHospital.ModifiedBy;
                    LobjtblHospital.ModifiedDate = DateTime.Now;
                }
                var query = _db.SaveChanges();
                if (query == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteHospital(int Id)
        {
            try
            {
                tblHospital LobjtblHospital = new tblHospital();
                LobjtblHospital = _db.tblHospitals.FirstOrDefault(x => x.HospitalId == Id);
                if (LobjtblHospital != null)
                {
                    LobjtblHospital.IsActive = false;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
