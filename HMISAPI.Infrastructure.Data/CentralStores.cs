﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Domain.Entities;
using System.Data.Linq.SqlClient;

namespace HMISAPI.Infrastructure.Data
{
    public class CentralStores : HMISAPI.Domain.Interfaces.ICentralStores
    {
        HospitalTenantContext _db = new HospitalTenantContext();

        #region equipment
        public EquipmentRepository GetEquipment(int id)
        {
            EquipmentRepository LobjEquip = new EquipmentRepository();
            LobjEquip = (from p in _db.tblEquipments
                         where p.IsActive == true && p.EquipmentID == id
                         select new EquipmentRepository
                         {
                             EquipmentID = p.EquipmentID,
                             Name = p.Name,
                             Code = p.Code,
                             ManufacturedBy = p.ManufacturedBy,
                             ManufacturedDate = p.ManufacturedDate,
                             ExpireDate = p.ExpireDate,
                             Comments = p.Comments

                         }).FirstOrDefault<EquipmentRepository>();
            return LobjEquip;
        }

        public List<EquipmentRepository> GetEquipment()
        {
            List<EquipmentRepository> LobjEquip = new List<EquipmentRepository>();
            var res = from p in _db.tblEquipments
                      where p.IsActive == true
                      select new EquipmentRepository
                      {
                          EquipmentID = p.EquipmentID,
                          Name = p.Name,
                          Code = p.Code,
                          ManufacturedBy = p.ManufacturedBy,
                          ManufacturedDate = p.ManufacturedDate,
                          ExpireDate = p.ExpireDate,
                          Comments = p.Comments

                      };
            LobjEquip = res.ToList<EquipmentRepository>();
            return LobjEquip;
        }

        public EquipmentRepository PostEquipment(EquipmentRepository PobjEquip)
        {
            tblEquipment LobjEquip = new tblEquipment();
            ObjectCopier.CopyObjectData(PobjEquip, LobjEquip);
            LobjEquip.CreatedOn = DateTime.Now;
            LobjEquip.IsActive = true;
            _db.tblEquipments.Add(LobjEquip);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjEquip, PobjEquip);
            return PobjEquip;
        }

        public bool DeleteEquipment(int id)
        {
            tblEquipment LobjEquip = new tblEquipment();
            LobjEquip = _db.tblEquipments.Where(x => x.EquipmentID == id).FirstOrDefault();
            {
                LobjEquip.IsActive = false;

            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        public bool PutEquipment(int id, EquipmentRepository PobjEquip)
        {
            tblEquipment LobjEquip = new tblEquipment();
            LobjEquip = _db.tblEquipments.Where(x => x.EquipmentID == id).FirstOrDefault();
            if (LobjEquip != null)
            {


                LobjEquip.Name = PobjEquip.Name;
                LobjEquip.Code = PobjEquip.Code;
                LobjEquip.ManufacturedBy = PobjEquip.ManufacturedBy;
                LobjEquip.ModifiedBy = PobjEquip.ModifiedBy;
                LobjEquip.ModifiedOn = DateTime.Now;
                LobjEquip.ManufacturedDate = PobjEquip.ManufacturedDate;
                LobjEquip.Comments = PobjEquip.Comments;


            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        #endregion

        #region store
        public StoreRepository GetStore(int id)
        {
            StoreRepository LobjStore = new StoreRepository();
            LobjStore = (from p in _db.tblStores
                         where p.IsActive == true && p.ItemNumber == id
                         select new StoreRepository
                         {
                             ItemNumber = p.ItemNumber,
                             Name = p.Name,
                             Quantity = p.Quantity,
                             ManufacturedBy = p.ManufacturedBy,
                             Cost = p.Cost,
                             SupplierName = p.SupplierName,
                             Comments = p.Comments,
                             InVoice = p.InVoice,
                             BranchID = p.BranchID

                         }).FirstOrDefault<StoreRepository>();
            return LobjStore;
        }
        public List<StoreRepository> GetStore()
        {
            List<StoreRepository> LobjStore = new List<StoreRepository>();
            var res = from p in _db.tblStores
                      where p.IsActive == true
                      select new StoreRepository
                      {
                          ItemNumber = p.ItemNumber,
                          Name = p.Name,
                          Quantity = p.Quantity,
                          ManufacturedBy = p.ManufacturedBy,
                          Cost = p.Cost,
                          SupplierName = p.SupplierName,
                          Comments = p.Comments,
                          InVoice = p.InVoice,
                          BranchID = p.BranchID

                      };
            LobjStore = res.ToList<StoreRepository>();
            return LobjStore;
        }

        public StoreRepository PostStore(StoreRepository PobjStore)
        {
            tblStore LobjStore = new tblStore();
            ObjectCopier.CopyObjectData(PobjStore, LobjStore);
            LobjStore.CreatedOn = DateTime.Now;
            LobjStore.IsActive = true;
            _db.tblStores.Add(LobjStore);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjStore, PobjStore);
            return PobjStore;
        }

        public bool PutStore(int id, StoreRepository PobjStore)
        {
            tblStore LobjStore = new tblStore();
            LobjStore = _db.tblStores.Where(x => x.ItemNumber == id).FirstOrDefault();
            if (LobjStore != null)
            {


                LobjStore.Name = PobjStore.Name;
                LobjStore.Quantity = PobjStore.Quantity;
                LobjStore.ManufacturedBy = PobjStore.ManufacturedBy;
                LobjStore.ModifiedBy = PobjStore.ModifiedBy;
                LobjStore.ModifiedOn = DateTime.Now;
                LobjStore.Cost = PobjStore.Cost;
                LobjStore.Comments = PobjStore.Comments;
                LobjStore.SupplierName = PobjStore.SupplierName;
                LobjStore.InVoice = PobjStore.InVoice;
                LobjStore.BranchID = PobjStore.BranchID;

            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool DeleteStore(int id)
        {
            tblStore LobjStore = new tblStore();
            LobjStore = _db.tblStores.Where(x => x.ItemNumber == id).FirstOrDefault();
            if (LobjStore != null)
            {
                LobjStore.IsActive = false;

            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        #endregion
        #region ChemicalUsed
        public ChemicalUsedRepository GetChemicalUsed(int id)
        {
            ChemicalUsedRepository LobjChemused = new ChemicalUsedRepository();
            LobjChemused = (from p in _db.tblChemicalUseds
                            where p.IsActive == true && p.TransactionID == id
                            select new ChemicalUsedRepository
                            {
                                ChemicalID = p.ChemicalID,
                                TransactionID = p.TransactionID,
                                Quantity = p.Quantity,
                                Date = p.Date,
                                TransactionType = p.TransactionType,
                                PurchaseCostPerQuantity = p.PurchaseCostPerQuantity,
                                SalesCostPerQuantity = p.SalesCostPerQuantity,


                            }).FirstOrDefault<ChemicalUsedRepository>();
            return LobjChemused;
        }
        public List<ChemicalUsedRepository> GetChemicalUsed()
        {
            List<ChemicalUsedRepository> LobjChemused = new List<ChemicalUsedRepository>();
            var res = from p in _db.tblChemicalUseds
                      where p.IsActive == true
                      select new ChemicalUsedRepository
                      {
                          ChemicalID = p.ChemicalID,
                          TransactionID = p.TransactionID,
                          Quantity = p.Quantity,
                          Date = p.Date,
                          TransactionType = p.TransactionType,
                          PurchaseCostPerQuantity = p.PurchaseCostPerQuantity,
                          SalesCostPerQuantity = p.SalesCostPerQuantity,
                      };
            LobjChemused = res.ToList<ChemicalUsedRepository>();
            return LobjChemused;
        }

        public ChemicalUsedRepository PostChemicalUsed(ChemicalUsedRepository PobjChemUsed)
        {
            tblChemicalUsed LobjChemused = new tblChemicalUsed();
            ObjectCopier.CopyObjectData(PobjChemUsed, LobjChemused);
            LobjChemused.CreatedOn = DateTime.Now;
            LobjChemused.IsActive = true;
            _db.tblChemicalUseds.Add(LobjChemused);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjChemused, PobjChemUsed);
            return PobjChemUsed;
        }

        public bool PutChemicalUsed(int id, ChemicalUsedRepository PobjChemUsed)
        {
            tblChemicalUsed LobjChemused = new tblChemicalUsed();
            LobjChemused = _db.tblChemicalUseds.Where(x => x.TransactionID == id).FirstOrDefault();
            if (LobjChemused != null)
            {


                LobjChemused.ChemicalID = PobjChemUsed.ChemicalID;
                LobjChemused.ModifiedBy = PobjChemUsed.ModifiedBy;
                LobjChemused.Quantity = PobjChemUsed.Quantity;
                LobjChemused.Date = PobjChemUsed.Date;
                LobjChemused.TransactionType = PobjChemUsed.TransactionType;
                LobjChemused.PurchaseCostPerQuantity = PobjChemUsed.PurchaseCostPerQuantity;
                LobjChemused.SalesCostPerQuantity = PobjChemUsed.SalesCostPerQuantity;
                LobjChemused.ModifiedOn = DateTime.Now;

            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool DeleteChemicalUsed(int id)
        {
            tblChemicalUsed LobjChemused = new tblChemicalUsed();
            LobjChemused = _db.tblChemicalUseds.Where(x => x.TransactionID == id).FirstOrDefault();
            if (LobjChemused != null)
            {
                LobjChemused.IsActive = false;

            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        #endregion

        #region ChemicalPurchase

        public ChemicalPurchaseRepository GetChemicalPurchase(int id)
        {
            ChemicalPurchaseRepository LobjPur = new ChemicalPurchaseRepository();
            LobjPur = (from p in _db.tblChemicalPurchases
                       where p.IsActive == true && p.ChemicalID == id
                       select new ChemicalPurchaseRepository
                            {
                                ChemicalID = p.ChemicalID,
                                Name = p.Name,
                                Code = p.Code,
                                ManufacturedBy = p.ManufacturedBy,
                                ManufacturedDate = p.ManufacturedDate,
                                PurchaseCostPerQuantity = p.PurchaseCostPerQuantity,
                                SalesCostPerQuantity = p.SalesCostPerQuantity,
                                Expirydate = p.Expirydate,
                                Quantity = p.Quantity,
                                TotalCost = p.TotalCost,


                            }).FirstOrDefault<ChemicalPurchaseRepository>();
            return LobjPur;
        }
        public List<ChemicalPurchaseRepository> GetChemicalPurchase()
        {
            List<ChemicalPurchaseRepository> LobjPur = new List<ChemicalPurchaseRepository>();
            var res = from p in _db.tblChemicalPurchases
                      where p.IsActive == true
                      select new ChemicalPurchaseRepository
                      {
                          ChemicalID = p.ChemicalID,
                          Name = p.Name,
                          Code = p.Code,
                          ManufacturedBy = p.ManufacturedBy,
                          ManufacturedDate = p.ManufacturedDate,
                          PurchaseCostPerQuantity = p.PurchaseCostPerQuantity,
                          SalesCostPerQuantity = p.SalesCostPerQuantity,
                          Expirydate = p.Expirydate,
                          Quantity = p.Quantity,
                          TotalCost = p.TotalCost,


                      };


            LobjPur = res.ToList<ChemicalPurchaseRepository>();
            return LobjPur;
        }

        public ChemicalPurchaseRepository PostChemicalPurchase(ChemicalPurchaseRepository pobjChemPur)
        {
            tblChemicalPurchase LobjPur = new tblChemicalPurchase();
            ObjectCopier.CopyObjectData(pobjChemPur, LobjPur);
            LobjPur.CreatedOn = DateTime.Now;
            LobjPur.IsActive = true;
            _db.tblChemicalPurchases.Add(LobjPur);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjPur, pobjChemPur);
            return pobjChemPur;
        }

        public bool PutChemicalPurchase(int id, ChemicalPurchaseRepository pobjChemPur)
        {
            tblChemicalPurchase LobjPur = new tblChemicalPurchase();
            LobjPur = _db.tblChemicalPurchases.Where(x => x.ChemicalID == id).FirstOrDefault();
            if (LobjPur != null)
            {



                LobjPur.Name = pobjChemPur.Name;
                LobjPur.Code = pobjChemPur.Code;
                LobjPur.ManufacturedBy = pobjChemPur.ManufacturedBy;
                LobjPur.ManufacturedDate = pobjChemPur.ManufacturedDate;
                LobjPur.PurchaseCostPerQuantity = pobjChemPur.PurchaseCostPerQuantity;
                LobjPur.SalesCostPerQuantity = pobjChemPur.SalesCostPerQuantity;
                LobjPur.Expirydate = pobjChemPur.Expirydate;
                LobjPur.Quantity = pobjChemPur.Quantity;
                LobjPur.TotalCost = pobjChemPur.TotalCost;
                LobjPur.ModifiedBy = pobjChemPur.ModifiedBy;
                LobjPur.ModifiedOn = DateTime.Now;

            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool DeleteChemicalPurchase(int id)
        {
             tblChemicalPurchase LobjPur = new tblChemicalPurchase();
            LobjPur = _db.tblChemicalPurchases.Where(x => x.ChemicalID == id).FirstOrDefault();
            if (LobjPur != null)
            {
            
                LobjPur.IsActive = false;

            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        #endregion
    }
}

