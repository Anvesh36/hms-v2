﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Domain.Entities;
using System.Data.Linq.SqlClient;

namespace HMISAPI.Infrastructure.Data
{
    public class Hrms : HMISAPI.Domain.Interfaces.IHrms
    {
        HospitalTenantContext _db = new HospitalTenantContext();
        #region EmpFamily
        public EmpFamilyRepository PostEmpFamily(EmpFamilyRepository PobjEmpFamily)
        {
            tblEmpFamily LobjtblEmp = new tblEmpFamily();
            ObjectCopier.CopyObjectData(PobjEmpFamily, LobjtblEmp);
            LobjtblEmp.CreatedOn = DateTime.Now;
            LobjtblEmp.IsActive = true;
            _db.tblEmpFamilies.Add(LobjtblEmp);


            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblEmp, PobjEmpFamily);

            return PobjEmpFamily;


        }
        public EmpFamilyRepository GetEmpFamily(int id)
        {
            EmpFamilyRepository LobjEmpFam = new EmpFamilyRepository();
            LobjEmpFam = (from empfam in _db.tblEmpFamilies
                      join emp in _db.tblEmployees on empfam.EID equals emp.EID
                          where empfam.EID==id
                      select new EmpFamilyRepository
                      {
                          FamilyID = empfam.FamilyID,
                          EID = empfam.EID,
                          Title = empfam.Title,
                          FirstName = empfam.FirstName,
                          LastName = empfam.LastName,
                          MiddleName = empfam.MiddleName,
                          Gender = empfam.Gender,
                          DOB = empfam.DOB,
                          PhoneNo = empfam.PhoneNo,
                          MobileNo = empfam.MobileNo,
                          Address1 = empfam.Address1,
                          State = empfam.State,
                          City = empfam.City,
                          //IsActive=empfam.IsActive,
                          
                          Country = empfam.Country,
                          Relationship = empfam.Relationship,
                          Age = empfam.Age,
                          BloodGroup = empfam.BloodGroup,
                          IsDependent = empfam.IsDependent,
                          IsNominee = empfam.IsNominee,
                          IsDeath = empfam.IsDeath,
                          IsEmergencyContact = empfam.IsEmergencyContact,
                          Occupation = empfam.Occupation,

                      }).FirstOrDefault < EmpFamilyRepository>();

            
            return LobjEmpFam;
        }

        public List<EmpFamilyRepository> GetEmpFamily()
        {
            List<EmpFamilyRepository> LobjEmpFam = new List<EmpFamilyRepository>();
            var res = from empfam in _db.tblEmpFamilies
                      join
                          emp in _db.tblEmployees on empfam.EID equals emp.EID
                      where empfam.IsActive==true

                      select new EmpFamilyRepository
                      {
                          FamilyID = empfam.FamilyID,
                          EID = empfam.EID,
                          Title = empfam.Title,
                          FirstName = empfam.FirstName,
                          LastName = empfam.LastName,
                          MiddleName = empfam.MiddleName,
                          Gender = empfam.Gender,
                          DOB = empfam.DOB,
                          PhoneNo = empfam.PhoneNo,
                          MobileNo = empfam.MobileNo,
                          Address1 = empfam.Address1,
                          State = empfam.State,
                          City = empfam.City,
                          //IsActive=empfam.IsActive,

                          Country = empfam.Country,
                          Relationship = empfam.Relationship,
                          Age = empfam.Age,
                          BloodGroup = empfam.BloodGroup,
                          IsDependent = empfam.IsDependent,
                          IsNominee = empfam.IsNominee,
                          IsDeath = empfam.IsDeath,
                          IsEmergencyContact = empfam.IsEmergencyContact,
                          Occupation = empfam.Occupation,

                      };

            LobjEmpFam = res.ToList<EmpFamilyRepository>();
            return LobjEmpFam;
        }
        public bool PutEmpFamily(int id, EmpFamilyRepository PobjEmp)
        {
            try
            {
                tblEmpFamily LobtblEmpFam = new tblEmpFamily();
                LobtblEmpFam = _db.tblEmpFamilies.Where(E => E.FamilyID == id).FirstOrDefault<tblEmpFamily>();
                if (LobtblEmpFam != null)
                {
                    LobtblEmpFam.Title = PobjEmp.Title;
                    LobtblEmpFam.FirstName = PobjEmp.FirstName;
                    LobtblEmpFam.LastName = PobjEmp.LastName;
                    LobtblEmpFam.MiddleName = PobjEmp.MiddleName;
                    LobtblEmpFam.Gender = PobjEmp.Gender;
                    LobtblEmpFam.DOB = PobjEmp.DOB;
                    LobtblEmpFam.PhoneNo = PobjEmp.PhoneNo;
                    LobtblEmpFam.MobileNo = PobjEmp.MobileNo;
                    LobtblEmpFam.Address1 = PobjEmp.Address1;
                    LobtblEmpFam.State = PobjEmp.State;
                    LobtblEmpFam.City = PobjEmp.City;
                    LobtblEmpFam.ModifiedBy = PobjEmp.ModifiedBy;
                    LobtblEmpFam.Country = PobjEmp.Country;
                    LobtblEmpFam.Relationship = PobjEmp.Relationship;
                    LobtblEmpFam.Age = PobjEmp.Age;
                    LobtblEmpFam.BloodGroup = PobjEmp.BloodGroup;
                    LobtblEmpFam.IsDependent = PobjEmp.IsDependent;
                    LobtblEmpFam.IsNominee = PobjEmp.IsNominee;
                    LobtblEmpFam.IsDeath = PobjEmp.IsDeath;
                    LobtblEmpFam.IsEmergencyContact = PobjEmp.IsEmergencyContact;
                    LobtblEmpFam.Occupation = PobjEmp.Occupation;
                    LobtblEmpFam.ModifiedOn = DateTime.Now;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteEmpFamily(int id)
        {
            try
            {
                tblEmpFamily LobtblEmpFam = new tblEmpFamily();

                LobtblEmpFam = _db.tblEmpFamilies.Where(E => E.FamilyID == id).FirstOrDefault<tblEmpFamily>();
                if (LobtblEmpFam != null)
                {
                    LobtblEmpFam.IsActive = false;
                    LobtblEmpFam.ModifiedOn = DateTime.Now;
                    //LobjtblPatient.ModifiedBy = 2;
                }
                //_db.tblEmpFamilies.Remove(LobtblEmpFam);
                int res = _db.SaveChanges();
                if (res == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region EmpProfessional
        public EmpProffesionalInfoRepository PostEmpProfess(EmpProffesionalInfoRepository PobjEmpPro)
        {
            tblEmpProffesionalInfo LobjtblEmp = new tblEmpProffesionalInfo();
            ObjectCopier.CopyObjectData(PobjEmpPro, LobjtblEmp);
            LobjtblEmp.CreatedDate = DateTime.Now;
            LobjtblEmp.IsActive = true;
            _db.tblEmpProffesionalInfoes.Add(LobjtblEmp);


            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblEmp, PobjEmpPro);

            return PobjEmpPro;


        }
        public List<EmpProffesionalInfoRepository> GetEmpProfess(int? Empid)
        {
            List<EmpProffesionalInfoRepository> LobjEmppro = new List<EmpProffesionalInfoRepository>();
            var res = from emppro in _db.tblEmpProffesionalInfoes
                      join
                          emp in _db.tblEmployees on emppro.EID equals emp.EID


                      select new EmpProffesionalInfoRepository
                      {
                          ProffID=emppro.ProffID,
                          EID=emppro.EID,
                          Department=emppro.Department,
                          Designation=emppro.Designation,
                          Qualification=emppro.Qualification,
                          Specialization=emppro.Specialization,
                          Section=emppro.Section,
                          Experience=emppro.Experience,
                          EmpType=emppro.EmpType,
                          EmployementType=emppro.EmployementType,
                          WorkLocation=emppro.WorkLocation,
                          IsActive=emppro.IsActive,
                          CreatedBy=emppro.CreatedBy

                      };
            if ( Empid!= null)
            {
                res = res.Where(p => p.EID == Empid);
            }

            LobjEmppro = res.ToList<EmpProffesionalInfoRepository>();
            return LobjEmppro;
        }
        public bool PutEmpProfess(int id, EmpProffesionalInfoRepository PobjEmp)
        {
            try
            {
                tblEmpProffesionalInfo LobtblEmppro = new tblEmpProffesionalInfo();
                LobtblEmppro = _db.tblEmpProffesionalInfoes.Where(E => E.ProffID == id).FirstOrDefault<tblEmpProffesionalInfo>();
                if (LobtblEmppro != null)
                {
                    LobtblEmppro.Department=PobjEmp.Department;
                          LobtblEmppro.Designation=PobjEmp.Designation;
                          LobtblEmppro.Qualification=PobjEmp.Qualification;
                          LobtblEmppro.Specialization=PobjEmp.Specialization;
                          LobtblEmppro.Section=PobjEmp.Section;
                          LobtblEmppro.Experience=PobjEmp.Experience;
                          LobtblEmppro.EmpType=PobjEmp.EmpType;
                         LobtblEmppro. EmployementType=PobjEmp.EmployementType;
                         LobtblEmppro.WorkLocation = PobjEmp.WorkLocation;
                    LobtblEmppro.ModifiedDate = DateTime.Now;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteEmpProfess(int id)
        {
            try
            {
                tblEmpProffesionalInfo LobjtblEmpPro = new tblEmpProffesionalInfo();

                LobjtblEmpPro = _db.tblEmpProffesionalInfoes.Where(E => E.ProffID == id).FirstOrDefault<tblEmpProffesionalInfo>();
                if (LobjtblEmpPro != null)
                {
                    LobjtblEmpPro.IsActive = false;
                    LobjtblEmpPro.ModifiedDate = DateTime.Now;
                    //LobjtblPatient.ModifiedBy = 2;
                }
                //_db.tblEmpFamilies.Remove(LobtblEmpFam);
                int res = _db.SaveChanges();
                if (res == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region EmpPersonalInfo
        public List<EmployeePersonalInfoRepository> GetEmpPersonalInfo()
        {
            List<EmployeePersonalInfoRepository> LobjlstEmpPersonalInfo = new List<EmployeePersonalInfoRepository>();
            var EmpPersonalInfo = from p in _db.tblEmpPersonalInfoes
                                  join e in _db.tblEmployees on p.EID equals e.EID
                                  where p.IsActive == true
                                  select new EmployeePersonalInfoRepository
                                  {
                                      PersID = p.PersID,
                                      Name = e.FirstName + " " + e.LastName,
                                      Age = e.Age,
                                      Gender = e.Gender,
                                      AddressLine = p.AddressLine,
                                      AddreesLine2 = p.AddreesLine2,
                                      MobileNo = p.MobileNo,
                                      LandLine = p.LandLine,
                                      Email = p.Email,
                                      Nationality = p.Nationality,
                                      Religion = p.Religion,
                                      MaritalStatus = p.MaritalStatus,
                                      DateOfMarriage = p.DateOfMarriage,
                                      City = p.City,
                                      State = p.State,
                                      Country = p.Country,
                                      Pincode = p.Pincode,
                                      AadharNumber = p.AadharNumber,
                                      PassPortNumber = p.PassPortNumber,
                                      PanCardNumber = p.PanCardNumber,
                                      IsActive = p.IsActive
                                  };
            LobjlstEmpPersonalInfo = EmpPersonalInfo.ToList<EmployeePersonalInfoRepository>();
            return LobjlstEmpPersonalInfo;
        }
        public EmployeePersonalInfoRepository GetEmpPersonalInfo(int id)
        {
            EmployeePersonalInfoRepository LobjlstEmpPersonalInfo = new EmployeePersonalInfoRepository();
            LobjlstEmpPersonalInfo = (from p in _db.tblEmpPersonalInfoes
                                      join e in _db.tblEmployees on p.EID equals e.EID
                                      where p.IsActive == true && p.PersID == id
                                      select new EmployeePersonalInfoRepository
                                      {
                                          PersID = p.PersID,
                                          Name = e.FirstName + " " + e.LastName,
                                          Age = e.Age,
                                          Gender = e.Gender,
                                          AddressLine = p.AddressLine,
                                          AddreesLine2 = p.AddreesLine2,
                                          MobileNo = p.MobileNo,
                                          LandLine = p.LandLine,
                                          Email = p.Email,
                                          Nationality = p.Nationality,
                                          Religion = p.Religion,
                                          MaritalStatus = p.MaritalStatus,
                                          DateOfMarriage = p.DateOfMarriage,
                                          City = p.City,
                                          State = p.State,
                                          Country = p.Country,
                                          Pincode = p.Pincode,
                                          AadharNumber = p.AadharNumber,
                                          PassPortNumber = p.PassPortNumber,
                                          PanCardNumber = p.PanCardNumber,
                                          IsActive = p.IsActive
                                      }).FirstOrDefault();
            return LobjlstEmpPersonalInfo;
        }
        public EmployeePersonalInfoRepository PostEmpPersonalInfo(EmployeePersonalInfoRepository PobjEmployeePersonalInfo)
        {
            tblEmpPersonalInfo LobjtblEmpPersonalInfo = new tblEmpPersonalInfo();
            ObjectCopier.CopyObjectData(PobjEmployeePersonalInfo, LobjtblEmpPersonalInfo);
            LobjtblEmpPersonalInfo.CreatedDate = DateTime.Now;
            LobjtblEmpPersonalInfo.IsActive = true;
            _db.tblEmpPersonalInfoes.Add(LobjtblEmpPersonalInfo);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblEmpPersonalInfo, PobjEmployeePersonalInfo);
            return PobjEmployeePersonalInfo;
        }

        public bool PutEmpPersonalInfo(int id, EmployeePersonalInfoRepository PobjEmployeePersonalInfo)
        {
            tblEmpPersonalInfo LobjEmployeePersonalInfo = new tblEmpPersonalInfo();

            LobjEmployeePersonalInfo = _db.tblEmpPersonalInfoes.Where(s => s.PersID == id).FirstOrDefault<tblEmpPersonalInfo>();
            if (LobjEmployeePersonalInfo != null)
            {
                LobjEmployeePersonalInfo.AddressLine = PobjEmployeePersonalInfo.AddressLine;
                LobjEmployeePersonalInfo.AddreesLine2 = PobjEmployeePersonalInfo.AddreesLine2;
                LobjEmployeePersonalInfo.MobileNo = PobjEmployeePersonalInfo.MobileNo;
                LobjEmployeePersonalInfo.LandLine = PobjEmployeePersonalInfo.LandLine;
                LobjEmployeePersonalInfo.Email = PobjEmployeePersonalInfo.Email;
                LobjEmployeePersonalInfo.Nationality = PobjEmployeePersonalInfo.Nationality;
                LobjEmployeePersonalInfo.Religion = PobjEmployeePersonalInfo.Religion;
                LobjEmployeePersonalInfo.MaritalStatus = PobjEmployeePersonalInfo.MaritalStatus;
                LobjEmployeePersonalInfo.DateOfMarriage = PobjEmployeePersonalInfo.DateOfMarriage;
                LobjEmployeePersonalInfo.AadharNumber = PobjEmployeePersonalInfo.AadharNumber;
                LobjEmployeePersonalInfo.PassPortNumber = PobjEmployeePersonalInfo.PassPortNumber;
                LobjEmployeePersonalInfo.PanCardNumber = PobjEmployeePersonalInfo.PanCardNumber;
                LobjEmployeePersonalInfo.Pincode = PobjEmployeePersonalInfo.Pincode;
                LobjEmployeePersonalInfo.State = PobjEmployeePersonalInfo.State;
                LobjEmployeePersonalInfo.City = PobjEmployeePersonalInfo.City;
                LobjEmployeePersonalInfo.Country = PobjEmployeePersonalInfo.Country;
                LobjEmployeePersonalInfo.ModifiedBy = PobjEmployeePersonalInfo.ModifiedBy;
                LobjEmployeePersonalInfo.ModifiedDate = DateTime.Now;
            }
            int res = _db.SaveChanges();

            if (res >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteEmpPersonalInfo(int id)
        {

            tblEmpPersonalInfo LobjtblEmpPersonalInfo = new tblEmpPersonalInfo();
            LobjtblEmpPersonalInfo = _db.tblEmpPersonalInfoes.Where(s => s.PersID == id).FirstOrDefault<tblEmpPersonalInfo>();
            if (LobjtblEmpPersonalInfo != null)
            {
                LobjtblEmpPersonalInfo.IsActive = false;
                LobjtblEmpPersonalInfo.ModifiedDate = DateTime.Now;
                LobjtblEmpPersonalInfo.ModifiedBy = 2;
            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        public List<HrmsDashboardRepository> GetHrmsDashBoard()
        {
            HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
            List<HrmsDashboardRepository> LlstPatints = new List<HrmsDashboardRepository>();
            var suprs = from sup in LobjHMISTenantContext.tblEmployees
                        join addres in LobjHMISTenantContext.tblDepartments
                        on sup.DepartmentId equals addres.DeptID

                        select new HrmsDashboardRepository
                        {
                            FirstName = sup.FirstName,
                            EID = sup.EID,
                            DepartmentId = addres.DeptID,
                            DesignationName = sup.DesignationName,
                            MobileNo = sup.MobileNo,
                            Email = sup.Email,
                            ManagerId = sup.ManagerId
                        };
            LlstPatints = suprs.ToList<HrmsDashboardRepository>();
            return LlstPatints;

        }
        public CtcRepository PostCTC(CtcRepository PobjCTC)
        {
            tblEmpPaySlip LobjEmpPaySlip = new tblEmpPaySlip();
            tblEmpPaySetting LobjEmpPaySetting = new tblEmpPaySetting();

            ObjectCopier.CopyObjectData(PobjCTC.emppaysetting, LobjEmpPaySetting);
            ObjectCopier.CopyObjectData(PobjCTC, LobjEmpPaySlip);
            LobjEmpPaySlip.IsActive = true;
            // LobjSupplier.CreatedBy = 1;
            LobjEmpPaySlip.CreatedOn = DateTime.Now;

            // LobjStaff.CreatedDate = DateTime.Now;

            LobjEmpPaySetting.IsActive = true;
            // Lobjaddress.CreatedBy = 1;
            LobjEmpPaySetting.CreatedOn = DateTime.Now;
            LobjEmpPaySlip.tblEmpPaySetting = LobjEmpPaySetting;

            _db.tblEmpPaySettings.Add(LobjEmpPaySetting);
            _db.tblEmpPaySlips.Add(LobjEmpPaySlip);


            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjEmpPaySetting, PobjCTC.emppaysetting);
            ObjectCopier.CopyObjectData(LobjEmpPaySlip, PobjCTC);
            return PobjCTC;



        }

        //Get Employees
        public List<HrmsDashboardRepository> GetEmployees()
        {
            HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
            List<HrmsDashboardRepository> LlstPatints = new List<HrmsDashboardRepository>();
            var suprs = from sup in LobjHMISTenantContext.tblEmployees


                        select new HrmsDashboardRepository
                        {
                            EID = sup.EID,
                            FirstName = sup.FirstName,
                            LastName = sup.LastName,
                            DisplayName = sup.LastName,
                            AadhaarNo = sup.AadhaarNo,
                            Achievements = sup.Achievements,
                            Age = sup.Age,
                            Comments = sup.Comments,
                            DepartmentId = sup.DepartmentId,
                            DOB = sup.DOB,
                            DOJ = sup.DOJ,
                            DesignationName = sup.DesignationName,
                            EmerContactName = sup.EmerContactName,
                            Email = sup.Email,
                            EmployeeType = sup.EmployeeType,
                            EmploymentType = sup.EmploymentType,
                            EmployeeId = sup.EmployeeId,
                            LandLine = sup.LandLine
                        };
            LlstPatints = suprs.ToList<HrmsDashboardRepository>();
            return LlstPatints;

        }

        public HrmsDashboardRepository GetEmployees(int id)
        {
            HospitalTenantContext LobjHMISTenantContext = new HospitalTenantContext();
            HrmsDashboardRepository LlstPatints = new HrmsDashboardRepository();
            var suprs = from sup in LobjHMISTenantContext.tblEmployees where sup.EID==id


                        select new HrmsDashboardRepository
                        {
                            EID = sup.EID,
                            FirstName = sup.FirstName,
                            LastName = sup.LastName,
                            DisplayName = sup.LastName,
                            AadhaarNo = sup.AadhaarNo,
                            Achievements = sup.Achievements,
                            Age = sup.Age,
                            Comments = sup.Comments,
                            DepartmentId = sup.DepartmentId,
                            DOB = sup.DOB,
                            DOJ = sup.DOJ,
                            DesignationName = sup.DesignationName,
                            EmerContactName = sup.EmerContactName,
                            Email = sup.Email,
                            EmployeeType = sup.EmployeeType,
                            EmploymentType = sup.EmploymentType,
                            EmployeeId = sup.EmployeeId,
                            LandLine = sup.LandLine
                        };
           // LlstPatints = suprs.ToList<HrmsDashboardRepository>();
            return LlstPatints;

        }

        //Post Employee
        public HrmsDashboardRepository PostEmployee(HrmsDashboardRepository PobjEmployee)
        {
            tblEmployee LobjtblEmployee = new tblEmployee();
            ObjectCopier.CopyObjectData(PobjEmployee, LobjtblEmployee);
            LobjtblEmployee.CreatedDate = DateTime.Now;
            LobjtblEmployee.IsActive = true;
            LobjtblEmployee.CreatedBy = PobjEmployee.CreatedBy;

            _db.tblEmployees.Add(LobjtblEmployee);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblEmployee, PobjEmployee);
            return PobjEmployee;
        }

        //put Employee
        public bool PutEmployee(int EID, HrmsDashboardRepository PobjPutEmployee)
        {

            tblEmployee LobjtblManufacturer = new tblEmployee();
            LobjtblManufacturer = _db.tblEmployees.Where(m => m.EID == EID).FirstOrDefault<tblEmployee>();
            if (LobjtblManufacturer != null)
            {
                LobjtblManufacturer.FirstName = PobjPutEmployee.FirstName;
                LobjtblManufacturer.LastName = PobjPutEmployee.LastName;
                LobjtblManufacturer.DisplayName = PobjPutEmployee.DisplayName;
                LobjtblManufacturer.ModifiedDate = DateTime.Now;
                LobjtblManufacturer.ModifiedBy = PobjPutEmployee.ModifiedBy;
            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Delete Employee

        public bool DeleteEmployee(int eid)
        {

            tblEmployee LobjtblEmployee = new tblEmployee();
            LobjtblEmployee = _db.tblEmployees.Where(m => m.EID == eid).FirstOrDefault<tblEmployee>();
            if (LobjtblEmployee != null)
            {
                LobjtblEmployee.IsActive = false;
            }
            // _db.tblEmployees.Remove(LobjtblManufacturer);
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
