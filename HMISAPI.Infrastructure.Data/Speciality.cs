﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Domain.Entities;
using HMIS.Domain.Interfaces;

namespace HMISAPI.Infrastructure.Data
{
    public class Speciality : ISpeciality
    {
        HospitalTenantContext _db = new HospitalTenantContext();
        public SpecialityRepository PostSpeciality(SpecialityRepository PobjSpe)
        {
            tblSpeciality LobjtblSpe = new tblSpeciality();
            ObjectCopier.CopyObjectData(PobjSpe, LobjtblSpe);
            LobjtblSpe.CreatedOn = DateTime.Now;
            LobjtblSpe.IsActive = true;
            _db.tblSpecialities.Add(LobjtblSpe);


            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblSpe, PobjSpe);

            return PobjSpe;
        }
        public List<SpecialityRepository> GetSpeciality()
        {
            List<SpecialityRepository> LObjSpe = new List<SpecialityRepository>();
            var res = from p in _db.tblSpecialities
                      where p.IsActive == true

                      select new SpecialityRepository
                      {
                          SpID = p.SpID,
                          DrSettingsID = p.DrSettingsID,
                          SpName = p.SpName,
                          Description = p.Description,
                          Summary = p.Summary,
                          EstFrom = p.EstFrom

                      };

            LObjSpe = res.ToList<SpecialityRepository>();
            return LObjSpe;
        }
        public SpecialityRepository GetSpeciality(int id)
        {
            SpecialityRepository LObjSpe = new SpecialityRepository();
            LObjSpe = (from p in _db.tblSpecialities
                       where p.IsActive == true && p.SpID == id
                       select new SpecialityRepository
                       {
                           SpID = p.SpID,
                           DrSettingsID = p.DrSettingsID,
                           SpName = p.SpName,
                           Description = p.Description,
                           Summary = p.Summary,
                           EstFrom = p.EstFrom
                       }).FirstOrDefault();

            return LObjSpe;

        }
        public bool PutSpecialityDetail(int id, SpecialityRepository PobjSpe)
        {
            try
            {
                tblSpeciality LobtblSpe = new tblSpeciality();
                LobtblSpe = _db.tblSpecialities.Where(E => E.SpID == id).FirstOrDefault();
                if (LobtblSpe != null)
                {
                    LobtblSpe.SpName = PobjSpe.SpName;
                    LobtblSpe.Description = PobjSpe.Description;
                    LobtblSpe.Summary = PobjSpe.Summary;
                    LobtblSpe.EstFrom = PobjSpe.EstFrom;
                    LobtblSpe.ModifiedBy = PobjSpe.ModifiedBy;
                    LobtblSpe.ModifiedOn = DateTime.Now;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteSpecialityDetail(int id)
        {
            try
            {
                tblSpeciality LobtblSpe = new tblSpeciality();
                LobtblSpe = _db.tblSpecialities.Where(E => E.SpID == id).FirstOrDefault<tblSpeciality>();
                if (LobtblSpe != null)
                {
                    LobtblSpe.IsActive = false;
                    LobtblSpe.ModifiedOn = DateTime.Now;
                }

                int res = _db.SaveChanges();
                if (res == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
