﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Domain.Entities;
using System.Data.Linq.SqlClient;
using System.Data.Entity.Validation;


namespace HMISAPI.Infrastructure.Data
{
    public class Lab : HMISAPI.Domain.Interfaces.ILab
    {
        HospitalTenantContext _db = new HospitalTenantContext();
        #region labs
        public List<DiagnosisRepository> GetInvestigationsBySearch(string Pstrterm)
        {
            List<DiagnosisRepository> Lobjlstdiagnosis = new List<DiagnosisRepository>();
            var res = from sup in _db.tblDiagnosis
                      where sup.IsActive == true && sup.DiagName.StartsWith(Pstrterm)
                      select new DiagnosisRepository
                      {
                          DiagDepartmentName = sup.DiagDepartmentName,
                          DiagnosisID = sup.DiagnosisID,
                          DiagName = sup.DiagName,
                          Amount = sup.Amount,
                          Comments = sup.Comments
                      };
            Lobjlstdiagnosis = res.ToList<DiagnosisRepository>();
            return Lobjlstdiagnosis;
        }
        public List<RequestSummaryRepository> GetRequestSummary()
        {
            List<RequestSummaryRepository> LobjDiagRep = new List<RequestSummaryRepository>();

            var res = from tdr in _db.tblDiagRequisitions
                      join ts in _db.tblTransactions on tdr.PatientID equals ts.PatientID
                      join p in _db.tblPatients on tdr.PatientID equals p.PatientId
                      join tdinfo in _db.tblDiagRequisitionInfoes on tdr.DiagReqID equals tdinfo.DiagReqID

                      select new RequestSummaryRepository
                      {
                          DiagReqID = tdr.DiagReqID,
                          PatientID = tdr.PatientID,
                          PatientName = p.FirstName,
                          ReferredDoctor = tdr.ReferredDoctor,
                          ReportDate = tdr.ReportDate,
                          IsDelivered = tdinfo.IsDelivered,
                          IsSampleCollected = tdinfo.IsSampleCollected,
                          IsReportReady = tdinfo.IsReportReady,
                          DispatchedOn = tdinfo.DispatchedOn,
                          SampleCollectedOn = tdinfo.SampleCollectedOn,
                          ReportReadyOn = tdinfo.ReportReadyOn
                      };
            LobjDiagRep = res.ToList<RequestSummaryRepository>();

            return LobjDiagRep;
        }
        public DiagnosisRepository PostDiagnosis(DiagnosisRepository PobjDiagRep)
        {
            tblDiagnosi Lobjtbldiag = new tblDiagnosi();
            ObjectCopier.CopyObjectData(PobjDiagRep, Lobjtbldiag);
            Lobjtbldiag.CreatedDate = DateTime.Now;
            Lobjtbldiag.IsActive = true;
            _db.tblDiagnosis.Add(Lobjtbldiag);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(Lobjtbldiag, PobjDiagRep);

            return PobjDiagRep;
        }
        public bool PutDiagnosis(int Id, DiagnosisRepository PobjDiagRep)
        {
            try
            {
                tblDiagnosi Lobjtbldiag = new tblDiagnosi();
                Lobjtbldiag = _db.tblDiagnosis.Where(d => d.DiagnosisID == Id).FirstOrDefault<tblDiagnosi>();
                if (Lobjtbldiag != null)
                {
                    Lobjtbldiag.DiagName = PobjDiagRep.DiagName;
                    Lobjtbldiag.DiagDepartmentName = PobjDiagRep.DiagDepartmentName;
                    Lobjtbldiag.Comments = PobjDiagRep.Comments;
                    Lobjtbldiag.Amount = PobjDiagRep.Amount;
                    Lobjtbldiag.ModifiedDate = DateTime.Now;
                    Lobjtbldiag.ModifiedBy = PobjDiagRep.ModifiedBy;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DiagnosisRepository PostInvestigations(DiagnosisRepository PobjDiagRep)
        {
            tblDiagnosi LobjTbldiag = new tblDiagnosi();
            tblDiagnosisComponent LobjTblDiagComp = new tblDiagnosisComponent();
            ObjectCopier.CopyObjectData(PobjDiagRep, LobjTbldiag);
            ObjectCopier.CopyObjectData(PobjDiagRep.diagcomp, LobjTblDiagComp);
            LobjTblDiagComp.IsActive = true;
            LobjTblDiagComp.CreatedDate = DateTime.Now;
            LobjTbldiag.IsActive = true;
            LobjTbldiag.CreatedDate = DateTime.Now;
            _db.tblDiagnosis.Add(LobjTbldiag);
            _db.tblDiagnosisComponents.Add(LobjTblDiagComp);
            int res = _db.SaveChanges();

            ObjectCopier.CopyObjectData(LobjTblDiagComp, PobjDiagRep.diagcomp);
            ObjectCopier.CopyObjectData(LobjTbldiag, PobjDiagRep);
            return PobjDiagRep;
        }
        public List<DiagnosisRequisitionRepository> GetPatientsByID(int id)
        {
            List<DiagnosisRequisitionRepository> Lobjlstdiagnosis = new List<DiagnosisRequisitionRepository>();
            var res = from sup in _db.tblDiagRequisitions
                      join p in _db.tblPatients on sup.PatientID equals p.PatientId
                      where sup.PatientID == id
                      select new DiagnosisRequisitionRepository
                      {
                          PatientID = sup.PatientID,
                          ReferredDoctor = sup.ReferredDoctor,
                          ReportDate = sup.ReportDate,
                          Patients = new Patientinfo
                          {
                              FirstName = p.FirstName,
                              LastName = p.LastName,
                              GenderName = p.Gender,
                              Title = p.Title,
                              Age = p.Age,
                              ContactNumber = p.ContactNumber,
                          }
                      };
            Lobjlstdiagnosis = res.ToList<DiagnosisRequisitionRepository>();
            return Lobjlstdiagnosis;
        }
        public DiagnosisRequisitionRepository PostDiagnosisRequisition(DiagnosisRequisitionRepository pobjdiagrequisition)
        {
            int res = 0;
            tblDiagRequisition LobjtblDiagreq = new tblDiagRequisition();
            ObjectCopier.CopyObjectData(pobjdiagrequisition, LobjtblDiagreq);
            LobjtblDiagreq.CreatedBy = pobjdiagrequisition.CreatedBy;
            LobjtblDiagreq.CreatedDate = DateTime.Now;
            LobjtblDiagreq.IsActive = true;
            _db.tblDiagRequisitions.Add(LobjtblDiagreq);
            List<tblDiagRequisitionInfo> lsttbldigreqinfo = new List<tblDiagRequisitionInfo>();
            foreach (DiagnosisRequisitioninfoRepository lobjdiagreqinforep in pobjdiagrequisition.lstdiagreqinfo)
            {
                tblDiagRequisitionInfo LobjtblDiagreqinfo = new tblDiagRequisitionInfo();
                LobjtblDiagreqinfo.DiagnosisID = lobjdiagreqinforep.DiagnosisID;
                LobjtblDiagreqinfo.Discount = lobjdiagreqinforep.Discount;
                LobjtblDiagreqinfo.Amount = lobjdiagreqinforep.Amount;
                LobjtblDiagreqinfo.Comments = lobjdiagreqinforep.Comments;
                LobjtblDiagreqinfo.DispatchedOn = lobjdiagreqinforep.DispatchedOn;
                LobjtblDiagreqinfo.IsSampleCollected = lobjdiagreqinforep.IsSampleCollected;
                LobjtblDiagreqinfo.SampleCollectedOn = lobjdiagreqinforep.SampleCollectedOn;
                LobjtblDiagreqinfo.IsReportReady = lobjdiagreqinforep.IsReportReady;
                LobjtblDiagreqinfo.ReportReadyOn = lobjdiagreqinforep.ReportReadyOn;
                LobjtblDiagreqinfo.IsDelivered = lobjdiagreqinforep.IsDelivered;
                lsttbldigreqinfo.Add(LobjtblDiagreqinfo);
                LobjtblDiagreqinfo.CreatedBy = lobjdiagreqinforep.CreatedBy;
                LobjtblDiagreqinfo.CreatedDate = DateTime.Now;
               
            }
            LobjtblDiagreq.tblDiagRequisitionInfoes = lsttbldigreqinfo;
            res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblDiagreq, pobjdiagrequisition);
            return pobjdiagrequisition;
        }
        public DiagnosisRequisitionRepository PostOpRequest(DiagnosisRequisitionRepository pobjdiagrequisition)
        {
            int res = 0;
            tblDiagRequisition LobjtblDiagreq = new tblDiagRequisition();
            tblPatient LobjtblPatient = new tblPatient();

            ObjectCopier.CopyObjectData(pobjdiagrequisition.Patients, LobjtblPatient);
            ObjectCopier.CopyObjectData(pobjdiagrequisition, LobjtblDiagreq);

            LobjtblPatient.CreatedDate = DateTime.Now;
            LobjtblPatient.IsActive = true;

            LobjtblDiagreq.IsActive = true;
            LobjtblDiagreq.CreatedDate = DateTime.Now;

            LobjtblDiagreq.tblPatient = LobjtblPatient;
            _db.tblDiagRequisitions.Add(LobjtblDiagreq);
            res = _db.SaveChanges();

            ObjectCopier.CopyObjectData(LobjtblPatient, pobjdiagrequisition.Patients);
            ObjectCopier.CopyObjectData(LobjtblDiagreq, pobjdiagrequisition);
            return pobjdiagrequisition;
        }
        public List<OpCollectionsReport> GetDailyOpCollectionReport(DateTime PdtOpCollectionReport)
        {
            string str = PdtOpCollectionReport.ToString("yyyy-MM-dd");
            DateTime date = Convert.ToDateTime(str);

            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<OpCollectionsReport> LObjlstOpCollectionReport = new List<OpCollectionsReport>();
            var OpCollectionReport = from tr in LobjHospitalTenantContext.tblTransactions
                                     join p in LobjHospitalTenantContext.tblPatients on tr.PatientID equals p.PatientId
                                     where tr.TransDate == date && p.PatientType == "OP"

                                     select new OpCollectionsReport
                                     {
                                         PatientId = p.PatientId,
                                         PatientName = p.FirstName + " " + p.LastName,
                                         Amount = tr.TotalAmount,
                                         TransDate = tr.TransDate
                                     };
            LObjlstOpCollectionReport = OpCollectionReport.ToList<OpCollectionsReport>();
            return LObjlstOpCollectionReport;
        }
        public List<OpCollectionsReport> OpCollectionReportByDate(DateTime dtFromDate, DateTime dtToDate)
        {

            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<OpCollectionsReport> LObjlstOpCollectionReport = new List<OpCollectionsReport>();
            var OpCollectionReport = from tr in LobjHospitalTenantContext.tblTransactions
                                     join p in LobjHospitalTenantContext.tblPatients on tr.PatientID equals p.PatientId

                                     where tr.TransDate >= dtFromDate && tr.TransDate <= dtToDate && p.PatientType == "OP"

                                     select new OpCollectionsReport
                                     {
                                         PatientId = p.PatientId,
                                         PatientName = p.FirstName + " " + p.LastName,
                                         Amount = tr.TotalAmount,
                                         TransDate = tr.TransDate
                                     };
            LObjlstOpCollectionReport = OpCollectionReport.ToList<OpCollectionsReport>();
            return LObjlstOpCollectionReport;
        }

        public List<DiagnosisRequisitionRepository> GetDailyLabCollection(DateTime date)
        {

            //string str = date.ToString("yyyy-MM-dd");
            //DateTime datee = Convert.ToDateTime(str);

            List<DiagnosisRequisitionRepository> lstLobjtblDiagreq = new List<DiagnosisRequisitionRepository>();
            var res = from dr in _db.tblDiagRequisitions
                      join p in _db.tblPatients on dr.PatientID equals p.PatientId
                      where dr.ReportDate == date

                      select new DiagnosisRequisitionRepository
                      {
                          PatientID = dr.PatientID,
                          DiagReqID = dr.DiagReqID,
                          TotalAmount = dr.TotalAmount,
                          ReportDate = dr.ReportDate
                      };
            lstLobjtblDiagreq = res.ToList<DiagnosisRequisitionRepository>();
            return lstLobjtblDiagreq;
        }

        public List<OpCollectionsReport> OpCollectionReportByMonth(DateTime Date)
        {
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<OpCollectionsReport> LObjlstOpCollectionReport = new List<OpCollectionsReport>();
            var OpCollectionReport = from tr in LobjHospitalTenantContext.tblTransactions
                                     join p in LobjHospitalTenantContext.tblPatients on tr.PatientID equals p.PatientId

                                     where tr.TransDate.Month == Date.Month && tr.TransDate.Year == Date.Year && p.PatientType == "OP"

                                     select new OpCollectionsReport
                                     {
                                         PatientId = p.PatientId,
                                         PatientName = p.FirstName + " " + p.LastName,
                                         Amount = tr.TotalAmount,
                                         TransDate = tr.TransDate
                                     };
            LObjlstOpCollectionReport = OpCollectionReport.ToList<OpCollectionsReport>();
            return LObjlstOpCollectionReport;
        }
        public List<DiagnosisRequisitionRepository> GetLabCollectionByDate(DateTime Frmdate, DateTime Todate)
        {
            //string str = Frmdate.ToString("yyyy-MM-dd");
            //string str1 = Todate.ToString("yyyy-MM-dd");
            //DateTime FromDate = Convert.ToDateTime(str);
            //DateTime TODATE = Convert.ToDateTime(str1);
            List<DiagnosisRequisitionRepository> lstLobjtblDiagreq = new List<DiagnosisRequisitionRepository>();
            var res = from dr in _db.tblDiagRequisitions
                      join p in _db.tblPatients on dr.PatientID equals p.PatientId
                      where dr.ReportDate >= Frmdate && dr.ReportDate <= Todate

                      select new DiagnosisRequisitionRepository
                      {
                          PatientID = dr.PatientID,

                          DiagReqID = dr.DiagReqID,
                          TotalAmount = dr.TotalAmount,
                          ReportDate = dr.ReportDate
                      };
            lstLobjtblDiagreq = res.ToList<DiagnosisRequisitionRepository>();
            return lstLobjtblDiagreq;
        }

        public List<DiagnosisRequisitionRepository> GetMonthlyLabCollection(DateTime dtyear)
        {
            List<DiagnosisRequisitionRepository> lstLobjtblDiagreq = new List<DiagnosisRequisitionRepository>();
            var res = from dr in _db.tblDiagRequisitions
                      join p in _db.tblPatients on dr.PatientID equals p.PatientId
                      where dr.ReportDate.Month == dtyear.Month && dr.ReportDate.Year == dtyear.Year

                      select new DiagnosisRequisitionRepository
                      {
                          PatientID = dr.PatientID,

                          DiagReqID = dr.DiagReqID,
                          TotalAmount = dr.TotalAmount,
                          ReportDate = dr.ReportDate
                      };
            lstLobjtblDiagreq = res.ToList<DiagnosisRequisitionRepository>();
            return lstLobjtblDiagreq;
        }

        public List<DiagnosisRepository> GetTemplates(string pstrsearch)
        {
            List<DiagnosisRepository> LobjDiagRep = new List<DiagnosisRepository>();

            var res = from d in _db.tblDiagnosis
                      join dc in _db.tblDiagnosisComponents on d.DiagnosisID equals dc.DiagnosisID
                      join csr in _db.tblComponentStandardRanges on dc.ComponentID equals csr.ComponentID
                      where d.DiagName.StartsWith(pstrsearch)
                      select new DiagnosisRepository
                      {
                          DiagName = d.DiagName,
                          diagcomp = new DiagnosisComponentsRepository
                          {
                              DiagnosisID = dc.DiagnosisID,
                              ComponentID = dc.ComponentID,
                              CompName = dc.CompName,
                              CompStrdRang = new ComponentStandardRangesRepository
                               {
                                   StandardRange = csr.StandardRange,
                                   MinRange = csr.MinRange,
                                   MaxRange = csr.MaxRange,
                               }
                          }

                      };
            LobjDiagRep = res.ToList<DiagnosisRepository>();

            return LobjDiagRep;
        }

        public DiagnosisComponentsRepository PostTemplate(DiagnosisComponentsRepository PobjCompValue)
        {
            tblComponentValue lobjtblCompvalue = new tblComponentValue();
            ObjectCopier.CopyObjectData(PobjCompValue, lobjtblCompvalue);
            tblDiagnosisComponent LobjtblDiagcomp = new tblDiagnosisComponent();

            lobjtblCompvalue.CreatedDate = DateTime.Now;
            lobjtblCompvalue.IsActive = true;
            _db.tblComponentValues.Add(lobjtblCompvalue);

            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(lobjtblCompvalue, PobjCompValue);
            return PobjCompValue;
        }
        public DiagRequisitionInfoResultRepository PostDiagResults(DiagRequisitionInfoResultRepository PobjDiagReqResult)
        {
            tblDiagRequisitionInfoResult LobjtblDiagReqResult = new tblDiagRequisitionInfoResult();
            ObjectCopier.CopyObjectData(PobjDiagReqResult, LobjtblDiagReqResult);
            LobjtblDiagReqResult.CreatedDate = DateTime.Now;

            _db.tblDiagRequisitionInfoResults.Add(LobjtblDiagReqResult);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblDiagReqResult, PobjDiagReqResult);
            return PobjDiagReqResult;
        }
        public bool PutDiagResults(int id, DiagRequisitionInfoResultRepository PobjDiagReqResult)
        {
            try
            {
                tblDiagRequisitionInfoResult LobjtblDiagReqResult = new tblDiagRequisitionInfoResult();
                LobjtblDiagReqResult = _db.tblDiagRequisitionInfoResults.Where(d => d.DiagReqResultID == id).FirstOrDefault<tblDiagRequisitionInfoResult>();
                if (LobjtblDiagReqResult != null)
                {
                    LobjtblDiagReqResult.ComponentResValue = PobjDiagReqResult.ComponentResValue;
                    LobjtblDiagReqResult.ModifiedBy = PobjDiagReqResult.ModifiedBy;
                    LobjtblDiagReqResult.ModifiedDate = DateTime.Now;
                }

                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public TransactionRepository PostVocherPayment(TransactionRepository pobjtransaction)
        {
            int res = 0;
            tblTransaction Lobjtbltransaction = new tblTransaction();

            ObjectCopier.CopyObjectData(pobjtransaction, Lobjtbltransaction);

            Lobjtbltransaction.CreatedDate = DateTime.Now;

            _db.tblTransactions.Add(Lobjtbltransaction);

            res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(Lobjtbltransaction, pobjtransaction);

            return pobjtransaction;
        }

        public List<TransactionRepository> GetVocherPayment()
        {
            List<TransactionRepository> LObjlstTransactions = new List<TransactionRepository>();

            var varTransactions = from t in _db.tblTransactions

                                  select new TransactionRepository
                                  {
                                      TransID = t.TransID,
                                      HospitalBranchID = t.HospitalBranchID,
                                      MobileNo = t.MobileNo,
                                      Purpose = t.Purpose,
                                      Name = t.Name,
                                      TransDate = t.TransDate,
                                      PatientID = t.PatientID,
                                      EID = t.EID,
                                      CashAmount = t.CashAmount,
                                      CardAmount = t.CardAmount,
                                      TotalAmount = t.TotalAmount,
                                      PaymentMode = t.PaymentMode,
                                      TransType = t.TransType,
                                      PaidTo = t.PaidTo,
                                      TotalPaid = t.TotalPaid,
                                      DueAmount = t.DueAmount

                                  };
            LObjlstTransactions = varTransactions.ToList<TransactionRepository>();
            return LObjlstTransactions;
        }
        public bool PutVocherPayment(int TransID, TransactionRepository pobjtransaction)
        {
            tblTransaction Lobjtransaction = new tblTransaction();

            Lobjtransaction = _db.tblTransactions.Where(t => t.TransID == TransID).FirstOrDefault<tblTransaction>();
            if (Lobjtransaction != null)
            {

                Lobjtransaction.CardAmount = pobjtransaction.CardAmount;
                Lobjtransaction.CashAmount = pobjtransaction.CashAmount;
                Lobjtransaction.DueAmount = pobjtransaction.DueAmount;
                Lobjtransaction.TotalAmount = pobjtransaction.TotalAmount;
                Lobjtransaction.Purpose = pobjtransaction.Purpose;
                Lobjtransaction.PaidTo = pobjtransaction.PaidTo;
                Lobjtransaction.Name = pobjtransaction.Name;
                Lobjtransaction.PaymentMode = pobjtransaction.PaymentMode;
                Lobjtransaction.TotalPaid = pobjtransaction.TotalPaid;
                Lobjtransaction.TransType = pobjtransaction.TransType;
                Lobjtransaction.MobileNo = pobjtransaction.MobileNo;

                Lobjtransaction.ModifiedOn = DateTime.Now;
            }
            int res = _db.SaveChanges();
            if (res >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public List<GetDiagnosisReportRepository> GetDiagnosisReport()
        {
            List<GetDiagnosisReportRepository> LObjlstDiagnosisReport = new List<GetDiagnosisReportRepository>();

            var varDiagnosisReport = from d in _db.tblDiagnosis
                                     join dc in _db.tblDiagnosisComponents on d.DiagnosisID equals dc.DiagnosisID
                                     join cv in _db.tblComponentValues on dc.ComponentID equals cv.ComponentID
                                     join csr in _db.tblComponentStandardRanges on dc.ComponentID equals csr.ComponentID

                                     join drir in _db.tblDiagRequisitionInfoResults on dc.ComponentID equals drir.ComponentID

                                     select new GetDiagnosisReportRepository
                                     {
                                         DiagnosisID = d.DiagnosisID,
                                         DiagName = d.DiagName,
                                         ComponentID = dc.ComponentID,
                                         CompName = dc.CompName,
                                         ValName = cv.ValName,
                                         StandardRange = csr.StandardRange,
                                         MinRange = csr.MinRange,
                                         MaxRange = csr.MaxRange,
                                         Gender = csr.Gender,
                                         MinAge = csr.MinAge,
                                         MaxAge = csr.MaxAge,
                                         ComponentResValue = drir.ComponentResValue
                                     };
            LObjlstDiagnosisReport = varDiagnosisReport.ToList<GetDiagnosisReportRepository>();
            return LObjlstDiagnosisReport;
        }
        public DiagnosisRepository PostDiagnosisComponents(DiagnosisRepository PobjDiagnosis)
        {

            tblDiagnosi objDiagnosis = new tblDiagnosi();
            objDiagnosis = _db.tblDiagnosis.Find(PobjDiagnosis.DiagnosisID);

            List<tblDiagnosisComponent> LstDiagComponentRpository = new List<tblDiagnosisComponent>();
            foreach (DiagnosisComponentsRepository LobComponnet in PobjDiagnosis.lstdiagcomp)
            {
                tblDiagnosisComponent lobjDiagComponent = _db.tblDiagnosisComponents.Create();
                //lobjDiagComponent.DiagnosisID = objDiagnosis.DiagnosisID;
                lobjDiagComponent.CompName = LobComponnet.CompName;
                lobjDiagComponent.Units = LobComponnet.Units;
                lobjDiagComponent.IsActive = true;
                lobjDiagComponent.CreatedBy = LobComponnet.CreatedBy;
                lobjDiagComponent.CreatedDate = DateTime.Now;
                foreach (ComponentValueRepository Lobcompval in LobComponnet.LstCompValue)
                {
                    tblComponentValue LobjtblCompval = _db.tblComponentValues.Create();
                    LobjtblCompval.ValName = Lobcompval.ValName;
                    LobjtblCompval.Comments = Lobcompval.Comments;
                    LobjtblCompval.IsActive = true;
                    LobjtblCompval.CreatedBy = Lobcompval.CreatedBy;
                    LobjtblCompval.CreatedDate = DateTime.Now;
                    lobjDiagComponent.tblComponentValues.Add(LobjtblCompval);
                }
                List<tblComponentStandardRanx> LsttblStandradRange = new List<tblComponentStandardRanx>();
                foreach (ComponentStandardRangesRepository Lobstandrang in LobComponnet.LstCompStrdRang)
                {
                    tblComponentStandardRanx LobjtblStandradrange = _db.tblComponentStandardRanges.Create();
                    //LobjtblStandradrange.ComponentID = lobjDiagComponent.ComponentID;
                    LobjtblStandradrange.Gender = Lobstandrang.Gender;
                    LobjtblStandradrange.StandardRange = Lobstandrang.StandardRange;
                    LobjtblStandradrange.IsActive = true;
                    LobjtblStandradrange.CreatedBy = Lobstandrang.CreatedBy;
                    LobjtblStandradrange.CreatedDate = DateTime.Now;
                    lobjDiagComponent.tblComponentStandardRanges.Add(LobjtblStandradrange);
                }
                //lobjDiagComponent.tblComponentStandardRanges = LsttblStandradRange;


                //LstDiagComponentRpository.Add(lobjDiagComponent);
                objDiagnosis.tblDiagnosisComponents.Add(lobjDiagComponent);
            }
            string str = "";
            try
            {

                int res = _db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {

                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {
                        str += "Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage;
                    }
                }
            }

            return PobjDiagnosis;



        }


        #endregion
    }

}
