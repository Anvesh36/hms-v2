﻿using HMISAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Domain.Entities;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Infrastructure.Data;

namespace HMISAPI.Infrastructure.Data
{
    public class Diagnosis : HMIS.Domain.Interfaces.IDiagnosis
    {
        public List<GetPatientDetailsRepository> GetPatientDetailsByPatientId(GetPatientDetailsRepository PstrSearchTerm)
        {
            string ss = PstrSearchTerm.FirstName.ToString();
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<GetPatientDetailsRepository> LlstPatientDtails = new List<GetPatientDetailsRepository>();
            var PatientDetails = from pd in LobjHospitalTenantContext.tblPatients
                                 where pd.FirstName.StartsWith(ss)
                                 select new GetPatientDetailsRepository
                                 {
                                     PatientID = pd.PatientId,

                                     LastName = pd.LastName,
                                     ContactNumber = pd.ContactNumber,
                                     GuardianName = pd.GuardianName
                                 };
            LlstPatientDtails = PatientDetails.ToList<GetPatientDetailsRepository>();
            return LlstPatientDtails;
        }
        public List<GetAmountByDiagnosisRepository> GetAmountByDiagnosis(GetAmountByDiagnosisRepository PstrSearchTerm)
        {
            string DiagnosisName = PstrSearchTerm.DiagName.ToString();
            HospitalTenantContext LobjHospitalTenantContext = new HospitalTenantContext();
            List<GetAmountByDiagnosisRepository> LlstAmount = new List<GetAmountByDiagnosisRepository>();
            var PatientDetails = from pd in LobjHospitalTenantContext.tblDiagnosis
                                 where pd.DiagName.StartsWith(DiagnosisName)
                                 select new GetAmountByDiagnosisRepository
                                 {
                                     DiagnosisID = pd.DiagnosisID,

                                     Amount = pd.Amount

                                 };
            LlstAmount = PatientDetails.ToList<GetAmountByDiagnosisRepository>();
            return LlstAmount;
        }
    }
}

