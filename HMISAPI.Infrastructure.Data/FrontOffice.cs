﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Domain.Interfaces;
using HMIS.Domain.Interfaces;
using HMISAPI.Infrastructure.Repository;
using HMISAPI.Domain.Entities;

namespace HMISAPI.Infrastructure.Data
{
   public class FrontOffice:IFrontOffice
    {
       HospitalTenantContext _db = new HospitalTenantContext();

       public CallRegisterRepository PostCallRegister(CallRegisterRepository PobjCallRegister)
       {
           tblCallRegister LobjtblCallRegister = new tblCallRegister();
           ObjectCopier.CopyObjectData(PobjCallRegister, LobjtblCallRegister);
           LobjtblCallRegister.CreatedOn = DateTime.Now;
           LobjtblCallRegister.IsActive = true;
           _db.tblCallRegisters.Add(LobjtblCallRegister);
           int res = _db.SaveChanges();
           ObjectCopier.CopyObjectData(LobjtblCallRegister, PobjCallRegister);
           return PobjCallRegister;
       }
       public bool PutCallRegister(int id, CallRegisterRepository PobjCallRegister)
       {
           try
           {
               tblCallRegister LobtblCallRegister = new tblCallRegister();
               LobtblCallRegister = _db.tblCallRegisters.FirstOrDefault(r =>r.CallInteractionID == id);
               if (LobtblCallRegister != null)
               {
                   LobtblCallRegister.CallerName = PobjCallRegister.CallerName;
                   LobtblCallRegister.ContactNo = PobjCallRegister.ContactNo;
                   LobtblCallRegister.CallingFrom = PobjCallRegister.CallingFrom;
                   LobtblCallRegister.Area = PobjCallRegister.Area;
                   LobtblCallRegister.CallType = PobjCallRegister.CallType;
                   LobtblCallRegister.CallDetails = PobjCallRegister.CallDetails;
                   LobtblCallRegister.CallStartDt = PobjCallRegister.CallStartDt;
                   LobtblCallRegister.CallEndDt = PobjCallRegister.CallEndDt;
                   LobtblCallRegister.Remarks = PobjCallRegister.Remarks;
                   LobtblCallRegister.Status = PobjCallRegister.Status;
                   LobtblCallRegister.ModifiedOn = DateTime.Now;
                   LobtblCallRegister.ModifiedBy = PobjCallRegister.ModifiedBy;

               }
               int data = _db.SaveChanges();
               if (data == 1)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

       public List<CallRegisterRepository> GetCallRegInfo()
       {
           List<CallRegisterRepository> LobjCallReg = new List<CallRegisterRepository>();
           var res = from p in _db.tblCallRegisters
                     join dept in _db.tblDepartments on p.DepartmentId equals dept.DeptID
                     join q in _db.tblPatients on p.PatientId equals q.PatientId
                     where p.IsActive == true
                     select new CallRegisterRepository
                     {
                         CallInteractionID = p.CallInteractionID,
                         CallerName = p.CallerName,
                         ContactNo = p.ContactNo,
                         CallingFrom = p.CallingFrom,
                         Area = p.Area,
                         CallType = p.CallType,
                         DepartmentId = p.DepartmentId,
                         CallDetails = p.CallDetails,
                         CallStartDt = p.CallStartDt,
                         CallEndDt = p.CallEndDt,
                         PatientId = p.PatientId,
                         Remarks = p.Remarks,
                         Status = p.Status,
                         DeptInfo = new DepartmentRepository
                         {
                             DeptID = dept.DeptID,
                             DeptName = dept.DeptName,
                             DeptHead = dept.DeptHead
                         },
                         patient = new Patientinfo
                         {
                             PatientID = q.PatientId,
                             PatientType = q.PatientType,
                             FirstName = q.FirstName,
                             Age = q.Age,
                             GenderName = q.Gender
                         }

                     };
           LobjCallReg = res.ToList<CallRegisterRepository>();
           return LobjCallReg;
       }
       public CallRegisterRepository GetCallRegInfo(int id)
       {

           CallRegisterRepository LobjCallReg = new CallRegisterRepository();
           LobjCallReg = (from p in _db.tblCallRegisters
                          join dept in _db.tblDepartments on p.DepartmentId equals dept.DeptID
                          join q in _db.tblPatients on p.PatientId equals q.PatientId
                          where p.IsActive == true && p.CallInteractionID == id
                          select new CallRegisterRepository
                          {
                              CallInteractionID = p.CallInteractionID,
                              CallerName = p.CallerName,
                              ContactNo = p.ContactNo,
                              CallingFrom = p.CallingFrom,
                              Area = p.Area,
                              CallType = p.CallType,
                              DepartmentId = p.DepartmentId,
                              CallDetails = p.CallDetails,
                              CallStartDt = p.CallStartDt,
                              CallEndDt = p.CallEndDt,
                              PatientId = p.PatientId,
                              Remarks = p.Remarks,
                              Status = p.Status,
                              DeptInfo = new DepartmentRepository
                              {
                                  DeptID = dept.DeptID,
                                  DeptName = dept.DeptName,
                                  DeptHead = dept.DeptHead
                              },
                              patient = new Patientinfo
                              {
                                  PatientID = q.PatientId,
                                  PatientType = q.PatientType,
                                  FirstName = q.FirstName,
                                  Age = q.Age,
                                  GenderName = q.Gender
                              }

                          }).FirstOrDefault();

           return LobjCallReg;


       }

       public bool DeleteCallRegInfo(int id)
       {
           try
           {
               tblCallRegister LobjCallReg = new tblCallRegister();
               LobjCallReg = _db.tblCallRegisters.Where(E => E.CallInteractionID == id).FirstOrDefault<tblCallRegister>();
               if (LobjCallReg != null)
               {
                   LobjCallReg.IsActive = false;
                   LobjCallReg.ModifiedOn = DateTime.Now;
               }

               int res = _db.SaveChanges();
               if (res == 1)
               {
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }
    }
}
