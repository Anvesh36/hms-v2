﻿using HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Domain.Entities;
using System.Data.Linq.SqlClient;

namespace HMISAPI.Infrastructure.Data
{
    public class BloodBank : HMISAPI.Domain.Interfaces.IBloodBank
    {
        HospitalTenantContext _db = new HospitalTenantContext();

        #region Donor

        public BBDonorRepository PostDonor(BBDonorRepository PobjBBDonor)
        {
            tblBBDonor LobjtblDonor = new tblBBDonor();
            tblAddress LobjtblAddress = new tblAddress();
            ObjectCopier.CopyObjectData(PobjBBDonor, LobjtblDonor);
            ObjectCopier.CopyObjectData(PobjBBDonor.Address, LobjtblAddress);
            LobjtblDonor.CreatedDate = DateTime.Now;
            LobjtblDonor.IsActive = true;
            LobjtblAddress.CreatedDate = DateTime.Now;
            LobjtblAddress.IsActive = true;
            _db.tblBBDonors.Add(LobjtblDonor);
            _db.tblAddresses.Add(LobjtblAddress);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblDonor, PobjBBDonor);
            ObjectCopier.CopyObjectData(LobjtblAddress, PobjBBDonor.Address);
            return PobjBBDonor;

        }
        public List<BBDonorRepository> GetDonor()
        {
            List<BBDonorRepository> LstLobjdonor = new List<BBDonorRepository>();
            var res = from donors in _db.tblBBDonors
                      join addr
                          in _db.tblAddresses on donors.AddressID equals addr.AddressID
                      where donors.IsActive == true
                      select new BBDonorRepository
                      {
                          DonorId = donors.DonorId,
                          DonorName = donors.DonorName,
                          Gender = donors.Gender,
                          DOB = donors.DOB,
                          Age = donors.Age,
                          Occupation = donors.Occupation,
                          MaritalStatus = donors.MaritalStatus,
                          DonationType = donors.DonationType,
                          BloodGroup = donors.BloodGroup,
                          DonorSource = donors.DonorSource,
                          OldRefNo = donors.OldRefNo,
                          MobileNo = donors.MobileNo,
                          Email = donors.Email,
                          Address = new AddressRepository
                          {
                              AddressLine1 = addr.AddressLine1,
                              AddressLine2 = addr.AddressLine2,
                              State = addr.State,
                              Country = addr.Country,
                              ZipCode = addr.ZipCode,
                              City = addr.City

                          }
                      };

            LstLobjdonor = res.ToList<BBDonorRepository>();
            return LstLobjdonor;
        }

        public BBDonorRepository GetDonor(int id)
        {
            BBDonorRepository LstLobjdonor = new BBDonorRepository();
            LstLobjdonor = (from donors in _db.tblBBDonors
                            join addr
                                in _db.tblAddresses on donors.AddressID equals addr.AddressID
                            where donors.IsActive == true && donors.DonorId == id
                            select new BBDonorRepository
                            {
                                DonorId = donors.DonorId,
                                DonorName = donors.DonorName,
                                Gender = donors.Gender,
                                DOB = donors.DOB,
                                Age = donors.Age,
                                Occupation = donors.Occupation,
                                MaritalStatus = donors.MaritalStatus,
                                DonationType = donors.DonationType,
                                BloodGroup = donors.BloodGroup,
                                DonorSource = donors.DonorSource,
                                OldRefNo = donors.OldRefNo,
                                MobileNo = donors.MobileNo,
                                Email = donors.Email,
                                Address = new AddressRepository
                                {
                                    AddressLine1 = addr.AddressLine1,
                                    AddressLine2 = addr.AddressLine2,
                                    State = addr.State,
                                    Country = addr.Country,
                                    ZipCode = addr.ZipCode,
                                    City = addr.City

                                }
                            }).FirstOrDefault<BBDonorRepository>();


            return LstLobjdonor;
        }
        public bool PutDonor(int id, BBDonorRepository PobjBBDonor)
        {
            try
            {
                tblBBDonor LobtblDonor = new tblBBDonor();
                LobtblDonor = _db.tblBBDonors.Where(P => P.DonorId == id).FirstOrDefault<tblBBDonor>();
                if (LobtblDonor != null)
                {
                    LobtblDonor.DonorName = PobjBBDonor.DonorName;
                    LobtblDonor.Gender = PobjBBDonor.Gender;
                    LobtblDonor.DOB = PobjBBDonor.DOB;
                    LobtblDonor.Age = PobjBBDonor.Age;
                    LobtblDonor.Occupation = PobjBBDonor.Occupation;
                    LobtblDonor.MaritalStatus = PobjBBDonor.MaritalStatus;
                    LobtblDonor.DonationType = PobjBBDonor.DonationType;
                    LobtblDonor.BloodGroup = PobjBBDonor.BloodGroup;
                    LobtblDonor.DonorSource = PobjBBDonor.DonorSource;
                    LobtblDonor.OldRefNo = PobjBBDonor.OldRefNo;
                    LobtblDonor.MobileNo = PobjBBDonor.MobileNo;
                    LobtblDonor.Email = PobjBBDonor.Email;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region PhysicalExamination
        public BBPhysicalExaminationRepository PostPhyExam(BBPhysicalExaminationRepository Pobjphyex)
        {
            tblBBPhysicalExamination Lobjtblphyexam = new tblBBPhysicalExamination();
            ObjectCopier.CopyObjectData(Pobjphyex, Lobjtblphyexam);
            Lobjtblphyexam.CreatedDate = DateTime.Now;
            Lobjtblphyexam.IsActive = true;
            _db.tblBBPhysicalExaminations.Add(Lobjtblphyexam);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(Lobjtblphyexam, Pobjphyex);
            return Pobjphyex;
        }
        public List<BBPhysicalExaminationRepository> GetPhyExam()
        {
            List<BBPhysicalExaminationRepository> LobjPhyexam = new List<BBPhysicalExaminationRepository>();
            var res = from phyexa in _db.tblBBPhysicalExaminations
                      where phyexa.IsActive == true
                      select new BBPhysicalExaminationRepository
                      {
                          PhyExamID = phyexa.PhyExamID,
                          DonationId = phyexa.DonationId,
                          Heamoglobin = phyexa.Heamoglobin,
                          Temperature = phyexa.Temperature,
                          Weight = phyexa.Weight,
                          Pulse = phyexa.Pulse,
                          Systolic = phyexa.Systolic,
                          Diastolic = phyexa.Diastolic,
                          RhType = phyexa.RhType,
                          BGbyDonoar = phyexa.BGbyDonoar,
                          BGafterScreening = phyexa.BGafterScreening,
                          BagType = phyexa.BagType,
                          BagVolume = phyexa.BagVolume,
                          Remarks = phyexa.Remarks,
                          DeferralType = phyexa.DeferralType,
                          DeferralRemarks = phyexa.DeferralRemarks

                      };

            LobjPhyexam = res.ToList<BBPhysicalExaminationRepository>();
            return LobjPhyexam;
        }

        public BBPhysicalExaminationRepository GetPhyExam(int id)
        {
            BBPhysicalExaminationRepository LobjPhyexam = new BBPhysicalExaminationRepository();
            LobjPhyexam = (from phyexa in _db.tblBBPhysicalExaminations
                           where phyexa.IsActive == true && phyexa.PhyExamID == id
                           select new BBPhysicalExaminationRepository
                           {
                               PhyExamID = phyexa.PhyExamID,
                               DonationId = phyexa.DonationId,
                               Heamoglobin = phyexa.Heamoglobin,
                               Temperature = phyexa.Temperature,
                               Weight = phyexa.Weight,
                               Pulse = phyexa.Pulse,
                               Systolic = phyexa.Systolic,
                               Diastolic = phyexa.Diastolic,
                               RhType = phyexa.RhType,
                               BGbyDonoar = phyexa.BGbyDonoar,
                               BGafterScreening = phyexa.BGafterScreening,
                               BagType = phyexa.BagType,
                               BagVolume = phyexa.BagVolume,
                               Remarks = phyexa.Remarks,
                               DeferralType = phyexa.DeferralType,
                               DeferralRemarks = phyexa.DeferralRemarks

                           }).FirstOrDefault<BBPhysicalExaminationRepository>();


            return LobjPhyexam;
        }
        public bool PutPhyExam(int id, BBPhysicalExaminationRepository Pobjphyex)
        {
            try
            {
                tblBBPhysicalExamination LobtblPhyexm = new tblBBPhysicalExamination();
                LobtblPhyexm = _db.tblBBPhysicalExaminations.Where(P => P.PhyExamID == id).FirstOrDefault<tblBBPhysicalExamination>();
                if (LobtblPhyexm != null)
                {
                    LobtblPhyexm.Heamoglobin = Pobjphyex.Heamoglobin;
                    LobtblPhyexm.Temperature = Pobjphyex.Temperature;
                    LobtblPhyexm.Weight = Pobjphyex.Weight;
                    LobtblPhyexm.Pulse = Pobjphyex.Pulse;
                    LobtblPhyexm.Systolic = Pobjphyex.Systolic;
                    LobtblPhyexm.Diastolic = Pobjphyex.Diastolic;
                    LobtblPhyexm.RhType = Pobjphyex.RhType;
                    LobtblPhyexm.BGbyDonoar = Pobjphyex.BGbyDonoar;
                    LobtblPhyexm.BGafterScreening = Pobjphyex.BGafterScreening;
                    LobtblPhyexm.BagType = Pobjphyex.BagType;
                    LobtblPhyexm.BagVolume = Pobjphyex.BagVolume;
                    LobtblPhyexm.Remarks = Pobjphyex.Remarks;
                    LobtblPhyexm.DeferralType = Pobjphyex.DeferralType;
                    LobtblPhyexm.DeferralRemarks = Pobjphyex.DeferralRemarks;

                    LobtblPhyexm.ModifiedBy = Pobjphyex.ModifiedBy;
                    LobtblPhyexm.ModifiedDate = DateTime.Now;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Refrigerator
        public List<BBRefrigeratorRepository> GetRefrigerator(int? id)
        {
            List<BBRefrigeratorRepository> LobjRefrig = new List<BBRefrigeratorRepository>();
            var res = from refrig in _db.tblBBRefrigerators
                      where refrig.IsActive == true
                      select new BBRefrigeratorRepository
                      {
                          RefrigratorName = refrig.RefrigratorName,
                          ProductSize = refrig.ProductSize,
                          Capacity = refrig.Capacity,
                          NoofRacks = refrig.NoofRacks,
                          Totalpackets = refrig.Totalpackets,
                          ElectricalReq = refrig.ElectricalReq,
                          Price = refrig.Price,
                          Purchasedon = refrig.Purchasedon,
                          WarrantyTilldate = refrig.WarrantyTilldate,
                          Refrigeratorid = refrig.Refrigeratorid

                      };
            if (id != null)
            {
                res = res.Where(p => p.Refrigeratorid == id);
            }
            LobjRefrig = res.ToList<BBRefrigeratorRepository>();
            return LobjRefrig;
        }
        public bool PutRefrigerator(int id, BBRefrigeratorRepository Pobjref)
        {
            try
            {
                tblBBRefrigerator Lobtblrefrig = new tblBBRefrigerator();
                Lobtblrefrig = _db.tblBBRefrigerators.Where(R => R.Refrigeratorid == id).FirstOrDefault<tblBBRefrigerator>();
                if (Lobtblrefrig != null)
                {
                    Lobtblrefrig.RefrigratorName = Pobjref.RefrigratorName;
                    Lobtblrefrig.ProductSize = Pobjref.ProductSize;
                    Lobtblrefrig.Capacity = Pobjref.Capacity;
                    Lobtblrefrig.NoofRacks = Pobjref.NoofRacks;
                    Lobtblrefrig.Totalpackets = Pobjref.Totalpackets;
                    Lobtblrefrig.ElectricalReq = Pobjref.ElectricalReq;
                    Lobtblrefrig.Price = Pobjref.Price;
                    Lobtblrefrig.Purchasedon = Pobjref.Purchasedon;
                    Lobtblrefrig.WarrantyTilldate = Pobjref.WarrantyTilldate;
                    Lobtblrefrig.ModifiedBy = Pobjref.ModifiedBy;
                    Lobtblrefrig.ModifiedDate = DateTime.Now;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BBRefrigeratorRepository PostRefrigerator(BBRefrigeratorRepository Pobjref)
        {
            tblBBRefrigerator LobjRefr = new tblBBRefrigerator();
            ObjectCopier.CopyObjectData(Pobjref, LobjRefr);
            LobjRefr.CreatedDate = DateTime.Now;
            LobjRefr.IsActive = true;
            _db.tblBBRefrigerators.Add(LobjRefr);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjRefr, Pobjref);
            return Pobjref;
        }

        #endregion

        #region CellGrDetail

        public BBGroupingCellRepository PostCellGrDetail(BBGroupingCellRepository PobjgrCell)
        {
            tblBBGroupingCell LobjtblBBgrcell = new tblBBGroupingCell();
            ObjectCopier.CopyObjectData(PobjgrCell, LobjtblBBgrcell);
            LobjtblBBgrcell.Createdon = DateTime.Now;
            LobjtblBBgrcell.IsActive = true;
            _db.tblBBGroupingCells.Add(LobjtblBBgrcell);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblBBgrcell, PobjgrCell);
            return PobjgrCell;
        }
        public List<BBGroupingCellRepository> GetCellGroup()
        {

            List<BBGroupingCellRepository> LlstCellGr = new List<BBGroupingCellRepository>();
            var res = from grouping in _db.tblBBGroupingCells
                      where grouping.IsActive == true

                      select new BBGroupingCellRepository
                      {
                          BBGroupingcellid = grouping.BBGroupingcellid,
                          Donationid = grouping.Donationid,
                          AntiA = grouping.AntiA,
                          AntiB = grouping.AntiB,
                          AntiAB = grouping.AntiAB,
                          AntiD = grouping.AntiD,
                          AntiH = grouping.AntiH,
                          AntiA1 = grouping.AntiA1,
                          BloodGroup = grouping.BloodGroup,
                          RHType = grouping.RHType,
                          IrregulatorAB = grouping.IrregulatorAB,
                          Assignedto = grouping.Assignedto,


                      };

            LlstCellGr = res.ToList<BBGroupingCellRepository>();
            return LlstCellGr;
        }
        public BBGroupingCellRepository GetCellGroup(int id)
        {

            BBGroupingCellRepository LlstCellGr = new BBGroupingCellRepository();
            LlstCellGr = (from grouping in _db.tblBBGroupingCells
                          where grouping.IsActive == true && grouping.BBGroupingcellid == id

                          select new BBGroupingCellRepository
                          {
                              BBGroupingcellid = grouping.BBGroupingcellid,
                              Donationid = grouping.Donationid,
                              AntiA = grouping.AntiA,
                              AntiB = grouping.AntiB,
                              AntiAB = grouping.AntiAB,
                              AntiD = grouping.AntiD,
                              AntiH = grouping.AntiH,
                              AntiA1 = grouping.AntiA1,
                              BloodGroup = grouping.BloodGroup,
                              RHType = grouping.RHType,
                              IrregulatorAB = grouping.IrregulatorAB,
                              Assignedto = grouping.Assignedto,
                          }).FirstOrDefault<BBGroupingCellRepository>();

            return LlstCellGr;
        }
        public bool PutCellGrDetail(int id, BBGroupingCellRepository PobjgrCell)
        {
            try
            {
                tblBBGroupingCell LobtblGrCell = new tblBBGroupingCell();
                LobtblGrCell = _db.tblBBGroupingCells.Where(E => E.BBGroupingcellid == id).FirstOrDefault<tblBBGroupingCell>();
                if (LobtblGrCell != null)
                {
                    LobtblGrCell.AntiA = PobjgrCell.AntiA;
                    LobtblGrCell.AntiB = PobjgrCell.AntiB;
                    LobtblGrCell.AntiAB = PobjgrCell.AntiAB;
                    LobtblGrCell.AntiD = PobjgrCell.AntiD;
                    LobtblGrCell.AntiH = PobjgrCell.AntiH;
                    LobtblGrCell.AntiA1 = PobjgrCell.AntiA1;
                    LobtblGrCell.BloodGroup = PobjgrCell.BloodGroup;
                    LobtblGrCell.RHType = PobjgrCell.RHType;
                    LobtblGrCell.IrregulatorAB = PobjgrCell.IrregulatorAB;
                    LobtblGrCell.Assignedto = PobjgrCell.Assignedto;
                    LobtblGrCell.ModifiedBy = PobjgrCell.ModifiedBy;
                    LobtblGrCell.ModifiedDate = DateTime.Now;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region serumGroup
        public BBGroupingSerumRepository PostBBSerumGroup(BBGroupingSerumRepository PobjgrSerum)
        {
            tblBBGroupingSerum LobjtblBBgrSerum = new tblBBGroupingSerum();
            ObjectCopier.CopyObjectData(PobjgrSerum, LobjtblBBgrSerum);
            LobjtblBBgrSerum.CreatedDate = DateTime.Now;
            LobjtblBBgrSerum.IsActive = true;
            _db.tblBBGroupingSerums.Add(LobjtblBBgrSerum);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblBBgrSerum, PobjgrSerum);
            return PobjgrSerum;

        }

        public List<BBGroupingSerumRepository> GetBBSerumGroup()
        {

            List<BBGroupingSerumRepository> LlstSerumGr = new List<BBGroupingSerumRepository>();
            var res = from p in _db.tblBBGroupingSerums
                      where p.IsActive == true
                      select new BBGroupingSerumRepository
                      {
                          BBGroupingSerumid = p.BBGroupingSerumid,
                          Donationid = p.Donationid,
                          A = p.A,
                          B = p.B,
                          O = p.O,
                          SerumGroup = p.SerumGroup,
                          IrregulatorAB = p.IrregulatorAB,
                          Assignedto = p.Assignedto,
                          Remarks = p.Remarks,

                      };

            LlstSerumGr = res.ToList<BBGroupingSerumRepository>();
            return LlstSerumGr;
        }

        public BBGroupingSerumRepository GetBBSerumGroup(int id)
        {

            BBGroupingSerumRepository LlstSerumGr = new BBGroupingSerumRepository();
            var res = (from p in _db.tblBBGroupingSerums
                       where p.IsActive == true && p.BBGroupingSerumid == id
                       select new BBGroupingSerumRepository
                       {
                           BBGroupingSerumid = p.BBGroupingSerumid,
                           Donationid = p.Donationid,
                           A = p.A,
                           B = p.B,
                           O = p.O,
                           SerumGroup = p.SerumGroup,
                           IrregulatorAB = p.IrregulatorAB,
                           Assignedto = p.Assignedto,
                           Remarks = p.Remarks,

                       }).FirstOrDefault<BBGroupingSerumRepository>();

            return LlstSerumGr;
        }

        public bool PutSerumGrDetail(int id, BBGroupingSerumRepository PobjgrSerum)
        {
            try
            {
                tblBBGroupingSerum LobtblGrSerum = new tblBBGroupingSerum();
                LobtblGrSerum = _db.tblBBGroupingSerums.Where(E => E.BBGroupingSerumid == id).FirstOrDefault<tblBBGroupingSerum>();
                if (LobtblGrSerum != null)
                {
                    LobtblGrSerum.A = PobjgrSerum.A;
                    LobtblGrSerum.B = PobjgrSerum.B;
                    LobtblGrSerum.O = PobjgrSerum.O;
                    LobtblGrSerum.SerumGroup = PobjgrSerum.SerumGroup;
                    LobtblGrSerum.IrregulatorAB = PobjgrSerum.IrregulatorAB;
                    LobtblGrSerum.Assignedto = PobjgrSerum.Assignedto;

                    LobtblGrSerum.ModifiedBy = PobjgrSerum.ModifiedBy;
                    LobtblGrSerum.ModifiedDate = DateTime.Now;
                    LobtblGrSerum.Remarks = PobjgrSerum.Remarks;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Questionnaire
        public BBQuestionnaireRepository PostQuestionnaire(BBQuestionnaireRepository PobjQue)
        {
            tblBBQuestionnaire LobjQue = new tblBBQuestionnaire();
            ObjectCopier.CopyObjectData(PobjQue, LobjQue);
            LobjQue.Createdon = DateTime.Now;
            LobjQue.IsActive = true;
            _db.tblBBQuestionnaires.Add(LobjQue);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjQue, PobjQue);
            return PobjQue;
        }
        public List<BBQuestionnaireRepository> GetBBQuestionnaire()
        {
            List<BBQuestionnaireRepository> ListobjQue = new List<BBQuestionnaireRepository>();
            var res = from p in _db.tblBBQuestionnaires
                      where p.IsActive == true
                      select new BBQuestionnaireRepository
                      {
                          QuetID = p.QuetID,
                          Donationid = p.Donationid,
                          Question = p.Question,
                          Answer = p.Answer,
                          Remarks = p.Remarks,

                      };

            ListobjQue = res.ToList<BBQuestionnaireRepository>();
            return ListobjQue;
        }
        public BBQuestionnaireRepository GetBBQuestionnaire(int id)
        {
            BBQuestionnaireRepository ListobjQue = new BBQuestionnaireRepository();
            ListobjQue = (from p in _db.tblBBQuestionnaires
                          where p.IsActive == true && p.QuetID == id
                          select new BBQuestionnaireRepository
                          {
                              QuetID = p.QuetID,
                              Donationid = p.Donationid,
                              Question = p.Question,
                              Answer = p.Answer,
                              Remarks = p.Remarks,

                          }).FirstOrDefault<BBQuestionnaireRepository>();


            return ListobjQue;
        }
        public bool PutQuestionnaire(int id, BBQuestionnaireRepository PobjQue)
        {
            try
            {
                tblBBQuestionnaire LobjQue = new tblBBQuestionnaire();
                LobjQue = _db.tblBBQuestionnaires.Where(E => E.QuetID == id).FirstOrDefault<tblBBQuestionnaire>();
                if (LobjQue != null)
                {
                    LobjQue.Question = PobjQue.Question;
                    LobjQue.Answer = PobjQue.Answer;
                    LobjQue.Remarks = PobjQue.Remarks;
                    LobjQue.ModifiedBy = PobjQue.ModifiedBy;
                    LobjQue.Modifiedon = DateTime.Now;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region BloodRequest

        public BBTransfusionRequestRepository PostTransfusionReq(BBTransfusionRequestRepository PobjTreq)
        {
            tblBBTransfusionRequest LobjTreq = new tblBBTransfusionRequest();
            ObjectCopier.CopyObjectData(PobjTreq, LobjTreq);
            LobjTreq.CreatedOn = DateTime.Now;
            LobjTreq.IsActive = true;
            _db.tblBBTransfusionRequests.Add(LobjTreq);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjTreq, PobjTreq);
            return PobjTreq;
        }
        public List<BBTransfusionRequestRepository> GetTransfusionReq()
        {
            List<BBTransfusionRequestRepository> LobjTreq = new List<BBTransfusionRequestRepository>();
            var res = from p in _db.tblBBTransfusionRequests
                      select new BBTransfusionRequestRepository
                      {
                          RequestID = p.RequestID,
                          Patientid = p.Patientid,
                          PatientType = p.PatientType,
                          BloodGroup = p.BloodGroup,
                          Component = p.Component,
                          NoOfUnits = p.NoOfUnits,
                          Attachments = p.Attachments,
                          isCrossMatch = p.isCrossMatch,
                          RequestedBy = p.RequestedBy,
                          AssignTo = p.AssignTo,
                          ReqStatus = p.ReqStatus,

                      };
            LobjTreq = res.ToList<BBTransfusionRequestRepository>();
            return LobjTreq;
        }
        public BBTransfusionRequestRepository GetTransfusionReq(int id)
        {
            BBTransfusionRequestRepository Lobjreq = new BBTransfusionRequestRepository();
            Lobjreq = (from p in _db.tblBBTransfusionRequests
                       where p.IsActive == true && p.RequestID == id
                       select new BBTransfusionRequestRepository
                       {
                           RequestID = p.RequestID,
                           Patientid = p.Patientid,
                           PatientType = p.PatientType,
                           BloodGroup = p.BloodGroup,
                           Component = p.Component,
                           NoOfUnits = p.NoOfUnits,
                           Attachments = p.Attachments,
                           isCrossMatch = p.isCrossMatch,
                           RequestedBy = p.RequestedBy,
                           AssignTo = p.AssignTo,
                           ReqStatus = p.ReqStatus
                       }).FirstOrDefault<BBTransfusionRequestRepository>();

            return Lobjreq;
        }
        public bool PutTransfusionReq(int id, BBTransfusionRequestRepository PobjTreq)
        {
            try
            {
                tblBBTransfusionRequest LobjTreq = new tblBBTransfusionRequest();
                LobjTreq = _db.tblBBTransfusionRequests.Where(E => E.RequestID == id).FirstOrDefault<tblBBTransfusionRequest>();
                if (LobjTreq != null)
                {
                    LobjTreq.PatientType = PobjTreq.PatientType;
                    LobjTreq.BloodGroup = PobjTreq.BloodGroup;
                    LobjTreq.Component = PobjTreq.Component;
                    LobjTreq.NoOfUnits = PobjTreq.NoOfUnits;
                    LobjTreq.Attachments = PobjTreq.Attachments;
                    LobjTreq.isCrossMatch = PobjTreq.isCrossMatch;
                    LobjTreq.RequestedBy = PobjTreq.RequestedBy;
                    LobjTreq.AssignTo = PobjTreq.AssignTo;
                    LobjTreq.ReqStatus = PobjTreq.ReqStatus;
                    // LobjTreq.IsActive = PobjTreq.IsActive;
                    LobjTreq.ModifiedBy = PobjTreq.ModifiedBy;
                    LobjTreq.ModifiedOn = DateTime.Now;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteBBTransfusionReq(int id)
        {
            try
            {
                tblBBTransfusionRequest LobtblBBTransReq = new tblBBTransfusionRequest();
                LobtblBBTransReq = _db.tblBBTransfusionRequests.Where(E => E.RequestID == id).FirstOrDefault<tblBBTransfusionRequest>();
                if (LobtblBBTransReq != null)
                {
                    LobtblBBTransReq.IsActive = false;
                    LobtblBBTransReq.ModifiedOn = DateTime.Now;
                }

                int res = _db.SaveChanges();
                if (res == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region BBSetting

        public BBSettingRepository PostBBSetting(BBSettingRepository PobjBBSet)
        {
            tblBBSetting LobjBBSet = new tblBBSetting();
            ObjectCopier.CopyObjectData(PobjBBSet, LobjBBSet);
            LobjBBSet.CreatedOn = DateTime.Now;
            LobjBBSet.IsActive = true;
            _db.tblBBSettings.Add(LobjBBSet);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjBBSet, PobjBBSet);
            return PobjBBSet;

        }
        public List<BBSettingRepository> GetBBSetting()
        {
            List<BBSettingRepository> LobjBBSet = new List<BBSettingRepository>();
            var res = from p in _db.tblBBSettings
                      join addr
                          in _db.tblAddresses on p.AddressID equals addr.AddressID
                      join bb in _db.tblBloodBanks on p.BloodbankId equals bb.Bloodbankid

                      where p.IsActive == true

                      select new BBSettingRepository
                      {
                          BBSettingID = p.BBSettingID,

                          DisplayName = p.DisplayName,
                          AuthDrName = p.AuthDrName,
                          BagsCapacity = p.BagsCapacity,
                          NoOfRefrigrators = p.NoOfRefrigrators,
                          establishedOn = p.establishedOn,
                          LicenceNum = p.LicenceNum,
                          BloodBank = new BloodBankRepository
                          {
                              Bloodbankid = bb.Bloodbankid,
                              Floorid = bb.Floorid,
                              Hospitalid = bb.Hospitalid,
                              Name = bb.InchargeName,
                              Inchargeno = bb.Inchargeno,
                              OperatingTimes = bb.OperatingTimes,
                              BBShortCode = bb.BBShortCode
                          },
                          address = new AddressRepository
                          {
                              AddressID = addr.AddressID,
                              AddressLine1 = addr.AddressLine1,
                              AddressLine2 = addr.AddressLine2,
                              State = addr.State,
                              City = addr.City,
                              ZipCode = addr.ZipCode,
                              Country = addr.Country,
                          }
                      };

            LobjBBSet = res.ToList<BBSettingRepository>();
            return LobjBBSet;
        }
        public BBSettingRepository GetBBSetting(int id)
        {
            BBSettingRepository LobjBBSet = new BBSettingRepository();
            LobjBBSet = (from p in _db.tblBBSettings
                         join addr
                             in _db.tblAddresses on p.AddressID equals addr.AddressID
                         join bb in _db.tblBloodBanks on p.BloodbankId equals bb.Bloodbankid

                         where p.IsActive == true && p.BBSettingID == id

                         select new BBSettingRepository
                         {
                             BBSettingID = p.BBSettingID,

                             DisplayName = p.DisplayName,
                             AuthDrName = p.AuthDrName,
                             BagsCapacity = p.BagsCapacity,
                             NoOfRefrigrators = p.NoOfRefrigrators,
                             establishedOn = p.establishedOn,
                             LicenceNum = p.LicenceNum,
                             BloodBank = new BloodBankRepository
                             {
                                 Bloodbankid = bb.Bloodbankid,
                                 Floorid = bb.Floorid,
                                 Hospitalid = bb.Hospitalid,
                                 Name = bb.InchargeName,
                                 Inchargeno = bb.Inchargeno,
                                 OperatingTimes = bb.OperatingTimes,
                                 BBShortCode = bb.BBShortCode
                             },
                             address = new AddressRepository
                             {
                                 AddressID = addr.AddressID,
                                 AddressLine1 = addr.AddressLine1,
                                 AddressLine2 = addr.AddressLine2,
                                 State = addr.State,
                                 City = addr.City,
                                 ZipCode = addr.ZipCode,
                                 Country = addr.Country,
                             }
                         }).FirstOrDefault<BBSettingRepository>();


            return LobjBBSet;
        }
        public bool PutBBSetting(int id, BBSettingRepository PobjBBSet)
        {
            try
            {
                tblBBSetting LobjBBSet = new tblBBSetting();
                LobjBBSet = _db.tblBBSettings.Where(E => E.BBSettingID == id).FirstOrDefault<tblBBSetting>();
                if (LobjBBSet != null)
                {
                    LobjBBSet.DisplayName = PobjBBSet.DisplayName;
                    LobjBBSet.logo = PobjBBSet.logo;
                    LobjBBSet.AuthDrName = PobjBBSet.AuthDrName;
                    LobjBBSet.BagsCapacity = PobjBBSet.BagsCapacity;
                    LobjBBSet.NoOfRefrigrators = PobjBBSet.NoOfRefrigrators;
                    LobjBBSet.establishedOn = PobjBBSet.establishedOn;
                    LobjBBSet.LicenceNum = PobjBBSet.LicenceNum;
                    LobjBBSet.ModifiedBy = PobjBBSet.ModifiedBy;
                    LobjBBSet.ModifiedOn = DateTime.Now;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteBBSetting(int id)
        {
            try
            {
                tblBBSetting LobjBBSet = new tblBBSetting();
                LobjBBSet = _db.tblBBSettings.Where(E => E.BBSettingID == id).FirstOrDefault<tblBBSetting>();
                if (LobjBBSet != null)
                {
                    LobjBBSet.IsActive = false;
                    LobjBBSet.ModifiedOn = DateTime.Now;
                }

                int res = _db.SaveChanges();
                if (res == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Mandatory
        public List<BBMandatoryTestrRepository> GetBBMandatoryTest()
        {
            List<BBMandatoryTestrRepository> LobjBBMandatory = new List<BBMandatoryTestrRepository>();
            var mandatorytest = from m in _db.tblBBMandatoryTests
                                where m.IsActive == true

                                select new BBMandatoryTestrRepository
                                {
                                    MandatoryTestID = m.MandatoryTestID,
                                    Donationid = m.Donationid,
                                    HIV1 = m.HIV1,
                                    HIV2 = m.HIV2,
                                    HBSAG = m.HBSAG,
                                    HCV = m.HCV,
                                    VDRL = m.VDRL,
                                    MP = m.MP,
                                    TestResult = m.TestResult,
                                    Remarks = m.Remarks,
                                    IsActive = m.IsActive
                                };
            LobjBBMandatory = mandatorytest.ToList<BBMandatoryTestrRepository>();
            return LobjBBMandatory;
        }
        public BBMandatoryTestrRepository PostBBMandatoryTest(BBMandatoryTestrRepository PobjBBMandatoryTest)
        {
            tblBBMandatoryTest LobjtblMandatoryTest = new tblBBMandatoryTest();
            ObjectCopier.CopyObjectData(PobjBBMandatoryTest, LobjtblMandatoryTest);
            LobjtblMandatoryTest.CreatedOn = DateTime.Now;
            LobjtblMandatoryTest.IsActive = true;
            _db.tblBBMandatoryTests.Add(LobjtblMandatoryTest);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblMandatoryTest, PobjBBMandatoryTest);
            return PobjBBMandatoryTest;
        }
        public bool PutBBMandatoryTest(int id, BBMandatoryTestrRepository PobjMandatory)
        {
            try
            {
                tblBBMandatoryTest LobtblBBMandatory = new tblBBMandatoryTest();
                LobtblBBMandatory = _db.tblBBMandatoryTests.Where(m => m.MandatoryTestID == id).FirstOrDefault<tblBBMandatoryTest>();
                if (LobtblBBMandatory != null)
                {
                    LobtblBBMandatory.HIV1 = PobjMandatory.HIV1;
                    LobtblBBMandatory.HIV2 = PobjMandatory.HIV2;
                    LobtblBBMandatory.HBSAG = PobjMandatory.HBSAG;
                    LobtblBBMandatory.HCV = PobjMandatory.HCV;
                    LobtblBBMandatory.VDRL = PobjMandatory.VDRL;
                    LobtblBBMandatory.MP = PobjMandatory.MP;
                    LobtblBBMandatory.TestResult = PobjMandatory.TestResult;
                    LobtblBBMandatory.Remarks = PobjMandatory.Remarks;
                    LobtblBBMandatory.ModifiedOn = DateTime.Now;
                    LobtblBBMandatory.ModifiedBy = PobjMandatory.ModifiedBy;

                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region HaemotologyTest
        public List<BBHaemotologyTestRepository> GetBBHaemotologyTest()
        {
            List<BBHaemotologyTestRepository> LobjBBHaemotology = new List<BBHaemotologyTestRepository>();
            var HaemotologyTest = from h in _db.tblBBHaemotologyTests
                                  where h.IsActive == true

                                  select new BBHaemotologyTestRepository
                                  {
                                      HaemotologyID = h.HaemotologyID,
                                      Donationid = h.Donationid,
                                      Heamoglobin = h.Heamoglobin,
                                      PlateletCount = h.PlateletCount,
                                      WBCCount = h.WBCCount,
                                      MP = h.MP,
                                      G6PD = h.G6PD,
                                      Remarks = h.Remarks,
                                      IsActive = h.IsActive
                                  };
            LobjBBHaemotology = HaemotologyTest.ToList<BBHaemotologyTestRepository>();
            return LobjBBHaemotology;
        }

        public BBHaemotologyTestRepository PostBBHaemotologyTest(BBHaemotologyTestRepository PobjBBHaemotologyTest)
        {
            tblBBHaemotologyTest LobjtblHaemotologyTest = new tblBBHaemotologyTest();
            ObjectCopier.CopyObjectData(PobjBBHaemotologyTest, LobjtblHaemotologyTest);
            LobjtblHaemotologyTest.CreatedOn = DateTime.Now;
            LobjtblHaemotologyTest.IsActive = true;
            _db.tblBBHaemotologyTests.Add(LobjtblHaemotologyTest);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblHaemotologyTest, PobjBBHaemotologyTest);
            return PobjBBHaemotologyTest;
        }
        public bool PutBBHaemotologyTest(int id, BBHaemotologyTestRepository PobjBBHaemotologyTest)
        {
            try
            {
                tblBBHaemotologyTest LobtblBBHaemotology = new tblBBHaemotologyTest();
                LobtblBBHaemotology = _db.tblBBHaemotologyTests.Where(h => h.HaemotologyID == id).FirstOrDefault<tblBBHaemotologyTest>();
                if (LobtblBBHaemotology != null)
                {
                    LobtblBBHaemotology.Heamoglobin = PobjBBHaemotologyTest.Heamoglobin;
                    LobtblBBHaemotology.PlateletCount = PobjBBHaemotologyTest.PlateletCount;
                    LobtblBBHaemotology.WBCCount = PobjBBHaemotologyTest.WBCCount;
                    LobtblBBHaemotology.MP = PobjBBHaemotologyTest.MP;
                    LobtblBBHaemotology.G6PD = PobjBBHaemotologyTest.G6PD;
                    LobtblBBHaemotology.Remarks = PobjBBHaemotologyTest.Remarks;
                    LobtblBBHaemotology.ModifiedBy = PobjBBHaemotologyTest.ModifiedBy;
                    LobtblBBHaemotology.ModifiedOn = DateTime.Now;

                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ScreeningBiology
        public List<BBScreeningBiologyRepository> GetBBScreeningBiology()
        {
            List<BBScreeningBiologyRepository> LobjBBScreeningBiology = new List<BBScreeningBiologyRepository>();
            var ScreeningBiology = from b in _db.tblBBScreeningBiologies
                                   where b.IsActive == true

                                   select new BBScreeningBiologyRepository
                                   {
                                       Screeningid = b.Screeningid,
                                       Donationid = b.Donationid,
                                       Glucose = b.Glucose,
                                       Urea = b.Urea,
                                       ALT = b.ALT,
                                       TestResult = b.TestResult,
                                       IsActive = b.IsActive
                                   };
            LobjBBScreeningBiology = ScreeningBiology.ToList<BBScreeningBiologyRepository>();
            return LobjBBScreeningBiology;
        }
        public BBScreeningBiologyRepository PostBBScreeningBiology(BBScreeningBiologyRepository PobjBBScreeningBiology)
        {
            tblBBScreeningBiology LobjtblBBScreeningBiology = new tblBBScreeningBiology();
            ObjectCopier.CopyObjectData(PobjBBScreeningBiology, LobjtblBBScreeningBiology);
            LobjtblBBScreeningBiology.CreatedOn = DateTime.Now;
            LobjtblBBScreeningBiology.IsActive = true;
            _db.tblBBScreeningBiologies.Add(LobjtblBBScreeningBiology);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblBBScreeningBiology, PobjBBScreeningBiology);
            return PobjBBScreeningBiology;
        }

        public bool PutBBScreeningBiology(int id, BBScreeningBiologyRepository PobjBBScreeningBiology)
        {
            try
            {
                tblBBScreeningBiology LobtblBBScreeningBiology = new tblBBScreeningBiology();
                LobtblBBScreeningBiology = _db.tblBBScreeningBiologies.Where(b => b.Screeningid == id).FirstOrDefault<tblBBScreeningBiology>();
                if (LobtblBBScreeningBiology != null)
                {
                    LobtblBBScreeningBiology.Glucose = PobjBBScreeningBiology.Glucose;
                    LobtblBBScreeningBiology.Urea = PobjBBScreeningBiology.Urea;
                    LobtblBBScreeningBiology.ALT = PobjBBScreeningBiology.ALT;
                    LobtblBBScreeningBiology.TestResult = PobjBBScreeningBiology.TestResult;
                    LobtblBBScreeningBiology.ModifiedBy = PobjBBScreeningBiology.ModifiedBy;
                    LobtblBBScreeningBiology.ModifiedOn = DateTime.Now;

                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Discard
        public List<BBDiscardRepository> GetBBDiscard()
        {
            List<BBDiscardRepository> LobjBBDiscard = new List<BBDiscardRepository>();
            var Discard = from d in _db.tblBBDiscards
                          where d.IsActive == true

                          select new BBDiscardRepository
                          {
                              Discardid = d.Discardid,
                              Donationid = d.Donationid,
                              DisDate = d.DisDate,
                              DisComponent = d.DisComponent,
                              DisReason = d.DisReason,
                              DisBy = d.DisBy,
                              DisSummary = d.DisSummary
                          };

            LobjBBDiscard = Discard.ToList<BBDiscardRepository>();
            return LobjBBDiscard;
        }
        public BBDiscardRepository GetBBDiscard(int id)
        {
            BBDiscardRepository LobjBBDiscard = new BBDiscardRepository();
            LobjBBDiscard = (from d in _db.tblBBDiscards
                             where d.IsActive == true && d.Discardid == id

                             select new BBDiscardRepository
                             {
                                 Discardid = d.Discardid,
                                 Donationid = d.Donationid,
                                 DisDate = d.DisDate,
                                 DisComponent = d.DisComponent,
                                 DisReason = d.DisReason,
                                 DisBy = d.DisBy,
                                 DisSummary = d.DisSummary,
                                 IsActive = d.IsActive
                             }).FirstOrDefault();

            return LobjBBDiscard;
        }
        public BBDiscardRepository PostBBDiscard(BBDiscardRepository PobjBBDiscard)
        {
            tblBBDiscard LobjtblBBDIscard = new tblBBDiscard();
            ObjectCopier.CopyObjectData(PobjBBDiscard, LobjtblBBDIscard);
            LobjtblBBDIscard.CreatedDate = DateTime.Now;
            LobjtblBBDIscard.IsActive = true;
            _db.tblBBDiscards.Add(LobjtblBBDIscard);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblBBDIscard, PobjBBDiscard);
            return PobjBBDiscard;
        }

        public bool PutBBDiscard(int id, BBDiscardRepository PobjBBDiscard)
        {
            try
            {
                tblBBDiscard LobtblBBDiscard = new tblBBDiscard();
                LobtblBBDiscard = _db.tblBBDiscards.Where(d => d.Discardid == id).FirstOrDefault<tblBBDiscard>();
                if (LobtblBBDiscard != null)
                {
                    LobtblBBDiscard.DisDate = PobjBBDiscard.DisDate;
                    LobtblBBDiscard.DisComponent = PobjBBDiscard.DisComponent;
                    LobtblBBDiscard.DisBy = PobjBBDiscard.DisBy;
                    LobtblBBDiscard.DisSummary = PobjBBDiscard.DisSummary;
                    LobtblBBDiscard.ModifiedBy = PobjBBDiscard.ModifiedBy;
                    LobtblBBDiscard.ModifiedDate = DateTime.Now;

                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteBBDiscard(int Id)
        {
            try
            {
                tblBBDiscard LobjtblBBDiscard = new tblBBDiscard();
                LobjtblBBDiscard = _db.tblBBDiscards.FirstOrDefault(x => x.Discardid == Id);
                if (LobjtblBBDiscard != null)
                {
                    LobjtblBBDiscard.IsActive = false;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region BloodBank
        public List<BloodBankRepository> GetBloodBank()
        {
            List<BloodBankRepository> lobjBB = new List<BloodBankRepository>();
            var res = from bb in _db.tblBloodBanks
                      join
                          tower in _db.tblHospitalTowers on bb.Towerid equals tower.TowerId
                      join
                          floor in _db.tblFloors on bb.Floorid equals floor.FloorId
                      join
                          hospital in _db.tblHospitals on bb.Hospitalid equals hospital.HospitalId
                      where bb.IsActive == true
                      select new BloodBankRepository
                      {
                          Bloodbankid = bb.Bloodbankid,
                          Name = bb.Name,
                          Towerid = bb.Towerid,
                          Hospitalid = bb.Hospitalid,
                          Floorid = bb.Floorid,
                          InchargeName = bb.InchargeName,
                          Inchargeno = bb.Inchargeno,
                          OperatingTimes = bb.OperatingTimes,
                          BBShortCode = bb.BBShortCode,
                          HospitalTower = new HospitalTowerRepository
                          {
                              Name = tower.Name,
                              TowerId = tower.TowerId,
                              Type = tower.Type
                          },
                          Floor = new FloorRepository
                          {
                              FloorId = floor.FloorId,
                              FloorType = floor.FloorType,

                          },
                          Hospital = new HospitalRepository
                          {
                              HospitalId = hospital.HospitalId,
                              HospitalName = hospital.HospitalName,
                              HospitalCode = hospital.HospitalCode

                          },

                      };

            lobjBB = res.ToList<BloodBankRepository>();
            return lobjBB;

        }
        public BloodBankRepository GetBloodBank(int id)
        {
            BloodBankRepository lobjBB = new BloodBankRepository();
            lobjBB = (from bb in _db.tblBloodBanks
                      join
                          tower in _db.tblHospitalTowers on bb.Towerid equals tower.TowerId
                      join
                          floor in _db.tblFloors on bb.Floorid equals floor.FloorId
                      join
                          hospital in _db.tblHospitals on bb.Hospitalid equals hospital.HospitalId
                      where bb.IsActive == true && bb.Bloodbankid == id
                      select new BloodBankRepository
                      {
                          Bloodbankid = bb.Bloodbankid,
                          Name = bb.Name,
                          Towerid = bb.Towerid,
                          Hospitalid = bb.Hospitalid,
                          Floorid = bb.Floorid,
                          InchargeName = bb.InchargeName,
                          Inchargeno = bb.Inchargeno,
                          OperatingTimes = bb.OperatingTimes,
                          BBShortCode = bb.BBShortCode,
                          HospitalTower = new HospitalTowerRepository
                          {
                              Name = tower.Name,
                              TowerId = tower.TowerId,
                              Type = tower.Type
                          },
                          Floor = new FloorRepository
                          {
                              FloorId = floor.FloorId,
                              FloorType = floor.FloorType,

                          },
                          Hospital = new HospitalRepository
                          {
                              HospitalId = hospital.HospitalId,
                              HospitalName = hospital.HospitalName,
                              HospitalCode = hospital.HospitalCode

                          },

                      }).FirstOrDefault<BloodBankRepository>();



            return lobjBB;

        }
        public BloodBankRepository PostBloodBank(BloodBankRepository PobjBB)
        {
            tblBloodBank LobjBB = new tblBloodBank();
            ObjectCopier.CopyObjectData(PobjBB, LobjBB);
            LobjBB.CreatedOn = DateTime.Now;
            LobjBB.IsActive = true;
            _db.tblBloodBanks.Add(LobjBB);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjBB, PobjBB);
            return PobjBB;

        }

        public bool PutBloodBank(int id, BloodBankRepository PobjBB)
        {
            try
            {
                tblBloodBank LobtblBB = new tblBloodBank();
                LobtblBB = _db.tblBloodBanks.Where(P => P.Bloodbankid == id).FirstOrDefault<tblBloodBank>();
                if (LobtblBB != null)
                {
                    LobtblBB.Name = PobjBB.Name;
                    LobtblBB.Towerid = PobjBB.Towerid;
                    LobtblBB.Hospitalid = PobjBB.Hospitalid;
                    LobtblBB.Floorid = PobjBB.Floorid;
                    LobtblBB.InchargeName = PobjBB.InchargeName;
                    LobtblBB.Inchargeno = PobjBB.Inchargeno;
                    LobtblBB.OperatingTimes = PobjBB.OperatingTimes;
                    LobtblBB.BBShortCode = PobjBB.BBShortCode;
                    LobtblBB.ModifiedBy = PobjBB.ModifiedBy;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteBloodBank(int id)
        {
            try
            {
                tblBloodBank LobtblBB = new tblBloodBank();
                LobtblBB = _db.tblBloodBanks.Where(P => P.Bloodbankid == id).FirstOrDefault<tblBloodBank>();
                if (LobtblBB != null)
                {
                    LobtblBB.IsActive = false;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region BBDonation
        public List<BBDonationRepository> GetBBDonation()
        {

            List<BBDonationRepository> LlstDonations = new List<BBDonationRepository>();
            var Donation = from dntn in _db.tblBBDonations
                           where dntn.IsActive == true
                           select new BBDonationRepository
                           {
                               Donationid = dntn.Donationid,
                               Donarid = dntn.Donarid,
                               donationDate = dntn.donationDate,
                               DateofExpiry = dntn.DateofExpiry,
                               Bloodbankid = dntn.Bloodbankid,
                               Patientid = dntn.Patientid,
                               Status = dntn.Status,
                               IsActive = dntn.IsActive

                           };
            LlstDonations = Donation.ToList<BBDonationRepository>();
            return LlstDonations;
        }
        public BBDonationRepository GetBBDonation(int id)
        {

            BBDonationRepository LlstDonations = new BBDonationRepository();
            var Donation = from dntn in _db.tblBBDonations
                           where dntn.Donationid == id
                           select new BBDonationRepository
                           {
                               Donationid = dntn.Donationid,
                               Donarid = dntn.Donarid,
                               donationDate = dntn.donationDate,
                               DateofExpiry = dntn.DateofExpiry,
                               Bloodbankid = dntn.Bloodbankid,
                               Patientid = dntn.Patientid,
                               Status = dntn.Status,
                               IsActive = dntn.IsActive

                           };
            //LlstDonations = Donation.ToList<BBDonationRepository>();
            return LlstDonations;
        }
        public BBDonationRepository PostBBDonation(BBDonationRepository PobjBBDonation)
        {
            tblBBDonation LobjtblBBDonation = new tblBBDonation();
            ObjectCopier.CopyObjectData(PobjBBDonation, LobjtblBBDonation);
            LobjtblBBDonation.CreatedDate = DateTime.Now;
            LobjtblBBDonation.IsActive = true;
            _db.tblBBDonations.Add(LobjtblBBDonation);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblBBDonation, PobjBBDonation);
            return PobjBBDonation;
        }

        public bool PutBBDonation(int donationid, BBDonationRepository PobjBBDonation)
        {

            tblBBDonation LobjtblBBDonation = new tblBBDonation();
            LobjtblBBDonation = _db.tblBBDonations.Where(m => m.Donationid == donationid).FirstOrDefault<tblBBDonation>();
            if (LobjtblBBDonation != null)
            {
                LobjtblBBDonation.Donarid = PobjBBDonation.Donarid;
                LobjtblBBDonation.donationDate = PobjBBDonation.donationDate;
                LobjtblBBDonation.DateofExpiry = PobjBBDonation.DateofExpiry;
                LobjtblBBDonation.Bloodbankid = PobjBBDonation.Bloodbankid;
                LobjtblBBDonation.Patientid = PobjBBDonation.Patientid;
                LobjtblBBDonation.DonorComments = PobjBBDonation.DonorComments;
                LobjtblBBDonation.Status = PobjBBDonation.Status;
                LobjtblBBDonation.ModifiedDate = DateTime.Now;
                LobjtblBBDonation.ModifiedBy = PobjBBDonation.ModifiedBy;
            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteBBDonation(int id)
        {

            tblBBDonation LobjtblBBDonation = new tblBBDonation();
            LobjtblBBDonation = _db.tblBBDonations.Where(s => s.Donationid == id).FirstOrDefault<tblBBDonation>();
            if (LobjtblBBDonation != null)
            {
                LobjtblBBDonation.IsActive = false;
                LobjtblBBDonation.ModifiedDate = DateTime.Now;
                LobjtblBBDonation.ModifiedBy = 2;


            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region CampRegistration
        public List<BBCampRegistrationRepository> GetCampRegistration()
        {

            List<BBCampRegistrationRepository> LlstCampReg = new List<BBCampRegistrationRepository>();
            var Camprgstn = from camp in _db.tblBBCampRegistrations
                            where camp.IsActive == true
                            select new BBCampRegistrationRepository
                            {

                                CampID = camp.CampID,
                                BBCode = camp.BBCode,
                                District = camp.District,
                                Mandal = camp.Mandal,
                                CityorVillage = camp.CityorVillage,
                                Location = camp.Location,
                                CampName = camp.CampName,
                                DateOfCamp = camp.DateOfCamp,
                                StartDateTime = camp.StartDateTime,
                                EndDateTime = camp.EndDateTime,
                                Incharge = camp.Incharge,
                                InchargeCtNo = camp.InchargeCtNo,
                                IsActive = camp.IsActive
                            };
            LlstCampReg = Camprgstn.ToList<BBCampRegistrationRepository>();
            return LlstCampReg;
        }
        public BBCampRegistrationRepository GetCampRegistration(int id)
        {

            BBCampRegistrationRepository LlstCampReg = new BBCampRegistrationRepository();
            var Camprgstn = from camp in _db.tblBBCampRegistrations
                            where camp.CampID == id
                            select new BBCampRegistrationRepository
                            {

                                CampID = camp.CampID,
                                BBCode = camp.BBCode,
                                District = camp.District,
                                Mandal = camp.Mandal,
                                CityorVillage = camp.CityorVillage,
                                Location = camp.Location,
                                CampName = camp.CampName,
                                DateOfCamp = camp.DateOfCamp,
                                StartDateTime = camp.StartDateTime,
                                EndDateTime = camp.EndDateTime,
                                Incharge = camp.Incharge,
                                InchargeCtNo = camp.InchargeCtNo,
                                IsActive = camp.IsActive
                            };
            //LlstCampReg = Camprgstn.ToList<BBCampRegistrationRepository>();
            return LlstCampReg;
        }
        public BBCampRegistrationRepository PostCampRegistration(BBCampRegistrationRepository PobjCamprgistn)
        {
            tblBBCampRegistration LobjtblBBCampRegistration = new tblBBCampRegistration();
            ObjectCopier.CopyObjectData(PobjCamprgistn, LobjtblBBCampRegistration);
            LobjtblBBCampRegistration.CreatedOn = DateTime.Now;
            LobjtblBBCampRegistration.IsActive = true;
            _db.tblBBCampRegistrations.Add(LobjtblBBCampRegistration);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblBBCampRegistration, PobjCamprgistn);
            return PobjCamprgistn;
        }
        public bool PutCampRegistration(int donationid, BBCampRegistrationRepository PobjBBCampregistn)
        {

            tblBBCampRegistration LobjtblBBCampRegistration = new tblBBCampRegistration();
            LobjtblBBCampRegistration = _db.tblBBCampRegistrations.Where(m => m.CampID == donationid).FirstOrDefault<tblBBCampRegistration>();
            if (LobjtblBBCampRegistration != null)
            {
                LobjtblBBCampRegistration.BBCode = PobjBBCampregistn.BBCode;
                LobjtblBBCampRegistration.District = PobjBBCampregistn.District;
                LobjtblBBCampRegistration.Mandal = PobjBBCampregistn.Mandal;
                LobjtblBBCampRegistration.CityorVillage = PobjBBCampregistn.CityorVillage;
                LobjtblBBCampRegistration.Location = PobjBBCampregistn.Location;
                LobjtblBBCampRegistration.CampName = PobjBBCampregistn.CampName;
                LobjtblBBCampRegistration.DateOfCamp = PobjBBCampregistn.DateOfCamp;
                LobjtblBBCampRegistration.StartDateTime = PobjBBCampregistn.StartDateTime;
                LobjtblBBCampRegistration.EndDateTime = PobjBBCampregistn.EndDateTime;
                LobjtblBBCampRegistration.Incharge = PobjBBCampregistn.Incharge;
                LobjtblBBCampRegistration.InchargeCtNo = PobjBBCampregistn.InchargeCtNo;
                LobjtblBBCampRegistration.ModifiedDate = DateTime.Now;
                LobjtblBBCampRegistration.ModifiedBy = PobjBBCampregistn.ModifiedBy;
            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DeleteCampRegistration(int id)
        {

            tblBBCampRegistration LobjtblBBCampRegistration = new tblBBCampRegistration();
            LobjtblBBCampRegistration = _db.tblBBCampRegistrations.Where(s => s.CampID == id).FirstOrDefault<tblBBCampRegistration>();
            if (LobjtblBBCampRegistration != null)
            {
                LobjtblBBCampRegistration.IsActive = false;
                LobjtblBBCampRegistration.ModifiedDate = DateTime.Now;
                LobjtblBBCampRegistration.ModifiedBy = 2;


            }
            int res = _db.SaveChanges();
            if (res == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region BBIssue
        public List<BBIssueRepository> GetBBIssue()
        {
            List<BBIssueRepository> LobjBBIssue = new List<BBIssueRepository>();
            var BBIssue = from issue in _db.tblBBIssues
                          join tr in _db.tblBBTransfusionRequests on issue.RequestId equals tr.RequestID
                          join dntn in _db.tblBBDonations on issue.Donationid equals dntn.Donationid
                          join donors in _db.tblBBDonors on issue.Donorid equals donors.DonorId
                          where issue.IsActive == true

                          select new BBIssueRepository
                          {
                              IssueID = issue.IssueID,
                              TransfusionRequest = new BBTransfusionRequestRepository
                              {
                                  RequestID = tr.RequestID,
                                  Patientid = tr.Patientid,
                                  PatientType = tr.PatientType,
                                  BloodGroup = tr.BloodGroup,
                                  Component = tr.Component,
                                  NoOfUnits = tr.NoOfUnits,
                                  Attachments = tr.Attachments,
                                  isCrossMatch = tr.isCrossMatch,
                                  RequestedBy = tr.RequestedBy,
                                  AssignTo = tr.AssignTo,
                                  ReqStatus = tr.ReqStatus
                              },
                              Donation = new BBDonationRepository
                              {
                                  Donationid = dntn.Donationid,
                                  Donarid = dntn.Donarid,
                                  donationDate = dntn.donationDate,
                                  DateofExpiry = dntn.DateofExpiry,
                                  Bloodbankid = dntn.Bloodbankid,
                                  Patientid = dntn.Patientid,
                                  Status = dntn.Status

                              },
                              Donor = new BBDonorRepository
                              {
                                  DonorId = donors.DonorId,
                                  DonorName = donors.DonorName,
                                  Gender = donors.Gender,
                                  DOB = donors.DOB,
                                  Age = donors.Age,
                                  Occupation = donors.Occupation,
                                  MaritalStatus = donors.MaritalStatus,
                                  DonationType = donors.DonationType,
                                  BloodGroup = donors.BloodGroup,
                                  DonorSource = donors.DonorSource,
                                  OldRefNo = donors.OldRefNo,
                                  MobileNo = donors.MobileNo,
                                  Email = donors.Email
                              },
                              NoofUnits = issue.NoofUnits,
                              isCrossMatch = issue.isCrossMatch,
                              IssuedOn = issue.IssuedOn,
                              IssuedTo = issue.IssuedTo,
                              BarCode = issue.BarCode,
                              IssuedBy = issue.IssuedBy,
                          };
            LobjBBIssue = BBIssue.ToList<BBIssueRepository>();
            return LobjBBIssue;
        }

        public BBIssueRepository GetBBIssue(int id)
        {
            BBIssueRepository LobjBBIssue = new BBIssueRepository();
            LobjBBIssue = (from issue in _db.tblBBIssues
                           join tr in _db.tblBBTransfusionRequests on issue.RequestId equals tr.RequestID
                           join dntn in _db.tblBBDonations on issue.Donationid equals dntn.Donationid
                           join donors in _db.tblBBDonors on issue.Donorid equals donors.DonorId
                           where issue.IsActive == true && issue.IssueID == id

                           select new BBIssueRepository
                           {
                               IssueID = issue.IssueID,
                               NoofUnits = issue.NoofUnits,
                               isCrossMatch = issue.isCrossMatch,
                               IssuedOn = issue.IssuedOn,
                               IssuedTo = issue.IssuedTo,
                               BarCode = issue.BarCode,
                               IssuedBy = issue.IssuedBy,
                               TransfusionRequest = new BBTransfusionRequestRepository
                               {
                                   RequestID = tr.RequestID,
                                   Patientid = tr.Patientid,
                                   PatientType = tr.PatientType,
                                   BloodGroup = tr.BloodGroup,
                                   Component = tr.Component,
                                   NoOfUnits = tr.NoOfUnits,
                                   Attachments = tr.Attachments,
                                   isCrossMatch = tr.isCrossMatch,
                                   RequestedBy = tr.RequestedBy,
                                   AssignTo = tr.AssignTo,
                                   ReqStatus = tr.ReqStatus
                               },
                               Donation = new BBDonationRepository
                               {
                                   Donationid = dntn.Donationid,
                                   Donarid = dntn.Donarid,
                                   donationDate = dntn.donationDate,
                                   DateofExpiry = dntn.DateofExpiry,
                                   Bloodbankid = dntn.Bloodbankid,
                                   Patientid = dntn.Patientid,
                                   Status = dntn.Status

                               },
                               Donor = new BBDonorRepository
                               {
                                   DonorId = donors.DonorId,
                                   DonorName = donors.DonorName,
                                   Gender = donors.Gender,
                                   DOB = donors.DOB,
                                   Age = donors.Age,
                                   Occupation = donors.Occupation,
                                   MaritalStatus = donors.MaritalStatus,
                                   DonationType = donors.DonationType,
                                   BloodGroup = donors.BloodGroup,
                                   DonorSource = donors.DonorSource,
                                   OldRefNo = donors.OldRefNo,
                                   MobileNo = donors.MobileNo,
                                   Email = donors.Email
                               }

                           }).FirstOrDefault();
            return LobjBBIssue;
        }

        public BBIssueRepository PostBBIssue(BBIssueRepository PobjBBIssue)
        {
            tblBBIssue LobjtblBBIssue = new tblBBIssue();
            ObjectCopier.CopyObjectData(PobjBBIssue, LobjtblBBIssue);
            LobjtblBBIssue.CreatedDate = DateTime.Now;
            LobjtblBBIssue.IsActive = true;
            _db.tblBBIssues.Add(LobjtblBBIssue);
            int res = _db.SaveChanges();
            ObjectCopier.CopyObjectData(LobjtblBBIssue, PobjBBIssue);
            return PobjBBIssue;
        }

        public bool PutBBIssue(int id, BBIssueRepository PobjBBIssue)
        {
            try
            {
                tblBBIssue LobtblBBIssue = new tblBBIssue();
                LobtblBBIssue = _db.tblBBIssues.FirstOrDefault(i => i.IssueID == id);
                if (LobtblBBIssue != null)
                {
                    LobtblBBIssue.NoofUnits = PobjBBIssue.NoofUnits;
                    LobtblBBIssue.isCrossMatch = PobjBBIssue.isCrossMatch;
                    LobtblBBIssue.IssuedOn = PobjBBIssue.IssuedOn;
                    LobtblBBIssue.IssuedTo = PobjBBIssue.IssuedTo;
                    LobtblBBIssue.BarCode = PobjBBIssue.BarCode;
                    LobtblBBIssue.IssuedBy = PobjBBIssue.IssuedBy;
                    LobtblBBIssue.ModifiedBy = PobjBBIssue.ModifiedBy;
                    LobtblBBIssue.ModifiedDate = DateTime.Now;

                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteBBIssue(int Id)
        {
            try
            {
                tblBBIssue LobjtblBBIssue = new tblBBIssue();
                LobjtblBBIssue = _db.tblBBIssues.FirstOrDefault(i => i.IssueID == Id);
                if (LobjtblBBIssue != null)
                {
                    LobjtblBBIssue.IsActive = false;
                }
                int data = _db.SaveChanges();
                if (data == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public List<BBTransfusionRequestRepository> GetBloodMatching(string strbloodgroup, bool isCrossMatch, DateTime RequiredOn, string component)
        {

            List<BBTransfusionRequestRepository> LobjBloodMatch = new List<BBTransfusionRequestRepository>();


            string blooddmatching = strbloodgroup;

            if (!(isCrossMatch))
            {
                switch (strbloodgroup)
                {

                    case "ONeg ":
                        blooddmatching += "'O-'";

                        break;
                    case "OPos":
                        blooddmatching += "'O-','O+'";
                        break;
                    case "ANeg":
                        blooddmatching += "'O-',A-";
                        break;
                    case "APos":
                        blooddmatching += "'O-','O+','A-','A+'";
                        break;
                    case "BNeg":
                        blooddmatching += "'O-','B-'";
                        break;
                    case "BPos":
                        blooddmatching += "'ONeg','OPos','BNeg','BPos'";

                        break;
                    case "ABNeg":
                        blooddmatching += "'O-','A-','B-','AB-'";
                        break;
                    case "ABPos":
                        blooddmatching += "'O-','O+','A-','A+','B-','B+','AB-'";
                        break;

                }
            }
            var res = from donor in _db.tblBBDonors                              
                      join donation in _db.tblBBDonations on donor.DonorId equals donation.Donarid
                      join Phy in _db.tblBBPhysicalExaminations on donation.Donationid equals Phy.DonationId
                      where donation.Status == "Ready" && donation.CompExpiry > RequiredOn && donation.CompName == component && (blooddmatching.Contains(Phy.BGafterScreening))
                                                              
                      select new BBTransfusionRequestRepository
                          {
                             
                               Donationid = donation.Donationid,
                               Donarid = donation.Donarid,
                               DonorName = donor.DonorName,
                               CompName  = donation.CompName,
                               CompExpiry = donation.CompExpiry,
                               BGafterScreening = Phy.BGafterScreening,
                               BagType = Phy.BagType
                                 
                                  // isCrossMatch = p.isCrossMatch
                          };     
              
                        
                    LobjBloodMatch = res.ToList<BBTransfusionRequestRepository>();
                    return LobjBloodMatch;
            
        }
        public BBTransfusionRequestRepository PostTransReqConfirm(BBTransfusionRequestRepository PobjTreqConfirm)
        {
            HospitalTenantContext _db = new HospitalTenantContext();
            tblBBTransfusionRequest LobjTreq = new tblBBTransfusionRequest();
            tblBBDonation LobjBBDonation = new tblBBDonation();
            tblBBIssue LobjBBIssue = new tblBBIssue();
            ObjectCopier.CopyObjectData(PobjTreqConfirm, LobjTreq);
            ObjectCopier.CopyObjectData(PobjTreqConfirm.BBIssueDetails, LobjBBIssue);
            ObjectCopier.CopyObjectData(PobjTreqConfirm.Donation, LobjBBDonation);
            List<tblBBIssue> LobjListBBIssue = new List<tblBBIssue>();
            foreach(BBIssueRepository LobjBB in PobjTreqConfirm.BBIssueDetails)
            {
                tblBBIssue tblIssue = new tblBBIssue();               
                tblIssue.Donationid = LobjBB.Donationid;
                tblIssue.Donorid = LobjBB.Donorid;
                tblIssue.NoofUnits = LobjBB.NoofUnits;
                tblIssue.isCrossMatch = LobjBB.isCrossMatch;
                tblIssue.IssuedOn = LobjBB.IssuedOn;
                tblIssue.IssuedTo = LobjBB.IssuedTo;
                tblIssue.BarCode = LobjBB.BarCode;
                tblIssue.IssueStatus = LobjBB.IssueStatus;
                tblIssue.IssuedBy = LobjBB.IssuedBy;
                tblIssue.IsActive = true;
                tblIssue.CreatedDate = DateTime.Now;
                LobjListBBIssue.Add(tblIssue);
            }

            LobjTreq.tblBBIssues = LobjListBBIssue;              
                  
            LobjTreq.CreatedOn = DateTime.Now;
            LobjTreq.IsActive = true;

            LobjBBDonation.IsActive = true;
            LobjBBDonation.CreatedDate = DateTime.Now;

            _db.tblBBTransfusionRequests.Add(LobjTreq);
            _db.tblBBDonations.Add(LobjBBDonation); 

            int res = _db.SaveChanges(); 
            ObjectCopier.CopyObjectData(LobjTreq, PobjTreqConfirm);
            ObjectCopier.CopyObjectData(LobjBBIssue, PobjTreqConfirm.BBIssueDetails);
            ObjectCopier.CopyObjectData(LobjBBDonation, PobjTreqConfirm.Donation);
            return PobjTreqConfirm;

        }

            
    }
}
