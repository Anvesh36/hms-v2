﻿using Repository = HMISAPI.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMISAPI.Domain.Entities;
using System.Data.Linq.SqlClient;

namespace HMISAPI.Infrastructure.Data
{
    public class LookUp : HMISAPI.Domain.Interfaces.ILookUp
    {
        public List<Repository.LookUp> GetLookUps(string name, bool isActive = true)
        {
            List<Repository.LookUp> objLookUps = new List<Repository.LookUp>();
            using (HospitalTenantContext _db = new HospitalTenantContext())
            {
                List<tblLookup> objTblLookUps = new List<tblLookup>();
                if (isActive)
                {
                    objTblLookUps = _db.tblLookups.Where(c => c.tblLookupCategory.LookupCategoryName.ToLower() == name && c.IsActive == true).OrderBy(c=>c.LookupName).ToList<tblLookup>();
                }
                else
                {
                    objTblLookUps = _db.tblLookups.Where(c => c.tblLookupCategory.LookupCategoryName.ToLower() == name).OrderBy(c => c.LookupName).ToList<tblLookup>();
                }
                foreach (tblLookup objTblLookUp in objTblLookUps)
                {
                    objLookUps.Add(new Repository.LookUp() { ID = objTblLookUp.LookupId, Name = objTblLookUp.LookupName });
                }
            }
            return objLookUps;
        }

        public Repository.PatientLookUps GetPatientLookUps()
        {
            Repository.PatientLookUps objPatientLookUps = new Repository.PatientLookUps();
            objPatientLookUps.Titles = GetLookUps("Title");
            objPatientLookUps.Genders = GetLookUps("Gender");
            objPatientLookUps.BloodGroups = GetLookUps("BloodGroup");
            objPatientLookUps.MaritalStatuses = GetLookUps("MaritalStatus");
            objPatientLookUps.RelationShips = GetLookUps("RelationShip");
            return objPatientLookUps;
        }
        public Repository.LookUp CreateLookUp(Repository.LookUp poLookUp, string categoryName)
        {
            try
            {
                int res = 0;
                using (HospitalTenantContext _db = new HospitalTenantContext())
                {
                    tblLookup objTblLookUp = new tblLookup();
                    objTblLookUp.LookupCategoryId = _db.tblLookupCategories.Where(c => c.LookupCategoryName.ToLower() == categoryName.ToLower()).FirstOrDefault().LookupCategoryId;
                    objTblLookUp.LookupName = poLookUp.Name;
                    objTblLookUp.IsActive = true;
                    objTblLookUp.CreatedBy = 1;
                    objTblLookUp.CreatedDate = DateTime.Now;
                    _db.tblLookups.Add(objTblLookUp);
                    res = _db.SaveChanges();
                    poLookUp.ID = objTblLookUp.LookupId;
                }

                return (res == 1) ? poLookUp : null;
            }
            catch 
            {
                return null;
            }
        }
        public bool UpdateLookUp(int id, Repository.LookUp poLookUp)
        {
            try
            {
                int res = 0;
                using (HospitalTenantContext _db = new HospitalTenantContext())
                {
                    tblLookup objTblLookUp = _db.tblLookups.Where(c => c.LookupId == id).FirstOrDefault();
                    objTblLookUp.LookupName = poLookUp.Name;
                    res = _db.SaveChanges();
                }
                return (res == 1);
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteLookUp(int id)
        {
            try
            {
                int res = 0;
                using (HospitalTenantContext _db = new HospitalTenantContext())
                {
                    tblLookup objTblLookUp = _db.tblLookups.Where(c => c.LookupId == id).FirstOrDefault();
                    _db.tblLookups.Remove(objTblLookUp);
                    res = _db.SaveChanges();
                }
                return (res == 1);
            }
            catch
            {
                return false;
            }
        }
    }
}
